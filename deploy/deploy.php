<?php
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Deployer;

use Interactiv4\Deployer\Recipe\Magento2Recipe;

require 'recipe/common.php';

Magento2Recipe::configuration();
Magento2Recipe::tasks();

Magento2Recipe::buildProcess();
Magento2Recipe::deployProcess();

set('writable_mode', 'chmod');

set('static_deploy_options','--exclude-theme=Magento/blank --exclude-theme=Magento/luma en_US es_ES --jobs=2');
set('static_backend_deploy_options', 'es_ES en_US --jobs=2');

task('restart:all:caches', 'sudo service nginx restart && sudo service php-fpm restart && sudo service varnish restart && sudo redis-cli flushall');

task('restart:opcache', 'sudo service php-fpm reload');

set('dev_modules',[]);

set('default_timeout', 600);

// Rewrite task in order to don't set permission for writable dirs
task('deploy:writable', function () {
    $dirs = join(' ', get('writable_dirs'));

    if (empty($dirs)) {
        return;
    }

    try {
        cd('{{release_path}}');

        // Create directories if they don't exist
        run("mkdir -p $dirs");
    } catch (\RuntimeException $e) {
        $formatter = Deployer::get()->getHelper('formatter');

        $errorMessage = [
            "Unable to setup writable dirs.",
        ];
        write($formatter->formatBlock($errorMessage, 'error', true));

        throw $e;
    }
});

//dev
host('dev')
    ->hostname('logiscenter.devi4.com')
    ->stage('dev')
    ->roles('primary')
    ->user('magento')
    ->set('deploy_path', '/var/www/logiscenter');

//staging
host('staging')
    ->hostname('10.172.0.27')
    ->stage('staging')
    ->roles('primary')
    ->user('logiscenter')
    ->port('22')
    ->set('deploy_path', '/var/www/logiscenter.us/magento');

//production
host('prod_admin')
    ->hostname('10.172.0.73')
    ->stage('prod')
    ->roles('primary')
    ->user('logiscenter')
    ->port('22')
    ->set('deploy_path', '/var/www/logiscenter.us/magento');
//
host('prod_front_1')
    ->hostname('10.172.0.82')
    ->stage('prod')
    ->roles('slave')
    ->user('logiscenter')
    ->port('22')
    ->set('deploy_path', '/var/www/logiscenter.us/magento');