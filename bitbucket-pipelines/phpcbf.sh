#!/usr/bin/env bash
set -euox pipefail

echo "Resolving script path..."
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done

DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

cd $DIR/..

STRICT=0
HAS_ERRORS=0
EXTRA_ARGS=''

if [[ -n "${BITBUCKET_PR_ID-}" ]]; then
  # Strict mode for pull-requests triggered builds
  STRICT=1

  if [[ -s CHANGED_FILES_PHP ]] && ! magento/vendor/bin/phpcbf --standard=Interactiv4 --file-list=CHANGED_FILES_PHP -pvn; then
    HAS_ERRORS=1;
    echo 'WARNING: Executed with errors'
  fi

  if [[ -s CHANGED_FILES_PHTML ]] && ! magento/vendor/bin/phpcbf --standard=Interactiv4 --file-list=CHANGED_FILES_PHTML -pvn; then
    HAS_ERRORS=1;
    echo 'WARNING: Executed with errors'
  fi
elif ! magento/vendor/bin/phpcbf --standard=Interactiv4 -pvn; then
  HAS_ERRORS=1;
  echo 'WARNING: Executed with errors'
fi

if [[ "${HAS_ERRORS}" == '1' ]] && [[ "${STRICT}" == '1' ]]; then
  exit 1;
fi