#!/usr/bin/env bash
set -euox pipefail

echo "Resolving script path..."
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done

DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

cd $DIR/..

EXTRA_ARGS='magento/app/code/Interactiv4 magento/lib/internal/Interactiv4'
HAS_ERRORS=0
STRICT=0
SHOULD_EXECUTE=1

if [[ -n "${BITBUCKET_PR_ID-}" ]]; then
  # Strict mode for pull-requests triggered builds
  STRICT=1

  if [[ -s CHANGED_FILES_PHP ]]; then
    EXTRA_ARGS=$(< CHANGED_FILES_PHP)
  else
    SHOULD_EXECUTE=0
  fi
fi

# phpstan
if [[ "${SHOULD_EXECUTE}" == '1' ]]; then
  # Run compilation in order to generate needed classes, as they are looked for by phpstan
  magento/bin/magento setup:di:compile -vv -n

  if ! magento/vendor/bin/phpstan analyse -a magento/vendor/bitexpert/phpstan-magento/autoload.php --error-format=junit -c magento/vendor/bitexpert/phpstan-magento/extension.neon -l 0 ${EXTRA_ARGS} > ./test-reports/junit/phpstan.xml; then
    HAS_ERRORS=1
  fi
fi

php bitbucket-pipelines/phpstan-reporter.php

if [[ "${HAS_ERRORS}" == '1' ]] && [[ "${STRICT}" == '1' ]]; then
  exit 1;
fi