#!/usr/bin/env bash
set -eo pipefail

echo "Resolving script path..."
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done

DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

cd $DIR/..
export ROOT_DIR=`pwd`

# Configure composer repositories
if [ -n "${REPO_DEVI4_PUBLIC_KEY}" ]; then composer global config http-basic.${REPO_DEVI4_URL} ${REPO_DEVI4_PUBLIC_KEY} ${REPO_DEVI4_PRIVATE_KEY}; fi
if [ -n "${REPO_AMASTY_PUBLIC_KEY}" ]; then composer global config http-basic.${REPO_AMASTY_URL} ${REPO_AMASTY_PUBLIC_KEY} ${REPO_AMASTY_PRIVATE_KEY}; fi
if [ -n "${REPO_XTENTO_PUBLIC_KEY}" ]; then composer global config http-basic.${REPO_XTENTO_URL} ${REPO_XTENTO_PUBLIC_KEY} ${REPO_XTENTO_PRIVATE_KEY}; fi
