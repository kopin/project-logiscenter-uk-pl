#!/usr/bin/env bash
set -euox pipefail

echo "Resolving script path..."
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done

DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

cd $DIR/..

EXTRA_ARGS='.'
HAS_ERRORS=0
STRICT=0
SHOULD_EXECUTE=1

if [[ -n "${BITBUCKET_PR_ID-}" ]]; then
  # Strict mode for pull-requests triggered builds
  STRICT=1

  if [[ -s CHANGED_FILES_PHTML ]]; then
    EXTRA_ARGS=$(< CHANGED_FILES_PHTML)
  else
    SHOULD_EXECUTE=0
  fi
fi

# php-cs-fixer
if [[ "${SHOULD_EXECUTE}" == '1' ]] && ! magento/vendor/bin/php-cs-fixer fix --allow-risky=yes --config=.php_cs_phtml --path-mode=intersection -vvv --dry-run --format=junit --diff-format=udiff ${EXTRA_ARGS} | fgrep -v '#!/usr/bin/env php' > ./test-reports/junit/php-cs-fixer-phtml.xml; then
  HAS_ERRORS=1
fi

php bitbucket-pipelines/php-cs-fixer-phtml-reporter.php

if [[ "${HAS_ERRORS}" == '1' ]] && [[ "${STRICT}" == '1' ]]; then
  exit 1;
fi