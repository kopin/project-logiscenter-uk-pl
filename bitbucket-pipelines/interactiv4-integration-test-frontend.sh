#!/usr/bin/env bash
set -euox pipefail

echo "Resolving script path..."
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done

DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

cd $DIR/..

# Create integration tests database
mysql -h127.0.0.1 -uroot -proot_user_password -e 'CREATE DATABASE IF NOT EXISTS `build_igr_tests` /*!40100 DEFAULT CHARACTER SET utf8 */'

# Interactiv4 Integration Tests
AREA='frontend'
magento/bin/magento check-untested-di-definitions --area ${AREA} -v
magento/bin/magento check-virtual-plugins --area ${AREA} -v
bin/phpunit -c magento/dev/tests/integration --testsuite="Interactiv4 Integration Tests" --verbose --group area-${AREA} --log-junit ./test-reports/junit/integration-${AREA}.xml

# Magento Integration Tests
#bin/phpunit -c magento/dev/tests/integration --testsuite="Memory Usage Tests" --verbose
#bin/phpunit -c magento/dev/tests/integration --testsuite="Magento Integration Tests" --verbose
