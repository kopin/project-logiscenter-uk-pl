<?php
declare(strict_types=1);

// phpcs:disable

require_once __DIR__ . '/reporter.php';

/**
 * Class PhpCsReporter.
 */
class PhpCsReporter extends Reporter
{
    /**
     * Get normalized report name.
     *
     * @return string
     */
    public function getNormalizedReportName(): string
    {
        return 'phpcs';
    }

    /**
     * Get report title.
     *
     * @return string
     */
    public function getTitle(): string
    {
        return 'Php Code Style Report';
    }

    /**
     * Get annotation type.
     *
     * @return string|null
     */
    public function getAnnotationType(): ?string
    {
        return 'BUG';
    }
}

$reporter = new PhpCsReporter(\dirname(__DIR__) . '/test-reports/junit/phpcs.xml');

$reporter->execute();
