#!/usr/bin/env bash
set -euox pipefail

echo "Resolving script path..."
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done

DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

cd $DIR/..

# php files
SHOULD_EXECUTE=1
EXTRA_ARGS=''

if [[ -n "${BITBUCKET_PR_ID-}" ]]; then
  if [[ -s CHANGED_FILES_PHP ]]; then
    EXTRA_ARGS=$(< CHANGED_FILES_PHP)
  else
    SHOULD_EXECUTE=0
  fi
else
  EXTRA_ARGS=$(find magento/app/code/Interactiv4 magento/lib/internal/Interactiv4 magento/app/design/frontend/Interactiv4 magento/app/design/adminhtml/Interactiv4 -type f -name '*.php')
fi

if [[ "${SHOULD_EXECUTE}" == '1' ]]; then
  echo ${EXTRA_ARGS} | xargs -r -n1 -P32 -- php -l
fi

# phtml files
SHOULD_EXECUTE=1
EXTRA_ARGS=''

if [[ -n "${BITBUCKET_PR_ID-}" ]]; then
  if [[ -s CHANGED_FILES_PHTML ]]; then
    EXTRA_ARGS=$(< CHANGED_FILES_PHTML)
  else
    SHOULD_EXECUTE=0
  fi
else
  EXTRA_ARGS=$(find magento/app/code/Interactiv4 magento/lib/internal/Interactiv4 magento/app/design/frontend/Interactiv4 magento/app/design/adminhtml/Interactiv4 -type f -name '*.phtml')
fi

if [[ "${SHOULD_EXECUTE}" == '1' ]]; then
  echo ${EXTRA_ARGS} | xargs -r -n1 -P32 -- php -l
fi
