#!/usr/bin/env bash
set -euox pipefail

echo "Resolving script path..."
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done

DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

cd $DIR/..

if [[ -z "${BITBUCKET_PR_ID-}" ]]; then
  # Fixing only available for pull-requests triggered builds
  exit
fi

if [[ -s CHANGED_FILES_PHP ]]; then
  set +e
  composer --timeout=1800 run -- php-cs-fixer-php $(< CHANGED_FILES_PHP)
  composer --timeout=1800 run -- phpcbf --file-list=CHANGED_FILES_PHP
  set -e
fi

if [[ -s CHANGED_FILES_PHTML ]]; then
  set +e
  composer --timeout=1800 run -- php-cs-fixer-phtml $(< CHANGED_FILES_PHTML)
  composer --timeout=1800 run -- phpcbf --file-list=CHANGED_FILES_PHTML
  set -e
fi

magento/bin/magento fix-xml-format --raw-output
magento/bin/magento fix-xml-declaration --raw-output
magento/bin/magento fix-php-declaration --raw-output
magento/bin/magento fix-phtml-declaration --raw-output
magento/bin/magento init-versions --raw-output
magento/bin/magento fix-dependencies --raw-output
magento/bin/magento fix-module-sequence --raw-output
magento/bin/magento setup:module-sequence:update --raw-output
magento/bin/magento fix-composer-name --raw-output
magento/bin/magento fix-composer-type --raw-output
magento/bin/magento fix-composer-authors --raw-output
magento/bin/magento fix-composer-autoload --raw-output
magento/bin/magento fix-composer-config --raw-output
magento/bin/magento fix-composer-homepage --raw-output
magento/bin/magento fix-composer-license --raw-output
magento/bin/magento fix-composer-json --raw-output

if [[ "$(git diff --stat)" != '' ]]; then
  # Configure git
  git config user.name 'R2 Project Develop'
  git config user.email 'adrian.martinez+r2.project-develop@interactiv4.com'
  git config remote.origin.url "git@bitbucket.org:${BITBUCKET_WORKSPACE}/${BITBUCKET_REPO_SLUG}"

  git add . && git commit -n -m "R2: Fix Project Files" && git push

  echo "Stopping pipeline after pushing changes, waiting for a new one to be kicked off..."
  exit 1
fi