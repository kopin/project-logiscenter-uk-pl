<?php
declare(strict_types=1);

// phpcs:disable

require_once __DIR__ . '/reporter.php';

/**
 * Class PhpCsFixerPhpReporter.
 */
class PhpCsFixerPhpReporter extends Reporter
{
    /**
     * Get normalized report name.
     *
     * @return string
     */
    public function getNormalizedReportName(): string
    {
        return 'php-cs-fixer-php';
    }

    /**
     * Get report title.
     *
     * @return string
     */
    public function getTitle(): string
    {
        return 'Php Code Style Fixer Report - Php';
    }

    /**
     * Get annotation type.
     *
     * @return string|null
     */
    public function getAnnotationType(): ?string
    {
        return 'BUG';
    }
}

$reporter = new PhpCsFixerPhpReporter(\dirname(__DIR__) . '/test-reports/junit/php-cs-fixer-php.xml');

$reporter->execute();
