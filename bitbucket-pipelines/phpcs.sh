#!/usr/bin/env bash
set -euox pipefail

echo "Resolving script path..."
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done

DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

cd $DIR/..

EXTRA_ARGS=''
HAS_ERRORS=0
STRICT=0
SHOULD_EXECUTE=1

if [[ -n "${BITBUCKET_PR_ID-}" ]]; then
  # Strict mode for pull-requests triggered builds
  STRICT=1

  if [[ -s CHANGED_FILES_PHP ]]; then
    EXTRA_ARGS=' --file-list=CHANGED_FILES_PHP'
  else
    SHOULD_EXECUTE=0
  fi
fi

# phpcs
if [[ "${SHOULD_EXECUTE}" == '1' ]] && ! magento/vendor/bin/phpcs --standard=Interactiv4 --report-junit=./test-reports/junit/phpcs.xml --report-full --report-source --report-width=120 ${EXTRA_ARGS} -pvn; then
  HAS_ERRORS=1
  echo 'WARNING: Executed with errors'
fi

php bitbucket-pipelines/phpcs-reporter.php

if [[ "${HAS_ERRORS}" == '1' ]] && [[ "${STRICT}" == '1' ]]; then
  exit 1;
fi