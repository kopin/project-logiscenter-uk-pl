<?php
declare(strict_types=1);

// phpcs:disable

require_once __DIR__ . '/reporter.php';

/**
 * Class PhpCsFixerPhtmlReporter.
 */
class PhpCsFixerPhtmlReporter extends Reporter
{
    /**
     * Get normalized report name.
     *
     * @return string
     */
    public function getNormalizedReportName(): string
    {
        return 'php-cs-fixer-phtml';
    }

    /**
     * Get report title.
     *
     * @return string
     */
    public function getTitle(): string
    {
        return 'Php Code Style Fixer Report - Phtml';
    }

    /**
     * Get annotation type.
     *
     * @return string|null
     */
    public function getAnnotationType(): ?string
    {
        return 'BUG';
    }
}

$reporter = new PhpCsFixerPhtmlReporter(\dirname(__DIR__) . '/test-reports/junit/php-cs-fixer-phtml.xml');

$reporter->execute();
