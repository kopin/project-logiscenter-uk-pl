<?php
declare(strict_types=1);

// phpcs:disable

require_once __DIR__ . '/reporter.php';

/**
 * Class PhpMdReporter.
 */
class PhpMdReporter extends Reporter
{
    /**
     * Get normalized report name.
     *
     * @return string
     */
    public function getNormalizedReportName(): string
    {
        return 'phpmd';
    }

    /**
     * Get report title.
     *
     * @return string
     */
    public function getTitle(): string
    {
        return 'Php Mess Detector Report';
    }

    /**
     * Get annotation type.
     *
     * @return string|null
     */
    public function getAnnotationType(): ?string
    {
        return 'CODE_SMELL';
    }

    /**
     * Get annotation severity.
     *
     * @return string|null
     */
    public function getAnnotationSeverity(): ?string
    {
        return 'LOW';
    }
}

$reporter = new PhpMdReporter(\dirname(__DIR__) . '/test-reports/junit/phpmd.xml');

$reporter->execute();
