#!/usr/bin/env bash
set -euox pipefail

echo "Resolving script path..."
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done

DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

cd $DIR/..

# Enable all modules, including possibly new ones added in new
magento/bin/magento module:enable --all --clear-static-content

# Install Magento
function get_property() {
  grep <"../_conf/environment.properties" '-v' '^#' | grep "${1}" | cut '-s' '-d' '=' '-f' '2-'
}

ENVIRONMENT_ID="build"
SUBDOMAIN="build"
SLD="r2"
TLD="lo"
USE_SECURE='1'
ENCRYPTION_KEY="$(head /dev/urandom | env LC_CTYPE=C tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)"
CACHE_ID_PREFIX="_$(head /dev/urandom | env LC_CTYPE=C tr -dc 'a-zA-Z0-9' | fold -w 3 | head -n 1)"

magento/bin/magento 'setup:install' \
    '--admin-firstname' "$(get_property 'install.admin.firstname')" \
    '--admin-lastname' "$(get_property 'install.admin.lastname')" \
    '--admin-email' "$(get_property 'install.admin.email')" \
    '--admin-user' "$(get_property 'install.admin.username')" \
    '--admin-password' "$(get_property 'install.admin.password')" \
    '--base-url' "http://${SUBDOMAIN}.${SLD}.${TLD}/" \
    '--backend-frontname' "$(get_property 'magento.admin.frontname')" \
    '--db-host' '127.0.0.1:3306' \
    '--db-name' "${ENVIRONMENT_ID}" \
    '--db-user' "test_user" \
    '--db-password' "test_user_password" \
    '--language' "$(get_property 'install.locale')" \
    '--currency' "$(get_property 'install.currency')" \
    '--timezone' "$(get_property 'install.timezone')" \
    '--key' "${ENCRYPTION_KEY}" \
    '--use-rewrites' '1' \
    '--use-secure' "${USE_SECURE}" \
    '--base-url-secure' "https://${SUBDOMAIN}.${SLD}.${TLD}/" \
    '--use-secure-admin' "${USE_SECURE}" \
    '--admin-use-security-key' '1' \
    '--cleanup-database' \
    '--db-init-statements' 'SET NAMES utf8;' \
    '--sales-order-increment-prefix' 'P' \
    '--cache-backend' 'redis' \
    '--cache-backend-redis-server' '127.0.0.1' \
    '--cache-backend-redis-port' '6379' \
    '--cache-backend-redis-db' '0' \
    '--cache-backend-redis-compress-data' '1' \
    '--cache-backend-redis-compression-lib' 'gzip' \
    '--cache-id-prefix' "${CACHE_ID_PREFIX}" \
    '--page-cache' 'redis' \
    '--page-cache-redis-server' '127.0.0.1' \
    '--page-cache-redis-port' '6379' \
    '--page-cache-redis-db' '1' \
    '--page-cache-redis-compress-data' '1' \
    '--page-cache-redis-compression-lib' 'gzip' \
    '--page-cache-id-prefix' "${CACHE_ID_PREFIX}" \
    '--session-save' 'redis' \
    '--session-save-redis-host'	'127.0.0.1' \
    '--session-save-redis-port'	'6379' \
    '--session-save-redis-timeout' '2.5' \
    '--session-save-redis-persistent-id' "${CACHE_ID_PREFIX}-session" \
    '--session-save-redis-db' '2' \
    '--session-save-redis-compression-threshold' '2048' \
    '--session-save-redis-compression-lib' 'gzip' \
    '--session-save-redis-log-level' '1' \
    '--session-save-redis-max-concurrency' '6' \
    '--session-save-redis-break-after-frontend' '5' \
    '--session-save-redis-break-after-adminhtml' '30' \
    '--session-save-redis-first-lifetime' '600' \
    '--session-save-redis-bot-first-lifetime'	'60' \
    '--session-save-redis-bot-lifetime'	'7200' \
    '--session-save-redis-disable-locking' '0' \
    '--session-save-redis-min-lifetime'	'60' \
    '--session-save-redis-max-lifetime'	'2592000' \
    '--http-cache-hosts' '127.0.0.1:6081' \
    '-vvv' '--no-interaction' '--ansi'

# Check no cyclic dependencies after fixing dependencies
magento/bin/magento interactiv4:develop:init-versions --raw-output -vvv
magento/bin/magento interactiv4:develop:fix-dependencies --raw-output -vvv
magento/bin/magento interactiv4:develop:fix-module-sequence --raw-output -vvv
(magento/bin/magento setup:upgrade --keep-generated -n) || (magento/bin/magento list -vvv && exit 1)

# Force reinstall packages from composer.lock
rm -rf bin/ magento/vendor
# Clean some automatic deployed folders
rm -rf bitbucket-pipelines/ magento/app/etc/interactiv4*

# Check if project can be installed from composer.lock, without dev dependencies from now on
composer install --no-dev

# Setup upgrade to remove possible removed modules by composer install --no-dev
magento/bin/magento setup:upgrade --keep-generated -n
# Check setup:db:status returns exit code 0 after setup:upgrade
magento/bin/magento setup:db:status

# Set production mode
magento/bin/magento deploy:mode:set production --skip-compilation -vv -n
magento/bin/magento setup:di:compile -vv -n
magento/bin/magento setup:static-content:deploy \
  --force \
  --area adminhtml \
  --area frontend \
  --theme Magento/luma \
  --theme Magento/backend \
  --theme Interactiv4/Milkyway \
  --language en_US \
  --language es_ES \
  --jobs 3 \
  --max-execution-time 600 \
  --symlink-locale \
  en_US es_ES

# Dump autoload with classmap authoritative
composer dump-autoload --classmap-authoritative

# Check simple magento command works with classmap authoritative autoload
magento/bin/magento list -vvv
