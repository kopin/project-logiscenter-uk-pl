<?php
declare(strict_types=1);

/**
 * Class Reporter.
 *
 * phpcs:disable
 */
abstract class Reporter
{
    public const REPORT_RESULT_FAILED = 'FAILED';
    public const REPORT_RESULT_PASSED = 'PASSED';
    public const REPORT_RESULT_PENDING = 'PENDING';

    /**
     * @var string
     */
    private $reportFile;

    /**
     * @var bool
     */
    private $isUseProxy;

    /**
     * @var string
     */
    private $buildNumber;

    /**
     * @var string
     */
    private $bitbucketCommit;

    /**
     * @var string
     */
    private $bitbucketWorkspace;

    /**
     * @var string
     */
    private $bitbucketRepoSlug;

    /**
     * @var string
     */
    private $reportId;

    /**
     * @var string
     */
    private $proxyUrl = 'http://localhost:29418/';

    /**
     * @var array
     */
    private $failures;

    /**
     * Reporter constructor.
     *
     * @param string      $reportFile
     * @param bool        $isUseProxy
     * @param string|null $buildNumber
     * @param string|null $bitbucketCommit
     * @param string|null $bitbucketWorkspace
     * @param string|null $bitbucketRepoSlug
     *
     */
    public function __construct(
        string $reportFile,
        bool $isUseProxy = true,
        ?string $buildNumber = null,
        ?string $bitbucketCommit = null,
        ?string $bitbucketWorkspace = null,
        ?string $bitbucketRepoSlug = null
    ) {
        $this->reportFile = $reportFile;
        $this->buildNumber = $buildNumber ?? (string) \getenv('BITBUCKET_BUILD_NUMBER');
        $this->bitbucketCommit = $bitbucketCommit ?? (string) \getenv('BITBUCKET_COMMIT');
        $this->bitbucketWorkspace = $bitbucketWorkspace ?? (string) \getenv('BITBUCKET_WORKSPACE');
        $this->bitbucketRepoSlug = $bitbucketRepoSlug ?? (string) \getenv('BITBUCKET_REPO_SLUG');
        $this->isUseProxy = $isUseProxy;

        $this->preFlightCheck();

        $this->reportId = "{$this->bitbucketRepoSlug}-{$this->bitbucketCommit}-{$this->getNormalizedReportName()}-{$this->buildNumber}";
    }

    /**
     * Get normalized report name.
     *
     * @return string
     */
    abstract public function getNormalizedReportName(): string;

    /**
     * Check mandatory fields are defined and report file is readable (if exists).
     */
    private function preFlightCheck(): void
    {
        if ('' === $this->buildNumber) {
            throw new \InvalidArgumentException('Unable to determine build number');
        }

        if ('' === $this->bitbucketCommit) {
            throw new \InvalidArgumentException('Unable to determine commit');
        }

        if ('' === $this->bitbucketWorkspace) {
            throw new \InvalidArgumentException('Unable to determine workspace');
        }

        if ('' === $this->bitbucketRepoSlug) {
            throw new \InvalidArgumentException('Unable to determine repo slug');
        }

        if (\is_file($this->reportFile) && !\is_readable($this->reportFile)) {
            throw new \RuntimeException("{$this->reportFile} exists but is not readable.");
        }
    }

    /**
     * Execute reporter.
     */
    public function execute(): void
    {
        $this->createReport();
        $this->createAnnotations();
    }

    /**
     * Create report.
     */
    private function createReport(): void
    {
        $this->apiRequest(
            $this->getApiReportUrl(),
            [
                'title' => $this->getTitle(),
                'details' => $this->getDetails(),
                'remote_link_enabled' => true,
                'logo_url' => $this->getLogoUrl(),
                'report_type' => $this->getReportType(),
                'reporter' => $this->getReporter(),
                'link' => $this->getLink(),
                'result' => $this->getResult(),
                'data' => $this->getData()
            ]
        );
    }

    /**
     * Create annotations.
     */
    private function createAnnotations(): void
    {
        // Only 1000 annotations can be associated to a single report
        $defects = \array_slice($this->getDefects(), 0, 1000);

        // Only 100 annotations can be inserted in bulk at once
        foreach (\array_chunk($defects, 100) as $failuresData) {
            $this->apiRequest(
                $this->getAnnotationsUrl(),
                $failuresData,
                'POST'
            );
        }
    }

    /**
     * Get report title.
     *
     * @return string
     */
    public function getTitle(): string
    {
        return 'Php Static Analysis Report';
    }

    /**
     * Get report details.
     *
     * @return string
     */
    public function getDetails(): string
    {
        $failuresNumber = \count($this->getDefects());

        return 0 < $failuresNumber
            ? "There were found {$failuresNumber} product defects."
            : 'No product defects were found.';
    }

    /**
     * Get report logo url.
     *
     * @return string
     */
    public function getLogoUrl(): string
    {
        return 'https://avatar-management--avatars.us-west-2.prod.public.atl-paas.net/5b8857362786fb2bf79a6a9c/f08763c6-9291-4156-b4c4-f51a29c50bb4/128';
    }

    /**
     * Get report type.
     *
     * @return string
     */
    public function getReportType(): string
    {
        return 'BUG';
    }

    /**
     * Get reporter name.
     *
     * @return string
     */
    public function getReporter(): string
    {
        return \getenv('CI') ? 'CI (Bitbucket Pipelines)' : 'Reporter';
    }

    /**
     * Get report link.
     *
     * @return string
     */
    public function getLink(): string
    {
        $prId = \getenv('BITBUCKET_PR_ID');

        return $prId
            ? "https://bitbucket.org/{$this->bitbucketWorkspace}/{$this->bitbucketRepoSlug}/pull-requests/{$prId}"
            : "https://bitbucket.org/{$this->bitbucketWorkspace}/{$this->bitbucketRepoSlug}/commits/{$this->bitbucketCommit}";
    }

    /**
     * Get report miscellaneous data.
     *
     * @return array
     */
    public function getData(): array
    {
        return [];
    }

    /**
     * Get report result.
     *
     * @return string
     */
    public function getResult(): string
    {
        return \count($this->getDefects()) ? self::REPORT_RESULT_FAILED : self::REPORT_RESULT_PASSED;
    }

    /**
     * Get defects (failure / errors) data.
     *
     * @return array
     */
    /**
     * Get defects (failure / errors) data.
     *
     * @return array
     */
    private function getDefects(): array
    {
        if (null !== $this->failures) {
            return $this->failures;
        }

        $this->failures = [];

        if (!\is_file($this->reportFile)) {
            return $this->failures;
        }

        $dom = new \DOMDocument();
        $dom->loadXML(\file_get_contents($this->reportFile));

        $testCaseNodes = $dom->getElementsByTagName('testcase');

        $annotationId = 0;

        /** @var \DOMElement $testCaseNode */
        foreach ($testCaseNodes as $testCaseNode) {
            $fileAttr = $testCaseNode->getAttribute('file') ?: $testCaseNode->getAttribute('name');
            $fileAttrParts = \array_map('trim', \explode(' ', $fileAttr));

            $file = \array_filter(
                $fileAttrParts,
                static function ($fileAttrPart) {
                    return (bool) \preg_match('/\.(php|phtml)/', $fileAttrPart);
                }
            );

            $file = \reset($file) ?: '';
            $line = '';

            // In case line is included within file name
            if (false !== \strpos($file, ':')) {
                [$file, $line] = \explode(':', $file, 2);
            }

            // In case line is in other part of the attribute
            if ('' === $line) {
                foreach ($fileAttrParts as $fileAttrPart) {
                    // phpcs case
                    if (\preg_match('/\((\d+):(\d+)\)/', $fileAttrPart, $matches)) {
                        $line = $matches[1];
                        break;
                    }
                }
            }

            /** @var \DOMNode $failureNode */
            foreach ($testCaseNode->getElementsByTagName('failure') as $failureNode) {
                // In case line is part of the content (php-cs-fixer case)
                if ('' === $line && \preg_match('/@@ -(\d+),(\d+) /', $failureNode->textContent, $matches)) {
                    $line = $matches[1];
                }

                $file = \trim(\str_replace(\dirname(__DIR__), '', $file), '/');
                $line = \trim($line);

                $annotationId++;
                $type = $failureNode->getAttribute('type');
                $message = $failureNode->getAttribute('message');

                $this->failures[] = [
                    'external_id' => $this->reportId . '-annotation' .  $annotationId,
                    'title' => $this->getTitle(),
                    'annotation_type' => $this->getAnnotationType() ?? $type,
                    'result' => 'FAILED',
                    'severity' => $this->getAnnotationSeverity() ?? 'MEDIUM',
                    'summary' => \substr(($message ?: $failureNode->textContent), 0, 450),
                    'path' => $file,
                    'line' => $line
                ];
            }

            /** @var \DOMNode $failureNode */
            foreach ($testCaseNode->getElementsByTagName('error') as $errorNode) {
                // In case line is part of the content (php-cs-fixer case)
                if ('' === $line && \preg_match('/@@ -(\d+),(\d+) /', $failureNode->textContent, $matches)) {
                    $line = $matches[1];
                }

                $file = \trim(\str_replace(\dirname(__DIR__), '', $file), '/');
                $line = \trim($line);

                $annotationId++;
                $type = $errorNode->getAttribute('type');
                $message = $errorNode->getAttribute('message');

                $this->failures[] = [
                    'external_id' => $this->reportId . '-annotation' .  $annotationId,
                    'title' => $this->getTitle(),
                    'annotation_type' => $this->getAnnotationType() ?? $type,
                    'result' => 'FAILED',
                    'severity' => 'CRITICAL',
                    'summary' => \substr(($message ?: $failureNode->textContent), 0, 450),
                    'path' => $file,
                    'line' => $line
                ];
            }
        }

        return $this->failures;
    }

    /**
     * Get annotation type.
     *
     * @return string|null
     */
    public function getAnnotationType(): ?string
    {
        return null;
    }

    /**
     * Get annotation severity.
     *
     * @return string|null
     */
    public function getAnnotationSeverity(): ?string
    {
        return null;
    }

    /**
     * Send request to Bitbucket API.
     *
     * @param string $url
     * @param array $payloadData
     * @param string $method
     *
     * @return void
     */
    private function apiRequest(
        string $url,
        array $payloadData,
        string $method = 'PUT'
    ): void {
        $ch = \curl_init($url);

        if ($this->isUseProxy) {
            \curl_setopt($ch, CURLOPT_PROXY, $this->proxyUrl);
        }

        $payload = \json_encode($payloadData);

        \curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        \curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        \curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        \curl_setopt($ch, CURLOPT_HTTPHEADER, ['content-type: application/json']);
        \curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        $response = curl_exec($ch);
        $curlInfo = curl_getinfo($ch);

        if ($curlInfo['http_code'] !== 200) {
            if (\curl_errno($ch)) {
                echo 'Curl error: ' . \curl_error($ch) . PHP_EOL;
            }

            echo 'Payload: ' . $payload;

            throw new \RuntimeException("$url returned {$curlInfo['http_code']} code: {$response}");
        }
    }

    /**
     * Get API base url.
     *
     * @return string
     */
    private function getApiBaseUrl(): string
    {
        $protocol = $this->isUseProxy ? 'http' : 'https';

        return "$protocol://api.bitbucket.org/2.0";
    }

    /**
     * Get API report url.
     *
     * @return string
     */
    private function getApiReportUrl(): string
    {
        return "{$this->getApiBaseUrl()}/repositories/{$this->bitbucketWorkspace}/{$this->bitbucketRepoSlug}/commit/{$this->bitbucketCommit}/reports/{$this->reportId}";
    }

    /**
     * Get API annotations url.
     *
     * @return string
     */
    private function getAnnotationsUrl(): string
    {
        return "{$this->getApiReportUrl()}/annotations";
    }
}
