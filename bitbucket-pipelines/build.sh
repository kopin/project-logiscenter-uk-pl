#!/usr/bin/env bash
set -euox pipefail

echo "Resolving script path..."
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done

DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

cd $DIR/..

composer install

# Force use proper installation config file for integration tests
rm -f magento/dev/tests/integration/etc/install-config-mysql.php
cp magento/dev/tests/integration/etc/install-config-mysql-ce.php.dist magento/dev/tests/integration/etc/install-config-mysql.php

# Build list of changed files in when pipeline kicked for PR
# Please note '-' after variable name, do not remove it, it helps to make it work with set -u
if [ -n "${BITBUCKET_PR_DESTINATION_BRANCH-}" ]; then
  git config remote.origin.fetch +refs/heads/*:refs/remotes/origin/*
  git fetch origin
  git --no-pager diff --name-only --diff-filter=ACMRTUXB origin/${BITBUCKET_PR_DESTINATION_BRANCH}...${BITBUCKET_COMMIT} > CHANGED_FILES
  set +e
  cat CHANGED_FILES | grep -E '(app/code/Interactiv4|app/design/frontend/Interactiv4|app/design/adminhtml/Interactiv4|lib/internal/Interactiv4)/.*\.(php)(\.sample)?$' > CHANGED_FILES_PHP
  cat CHANGED_FILES | grep -E '(app/code/Interactiv4|app/design/frontend/Interactiv4|app/design/adminhtml/Interactiv4|lib/internal/Interactiv4)/.*\.(phtml)(\.sample)?$$' > CHANGED_FILES_PHTML
  set -e
fi
