#!/usr/bin/env bash
set -euox pipefail

echo "Resolving script path..."
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done

DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

cd $DIR/..

STRICT=0
HAS_ERRORS=0
JOINED_ARGS='magento/app/code/Interactiv4,magento/lib/internal/Interactiv4'
SHOULD_EXECUTE=1

if [[ -n "${BITBUCKET_PR_ID-}" ]]; then
  # Strict mode for pull-requests triggered builds
  STRICT=1

  if [[ -s CHANGED_FILES_PHP ]]; then
    EXTRA_ARGS=$(< CHANGED_FILES_PHP)
    printf -v JOINED_ARGS '%s,' ${EXTRA_ARGS[@]}
    JOINED_ARGS="${JOINED_ARGS%,}"
  else
    SHOULD_EXECUTE=0
  fi
fi

# phpmd
if [[ "${SHOULD_EXECUTE}" == '1' ]] && ! magento/vendor/bin/phpmd ${JOINED_ARGS} xml phpmd.xml | xsltproc junit.xslt - > ./test-reports/junit/phpmd.xml; then
  HAS_ERRORS=1
fi

php bitbucket-pipelines/phpmd-reporter.php

if [[ "${HAS_ERRORS}" == '1' ]] && [[ "${STRICT}" == '1' ]]; then
  exit 1;
fi