<?php
declare(strict_types=1);

// phpcs:disable

require_once __DIR__ . '/reporter.php';

/**
 * Class PhpStanReporter.
 */
class PhpStanReporter extends Reporter
{
    /**
     * Get normalized report name.
     *
     * @return string
     */
    public function getNormalizedReportName(): string
    {
        return 'phpstan';
    }

    /**
     * Get report title.
     *
     * @return string
     */
    public function getTitle(): string
    {
        return 'Php Static Analysis Report';
    }

    /**
     * Get annotation type.
     *
     * @return string|null
     */
    public function getAnnotationType(): ?string
    {
        return 'BUG';
    }

    /**
     * Get annotation severity.
     *
     * @return string|null
     */
    public function getAnnotationSeverity(): ?string
    {
        return 'HIGH';
    }
}

$reporter = new PhpStanReporter(\dirname(__DIR__) . '/test-reports/junit/phpstan.xml');

$reporter->execute();
