<?php
declare(strict_types=1);
/*
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

use Interactiv4\Environment\Api\EnvironmentInterface;

return [
    // Optional, defaults to false if not defined
    'enabled' => true,
    // Required, won't send email if not defined
    'to_email' => 'logiscenter@interactiv4.com,dpsuarez@logiscenter.com',
    // Optional
    'to_name' => '',
    // Required, won't send email if not defined
    'from_email' => 'logiscenter@interactiv4.com',
    // Optional
    'from_name' => '',
    // Required, won't send email if not defined
    'reply_to_email' => 'logiscenter@interactiv4.com',
    // Optional
    'reply_to_name' => '',
    // Environments for which it is allowed to send emails
    // Leave empty for all environments
    'allowed_environments' => [
        // EnvironmentInterface::LOCAL,
        EnvironmentInterface::DEV,
        EnvironmentInterface::STAGING,
        EnvironmentInterface::PRODUCTION,
    ],
];
