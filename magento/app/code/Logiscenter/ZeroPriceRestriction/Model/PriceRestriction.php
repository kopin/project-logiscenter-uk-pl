<?php

/**
 * @author    Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ZeroPriceRestriction\Model;

use Logiscenter\ZeroPriceRestrictionConfig\Api\Config\ZeroPriceRestrictionModuleStatusInterface;
use Magento\Framework\Pricing\SaleableInterface;

/**
 * Remove products from cart with zero price
 */
class PriceRestriction
{
    /**
     * @var ZeroPriceRestrictionModuleStatusInterface
     */
    private $statusConfig;

    /**
     * @param ZeroPriceRestrictionModuleStatusInterface $statusConfig
     */
    public function __construct(ZeroPriceRestrictionModuleStatusInterface $statusConfig)
    {
        $this->statusConfig = $statusConfig;
    }

    /**
     * Checks if can show price for provided product
     *
     * @param SaleableInterface $saleableItem
     *
     * @return bool
     */
    public function canShowPrice(SaleableInterface $saleableItem): bool
    {
        return !$this->statusConfig->isEnabled() || ($this->statusConfig->isEnabled() && $saleableItem->isSalable());
    }
}
