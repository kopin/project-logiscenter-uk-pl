<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ZeroPriceRestriction\Observer;

use Logiscenter\ZeroPriceRestrictionConfig\Api\Config\ZeroPriceRestrictionModuleStatusInterface;
use Magento\Catalog\Pricing\Price\FinalPrice;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

/**
 * Make products with zero price not salable
 */
class ZeroPriceProductsNotSalable implements ObserverInterface
{
    /**
     * @var ZeroPriceRestrictionModuleStatusInterface
     */
    private $statusConfig;

    /**
     * @param ZeroPriceRestrictionModuleStatusInterface $statusConfig
     */
    public function __construct(ZeroPriceRestrictionModuleStatusInterface $statusConfig)
    {
        $this->statusConfig = $statusConfig;
    }

    /**
     * Observer for catalog_product_is_salable_after
     *
     * @param Observer $observer
     *
     * @return void
     */
    public function execute(Observer $observer)
    {
        if (!$this->statusConfig->isEnabled()) {
            return;
        }
        $object = $observer->getEvent()->getSalable();
        if (!$object->getIsSalable()) {
            return;
        }
        $product = $object->getProduct();
        $object->setIsSalable((float)$product->getPriceInfo()->getPrice(FinalPrice::PRICE_CODE)->getValue() !== 0.0);
    }
}
