<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ZeroPriceRestriction\Observer;

use Logiscenter\ZeroPriceRestrictionConfig\Api\Config\ZeroPriceRestrictionModuleStatusInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Quote\Model\Quote;
use Magento\Quote\Model\ResourceModel\Quote as QuoteResource;

/**
 * Remove products from cart with zero price
 */
class QuoteExcludeZeroPriceProducts implements ObserverInterface
{
    /**
     * @var QuoteResource
     */
    private $resource;

    /**
     * @var ZeroPriceRestrictionModuleStatusInterface
     */
    private $statusConfig;


    /**
     * @param QuoteResource                     $resource
     * @param ZeroPriceRestrictionModuleStatusInterface $statusConfig
     */
    public function __construct(QuoteResource $resource, ZeroPriceRestrictionModuleStatusInterface $statusConfig)
    {
        $this->resource = $resource;
        $this->statusConfig = $statusConfig;
    }

    /**
     * Observer for sales_quote_collect_totals_before
     *
     * @param Observer $observer
     *
     * @return void
     */
    public function execute(Observer $observer)
    {
        if (!$this->statusConfig->isEnabled()) {
            return;
        }
        $event = $observer->getEvent();
        /** @var Quote $quote */
        $quote = $event->getQuote();
        if (!$quote) {
            return;
        }
        $save = false;
        foreach ($quote->getAllVisibleItems() as $item) {
            if ((float)$item->getPrice() === 0.0) {
                $item->getProduct()->setData('salable', false);
                $quote->removeItem($item->getId());
                $save = true;
            }
        }

        if ($save) {
            $this->resource->save($quote);
        }
    }
}
