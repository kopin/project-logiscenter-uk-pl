<?php

/**
 * @author    Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ZeroPriceRestriction\Plugin;

use Logiscenter\ZeroPriceRestrictionConfig\Api\Config\ZeroPriceRestrictionModuleStatusInterface;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Product\Type\AbstractType;
use Magento\Framework\DataObject;
use Magento\Quote\Model\Quote;

/**
 * Blocks add zero price simple product to cart plugin class
 */
class CheckIfProductHasPrice
{
    /**
     * @var ZeroPriceRestrictionModuleStatusInterface
     */
    private $statusConfig;

    /**
     * @param ZeroPriceRestrictionModuleStatusInterface $statusConfig
     */
    public function __construct(ZeroPriceRestrictionModuleStatusInterface $statusConfig)
    {
        $this->statusConfig = $statusConfig;
    }

    /**
     * Set product qty 0 and is not salable to zero price product
     *
     * @param Quote                 $subject
     * @param mixed                 $product
     * @param null|float|DataObject $request
     * @param null|string           $processMode
     *
     * @return array
     */
    public function beforeAddProduct(
        Quote $subject,
        Product $product,
        $request,
        $processMode = AbstractType::PROCESS_MODE_FULL
    ) {
        if ($this->statusConfig->isEnabled() && (float)$product->getPrice() === 0.0) {
            $request->setQty(0);
            $product->setData('salable', false);
        }

        return [$product, $request, $processMode];
    }
}
