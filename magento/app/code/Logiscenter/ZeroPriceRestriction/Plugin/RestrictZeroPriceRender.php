<?php
declare(strict_types=1);
/**
 * @author    Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Logiscenter\ZeroPriceRestriction\Plugin;

use Logiscenter\ZeroPriceRestriction\Model\PriceRestriction;
use Logiscenter\ZeroPriceRestrictionConfig\Api\Config\ZeroPriceRestrictionModuleStatusInterface;
use Magento\Framework\Pricing\Render;
use Magento\Framework\Pricing\SaleableInterface;

/**
 * Prevent price rendering for not salable items
 */
class RestrictZeroPriceRender
{
    /**
     * @var ZeroPriceRestrictionModuleStatusInterface
     */
    private $statusConfig;

    /**
     * @var PriceRestriction
     */
    private $priceRestriction;

    /**
     * @param ZeroPriceRestrictionModuleStatusInterface $statusConfig
     */
    public function __construct(
        ZeroPriceRestrictionModuleStatusInterface $statusConfig,
        PriceRestriction $priceRestriction
    ) {
        $this->statusConfig = $statusConfig;
        $this->priceRestriction = $priceRestriction;
    }

    /**
     * Do not render the product price if product is not salable
     *
     * @param Render                            $subject
     * @param                                   $result
     * @param                                   $priceCode
     * @param SaleableInterface                 $saleableItem
     *
     * @return mixed|string
     */
    public function afterRender(Render $subject, $result, $priceCode, SaleableInterface $saleableItem)
    {
        if (!$this->priceRestriction->canShowPrice($saleableItem)) {
            $result = '';
        }

        return $result;
    }
}
