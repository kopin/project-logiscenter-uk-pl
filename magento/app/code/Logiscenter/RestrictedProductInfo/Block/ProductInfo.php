<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\RestrictedProductInfo\Block;

use Interactiv4\Storefront\Api\Block\CacheableInterface;
use Interactiv4\Storefront\Api\Block\CacheableTrait;
use Logiscenter\RestrictedProductInfo\Model\ProductIdProvider;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\Data\ProductInterfaceFactory;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

/**
 * Restricted product info block class
 */
class ProductInfo extends Template implements CacheableInterface
{
    use CacheableTrait;

    private const CACHE_KEY                     = 'product_info_proveedor_block_html_';
    private const INFO_PROVEEDOR_ATTRIBUTE_CODE = 'info_proveedor';

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var ProductInterface|IdentityInterface
     */
    private $product;

    /**
     * @var ProductInterfaceFactory
     */
    private $productFactory;

    /**
     * @var ProductIdProvider
     */
    private $productIdProvider;

    /**
     * @inheritDoc
     */
    public function __construct(
        Context $context,
        ProductRepositoryInterface $productRepository,
        ProductInterfaceFactory $productFactory,
        ProductIdProvider $productIdProvider,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->productRepository = $productRepository;
        $this->productFactory = $productFactory;
        $this->productIdProvider = $productIdProvider;
        $this->setCacheKey(self::CACHE_KEY . $this->productIdProvider->getProductId());
    }

    /**
     * @inheritDoc
     */
    // @codingStandardsIgnoreStart
    protected function _beforeToHtml()
    {
        // @codingStandardsIgnoreEnd
        $this->initProduct();

        return parent::_beforeToHtml();
    }

    /**
     * Return restricted product info attribute value
     *
     * @return array
     */
    public function getProductInfo()
    {
        $data = [];

        if ($resourceAttribute = $this->product->getResource()->getAttribute(self::INFO_PROVEEDOR_ATTRIBUTE_CODE)) {
            $label = $resourceAttribute->getFrontend()->getLabel();
            $data['label'] = $label ?: __('Provider info');
        }

        if ($attribute = $this->product->getCustomAttribute(self::INFO_PROVEEDOR_ATTRIBUTE_CODE)) {
            $data['value'] = $attribute->getValue();
        }

        return $data;
    }

    /**
     * Init product for retrieve attribute data data
     */
    private function initProduct()
    {
        try {
            $this->product = $this->productRepository->getById($this->productIdProvider->getProductId());
        } catch (NoSuchEntityException $e) {
            //set dummy product model in case if requested does not exist
            $this->product = $this->productFactory->create();
        }
        array_map(
            function ($identity) {
                $this->addCacheTag($identity);
            },
            $this->product->getIdentities()
        );
    }
}
