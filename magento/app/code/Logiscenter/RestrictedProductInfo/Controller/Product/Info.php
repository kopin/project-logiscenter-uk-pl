<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\RestrictedProductInfo\Controller\Product;

use Logiscenter\RestrictedProductInfoConfig\Api\Config\RestrictedProductInfoConfigInterface;
use Logiscenter\RestrictedProductInfoConfig\Api\Config\RestrictedProductInfoModuleStatusInterface;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\HTTP\PhpEnvironment\RemoteAddress;
use Magento\Framework\View\Result\LayoutFactory;
use Psr\Log\LoggerInterface;

/**
 * Product restricted info action class
 */
class Info extends Action implements HttpGetActionInterface
{
    /**
     * @var RestrictedProductInfoConfigInterface
     */
    private $productInfoConfig;

    /**
     * @var RemoteAddress
     */
    private $address;

    /**
     * @var LayoutFactory
     */
    private $layoutFactory;

    /**
     * @var RestrictedProductInfoModuleStatusInterface
     */
    private $statusConfig;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param Context                                    $context
     * @param LayoutFactory                              $layoutFactory
     * @param RestrictedProductInfoConfigInterface       $productInfoConfig
     * @param RemoteAddress                              $remoteAddress
     * @param RestrictedProductInfoModuleStatusInterface $statusConfig
     * @param LoggerInterface                            $logger
     */
    public function __construct(
        Context $context,
        LayoutFactory $layoutFactory,
        RestrictedProductInfoConfigInterface $productInfoConfig,
        RemoteAddress $remoteAddress,
        RestrictedProductInfoModuleStatusInterface $statusConfig,
        LoggerInterface $logger
    ) {
        parent::__construct($context);
        $this->layoutFactory = $layoutFactory;
        $this->productInfoConfig = $productInfoConfig;
        $this->address = $remoteAddress;
        $this->statusConfig = $statusConfig;
        $this->logger = $logger;
    }

    /**
     * @inheritDoc
     */
    public function execute()
    {
        $userIp = $this->address->getRemoteAddress();
        $header = $_SERVER['HTTP_X_FORWARDED_FOR'] ?? '';
        $this->logger->info(self::class . " IP_REMOTE_ADDRESS " . $userIp);
        $this->logger->info(self::class . " HTTP_X_FORWARDED_FOR HEADER " . $header);
        if (
            !in_array($this->address->getRemoteAddress(), $this->productInfoConfig->getWhitelistedIps()) ||
            !$this->statusConfig->isEnabled()
        ) {
            return null;
        }
        $this->getResponse()->setNoCacheHeaders();

        return $this->layoutFactory->create();
    }
}
