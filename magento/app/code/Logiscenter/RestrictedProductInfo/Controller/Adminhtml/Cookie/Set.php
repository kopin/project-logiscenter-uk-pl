<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\RestrictedProductInfo\Controller\Adminhtml\Cookie;

use Logiscenter\RestrictedProductInfo\Service\SetRestrictedCookie;
use Logiscenter\RestrictedProductInfoConfig\Api\Config\RestrictedProductInfoModuleStatusInterface;
use Magento\Backend\App\AbstractAction;
use Magento\Backend\App\Action;
use Magento\Framework\App\Action\HttpGetActionInterface;

/**
 * Set cookie action class
 */
class Set extends AbstractAction implements HttpGetActionInterface
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Logiscenter_RestrictedProductInfo::set';

    /**
     * @var SetRestrictedCookie
     */
    private $setRestrictedCookie;

    /**
     * @var RestrictedProductInfoModuleStatusInterface
     */
    private $moduleStatus;

    /**
     * @param Action\Context                             $context
     * @param SetRestrictedCookie                        $setRestrictedCookie
     * @param RestrictedProductInfoModuleStatusInterface $moduleStatus
     */
    public function __construct(
        Action\Context $context,
        SetRestrictedCookie $setRestrictedCookie,
        RestrictedProductInfoModuleStatusInterface $moduleStatus
    ) {
        parent::__construct($context);
        $this->setRestrictedCookie = $setRestrictedCookie;
        $this->moduleStatus = $moduleStatus;
    }

    /**
     * @inheritDoc
     */
    public function execute()
    {
        if (!$this->moduleStatus->isEnabled()) {
            $this->messageManager->addErrorMessage(__('Module is disabled in config'));
            return $this->_redirect('admin/dashboard/index');
        }

        try {
            $isWhitelisted = $this->setRestrictedCookie->execute();
            $this->messageManager->addSuccessMessage(
                $isWhitelisted
                    ? __('Cookie has been set correctly.')
                    : __(
                        'Cookie has been set correctly, but your IP is not whitelisted. '
                        . 'Please, contact to administrator.'
                    )
            );
        } catch (\Exception $exception) {
            $this->messageManager->addErrorMessage(
                __(
                    'An error occurred when setting the cookie. Please, try again or please contact your administrator'
                )
            );
        }

        return $this->_redirect('*/*/view');
    }
}
