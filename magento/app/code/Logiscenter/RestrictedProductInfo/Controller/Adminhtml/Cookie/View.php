<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\RestrictedProductInfo\Controller\Adminhtml\Cookie;

use Logiscenter\RestrictedProductInfoConfig\Api\Config\RestrictedProductInfoModuleStatusInterface;
use Magento\Backend\App\AbstractAction;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\View\Result\PageFactory;

/**
 * View cookies action class
 */
class View extends AbstractAction implements HttpGetActionInterface
{
    const MENU_ID = 'Logiscenter_Base::logiscenter';

    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Logiscenter_RestrictedProductInfo::view';

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var RestrictedProductInfoModuleStatusInterface
     */
    private $moduleStatus;

    /**
     * Index constructor.
     *
     * @param Context     $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        RestrictedProductInfoModuleStatusInterface $moduleStatus
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->moduleStatus = $moduleStatus;
    }

    /**
     * @inheridoc
     */
    public function execute()
    {
        if (!$this->moduleStatus->isEnabled()) {
            $this->messageManager->addErrorMessage(__('Module is disabled in config'));
            return $this->_redirect('admin/dashboard/index');
        }

        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu(static::MENU_ID);

        return $resultPage;
    }
}
