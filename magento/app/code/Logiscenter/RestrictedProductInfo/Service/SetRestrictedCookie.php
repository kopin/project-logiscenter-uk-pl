<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\RestrictedProductInfo\Service;

use Exception;
use Logiscenter\RestrictedProductInfoConfig\Api\Config\RestrictedProductInfoConfigInterface;
use Logiscenter\RestrictedProductInfoConfig\Api\Config\RestrictedProductInfoModuleStatusInterface;
use Magento\Framework\HTTP\PhpEnvironment\RemoteAddress;
use Magento\Framework\Session\Config;
use Magento\Framework\Session\Config\ConfigInterface;
use Magento\Framework\Stdlib\Cookie\CookieMetadataFactory;
use Magento\Framework\Stdlib\CookieManagerInterface;
use Psr\Log\LoggerInterface;

/**
 * Set cookie for customer service class
 */
class SetRestrictedCookie
{
    /**
     *
     */
    private const COOKIE_NAME = 'lc_ac';

    /**
     * @var CookieManagerInterface
     */
    private $cookieManager;

    /**
     * @var RestrictedProductInfoConfigInterface
     */
    private $restrictedProductInfoConfig;

    /**
     * @var RemoteAddress
     */
    private $remoteAddress;

    /**
     * @var CookieMetadataFactory
     */
    private $metadataFactory;

    /**
     * @var RestrictedProductInfoModuleStatusInterface
     */
    private $statusConfig;

    /**
     * @var ConfigInterface
     */
    private $config;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param CookieManagerInterface                     $cookieManager
     * @param RestrictedProductInfoConfigInterface       $restrictedProductInfoConfig
     * @param CookieMetadataFactory                      $metadataFactory
     * @param RemoteAddress                              $remoteAddress
     * @param Config                                     $config
     * @param RestrictedProductInfoModuleStatusInterface $statusConfig
     * @param LoggerInterface                            $logger
     */
    public function __construct(
        CookieManagerInterface $cookieManager,
        RestrictedProductInfoConfigInterface $restrictedProductInfoConfig,
        CookieMetadataFactory $metadataFactory,
        RemoteAddress $remoteAddress,
        Config $config,
        RestrictedProductInfoModuleStatusInterface $statusConfig,
        LoggerInterface $logger
    ) {
        $this->cookieManager = $cookieManager;
        $this->restrictedProductInfoConfig = $restrictedProductInfoConfig;
        $this->remoteAddress = $remoteAddress;
        $this->metadataFactory = $metadataFactory;
        $this->statusConfig = $statusConfig;
        $this->config = $config;
        $this->logger = $logger;
    }

    /**
     * Set customer ip address cookie value
     *
     * @return bool
     * @throws Exception
     */
    public function execute(): bool
    {
        if (!$this->statusConfig->isEnabled()) {
            return false;
        }

        $metadata = $this->metadataFactory
            ->createPublicCookieMetadata()
            ->setDurationOneYear()
            ->setPath($this->config->getCookiePath())
            ->setDomain($this->config->getCookieDomain())
            ->setSecure($this->config->getCookieSecure())
            ->setHttpOnly(false)
            ->setSameSite($this->config->getCookieSameSite());
        $userIp = $this->remoteAddress->getRemoteAddress();
        $header = $_SERVER['HTTP_X_FORWARDED_FOR'] ?? '';
        $this->logger->info(self::class . " IP_REMOTE_ADDRESS " . $userIp);
        $this->logger->info(self::class . " HTTP_X_FORWARDED_FOR HEADER " . $header);
        $this->cookieManager->setPublicCookie(self::COOKIE_NAME, $userIp, $metadata);
        $allowedIps = $this->restrictedProductInfoConfig->getWhitelistedIps();

        return in_array($userIp, $allowedIps);
    }
}
