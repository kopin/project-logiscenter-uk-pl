<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\RestrictedProductInfo\ViewModel\Adminhtml;

use Magento\Framework\View\Element\Block\ArgumentInterface;

/**
 * Cookie data view model class
 */
class CookieInfo implements ArgumentInterface
{
    public function getCookies()
    {
        return $_COOKIE;
    }
}
