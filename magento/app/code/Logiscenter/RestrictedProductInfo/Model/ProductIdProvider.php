<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\RestrictedProductInfo\Model;

use Magento\Framework\App\RequestInterface;

class ProductIdProvider
{
    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @param RequestInterface $request
     */
    public function __construct(
        RequestInterface $request
    ) {
        $this->request = $request;
    }

    /**
     * Return product if from request parameter
     *
     * @return int
     */
    public function getProductId(): int
    {
        return (int)$this->request->getParam('product_id', 0);
    }
}
