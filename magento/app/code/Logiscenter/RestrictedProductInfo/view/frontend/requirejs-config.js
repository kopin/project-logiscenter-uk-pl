/*
* @author Interactiv4 Team
* @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
*/

var config = {
    map: {
        '*': {
            'restricted_product_info': 'Logiscenter_RestrictedProductInfo/js/info'
        }
    }
};