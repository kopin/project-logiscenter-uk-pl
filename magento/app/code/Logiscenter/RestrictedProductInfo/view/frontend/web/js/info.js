/*
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

define([
    'jquery',
    'Magento_Ui/js/modal/modal',
    'mage/url',
    'mage/storage',
    'mage/translate',
    'mage/cookies'
], function ($, modal, urlBuilder, storage, $t) {
    'use strict';

    $.widget('logiscenter_restricted_product_info.info', {
        /**
         * Options common to all instances of this widget.
         * @type {Object}
         */
        options: {
            container: '[data-container="restricted-product-info"]',
            actionStick: '[data-event="restricted-product-info-action-stick"]',
            cookieName: 'lc_ac'
        },

        /** @inheritdoc */
        _create: function () {
            if ($.mage.cookies.get(this.options.cookieName)) {
                this._getDataBlock();
            }
        },

        /**
         * Retrieve block info through ajax call
         *
         * @private
         */
        _getDataBlock: function () {
            let serviceUrl = urlBuilder.build('restricted_info/product/info', {}),
                productId = $('.product-add-form').find('input[name="product"]').val();

                $.ajax({
                url: serviceUrl,
                data: {
                    product_id: productId
                },
                cache: false,

                /** @inheritdoc */
                fail: function (xhr, textStatus, errorThrown) {
                    console.error('Some error happened getting restricted info product.');
                },

                /** @inheritdoc */
                success: function (response) {
                    if (typeof response === 'string') {
                        try {
                            $('.footer__bottom').append(response);
                            this._bind($(this.options.actionStick));
                        } catch (e) {
                            console.error('Some error happened getting restricted info product.');
                        }
                    }
                }.bind(this)
            });
        },

        /**
         * Bind the different case events to the elements
         *
         * @param {Object} element
         * @private
         */
        _bind: function (element) {
            element.on('click', function() {
                $(this.options.container).toggleClass('is-sticked');
            }.bind(this));
        }
    });

    return $.logiscenter_restricted_product_info.info;
});
