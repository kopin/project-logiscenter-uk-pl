<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ProductTabs\Model;

use Interactiv4\Framework\Data\AbstractOptionSource;

/**
 * Class TabConfig
 */
class TabConfig
{
    /**
     * @var AbstractOptionSource[]
     */
    private $tabsSources;

    /**
     * TabConfig constructor.
     *
     * @param AbstractOptionSource[] $tabsSources
     */
    public function __construct(
        array $tabsSources
    ) {
        $this->tabsSources = $tabsSources;
    }

    /**
     * @var array $tabsData
     */
    private $tabsData = [];

    /**
     * @return array
     */
    public function getTabs(): array
    {
        if (!$this->tabsData) {
            foreach ($this->getTabsFromSources() as $key => $data) {
                $title = (array_key_exists('title', $data)) ? __($data['title']) : __('Related with %1:');
                $this->tabsData[$key] =  [
                    'title'         => __($data['label']),
                    'type'          =>  'template',
                    'data'          =>  [
                            'type'      =>  'Magento\Catalog\Block\Product\View',
                            'name'      =>  $data['label'],
                            'template'  =>  'Logiscenter_ProductTabs::tab-content.phtml',
                            'sortOrder' =>  $data['sortOrder'],
                            'title'     =>  $title,
                            'pageSize'  =>  $data['pageSize']
                    ]
                ];
            }
        }
        return $this->tabsData;
    }

    /**
     * @return array
     */
    public function getTabsFromSources()
    {
        $tabsFromSources = [];
        foreach ($this->tabsSources as $source) {
            $tabsFromSources = array_merge($tabsFromSources, $source->toArray());
        }
        return $tabsFromSources;
    }
}
