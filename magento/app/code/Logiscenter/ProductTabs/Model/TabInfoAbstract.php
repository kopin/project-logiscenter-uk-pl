<?php

/**
 * @author    Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ProductTabs\Model;

use Interactiv4\ConfigHelper\Api\Config\Status\ModuleStatusInterface;
use Interactiv4\Framework\Data\AbstractOptionSource;
use Logiscenter\ElasticSearchTabs\Model\GetTabProducts;
use Logiscenter\Price\Model\Price\Config;
use Logiscenter\Price\Service\Price\GetFinalPriceWithTax;
use Logiscenter\Price\Service\Price\GetMsrpPrice;
use Logiscenter\AvailabilityInfo\Api\AvailabilityInfoProviderInterface as Info;
use Logiscenter\AvailabilityInfo\Api\GetAvailabilityInfoDataInterface;
use Logiscenter\ProductTabs\Api\Data\TabInfoInterface;
use Logiscenter\ProductTabsConfig\Api\Config\ProductTabsConfigInterface;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Helper\Image;
use Magento\Catalog\Model\ResourceModel\Collection\AbstractCollection;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Framework\Pricing\SaleableInterface;

/**
 * Class represent base tab logic
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
abstract class TabInfoAbstract implements TabInfoInterface
{
    /**
     * @var ModuleStatusInterface
     */
    private $statusConfig;

    /**
     * @var AbstractOptionSource
     */
    private $tabsSource;

    /**
     * @var Image
     */
    private $imageHelper;

    /**
     * @var PriceCurrencyInterface
     */
    private $priceCurrency;

    /**
     * @var GetAvailabilityInfoDataInterface
     */
    private $getAvailabilityInfoData;

    /**
     * @var GetMsrpPrice
     */
    private $getMsrpPrice;

    /**
     * @var GetFinalPriceWithTax
     */
    private $getFinalPriceWithTax;

    /**
     * @var Config
     */
    private $priceConfig;

    /**
     * @var GetTabProducts
     */
    private $getTabsProducts;

    /**
     * @var ProductTabsConfigInterface
     */
    private $tabsConfig;

    /**
     * @param AbstractOptionSource             $tabsSource
     * @param Image                            $imageHelper
     * @param PriceCurrencyInterface           $priceCurrency
     * @param GetMsrpPrice                     $getMsrpPrice
     * @param GetFinalPriceWithTax             $getFinalPriceWithTax
     * @param GetAvailabilityInfoDataInterface $getAvailabilityInfoData
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        ModuleStatusInterface            $statusConfig,
        AbstractOptionSource             $tabsSource,
        Image                            $imageHelper,
        PriceCurrencyInterface           $priceCurrency,
        GetMsrpPrice                     $getMsrpPrice,
        GetFinalPriceWithTax             $getFinalPriceWithTax,
        GetAvailabilityInfoDataInterface $getAvailabilityInfoData,
        Config                           $priceConfig,
        GetTabProducts                   $getTabProducts,
        ProductTabsConfigInterface       $tabsConfig
    ) {
        $this->statusConfig = $statusConfig;
        $this->tabsSource = $tabsSource;
        $this->imageHelper = $imageHelper;
        $this->priceCurrency = $priceCurrency;
        $this->getMsrpPrice = $getMsrpPrice;
        $this->getFinalPriceWithTax = $getFinalPriceWithTax;
        $this->getAvailabilityInfoData = $getAvailabilityInfoData;
        $this->priceConfig = $priceConfig;
        $this->getTabsProducts = $getTabProducts;
        $this->tabsConfig = $tabsConfig;
    }

    /**
     * @inheritDoc
     */
    public function getProductData(ProductInterface $product, string $tabCode, int $currentPage): array
    {
        $result = [];
        if (!$this->shouldBeProcessed($tabCode)) {
            return $result;
        }

        $productsList = $this->getFilteredProductCollection($product, $tabCode, $currentPage);

        foreach ($productsList as $listItem) {
            $imageUrl = $this->imageHelper->init($listItem, 'product_thumbnail_image')->getUrl();
            $availabilityInfo = $this->getAvailabilityInfoData->execute($listItem);
            $result['products'][] = [
                'entity_id'                  => $listItem->getId(),
                'name'                       => $listItem->getName(),
                'sku'                        => $listItem->getSku(),
                'short_description'          => $listItem->getShortDescription() ?? '',
                'url_key'                    => $listItem->getUrlKey(),
                'url_path'                   => $listItem->getProductUrl(),
                'final_price'                => $this->priceCurrency->convertAndFormat($listItem->getFinalPrice()),
                'regular_price'              => $this->priceCurrency->convertAndFormat($listItem->getPrice()),
                'qty'                        => $listItem->getQty() * 1 ?: 1,
                'is_salable'                 => $listItem->isSalable(),
                'stock_status'               => (bool)$listItem->getData('is_salable'),
                'stock_qty'                  => $listItem->getStockQty(),
                'image_url'                  => $imageUrl,
                'msrp_price'                 => $this->getMsrpPrice($listItem),
                'final_price_incl_tax'       => $this->getFinalPriceInclTax($listItem),
                'availability'               => $availabilityInfo[Info::KEY_AVAILABILITY_MESSAGE] ?? '',
                'tooltip_text'               => $availabilityInfo[Info::KEY_TOOLTIP_TEXT] ?? '',
                'allow_check_availability'   => $availabilityInfo[Info::KEY_CAN_SHOW_CHECK_AVAILABILITY_MODAL] ?? false,
                'message_type'               => $availabilityInfo[Info::KEY_MESSAGE_TYPE] ?? '',
                'visible_in_site_visibility' => $listItem->isVisibleInSiteVisibility(),
            ];
        }
        $result['totalSize'] = $this->getProductCount($product, $tabCode);

        return $result;
    }

    /**
     * @inheritDoc
     */
    public function getProductCount(ProductInterface $product, string $tabCode): int
    {
        return count($this->getTabProductsIds($product, $tabCode));
    }

    /**
     * @param ProductInterface $product
     * @param string           $tabCode
     * @param int|null         $currentPage
     *
     * @return AbstractCollection
     */
    protected function getFilteredProductCollection(
        ProductInterface $product,
        string           $tabCode,
        ?int              $currentPage
    ): AbstractCollection {
        $collection = $this->getProductsCollection($product, $tabCode);
        $productIds = $this->getTabProductsIds($product, $tabCode);

        if (empty($productIds)) {
            $collection->getSelect()->where('NULL');

            return $collection;
        }

        $collection->addIdFilter($productIds);

        if (!empty($currentPage)) {
            $pageSize = $this->tabsConfig->getPageSize();
            $collection->setPageSize($pageSize);
            $collection->setCurPage($currentPage);
        }

        return $collection;
    }

    /**
     * @param ProductInterface $product
     * @param string           $tabCode
     *
     * @return array
     */
    private function getTabProductsIds(ProductInterface $product, string $tabCode): array
    {
        $products = $this->getTabsProducts->getProducts((int)$product->getId(), $tabCode);
        if (!empty($products)) {
            $doc = reset($products);
            $productIds = $doc['products'] ?? [];
        }

        return $productIds ?? [];
    }

    /**
     * @param $tabCode
     *
     * @return bool
     */
    private function shouldBeProcessed($tabCode): bool
    {
        return $this->statusConfig->isEnabled() && \array_key_exists($tabCode, $this->tabsSource->toArray());
    }

    /**
     * Get msrp price if allowed
     *
     * @param SaleableInterface $product
     *
     * @return string
     */
    private function getMsrpPrice(SaleableInterface $product): string
    {
        return $this->priceConfig->canShowMsrpPrice($product)
            ? $this->getMsrpPrice->execute($product)
            : '';
    }

    /**
     * Get final price with tax if allowed
     *
     * @param SaleableInterface $product
     *
     * @return string
     */
    private function getFinalPriceInclTax(SaleableInterface $product): string
    {
        return $this->priceConfig->canShowFinalPriceInclTax($product)
            ? $this->getFinalPriceWithTax->execute($product)
            : '';
    }

    /**
     * @param ProductInterface $product
     * @param string           $tabCode
     *
     * @return AbstractCollection
     */
    abstract protected function getProductsCollection(ProductInterface $product, string $tabCode): AbstractCollection;
}
