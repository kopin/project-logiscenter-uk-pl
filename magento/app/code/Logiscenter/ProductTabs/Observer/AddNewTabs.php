<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ProductTabs\Observer;

use Logiscenter\ProductTabs\Model\TabConfig;
use Logiscenter\ProductTabsConfig\Api\Config\ProductTabsModuleStatusInterface;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Block\Product\View;
use Magento\Catalog\Helper\Data;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Layout;
use Logiscenter\ProductTabs\Api\Service\Tabs\ProcessorInterface;

/**
 * Class AddNewTabs
 */
class AddNewTabs implements ObserverInterface
{
    /**
     * @var string PARENT_BlOCK_NAME
     */
    const PARENT_BLOCK_NAME = 'product.info.details';

    /**
     * @var string RENDERING_TEMPLATE
     */
    const RENDERING_TEMPLATE = 'Logiscenter_ProductTabs::tab-renderer.phtml';

    /**
     * @var TabConfig $tabs
     */
    private $tabs;

    /**
     * @var Http
     */
    private $request;

    /**
     * @var ProcessorInterface
     */
    private $tabInfoInterface;

    /**
     * @var Data
     */
    private $productHelper;

    /**
     * @var ProductTabsModuleStatusInterface
     */
    private $configStatus;

    /**
     * AddNewTabs constructor.
     *
     * @param TabConfig                        $tabs
     * @param Http                             $request
     * @param ProcessorInterface               $tabInfoInterface
     * @param Data                             $productHelper
     * @param ProductTabsModuleStatusInterface $configStatus
     */
    public function __construct(
        TabConfig $tabs,
        Http $request,
        ProcessorInterface $tabInfoInterface,
        Data $productHelper,
        ProductTabsModuleStatusInterface $configStatus
    ) {
        $this->tabs = $tabs;
        $this->request = $request;
        $this->tabInfoInterface = $tabInfoInterface;
        $this->productHelper = $productHelper;
        $this->configStatus = $configStatus;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        if (!$this->configStatus->isEnabled()) {
            return;
        }
        /** @var Layout $layout */
        $layout = $observer->getLayout();
        $blocks = $layout->getAllBlocks();

        if ($this->shouldBeProcessed() && key_exists(self::PARENT_BLOCK_NAME, $blocks)) {
            /** @var Template $block */
            $block = $blocks[self::PARENT_BLOCK_NAME];
            $product = $this->getProduct();
            foreach ($this->tabs->getTabs() as $key => $tab) {
                if (!$this->shouldAddTab($product, $key)) {
                    continue;
                }
                $block->addChild(
                    $key,
                    View::class,
                    [
                        'template'    => self::RENDERING_TEMPLATE,
                        'title'       => $tab['title'],
                        'pageSize'    => $tab['data']['pageSize'],
                        'isCustomTab' => true,
                        'jsLayout'    => [
                            $tab,
                        ],
                    ]
                );
            }
        }
    }

    /**
     * @return ProductInterface|null
     */
    private function getProduct(): ?ProductInterface
    {
        return $this->productHelper->getProduct();
    }

    /**
     * @param ProductInterface $product
     * @param string           $tabCode
     *
     * @return bool
     */
    private function shouldAddTab(ProductInterface $product, string $tabCode): bool
    {
        return (bool)$this->tabInfoInterface->resultCount($product, $tabCode);
    }

    /**
     * @return bool
     */
    private function shouldBeProcessed(): bool
    {
        $product = $this->getProduct();
        $fullAction = $this->request->getFullActionName();

        return $product && $product->getTypeId() === 'grouped' && $fullAction === 'catalog_product_view';
    }
}
