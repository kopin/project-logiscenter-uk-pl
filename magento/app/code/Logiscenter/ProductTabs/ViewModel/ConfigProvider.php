<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ProductTabs\ViewModel;

use Logiscenter\ProductTabsConfig\Api\Config\ProductTabsConfigInterface;
use Magento\Framework\View\Element\Block\ArgumentInterface;

/**
 * Class to provide tabs configurations
 */
class ConfigProvider implements ArgumentInterface
{
    /**
     * @var ProductTabsConfigInterface
     */
    private $tabsConfig;

    /**
     * @param ProductTabsConfigInterface $tabsConfig
     */
    public function __construct(ProductTabsConfigInterface $tabsConfig)
    {
        $this->tabsConfig = $tabsConfig;
    }

    /**
     * @return int
     */
    public function getPageSize(): int
    {
        return (int)$this->tabsConfig->getPageSize();
    }
}
