/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

var config = {
    config: {
        mixins: {
            'mage/collapsible': {
                'Logiscenter_ProductTabs/js/mage/collapsible-mixin': true // Removed focus collapsible on dimensionsChanged
            }
        }
    },
    map: {
        '*': {
            productTabs: 'Logiscenter_ProductTabs/js/tabs',
            productTabsStorage: 'Logiscenter_ProductTabs/js/storage'
        }
    }
};
