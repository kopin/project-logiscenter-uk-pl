/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
define([
    'jquery',
    'underscore',
    'ko',
    'mageUtils',
    'jquery/jquery-storageapi'
], function ($, _, ko, utils) {
    'use strict';

    /**
     * Set data to localStorage with support check.
     *
     * @param {String} namespace
     * @param {Object} data
     */
    function setLocalStorageItem(namespace, data) {
        try {
            window.localStorage.setItem(namespace, JSON.stringify(data));
        } catch (e) {
            console.warn('localStorage is unavailable - skipping local caching of product data');
            console.error(e);
        }
    }

    return {

        /**
         * Class name
         */
        name: 'TabsStorage',

        namespace: 'product_data_storage',

        /**
         * Initializes class
         *
         * @return Chainable.
         */
        initialize: function () {
            this.initLocalStorage();
            return this;
        },

        /**
         * Initialize localStorage
         *
         * @return Chainable.
         */
        initLocalStorage: function () {
            this.localStorage = window.localStorage.getItem(this.namespace);
            return this;
        },

        /**
         *
         */
        addDataHandlerFiltered: function (productId, alias, data) {
            let storage = window.localStorage.getItem(this.namespace),
                object = JSON.parse(storage),
                product = null,
                tabs = null,
                aux = {};

            if(object.hasOwnProperty(productId)) {
                product = object[productId];
                tabs = JSON.parse(product['extension_attributes']['tabs']);
                _.each(tabs, function (tab, key) {
                    aux = tab;
                    if (tab.hasOwnProperty(alias)) {
                        aux[alias] = data;
                        tabs[key] = aux;
                    }
                });

                product['extension_attributes']['tabs'] = JSON.stringify(tabs);
                object[productId] = product;
            }

            this.internalDataHandler(object);
        },

        /**
         *
         */
        getDataHandlerFiltered: function (productId, alias) {
            let storage = window.localStorage.getItem(this.namespace),
                object = JSON.parse(storage),
                product = null,
                tabs = null,
                data = [];

            if(object.hasOwnProperty(productId)) {
                product = object[productId];
                tabs = JSON.parse(product['extension_attributes']['tabs']);
                _.each(tabs, function (tab, key) {
                    if (tab.hasOwnProperty(alias)) {
                        data = tab[alias];
                    }
                });
            }
            return data;
        },

        /**
         * Initializes handler to "data" property update
         */
        internalDataHandler: function (data) {
            setLocalStorageItem(this.namespace, data);
        }
    };
});

