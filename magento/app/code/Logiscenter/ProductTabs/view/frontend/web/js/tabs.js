/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
define([
    'underscore',
    'jquery',
    'jquery/ui',
    'productTabsStorage',
    'text!Logiscenter_ProductTabs/template/product-table.html',
    'text!Logiscenter_ProductTabs/template/product-item.html',
    'text!Logiscenter_ProductTabs/template/product-pager.html',
    'text!Logiscenter_ProductTabs/template/product-not-found.html',
    'mage/template',
    'mage/url',
    'mage/translate',
    'mage/tabs'
], function (_, $, jqueryui, storage, tableTemplate, itemTemplate, pagerTemplate, noProductsTemplate, template, urlBuilder, $t) {
    'use strict';

    /**
     * Recursively check for set localStorage value every 100ms.
     *
     * @param {String} key
     */
    function waitForLocalStorage(key, productId, cb, timer) {
        if (!localStorage.getItem(key)
            || (localStorage.getItem(key) && $.isEmptyObject(JSON.parse(localStorage.getItem(key))))
            || (localStorage.getItem(key) && !JSON.parse(localStorage.getItem(key)).hasOwnProperty(productId)))
            return (timer = setTimeout(waitForLocalStorage.bind(null, key, productId, cb), 100))
        clearTimeout(timer)
        if (typeof cb !== 'function') return localStorage.getItem(key)
        return cb(localStorage.getItem(key))
    }

    $.widget('logiscenter_product_tabs.tabs', $.mage.tabs, {
        options: {
            tabTitleSelector: '[data-container="tabs-title"]',
            tabContentSelector: '[data-container="tabs-content"]',
            loadMoreSelector: '[data-event="load-more"]',
            addToCartSelector: '[data-event="add-to-cart-associated-product"]',
            defaultPageSize: 20
        },

        /**
         * @private
         */
        _create: function () {
            this._super();

            let productId = $('.product-add-form').find('input[name="product"]').val()

            waitForLocalStorage(storage.namespace, productId, function () {
                storage.initialize();
                this._bindCustomEvents();
                this._preloadTabIndex();
            }.bind(this));
        },

        /**
         * This method is used to bind events associated with this widget.
         */
        _bindCustomEvents: function () {
            this._on(this.element.find(this.options.tabTitleSelector), {
                'click': '_selectTab'
            });

            // Attach a delegated event handler because these elements are added by ajax
            $(this.options.tabContentSelector).on('click', this.options.loadMoreSelector, function( event ) {
                this._loadMore(event);
            }.bind(this));

            // Attach a delegated event handler because these elements are added by ajax
            $(this.options.tabContentSelector).on('click', this.options.addToCartSelector, function( event ) {
                this._addToCart(event);
            }.bind(this));
        },


        /**
         * Preload the collection for the first tab active
         */
        _preloadTabIndex: function () {
            let activeTab = this.collapsibles[this.options.active];
            $(activeTab).trigger('click');
        },

        /**
         * Event called when the customer selects a custom tab.
         *
         * @param {EventObject} event - Event occurring.
         */
        _selectTab: function (event) {
            let tab = event.target,
                alias = $(tab).closest(this.options.tabTitleSelector).data('alias');

            if (alias && $(tab).closest(this.options.tabTitleSelector).hasClass('is-custom-tab')) {
                // Get data collection and process it for this tab. Only first time.
                if (!this._isTabInitialized(alias)) {
                    this._getDataCollection();
                }
            }
        },

        /**
         * Load more products at the content table and refresh pagination
         *
         * @param {EventObject} event - Event occurring.
         */
        _loadMore: function (event) {
            let alias = $(this.options.tabTitleSelector).closest('.active').data('alias'), // Active tab
                button = event.target,
                productId = $('.product-add-form').find('input[name="product"]').val(),
                serviceUrl = urlBuilder.build('product_tabs/products/get', {}),
                actualProductSize = $(this.options.tabContentSelector).closest('[data-alias="'+alias+'"]').find('.grouped__content-product').length,
                currentPage = Math.ceil(actualProductSize/this.options.limit) + 1;

            if (productId && serviceUrl) {
                $.ajax({
                    url: serviceUrl,
                    data: {
                        id: productId,
                        tab_code: alias,
                        current_page: currentPage
                    },
                    dataType: 'json',
                    cache: true,
                    beforeSend: function () {
                        $(button).text($t('Loading...'));
                        $(button).prop("disabled", true);
                        $(this.options.tabContentSelector).closest('[data-alias="'+alias+'"]').find('.table-wrapper.grouped').addClass('is-loading');
                    }.bind(this),
                    /** @inheritdoc */
                    fail: function (xhr, textStatus, errorThrown) {
                        console.error('Some error happened getting custom tab collections.');
                    },
                    success: function (response) {
                        this.totalSize = response.data.totalSize
                        storage.addDataHandlerFiltered(productId, alias, response.data);
                        this._processDataCollection();
                    }.bind(this),
                    complete: function () {
                        $(this.options.tabContentSelector).closest('[data-alias="'+alias+'"]').find('.table-wrapper.grouped').removeClass('is-loading');
                    }.bind(this)
                });
            }
        },

        /**
         * Add to cart associated products
         *
         * @param {EventObject} event - Event occurring.
         */
        _addToCart: function (event) {
            let button = event.target;
        },

        /**
         * Get data collection of a custom tab doing an ajax call.
         */
        _getDataCollection: function () {
            let alias = $(this.options.tabTitleSelector).closest('.active').data('alias'), // Active tab
                productId = $('.product-add-form').find('input[name="product"]').val(),
                serviceUrl = urlBuilder.build('product_tabs/products/get', {});
            // let serviceUrl = urlBuilder.build('media/dummy.json');

            if (productId && serviceUrl) {
                $.ajax({
                    url: serviceUrl,
                    data: {
                        id: productId,
                        tab_code: alias
                    },
                    dataType: 'json',
                    cache: true,

                    beforeSend: function () {
                        $(this.options.tabContentSelector).closest('[data-alias="'+alias+'"]').find('.table-wrapper.grouped').addClass('is-loading');
                        // Disable another tabs not active
                        $(this.options.tabTitleSelector).not('.active').addClass('is-disabled');
                    }.bind(this),

                    /** @inheritdoc */
                    fail: function (xhr, textStatus, errorThrown) {
                        console.error('Some error happened getting custom tab collections.');
                    },

                    /** @inheritdoc */
                    success: function (response) {
                        if (typeof response === 'object') {
                            try {
                                if (response.status == 'ok' && response.hasOwnProperty('data')) {
                                    if(response.data.products.length > 0){
                                        storage.addDataHandlerFiltered(productId, alias, response.data);
                                        this._processDataCollection();
                                    } else {
                                        this._appendNoProductsFound();
                                    }
                                }
                            } catch (e) {
                                console.error('Some error happened getting custom tab collections.');
                            }
                        }
                    }.bind(this),

                    complete: function () {
                        $(this.options.tabContentSelector).closest('[data-alias="'+alias+'"]').find('.table-wrapper.grouped').removeClass('is-loading');
                        // Mark tab as initialized if the append data is succesfully.
                        if (!this._isTabInitialized(alias)) {
                            $(this.options.tabContentSelector).closest('[data-alias="'+alias+'"]').addClass('is-initialized');
                        }
                        // Enable another tabs not active
                        $(this.options.tabTitleSelector).not('.active').removeClass('is-disabled');
                    }.bind(this)
                });
            }
        },

        /**
         * Proccess data collection of a custom tab, inserting the container, products items or not found block.
         */
        _processDataCollection: function () {
            let alias = $(this.options.tabTitleSelector).closest('.active').data('alias'), // Active tab
                productId = $('.product-add-form').find('input[name="product"]').val(),
                products = storage.getDataHandlerFiltered(productId, alias);

            if (products) {
                this._appendDataCollection(products);
            }
        },

        /**
         * Checks if a custom tab is initialized or not.
         *
         * @param {string} alias - Custom tab alias.
         * @returns {boolean} true if the variable is initialized.
         */
        _isTabInitialized: function (alias) {
            return $(this.options.tabContentSelector).closest('[data-alias="'+alias+'"]').hasClass('is-initialized');
        },

        /**
         * Append data collection of a custom tab.
         * Add the product container and products items.
         *
         * @param {array} data - response data.
         */
        _appendDataCollection: function (data) {
            let products = data.products;
            let alias = $(this.options.tabTitleSelector).closest('.active').data('alias'), // Active tab
                productSize = products.length,
                actualProductSize = $(this.options.tabContentSelector).closest('[data-alias="'+alias+'"]').find('.grouped__content-product').length;

            // Add the table products only the first load as container
            if (!this._isTabInitialized(alias)) {
                this._renderTableProducts(products);
            }

            // Render different product item founded
            _.each(products, function (product) {
                this._renderProductItem(product);
            }.bind(this));

            // Render pager
            this._renderPager(data);
        },

        /**
         * Append block of a custom tab when no product has found.
         */
        _appendNoProductsFound: function () {
            let alias = $(this.options.tabTitleSelector).closest('.active').data('alias'), // Active tab
                templateData,
                rendererComponent;

            templateData = {
                labels: {
                    noProducts: $t('No options of this product are available.')
                }
            };

            rendererComponent = template(noProductsTemplate, templateData);
            $(this.options.tabContentSelector)
                .closest('[data-alias="'+alias+'"]')
                .find('.table-wrapper.grouped')
                .append(rendererComponent);
        },

        /**
         * Render table products container.
         *
         * @param {array} products - Products to append.
         */
        _renderTableProducts: function (products) {
            let alias = $(this.options.tabTitleSelector).closest('.active').data('alias'), // Active tab
                templateData,
                rendererComponent;

            templateData = {
                products: products
            };

            rendererComponent = template(tableTemplate, templateData);
            $(this.options.tabContentSelector)
                .closest('[data-alias="'+alias+'"]')
                .find('.table-wrapper.grouped .table.data')
                .append(rendererComponent);
        },

        /**
         * Render product item.
         *
         * @param {array} product - Product to append.
         */
        _renderProductItem: function (product) {
            let alias = $(this.options.tabTitleSelector).closest('.active').data('alias'), // Active tab
                templateData,
                rendererComponent,
                actualWords,
                numMaxWords = 30;

            if (product.short_description) {
                product.original_short_description = product.short_description.replace(/(<([^>]+)>)/gi, ""); // Without HTML tags
                actualWords = product.original_short_description.split(' ').slice(0).length;
                if (actualWords > numMaxWords) {
                    product.edited_short_description = product.original_short_description.split(' ').slice(0, numMaxWords).join(' ') + '...';
                }
            }

            templateData = {
                product: product,
                labels: {
                    image: $t('Image'),
                    name: $t('Product Name'),
                    sku: $t('Part Number'),
                    description: $t('Description'),
                    info: $t('Info'),
                    price: $t('Price'),
                    quantity: $t('Quantity'),
                    actions: $t('Actions'),
                    add: $t('Add'),
                    specialPrice: $t('Special Price'),
                    regularPrice: $t('Regular Price'),
                    msrpPrice: $t('MSRP'),
                    finalPriceInclTax: $t('VAT incl.'),
                    readMore: $t('Read more'),
                    readLess: $t('Read less')
                }
            };

            rendererComponent = template(itemTemplate, templateData);
            $(this.options.tabContentSelector)
                .closest('[data-alias="'+alias+'"]')
                .find('.table-wrapper.grouped .table.data .grouped__content')
                .append(rendererComponent);
        },

        /**
         * Render pager.
         *
         * @param {array} products - Products to append.
         */
        _renderPager: function (data) {
            let size = data.totalSize,
                products = data.products,
                alias = $(this.options.tabTitleSelector).closest('.active').data('alias'), // Active tab
                templateData,
                rendererComponent,
                actualProductSize = $(this.options.tabContentSelector).closest('[data-alias="'+alias+'"]').find('.grouped__content-product').length;

            templateData = {
                showButton: (products.length >= this.options.limit), // TODO Need data from backend to show/hide button. If collection size == limit : not show button
                labels: {
                    viewedProducts: $t('You\'ve viewed %1 of %2 products.')
                        .replace('%1', actualProductSize)
                        .replace('%2', size),
                    showMore: $t('Show more')
                }
            };

            $(this.options.tabContentSelector).closest('[data-alias="'+alias+'"]').find('.models__pager').remove();

            rendererComponent = template(pagerTemplate, templateData);
            $(this.options.tabContentSelector)
                .closest('[data-alias="'+alias+'"]')
                .find('.models__wrapper')
                .append(rendererComponent);
        },

        // /**
        //  * Get all custom tab alias
        //  *
        //  * @returns {array}
        //  */
        // _getKeysAlias: function () {
        //     let alias = [];
        //     _.each($(this.options.tabTitleSelector).closest('.is-custom-tab'), function (item) {
        //         alias.push(item.dataset.alias);
        //     });
        //     return alias;
        // },
        //
        // /**
        //  * Adds all custom tab alias to the local storage initially empty.
        //  */
        // _insertInitialTabs: function () {
        //     let result = {}, alias = this._getKeysAlias();
        //     _.each(alias, function (item) {
        //         result[item] = {};
        //     });
        //     storage.internalDataHandler(result);
        // }
    });

    return $.logiscenter_product_tabs.tabs;
});
