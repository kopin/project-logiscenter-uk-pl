<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ProductTabs\Api\Service\Tabs;

use Magento\Catalog\Api\Data\ProductInterface;

/**
 * Class TabInfoInterface
 */
interface ProcessorInterface
{
    /**
     * @param ProductInterface $product
     * @param string           $tabCode
     *
     * @return array
     */
    public function execute(ProductInterface $product, string $tabCode, int $currentPage): array;

    /**
     * @param ProductInterface $product
     * @param string           $tabCode
     *
     * @return array
     */
    public function resultCount(ProductInterface $product, string $tabCode): int;
}
