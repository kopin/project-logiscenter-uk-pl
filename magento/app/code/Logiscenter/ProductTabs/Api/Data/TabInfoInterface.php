<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ProductTabs\Api\Data;

use Magento\Catalog\Api\Data\ProductInterface;

/**
 * Class TabInfoInterface
 */
interface TabInfoInterface
{

    /**
     * @param ProductInterface $product
     * @param string           $tabCode
     *
     * @return array
     */
    public function getProductData(ProductInterface $product, string $tabCode, int $currentPage): array;

    /**
     * @param ProductInterface $product
     * @param string           $tabCode
     *
     * @return int
     */
    public function getProductCount(ProductInterface $product, string $tabCode): int;
}
