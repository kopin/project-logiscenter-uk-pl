<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ProductTabs\Controller\Products;

use Logiscenter\ProductTabs\Api\Service\Tabs\ProcessorInterface;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Logiscenter\ProductTabsConfig\Api\Config\ProductTabsModuleStatusInterface;

/**
 * Controller to handle products tabs requests
 */
class Get extends Action implements HttpGetActionInterface
{
    /**
     * @var JsonFactory
     */
    private $resultJsonFactory;

    /**
     * @var ProcessorInterface
     */
    private $tabInfoInterface;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var ProductTabsModuleStatusInterface
     */
    private $configStatus;

    /**
     * Get constructor.
     *
     * @param Context                          $context
     * @param JsonFactory                      $resultJsonFactory
     * @param ProcessorInterface               $tabInfoInterface
     * @param ProductRepositoryInterface       $productRepository
     * @param ProductTabsModuleStatusInterface $configStatus
     */
    public function __construct(
        Context $context,
        JsonFactory $resultJsonFactory,
        ProcessorInterface $tabInfoInterface,
        ProductRepositoryInterface $productRepository,
        ProductTabsModuleStatusInterface $configStatus
    ) {
        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
        $this->tabInfoInterface = $tabInfoInterface;
        $this->productRepository = $productRepository;
        $this->configStatus = $configStatus;
    }

    /**
     * @inheritDoc
     */
    public function execute()
    {
        if (!$this->canExecute()) {
            return null;
        }
        $result = [
            'status' => 'ko',
            'data'   => [],
        ];

        $productId = (int)$this->getRequest()->getParam('id', 0);
        $tabCode = $this->getRequest()->getParam('tab_code', '');
        $currentPage = $this->getRequest()->getParam('current_page', 1);
        if ($productId && $tabCode) {
            try {
                $product = $this->productRepository->getById($productId);
            } catch (NoSuchEntityException $e) {
                return $this->resultJsonFactory->create()->setData($result);
            }
            $productData = $this->tabInfoInterface->execute($product, $tabCode, (int)$currentPage);
            $result = [
                'status' => 'ok',
                'data'   => $productData,
            ];
        }

        return $this->resultJsonFactory->create()->setData($result);
    }

    /**
     * @return bool
     */
    private function canExecute(): bool
    {
        return $this->configStatus->isEnabled() && $this->getRequest()->isGet() && $this->getRequest()->isAjax();
    }
}
