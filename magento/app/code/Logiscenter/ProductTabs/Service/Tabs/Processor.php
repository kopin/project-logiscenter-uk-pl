<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ProductTabs\Service\Tabs;

use Logiscenter\ProductTabs\Api\Data\TabInfoInterface;
use Logiscenter\ProductTabs\Api\Service\Tabs\ProcessorInterface;
use Magento\Catalog\Api\Data\ProductInterface;
use Logiscenter\ProductTabsConfig\Api\Config\ProductTabsModuleStatusInterface;

/**
 * Class tabs processor
 */
class Processor implements ProcessorInterface
{
    /**
     * @var TabInfoInterface[]
     */
    private $tabInfoList;

    /**
     * @var ProductTabsModuleStatusInterface
     */
    private $configStatus;

    /**
     * Processor constructor.
     *
     * @param array                            $tabInfoList
     * @param ProductTabsModuleStatusInterface $configStatus
     */
    public function __construct(
        array $tabInfoList,
        ProductTabsModuleStatusInterface $configStatus
    ) {
        $this->tabInfoList = $tabInfoList;
        $this->configStatus = $configStatus;
    }

    /**
     * @inheritDoc
     */
    public function execute(
        ProductInterface $product,
        string $tabCode,
        int $currentPage = 1
    ): array {
        $result = [];
        if (!$this->configStatus->isEnabled() || !$product->getId()) {
            return $result;
        }
        foreach ($this->tabInfoList as $tabInfo) {
            // phpcs:ignore Magento2.Performance.ForeachArrayMerge.ForeachArrayMerge
            $result = array_merge($result, $tabInfo->getProductData($product, $tabCode, $currentPage));
        }

        return $result;
    }

    /**
     * @inheritDoc
     */
    public function resultCount(
        ProductInterface $product,
        string $tabCode
    ): int {
        $result = 0;
        if (!$this->configStatus->isEnabled() || !$product->getId()) {
            return $result;
        }
        foreach ($this->tabInfoList as $tabInfo) {
            $result += $tabInfo->getProductCount($product, $tabCode);
        }
        return $result;
    }
}
