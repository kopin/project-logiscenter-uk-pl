<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ProductTabs\Ui\DataProvider\Product\Listing\Collector;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\Data\ProductRenderExtensionFactory;
use Magento\Catalog\Api\Data\ProductRenderInterface;
use Magento\Catalog\Ui\DataProvider\Product\ProductRenderCollectorInterface;
use Magento\Framework\Json\Helper\Data as JsonHelper;
use Logiscenter\ProductTabs\Model\TabConfig;

/**
 * Collect information needed to render product tabs on front
 */
class Tabs implements ProductRenderCollectorInterface
{
    const KEY = "tabs";

    /**
     * @var ProductRenderExtensionInterfaceFactory
     */
    private $productRenderExtensionFactory;

    /**
     * @var TabConfig
     */
    private $tabs;

    /**
     * @var JsonHelper
     */
    private $jsonHelper;

    /**
     * @param ProductRenderExtensionFactory $productRenderExtensionFactory
     * @param TabConfig $tabs
     * @param JsonHelper $jsonHelper
     */
    public function __construct(
        ProductRenderExtensionFactory $productRenderExtensionFactory,
        TabConfig $tabs,
        JsonHelper $jsonHelper
    ) {
        $this->productRenderExtensionFactory = $productRenderExtensionFactory;
        $this->tabs = $tabs;
        $this->jsonHelper = $jsonHelper;
    }

    /**
     * @inheritdoc
     */
    public function collect(ProductInterface $product, ProductRenderInterface $productRender)
    {
        /** @var ProductRenderExtensionInterface $extensionAttributes */
        $extensionAttributes = $productRender->getExtensionAttributes();

        if (!$extensionAttributes) {
            $extensionAttributes = $this->productRenderExtensionFactory->create();
        }

        $tabsFromSources = $this->tabs->getTabsFromSources();

        if ($tabsFromSources) {
            $existingTabs = [array_map(function () {
                return '';
            }, $tabsFromSources)];

            $extensionAttributes->setTabs($this->jsonHelper->jsonEncode($existingTabs));
        }

        $productRender->setExtensionAttributes($extensionAttributes);
    }
}
