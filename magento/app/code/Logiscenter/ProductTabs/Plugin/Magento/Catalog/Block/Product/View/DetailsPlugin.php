<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ProductTabs\Plugin\Magento\Catalog\Block\Product\View;

use Logiscenter\ProductTabs\Model\TabConfig;
use Magento\Catalog\Block\Product\View\Details as MagentoDetails;
use Logiscenter\ProductTabsConfig\Api\Config\ProductTabsModuleStatusInterface;

/**
 * Class DetailsPlugin
 */
class DetailsPlugin
{
    /**
     * @var TabConfig $tabs
     */
    private $tabs;

    /**
     * @var ProductTabsModuleStatusInterface
     */
    private $configStatus;

    /**
     * DetailsPlugin constructor.
     *
     * @param TabConfig                        $tabs
     * @param ProductTabsModuleStatusInterface $configStatus
     */
    public function __construct(
        TabConfig $tabs,
        ProductTabsModuleStatusInterface $configStatus
    ) {
        $this->tabs = $tabs;
        $this->configStatus = $configStatus;
    }

    /**
     * @param MagentoDetails $subject
     * @param array          $result
     *
     * @return array
     */
    public function afterGetGroupSortedChildNames(
        MagentoDetails $subject,
        array $result
    ): array {
        if ($this->configStatus->isEnabled() && !empty($this->tabs->getTabs())) {
            $partialResult = [];
            $childNames = $subject->getChildNames();
            foreach ($this->tabs->getTabs() as $key => $tab) {
                $childName = 'product.info.details.' . $key;
                if (!in_array($childName, $childNames)) {
                    continue;
                }
                $sortOrder = $tab['data']['sortOrder'];
                $partialResult[$sortOrder] = $childName;
            }
            ksort($partialResult);
            $result = array_merge($partialResult, $result);
        }

        return $result;
    }
}
