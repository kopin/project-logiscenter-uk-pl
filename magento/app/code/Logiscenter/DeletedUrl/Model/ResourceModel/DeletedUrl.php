<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\DeletedUrl\Model\ResourceModel;

use Logiscenter\DeletedUrl\Api\Data\DeletedUrlInterface;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Deleted urls resource model class
 */
class DeletedUrl extends AbstractDb
{
    /**
     * @inheritDoc
     */
    // @codingStandardsIgnoreStart
    protected function _construct()
    {
        // @codingStandardsIgnoreEnd
        $this->_init(DeletedUrlInterface::TABLE_NAME, DeletedUrlInterface::FIELD_URL_ID);
    }
}
