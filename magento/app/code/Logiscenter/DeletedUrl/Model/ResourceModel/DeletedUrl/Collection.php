<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\DeletedUrl\Model\ResourceModel\DeletedUrl;

use Logiscenter\DeletedUrl\Model\DeletedUrl;
use Logiscenter\DeletedUrl\Model\ResourceModel\DeletedUrl as DeletedUrlResource;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Delete urls collection class
 */
class Collection extends AbstractCollection
{
    /**
     * @inheritDoc
     *
     */
    // @codingStandardsIgnoreStart
    public function _construct()
    {
        // @codingStandardsIgnoreEnd
        $this->_init(DeletedUrl::class, DeletedUrlResource::class);
    }
}
