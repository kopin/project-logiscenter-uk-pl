<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\DeletedUrl\Model;

use Logiscenter\DeletedUrl\Api\Data\DeletedUrlInterface;
use Logiscenter\DeletedUrl\Api\DeletedUrlFinderInterface;
use Logiscenter\DeletedUrl\Model\ResourceModel\DeletedUrl as DeletedUrlResource;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\DB\Select;

/**
 * Deleted urls finder model class
 */
class DeletedUrlFinder implements DeletedUrlFinderInterface
{
    /**
     * @var DeletedUrlResource
     */
    private $deletedUrlResource;

    /**
     * @var AdapterInterface
     */
    private $connection;

    /**
     * @param DeletedUrlResource $deletedUrlResource
     */
    public function __construct(
        DeletedUrlResource $deletedUrlResource
    ) {
        $this->deletedUrlResource = $deletedUrlResource;
        $this->connection = $deletedUrlResource->getConnection();
    }

    /**
     * @inheritDoc
     */
    public function findOneByData(array $data)
    {
        if (array_key_exists(DeletedUrlInterface::FIELD_URL, $data) &&
            is_string($data[DeletedUrlInterface::FIELD_URL])
        ) {
            $requestPath = $data[DeletedUrlInterface::FIELD_URL];
            $decodedRequestPath = urldecode($requestPath);
            $data[DeletedUrlInterface::FIELD_URL] = array_unique(
                [
                    rtrim($requestPath, '/'),
                    rtrim($requestPath, '/') . '/',
                    rtrim($decodedRequestPath, '/'),
                    rtrim($decodedRequestPath, '/') . '/',
                ]
            );
        }

        return $this->connection->fetchRow($this->prepareSelect($data));
    }

    /**
     * Prepare select statement for specific filter
     *
     * @param array $data
     *
     * @return Select
     */
    private function prepareSelect(array $data): Select
    {
        $select = $this->connection->select();
        $select->from($this->deletedUrlResource->getMainTable());

        foreach ($data as $column => $value) {
            $select->where($this->connection->quoteIdentifier($column) . ' IN (?)', $value);
        }

        return $select;
    }
}
