<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\DeletedUrl\Model;

use LogicException;
use Logiscenter\DeletedUrl\Api\Data\DeletedUrlInterface;
use Logiscenter\DeletedUrl\Api\DeletedUrlPersistInterface;
use Logiscenter\DeletedUrl\Model\ResourceModel\DeletedUrl as DeletedUrlResource;
use Magento\UrlRewrite\Service\V1\Data\UrlRewrite;

/**
 * Deleted URL persist interface
 */
class DeletedUrlPersist implements DeletedUrlPersistInterface
{
    /**
     * @var DeletedUrlResource
     */
    private $resource;

    /**
     * @param DeletedUrlResource $resource
     */
    public function __construct(DeletedUrlResource $resource)
    {
        $this->resource = $resource;
    }

    /**
     * @inheritDoc
     */
    public function add(array $urls): void
    {
        if (empty($urls)) {
            return;
        }
        $data = [];
        foreach ($urls as $url) {
            if (!$url instanceof UrlRewrite) {
                throw new LogicException('Wrong data specified for store deleted urls');
            }
            $url = $url->toArray();
            $data[] = [
                DeletedUrlInterface::FIELD_ENTITY_TYPE => $url[UrlRewrite::ENTITY_TYPE],
                DeletedUrlInterface::FIELD_ENTITY_ID   => $url[UrlRewrite::ENTITY_ID],
                DeletedUrlInterface::FIELD_URL         => $url[UrlRewrite::REQUEST_PATH],
                DeletedUrlInterface::FIELD_STORE_ID    => $url[UrlRewrite::STORE_ID],
            ];
        }
        $this->resource->getConnection()->insertOnDuplicate($this->resource->getMainTable(), $data);
    }
}
