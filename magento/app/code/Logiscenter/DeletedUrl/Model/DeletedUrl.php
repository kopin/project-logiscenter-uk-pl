<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\DeletedUrl\Model;

use Logiscenter\DeletedUrl\Api\Data\DeletedUrlInterface;
use Magento\Framework\Model\AbstractModel;

/**
 * Deleted url entity model class
 */
class DeletedUrl extends AbstractModel implements DeletedUrlInterface
{
    /**
     * @inheritDoc
     */
    public function getUrlId(): int
    {
        return (int)$this->getData(self::FIELD_URL_ID);
    }

    /**
     * @inheritDoc
     */
    public function setUrlId(int $urlId): DeletedUrlInterface
    {
        return $this->setData(self::FIELD_URL_ID, $urlId);
    }

    /**
     * @inheritDoc
     */
    public function getStoreId(): int
    {
        return (int)$this->getData(self::FIELD_STORE_ID);
    }

    /**
     * @inheritDoc
     */
    public function setStoreId(int $storeId): DeletedUrlInterface
    {
        return $this->setData(self::FIELD_STORE_ID, $storeId);
    }

    /**
     * @inheritDoc
     */
    public function getEntityType(): string
    {
        return (string)$this->getData(self::FIELD_ENTITY_TYPE);
    }

    /**
     * @inheritDoc
     */
    public function setEntityType(string $entityType): DeletedUrlInterface
    {
        return $this->setData(self::FIELD_ENTITY_TYPE, $entityType);
    }

    /**
     * @inheritDoc
     */
    public function getUrl(): string
    {
        return (string)$this->getData(self::FIELD_URL);
    }

    /**
     * @inheritDoc
     */
    public function setUrl(string $url): DeletedUrlInterface
    {
        return $this->setData(self::FIELD_URL, $url);
    }
}
