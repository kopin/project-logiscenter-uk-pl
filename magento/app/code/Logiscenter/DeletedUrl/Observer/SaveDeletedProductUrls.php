<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\DeletedUrl\Observer;

use Logiscenter\DeletedUrl\Service\CopyUrlsToDeletedUrls;
use Logiscenter\DeletedUrlConfig\Api\Config\DeletedUrlModuleStatusInterface;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

/**
 * Save deleted url
 */
class SaveDeletedProductUrls implements ObserverInterface
{
    /**
     * @var CopyUrlsToDeletedUrls
     */
    private $copyUrlsToDeletedUrls;

    /**
     * @var DeletedUrlModuleStatusInterface
     */
    private $statusConfig;

    /**
     * @param CopyUrlsToDeletedUrls           $copyUrlsToDeletedUrls
     * @param DeletedUrlModuleStatusInterface $statusConfig
     */
    public function __construct(
        CopyUrlsToDeletedUrls $copyUrlsToDeletedUrls,
        DeletedUrlModuleStatusInterface $statusConfig
    ) {
        $this->copyUrlsToDeletedUrls = $copyUrlsToDeletedUrls;
        $this->statusConfig = $statusConfig;
    }

    /**
     * @inheritDoc
     */
    public function execute(Observer $observer)
    {
        if (!$this->statusConfig->isEnabled()) {
            return;
        }
        if (!$observer->getEvent()->getObject() instanceof ProductInterface) {
            return;
        }
        /** @var ProductInterface $product */
        $product = $observer->getEvent()->getObject();

        if ($product->getId()) {
            $this->copyUrlsToDeletedUrls->execute((int)$product->getId(), 'product');
        }
    }
}
