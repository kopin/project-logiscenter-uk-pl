<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\DeletedUrl\Controller\Gone;

use Logiscenter\DeletedUrlConfig\Api\Config\DeletedUrlModuleStatusInterface;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\Controller\Result\ForwardFactory;
use Magento\Framework\View\Result\Page;
use Magento\Framework\View\Result\PageFactory;

/**
 * Default no route gone action class
 */
class DefaultGone extends Action implements HttpGetActionInterface
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var DeletedUrlModuleStatusInterface
     */
    private $statusConfig;

    /**
     * @param Context                         $context
     * @param PageFactory                     $resultPageFactory
     * @param DeletedUrlModuleStatusInterface $statusConfig
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        DeletedUrlModuleStatusInterface $statusConfig
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->statusConfig = $statusConfig;
    }

    /**
     * @return Page
     */
    public function execute()
    {
        if (!$this->statusConfig->isEnabled()) {
            $this->_redirect('cms/noroute/');
        }
        $resultLayout = $this->resultPageFactory->create();
        $resultLayout->setStatusHeader(410, '1.1', 'Gone');
        $resultLayout->setHeader('Status', '410 Gone');

        return $resultLayout;
    }
}
