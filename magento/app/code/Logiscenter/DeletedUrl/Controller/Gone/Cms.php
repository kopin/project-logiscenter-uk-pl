<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\DeletedUrl\Controller\Gone;

use Logiscenter\DeletedUrlConfig\Api\Config\DeletedUrlModuleStatusInterface;
use Magento\Cms\Helper\Page;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Controller\Result\ForwardFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Store\Model\ScopeInterface;

/**
 * Cms no route gone action class
 */
class Cms extends Action implements HttpGetActionInterface
{
    /**
     * @var ForwardFactory
     */
    protected $resultForwardFactory;

    /**
     * @var ScopeConfigInterface
     */
    private $config;

    /**
     * @var Page
     */
    private $pageHelper;

    /**
     * @var DeletedUrlModuleStatusInterface
     */
    private $statusConfig;

    /**
     * @param Context                         $context
     * @param ForwardFactory                  $resultForwardFactory
     * @param ScopeConfigInterface            $config
     * @param Page                            $pageHelper
     * @param DeletedUrlModuleStatusInterface $statusConfig
     */
    public function __construct(
        Context $context,
        ForwardFactory $resultForwardFactory,
        ScopeConfigInterface $config,
        Page $pageHelper,
        DeletedUrlModuleStatusInterface $statusConfig
    ) {
        parent::__construct($context);
        $this->resultForwardFactory = $resultForwardFactory;
        $this->config = $config;
        $this->pageHelper = $pageHelper;
        $this->statusConfig = $statusConfig;
    }

    /**
     * Render CMS noroute page with 410 Gone status
     *
     * @return ResultInterface
     */
    public function execute()
    {
        if (!$this->statusConfig->isEnabled()) {
            $this->_redirect('cms/noroute/');
        }
        $pageId = $this->config->getValue(Page::XML_PATH_NO_ROUTE_PAGE, ScopeInterface::SCOPE_STORE);
        $resultPage = $this->pageHelper->prepareResultPage($this, $pageId);
        if ($resultPage) {
            $resultPage->setStatusHeader(410, '1.1', 'Gone');
            $resultPage->setHeader('Status', '410 Gone');

            return $resultPage;
        }
        $resultForward = $this->resultForwardFactory->create();
        $resultForward->forward('defaultGone');

        return $resultForward;
    }
}
