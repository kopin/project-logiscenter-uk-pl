<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\DeletedUrl\Controller;

use Logiscenter\DeletedUrl\Api\DeletedUrlFinderInterface;
use Logiscenter\DeletedUrlConfig\Api\Config\DeletedUrlModuleStatusInterface;
use Magento\Framework\App\ActionFactory;
use Magento\Framework\App\ActionInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\Router\ActionList;
use Magento\Framework\App\RouterInterface;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Deleted URL router class
 */
class Router implements RouterInterface
{
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var ActionFactory
     */
    private $actionFactory;

    /**
     * @var ActionList
     */
    private $actionList;

    /**
     * @var DeletedUrlFinderInterface
     */
    private $urlFinder;

    /**
     * @var DeletedUrlModuleStatusInterface
     */
    private $statusConfig;

    /**
     * @param ActionFactory                   $actionFactory
     * @param ActionList                      $actionList
     * @param StoreManagerInterface           $storeManager
     * @param DeletedUrlFinderInterface       $urlFinder
     * @param DeletedUrlModuleStatusInterface $statusConfig
     */
    public function __construct(
        ActionFactory $actionFactory,
        ActionList $actionList,
        StoreManagerInterface $storeManager,
        DeletedUrlFinderInterface $urlFinder,
        DeletedUrlModuleStatusInterface $statusConfig
    ) {
        $this->storeManager = $storeManager;
        $this->actionFactory = $actionFactory;
        $this->actionList = $actionList;
        $this->urlFinder = $urlFinder;
        $this->statusConfig = $statusConfig;
    }

    /**
     * Checks if request matches entity for store and url that was deleted
     *
     * @param RequestInterface $request
     *
     * @return ActionInterface|null
     */
    public function match(RequestInterface $request)
    {
        if (!$this->statusConfig->isEnabled()) {
            return null;
        }

        $identifier = ltrim($request->getPathInfo(), '/');
        $storeId = $this->storeManager->getStore()->getId();
        $data = $this->urlFinder->findOneByData(
            [
                'url'      => $identifier,
                'store_id' => $storeId,
            ]
        );
        if (empty($data)) {
            return null;
        }

        $action = $request->getActionName() ?? 'cms';
        $request->setRouteName('deletedurls')->setControllerName('gone')->setActionName($action);
        $actionClassName = $this->actionList->get('Logiscenter_DeletedUrl', null, 'gone', $action);

        return $this->actionFactory->create($actionClassName);
    }
}
