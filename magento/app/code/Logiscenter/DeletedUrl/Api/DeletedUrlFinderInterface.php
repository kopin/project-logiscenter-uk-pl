<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\DeletedUrl\Api;

/**
 * Deleted URL entity finder interface
 */
interface DeletedUrlFinderInterface
{
    /**
     * Find delete url entity by specific data
     *
     * @param array $data
     * @return array
     */
    public function findOneByData(array $data);
}
