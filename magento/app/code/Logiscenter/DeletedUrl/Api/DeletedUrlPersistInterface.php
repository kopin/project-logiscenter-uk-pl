<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\DeletedUrl\Api;

/**
 * Deleted URL entity persist interface
 */
interface DeletedUrlPersistInterface
{
    /**
     * Save new deleted url entity or update old if exist
     *
     * @param array $urls
     *
     * @return void
     */
    public function add(array $urls): void;
}
