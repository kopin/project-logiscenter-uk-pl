<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\DeletedUrl\Api\Data;

/**
 * Deleted urls data interface
 */
interface DeletedUrlInterface
{
    public const TABLE_NAME = 'logiscenter_deleted_url';
    public const FIELD_URL_ID = 'url_id';
    public const FIELD_STORE_ID = 'store_id';
    public const FIELD_ENTITY_TYPE = 'entity_type';
    public const FIELD_ENTITY_ID = 'entity_id';
    public const FIELD_URL = 'url';

    /**
     * @return int
     */
    public function getUrlId(): int;

    /**
     * @param int $urlId
     *
     * @return $this
     */
    public function setUrlId(int $urlId): DeletedUrlInterface;

    /**
     * @return int
     */
    public function getStoreId(): int;

    /**
     * @param int $storeId
     *
     * @return $this
     */
    public function setStoreId(int $storeId): DeletedUrlInterface;

    /**
     * @return string
     */
    public function getEntityType(): string;

    /**
     * @param string $entityType
     *
     * @return $this
     */
    public function setEntityType(string $entityType): DeletedUrlInterface;

    /**
     * @return int
     */
    public function getEntityId();

    /**
     * @param int $entityId
     *
     * @return $this
     */
    public function setEntityId($entityId);

    /**
     * @return string
     */
    public function getUrl(): string;

    /**
     * @param string $url
     *
     * @return $this
     */
    public function setUrl(string $url): DeletedUrlInterface;
}
