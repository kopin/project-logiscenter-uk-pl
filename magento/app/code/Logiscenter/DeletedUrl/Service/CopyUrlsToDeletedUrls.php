<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\DeletedUrl\Service;

use Logiscenter\DeletedUrl\Api\DeletedUrlPersistInterface;
use Magento\UrlRewrite\Model\UrlFinderInterface;
use Magento\UrlRewrite\Service\V1\Data\UrlRewrite;

/**
 * Save deleted urls service class
 */
class CopyUrlsToDeletedUrls
{
    /**
     * @var UrlFinderInterface
     */
    private $urlFinder;

    /**
     * @var DeletedUrlPersistInterface
     */
    private $deletedUrlPersist;

    /**
     * @param UrlFinderInterface         $urlFinder
     * @param DeletedUrlPersistInterface $deletedUrlPersist
     */
    public function __construct(UrlFinderInterface $urlFinder, DeletedUrlPersistInterface $deletedUrlPersist)
    {
        $this->urlFinder = $urlFinder;
        $this->deletedUrlPersist = $deletedUrlPersist;
    }


    /**
     * Copy data from url_rewrites to deleted_urls table
     *
     * @param int    $entityId
     * @param string $entityType
     */
    public function execute(int $entityId, string $entityType): void
    {
        $urlRewrites = $this->urlFinder->findAllByData(
            [
                UrlRewrite::ENTITY_ID   => $entityId,
                UrlRewrite::ENTITY_TYPE => $entityType,
            ]
        );
        $this->deletedUrlPersist->add($urlRewrites);
    }
}
