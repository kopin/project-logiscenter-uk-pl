<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\PriceStockImport\Model\Bootstrap\Element\Transformation;

use Interactiv4\ImportExport\Api\Bootstrap\Element\Transformation\TransformationInterface;
use Interactiv4\ImportExport\Model\Bootstrap\Config;
use Interactiv4\ProductImport\Api\Validation\ValidatorInterfaceFactory;
use Magento\Framework\Validation\ValidationException;

class Validation implements TransformationInterface
{
    /**
     * @var ValidatorInterfaceFactory
     */
    private $validatorFactory;

    /**
     * Validation constructor.
     *
     * @param ValidatorInterfaceFactory $validatorFactory
     */
    public function __construct(
        ValidatorInterfaceFactory $validatorFactory
    ) {
        $this->validatorFactory = $validatorFactory;
    }

    /**
     * @param array $data
     *
     * @throws ValidationException
     *
     * @return array
     */
    public function process(array $data): array
    {
        $config = $data['config'];
        $importMode = $config->getInData('importMode') ?? '';
        if (!isset($data['mainSource']['rows'])) {
            return $data;
        }
        $headers = $data['mainSource']['headers'] ?? [];
        $validator = $this->validatorFactory->create();
        $validator->setImportMode($importMode)->validateHeaders($headers);
        $result = $validator->getResult();
        if (!$result->isValidFile()) {
            $this->logInvalidResult($data['config'], $result);
            $msg = __('Import file is not valid and will not be processed, see log for further detail');
            throw new ValidationException($msg);
        }

        $data['mainSource']['headersInfo'] = $validator->getHeadersInformationValues();

        return $data;
    }

    private function logInvalidResult(Config $config, $result): void
    {
        $this->log(
            $config,
            (string) __('ERROR: Import file is not valid and will not be processed, see details for more info:')
        );
        if ($result->getFileErrorsMsg()) {
            $this->log($config, (string) __('%1 File error(s):', \count($result->getFileErrorsMsg())));
        }
        foreach ($result->getFileErrorsMsg() as $error) {
            $this->log($config, (string) __($error));
        }
        if ($result->getTotalErrorRows()) {
            $this->log($config, (string) __('%1 Row(s) with error:', $result->getTotalErrorRows()));
        }
        foreach ($result->getRowErrorsMsg() as $error) {
            $this->log($config, (string) __($error));
        }
    }

    /**
     * @param Config $config
     * @param string $msg
     */
    private function log(Config $config, string $msg): void
    {
        $logFile = $config->getInData('logFile') ?? '';
        $config->log($logFile, 'VALIDATION: ' . $msg . PHP_EOL);
    }
}
