<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\PriceStockImport\Model\Bootstrap\Element\Transformation;

use Interactiv4\ImportExport\Api\Bootstrap\Element\Transformation\TransformationInterface;
use Interactiv4\ProductImport\Model\Bootstrap\Element\Transformation\Product\ProductInterface as ProductTransformer;
use Logiscenter\PriceStockImport\Model\Attribute\Update\ExcludeDiscountGroup;
use Logiscenter\PriceStockImport\Model\Attribute\Update\InfoProveedor;
use Magento\Framework\App\ResourceConnection;

/**
 * Transform extra attributes
 */
class ExtraInformation extends AbstractTransformer implements TransformationInterface
{
    private const COLUMNS = [
        'exclude_discount_group',
        'info_proveedor',
        'ean_code',
    ];

    /**
     * @var ExcludeDiscountGroup
     */
    private $excludeDiscountGroup;

    /**
     * @var InfoProveedor
     */
    private $infoProveedor;

    /**
     * ExtraInformation constructor.
     *
     * @param ResourceConnection   $resource
     * @param ExcludeDiscountGroup $excludeDiscountGroup
     * @param InfoProveedor        $infoProveedor
     */
    public function __construct(
        ResourceConnection $resource,
        ExcludeDiscountGroup $excludeDiscountGroup,
        InfoProveedor $infoProveedor
    ) {
        parent::__construct($resource);

        $this->excludeDiscountGroup = $excludeDiscountGroup;
        $this->infoProveedor = $infoProveedor;
    }

    /**
     * Process extra information transformation
     *
     * @param array $data
     *
     * @return array
     */
    public function process(array $data): array
    {
        if (!isset($data['mainSource']['rows'])) {
            return $data;
        }

        $rows = $data['mainSource']['rows'];
        $config = $data['config'];
        $productIdsBySku = $this->getIdsBySkusByBatches(\array_column($rows, ProductTransformer::COLUMN_SKU));
        foreach ($rows as $key => &$row) {
            try {
                $this->checkIfProductExist($row, $productIdsBySku);
                $row['entity_id'] = $productIdsBySku[$row[ProductTransformer::COLUMN_SKU]];
                $this->removeAttributesFromRow($row);
                $row = $this->excludeDiscountGroup->update($row);
                $row = $this->infoProveedor->update($row);

                if (isset($row['ean_code_nuevo']) && $row['ean_code_nuevo'] !== '') {
                    $row['ean_code'] = $row['ean_code_nuevo'];
                }

                $row = $this->filterRow($row);

                $data['mainSource']['toFinalTransformation'][$key] += $row;
            } catch (\Exception $e) {
                $this->logErrorMessage($config, $e, $row[ProductTransformer::COLUMN_SKU]);
            }
        }

        return $data;
    }

    /**
     * Filter the row
     *
     * @param array $row
     *
     * @return array
     */
    private function filterRow(array $row): array
    {
        $result = [];
        foreach (self::COLUMNS as $column) {
            $result[$column] = $row[$column] ?? '';
        }

        return $result;
    }

    /**
     * Remove attribute from row, because we don't need update the value for this attribute if new value is not
     * specified.
     *
     * @param array $row
     *
     * @return void
     */
    private function removeAttributesFromRow(array &$row): void
    {
        foreach (self::COLUMNS as $column) {
            $this->removeAttributeFromRow($row, $column);
        }
    }

    /**
     * @inheritdoc
     */
    protected function getCode(): string
    {
        return 'EXTRA_INFORMATION';
    }
}
