<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\PriceStockImport\Model\Bootstrap\Element\Transformation;

use Interactiv4\ImportExport\Api\Bootstrap\Element\Transformation\TransformationInterface;

/**
 * Provides only required columns to the next transformer
 */
class ColumnsFilter implements TransformationInterface
{
    /**
     * @var array
     */
    private $headers;

    /**
     * Prepare data for next transformer
     *
     * @param array $data
     *
     * @return array
     */
    public function process(array $data): array
    {
        if (!isset($data['mainSource']['toFinalTransformation'])) {
            return $data;
        }

        $rows = $data['mainSource']['toFinalTransformation'];
        unset($data['mainSource']['toFinalTransformation']);

        $data['mainSource']['rows'] = $rows;
        $data['mainSource']['headers'] = $this->getHeaders($rows);

        return $data;
    }

    /**
     * Get headers
     *
     * @param $rows
     *
     * @return array
     */
    private function getHeaders($rows): array
    {
        if ($this->headers === null) {
            $this->headers = array_keys(reset($rows));
        }

        return $this->headers;
    }
}
