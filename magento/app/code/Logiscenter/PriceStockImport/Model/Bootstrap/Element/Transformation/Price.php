<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\PriceStockImport\Model\Bootstrap\Element\Transformation;

use Interactiv4\ImportExport\Api\Bootstrap\Element\Transformation\TransformationInterface;
use Interactiv4\ProductImport\Model\Bootstrap\Element\Transformation\Product\ProductInterface as ProductTransformer;
use Logiscenter\PriceStockImport\Model\TableWriter;
use Magento\Framework\App\ResourceConnection;

/**
 * Transform price data
 */
class Price extends AbstractTransformer implements TransformationInterface
{
    private const COLUMNS = [
        'sku',
        'store_code',
        'price',
        'cost',
        'msrp',
    ];

    private const ATTRIBUTES_TO_UPDATE_MAPPING = [
        'price' => 'precio_nuevo',
        'cost'  => 'coste_nuevo',
        'msrp'  => 'msrp_nuevo',
    ];

    /**
     * @var TableWriter
     */
    private $tableWriter;

    /**
     * @param ResourceConnection $resource
     * @param TableWriter        $tableWriter
     */
    public function __construct(
        ResourceConnection $resource,
        TableWriter $tableWriter
    ) {
        parent::__construct($resource);

        $this->tableWriter = $tableWriter;
    }

    /**
     * Process stock transformation
     *
     * @param array $data
     *
     * @return array
     */
    public function process(array $data): array
    {
        if (!isset($data['mainSource']['rows'])) {
            return $data;
        }
        $rows = $data['mainSource']['rows'];
        $config = $data['config'];
        $productIdsBySku = $this->getIdsBySkusByBatches(\array_column($rows, ProductTransformer::COLUMN_SKU));
        foreach ($rows as $key => &$row) {
            try {
                $this->checkIfProductExist($row, $productIdsBySku);
                $row['entity_id'] = $productIdsBySku[$row[ProductTransformer::COLUMN_SKU]];
                $row = $this->updateDiscontinuedProduct($row);
                $row = $this->updateAttributes($row);
                $row = $this->filterRow($row);
                $data['mainSource']['toFinalTransformation'][$key] += $row;
            } catch (\Exception $e) {
                $this->logErrorMessage($config, $e, $row[ProductTransformer::COLUMN_SKU]);
            }
        }

        return $data;
    }

    /**
     * Update attributes if product discontinued
     *
     * @param array $row
     *
     * @return array
     */
    private function updateDiscontinuedProduct(array $row): array
    {
        $this->removeAttributeFromRow($row, 'descatalogado');
        if (isset($row['descatalogado_nuevo'])) {
            $row['descatalogado'] = $row['descatalogado_nuevo'];
        }

        if (isset($row['descatalogado']) && $row['descatalogado']) {
            if ($row['precio_nuevo'] > 0) {
                $this->tableWriter->addProduct($row);
            }
            $row['precio_nuevo'] = '0';
        }

        return $row;
    }

    /**
     * Filter the row
     *
     * @param array $row
     *
     * @return array
     */
    private function filterRow(array $row): array
    {
        $result = [];
        foreach (self::COLUMNS as $column) {
            if (isset($row[$column])) {
                $result[$column] = $row[$column];
            }
        }

        return $result;
    }

    /**
     * Update attributes
     *
     * @param array $row
     *
     * @return array
     */
    private function updateAttributes(array $row): array
    {
        foreach (self::ATTRIBUTES_TO_UPDATE_MAPPING as $sourceAttr => $destAttr) {
            $this->removeAttributeFromRow($row, $sourceAttr);
            if (isset($row[$destAttr]) && $row[$destAttr] !== '') {
                $row[$sourceAttr] = $row[$destAttr];
            }
        }

        return $row;
    }

    /**
     * @inheridoc
     */
    protected function getCode(): string
    {
        return 'PRICE';
    }
}
