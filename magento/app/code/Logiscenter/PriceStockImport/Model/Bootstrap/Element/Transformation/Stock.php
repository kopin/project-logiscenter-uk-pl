<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\PriceStockImport\Model\Bootstrap\Element\Transformation;

use Interactiv4\ImportExport\Api\Bootstrap\Element\Transformation\TransformationInterface;
use Interactiv4\ProductImport\Model\Bootstrap\Element\Transformation\Product\ProductInterface as ProductTransformer;
use Logiscenter\PriceStockImport\Model\TableWriter;
use Magento\Framework\App\ResourceConnection;

/**
 * Transform stock data
 */
class Stock extends AbstractTransformer implements TransformationInterface
{
    private const COLUMNS = [
        'sku',
        'store_code',
        'qty',
        'is_in_stock',
    ];

    private const ATTRIBUTES_TO_UPDATE_MAPPING = [
        'qty'         => 'stock',
        'is_in_stock' => 'disponible',
    ];

    /**
     * @var TableWriter
     */
    private $tableWriter;

    /**
     * @param ResourceConnection $resource
     * @param TableWriter        $tableWriter
     */
    public function __construct(
        ResourceConnection $resource,
        TableWriter $tableWriter
    ) {
        parent::__construct($resource);

        $this->tableWriter = $tableWriter;
    }

    /**
     * Process stock transformation
     *
     * @param array $data
     *
     * @return array
     */
    public function process(array $data): array
    {
        if (!isset($data['mainSource']['rows'])) {
            return $data;
        }
        $config = $data['config'];
        $rows = $data['mainSource']['rows'];
        $data['mainSource']['toFinalTransformation'] = [];
        $productIdsBySku = $this->getIdsBySkusByBatches(\array_column($rows, ProductTransformer::COLUMN_SKU));
        foreach ($rows as $key => &$row) {
            try {
                $this->checkIfProductExist($row, $productIdsBySku);
                $row['entity_id'] = $productIdsBySku[$row[ProductTransformer::COLUMN_SKU]];
                $row = $this->updateDiscontinuedProduct($row);
                $row = $this->updateStock($row);
                $row = $this->mapAttributes($row);
                $row = $this->filterRow($row);
                $data['mainSource']['toFinalTransformation'][$key] = $row;
            } catch (\Exception $e) {
                $this->logErrorMessage($config, $e, $row[ProductTransformer::COLUMN_SKU]);
            }
        }

        $this->log($config, 'Memory usage: ' . memory_get_usage(true));
        return $data;
    }

    /**
     * Update attributes if product is discontinued
     *
     * @param array $row
     *
     * @return array
     */
    private function updateDiscontinuedProduct(array $row): array
    {
        $this->removeAttributeFromRow($row, 'descatalogado');
        if (isset($row['descatalogado_nuevo'])) {
            $row['descatalogado'] = $row['descatalogado_nuevo'];
        }

        if (isset($row['descatalogado']) && $row['descatalogado']) {
            if ($row['stock_nuevo'] > 0) {
                $this->tableWriter->addProduct($row);
            }

            $row['stock_nuevo'] = 0;
            $row['disponible'] = 0;
        }

        return $row;
    }

    /**
     * Update stock data
     *
     * @param array $row
     *
     * @return array
     */
    private function updateStock(array $row): array
    {
        $this->removeAttributeFromRow($row, 'stock');
        if (isset($row['stock_nuevo'])) {
            $row['stock'] = $row['stock_nuevo'];
        }

        return $row;
    }

    /**
     * Perform attributes mapping
     *
     * @param array $row
     *
     * @return array
     */
    private function mapAttributes(array $row): array
    {
        foreach (self::ATTRIBUTES_TO_UPDATE_MAPPING as $destAttr => $sourceAttr) {
            if (isset($row[$sourceAttr])) {
                $row[$destAttr] = $row[$sourceAttr];
            }
        }

        return $row;
    }

    /**
     * Filter the row
     *
     * @param array $row
     *
     * @return array
     */
    private function filterRow(array $row): array
    {
        $result = [];
        foreach (self::COLUMNS as $column) {
            if (isset($row[$column])) {
                $result[$column] = (string)$row[$column];
            }
        }

        return $result;
    }

    /**
     * @inheritdoc
     */
    protected function getCode(): string
    {
        return 'STOCK';
    }
}
