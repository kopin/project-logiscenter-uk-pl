<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\PriceStockImport\Model\Bootstrap\Element\Transformation;

use Interactiv4\ImportExport\Model\Bootstrap\Config;
use Interactiv4\ProductImport\Model\Bootstrap\Element\Transformation\Product\ProductInterface as ProductTransformer;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Validation\ValidationException;

/**
 * Provides only required columns to the next transformer
 */
abstract class AbstractTransformer
{
    public const BATCH_SIZE = 1000;

    /**
     * @var ResourceConnection
     */
    private $resource;

    /**
     * AbstractTransformer constructor.
     *
     * @param ResourceConnection $resource
     */
    public function __construct(ResourceConnection $resource)
    {
        $this->resource = $resource;
    }

    /**
     * Checks if product exists in provided mapping
     *
     * @param array $row
     *
     * @return void
     */
    protected function checkIfProductExist(array $row, array $productIdsBySku): void
    {
        $sku = $row[ProductTransformer::COLUMN_SKU];
        unset($row['entity_id']);
        if (!isset($productIdsBySku[$sku])) {
            $msg = __(
                'Row with %1 SKU is skipped because such product does not exist',
                $sku
            );

            throw new ValidationException($msg);
        }
    }

    /**
     * @param array $skuList
     *
     * @return array
     */
    protected function getIdsBySkusByBatches(array $skuList): array
    {
        $chunks = \array_chunk($skuList, self::BATCH_SIZE);
        $result = [];
        foreach ($chunks as $chunk) {
            $result[] = $this->getProductIdsBySkuList($chunk);
        }

        return $this->mergeChunks($result);
    }

    /**
     * Remove attribute from row, because we don't need update the value for this attribute if new value is not
     * specified.
     *
     * @param array  $row
     * @param string $attribute
     */
    protected function removeAttributeFromRow(array &$row, string $attribute)
    {
        if (isset($row[$attribute])) {
            unset($row[$attribute]);
        }
    }

    /**
     * Log message depends of error type
     *
     * @param Config     $config
     * @param \Exception $e
     * @param string     $sku
     */
    protected function logErrorMessage(Config $config, \Exception $e, string $sku): void
    {
        if ($e instanceof ValidationException) {
            $this->log($config, $e->getMessage());

            return;
        }

        $this->log(
            $config,
            (string)__(
                'Unexpected error processing stock item %1: %2',
                $sku,
                $e->getMessage()
            )
        );
    }

    /**
     * @param array $skuList
     *
     * @return array
     */
    private function getProductIdsBySkuList(array $skuList): array
    {
        $connect = $this->resource->getConnection();
        $select = $connect->select()->from(
            'catalog_product_entity',
            ['sku', 'entity_id']
        )->where(
            'sku IN(?)',
            $skuList
        );

        return $connect->fetchPairs($select);
    }

    /**
     * Merge chunks for next transformer
     *
     * @param array $chunks
     *
     * @return array
     */
    private function mergeChunks(array $chunks): array
    {
        $result = [];
        foreach ($chunks as $chunk) {
            foreach ($chunk as $key => $value) {
                $result[$key] = $value;
            }
        }

        return $result;
    }

    /**
     * Log the error in appropriate log file
     *
     * @param Config $config
     * @param string $msg
     */
    protected function log(Config $config, string $msg): void
    {
        $logFile = $config->getInData('logFile') ?? '';
        $config->log($logFile, $this->getCode() . ': ' . $msg . PHP_EOL);
    }

    /**
     * Get transformation code
     *
     * @return string
     */
    abstract protected function getCode(): string;
}
