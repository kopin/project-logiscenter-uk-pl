<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\PriceStockImport\Model;

use Logiscenter\PriceStockImport\Api\GetStockImportDataInterface;
use Logiscenter\PriceStockImportConfig\Api\Config\PriceStockImportModuleStatusInterface;
use Magento\Catalog\Api\ProductAttributeRepositoryInterface;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Catalog\Model\Product\Type;
use Magento\Catalog\Model\ResourceModel\Product as ProductResource;
use Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\CollectionFactory as OptionCollectionFactory;
use Magento\Store\Api\WebsiteRepositoryInterface;

/**
 * Get catalog data for import table
 */
class GetStockImportData implements GetStockImportDataInterface
{
    /**
     * @var  OptionCollectionFactory
     */
    private $attributeOptionCollectionFactory;

    /**
     * @var ProductAttributeRepositoryInterface
     */
    private $attributeRepository;

    /**
     * @var ProductResource
     */
    private $productResource;

    /**
     * @var WebsiteRepositoryInterface
     */
    private $websiteRepository;

    /**
     * @var PriceStockImportModuleStatusInterface
     */
    private $moduleStatus;

    /**
     * @param OptionCollectionFactory               $attributeOptionCollectionFactory
     * @param ProductAttributeRepositoryInterface   $attributeRepository
     * @param ProductResource                       $productResource
     * @param WebsiteRepositoryInterface            $websiteRepository
     * @param PriceStockImportModuleStatusInterface $moduleStatus
     */
    public function __construct(
        OptionCollectionFactory $attributeOptionCollectionFactory,
        ProductAttributeRepositoryInterface $attributeRepository,
        ProductResource $productResource,
        WebsiteRepositoryInterface $websiteRepository,
        PriceStockImportModuleStatusInterface $moduleStatus
    ) {
        $this->attributeOptionCollectionFactory = $attributeOptionCollectionFactory;
        $this->attributeRepository = $attributeRepository;
        $this->productResource = $productResource;
        $this->websiteRepository = $websiteRepository;
        $this->moduleStatus = $moduleStatus;
    }

    /**
     * Collecting and return the data
     *
     * @return array
     */
    public function execute(): array
    {
        if (!$this->moduleStatus->isEnabled()) {
            return [];
        }
        $manufacturerId = $this->attributeRepository->get('manufacturer')->getAttributeId();
        $statusId = $this->attributeRepository->get('status')->getAttributeId();
        $discountGroupAttr = $this->attributeRepository->get('discount_group');
        $discountGroupId = $discountGroupAttr->getAttributeId();
        $nameId = $this->attributeRepository->get('name')->getAttributeId();
        $productTypeId = $this->attributeRepository->get('product_type')->getAttributeId();
        $descatalogadoId = $this->attributeRepository->get('descatalogado')->getAttributeId();
        $eanCodeId = $this->attributeRepository->get('ean_code')->getAttributeId();
        $priceId = $this->attributeRepository->get('price')->getAttributeId();
        $msrpId = $this->attributeRepository->get('msrp')->getAttributeId();
        $websiteId = $this->websiteRepository->get('us')->getId();

        $connection = $this->productResource->getConnection();
        $select = $connection->select();
        $select->from(
            ['cpe' => 'catalog_product_entity'],
            ['sku' => 'cpe.sku', 'entity_id' => 'cpe.entity_id']
        );
        $atIdxManufacturerConditions = [
            'at_idx_manufacturer.entity_id = cpe.entity_id',
            $connection->quoteInto('at_idx_manufacturer.attribute_id = ?', $manufacturerId),
            $connection->quoteInto('at_idx_manufacturer.store_id = ?', 0),
        ];
        $select->joinLeft(
            ['at_idx_manufacturer' => 'catalog_product_entity_int'],
            join(' AND ', $atIdxManufacturerConditions),
            []
        );
        $statusConditions = [
            'at_status.entity_id = cpe.entity_id',
            $connection->quoteInto('at_status.attribute_id = ?', $statusId),
            $connection->quoteInto('at_status.store_id = ?', 0),
        ];
        $select->join(['at_status' => 'catalog_product_entity_int'], join(' AND ', $statusConditions), []);
        $select->joinLeft(
            ['at_manufacturer' => 'eav_attribute_option_value'],
            'at_manufacturer.option_id = at_idx_manufacturer.value',
            ['manufacturer' => 'at_manufacturer.value']
        );
        $stockConditions = [
            'stock.product_id = cpe.entity_id',
            $connection->quoteInto('stock.website_id = ?', $websiteId),
        ];
        $select->joinLeft(
            ['stock' => 'cataloginventory_stock_status'],
            join(' AND ', $stockConditions),
            ['stock' => 'stock.qty']
        );
        $nameConditions = [
            'at_name.entity_id = cpe.entity_id',
            $connection->quoteInto('at_name.attribute_id = ?', $nameId),
            $connection->quoteInto('at_name.store_id = ?', 0),
        ];
        $select->joinLeft(
            ['at_name' => 'catalog_product_entity_varchar'],
            join(' AND ', $nameConditions),
            ['product_name' => 'at_name.value',]
        );
        $productTypeConditions = [
            'at_product_type.entity_id = cpe.entity_id',
            $connection->quoteInto('at_product_type.attribute_id = ?', $productTypeId),
            $connection->quoteInto('at_product_type.store_id = ?', 0),
        ];
        $select->joinLeft(
            ['at_product_type' => 'catalog_product_entity_varchar'],
            join(' AND ', $productTypeConditions),
            ['product_type' => 'at_product_type.value']
        );
        $descatalogadoConditions = [
            'at_descatalogado.entity_id = cpe.entity_id',
            $connection->quoteInto('at_descatalogado.attribute_id = ?', $descatalogadoId),
            $connection->quoteInto('at_descatalogado.store_id = ?', 0),
        ];
        $select->joinLeft(
            ['at_descatalogado' => 'catalog_product_entity_int'],
            join(' AND ', $descatalogadoConditions),
            ['descatalogado' => 'at_descatalogado.value']
        );
        $eanCodeConditions = [
            'at_ean_code.entity_id = cpe.entity_id',
            $connection->quoteInto('at_ean_code.attribute_id = ?', $eanCodeId),
            $connection->quoteInto('at_ean_code.store_id = ?', 0),
        ];
        $select->joinLeft(
            ['at_ean_code' => 'catalog_product_entity_varchar'],
            join(' AND ', $eanCodeConditions),
            ['ean_code' => 'at_ean_code.value']
        );

        $priceConditions = [
            'at_price.entity_id = cpe.entity_id',
            $connection->quoteInto('at_price.attribute_id = ?', $priceId),
            $connection->quoteInto('at_price.store_id = ?', 0),
        ];
        $select->joinLeft(
            ['at_price' => 'catalog_product_entity_decimal'],
            join(' AND ', $priceConditions),
            ['price' => 'at_price.value']
        );
        $msrpConditions = [
            'at_msrp.entity_id = cpe.entity_id',
            $connection->quoteInto('at_msrp.attribute_id = ?', $msrpId),
            $connection->quoteInto('at_msrp.store_id = ?', 0),
        ];
        $select->joinLeft(
            ['at_msrp' => 'catalog_product_entity_decimal'],
            join(' AND ', $msrpConditions),
            ['msrp' => 'at_msrp.value']
        );

        $discountGroupConditions = [
            'at_discount_group.entity_id = cpe.entity_id',
            $connection->quoteInto('at_discount_group.attribute_id = ?', $discountGroupId),
            $connection->quoteInto('at_discount_group.store_id = ?', 0),
        ];
        $select->joinLeft(
            ['at_discount_group' => 'catalog_product_entity_int'],
            join(' AND ', $discountGroupConditions),
            ['discount_group' => 'at_discount_group.value']
        );

        $select->joinLeft(
            ['cpl' => 'catalog_product_link'],
            'cpl.linked_product_id = cpe.entity_id',
            []
        );
        $select->where('at_status.value = ?', Status::STATUS_ENABLED);
        $select->where('cpe.type_id = ?', Type::TYPE_SIMPLE);
        $select->distinct();

        $result = $connection->fetchAll($select);

        $optionCollection = $this->attributeOptionCollectionFactory->create();
        $optionCollection->setAttributeFilter($discountGroupAttr->getAttributeId());
        $optionCollection->setStoreFilter(0);

        foreach ($result as &$item) {
            $discountGroup = $item['discount_group'] ?? null;
            if ($discountGroup) {
                $optionItem = $optionCollection->getItemById($discountGroup);
                if ($optionItem) {
                    $item['discount_sort_order'] = $optionItem->getSortOrder();
                    $item['discount_label'] = $optionItem->getValue();
                }
            }

            $item['agrupado'] = $this->getParentGroupedSku((int)$item['entity_id']);
        }

        return $result;
    }

    /**
     * Get sku of the parent grouped product
     *
     * @param int $childId
     *
     * @return string
     */
    private function getParentGroupedSku(int $childId): string
    {
        $connect = $this->productResource->getConnection();
        $select = $connect->select();
        $select->from(
            ['cpe' => 'catalog_product_entity'],
            ['sku' => 'cpe.sku']
        );
        $select->join(
            ['cpl' => 'catalog_product_link'],
            "cpl.linked_product_id = $childId AND cpl.link_type_id = 3",
            []
        );

        $select->where('cpe.entity_id = cpl.product_id');
        $select->where("cpe.type_id = 'grouped'");
        $result = $connect->fetchOne($select);

        return $result
            ?: '';
    }
}
