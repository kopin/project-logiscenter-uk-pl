<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\PriceStockImport\Model\Attribute\Update\Checker\ExcludeDiscountGroup;

/**
 * Checks precio_gancho condition
 */
class PrecioGanchoChecker
{
    /**
     * Check condition
     *
     * @param array $row
     *
     * @return bool
     */
    public function check(array $row): bool
    {
        return isset($row['precio_gancho']) && $row['precio_gancho'] > 0;
    }
}
