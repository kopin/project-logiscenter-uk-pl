<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\PriceStockImport\Model\Attribute\Update\Checker;

use Magento\CatalogRule\Model\ResourceModel\Rule as RuleResource;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class to check if need to update attribute
 */
class InfoProveedor
{
    /**
     * @var RuleResource
     */
    private $ruleResource;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @param RuleResource          $ruleResource
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(RuleResource $ruleResource, StoreManagerInterface $storeManager)
    {
        $this->ruleResource = $ruleResource;
        $this->storeManager = $storeManager;
    }

    /**
     * Checks if need update attribute
     *
     * @param array $row
     *
     * @return bool
     */
    public function check(array $row): bool
    {
        return $this->hasPrecioNuevo($row) && $this->hasPrecioGancho($row) && $this->hasCatalogPriceRule($row);
    }

    /**
     * Checks if product has catalog price rules
     *
     * @param array $row
     *
     * @return bool
     */
    private function hasCatalogPriceRule(array $row): bool
    {
        if (isset($row['entity_id'])) {
            $websiteId = $this->getWebsiteIdForStore($row['store_code']);

            if ($websiteId) {
                $result = $this->ruleResource->getRulesFromProduct(time(), $websiteId, 0, $row['entity_id']);
            }
        }

        return isset($result) && count($result);
    }

    /**
     * Checks precio_nuevo condition
     *
     * @param $row
     *
     * @return bool
     */
    private function hasPrecioNuevo($row): bool
    {
        return isset($row['precio_nuevo']) && $row['precio_nuevo'] > 0;
    }

    /**
     * Checks precio_gancho condition
     *
     * @param $row
     *
     * @return bool
     */
    private function hasPrecioGancho($row): bool
    {
        return isset($row['precio_gancho']) && (int)$row['precio_gancho'] === 0;
    }

    /**
     * Get website_id for provided store
     *
     * @param string $storeCode
     *
     * @return int|null
     */
    private function getWebsiteIdForStore(string $storeCode): ?int
    {
        try {
            $store = $this->storeManager->getStore($storeCode);
        } catch (NoSuchEntityException $e) {
            $store = null;
        }

        return $store
            ? (int)$store->getWebsiteId()
            : null;
    }
}
