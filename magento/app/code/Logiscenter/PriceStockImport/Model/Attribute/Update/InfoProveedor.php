<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\PriceStockImport\Model\Attribute\Update;

use Logiscenter\PriceStockImport\Api\UpdateAttributeInterface;
use Logiscenter\PriceStockImport\Model\Attribute\Update\Checker\InfoProveedor as InfoProveedorChecker;
use Magento\CatalogRule\Model\ResourceModel\Rule as RuleResource;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class to update info_proveedor attribute
 */
class InfoProveedor implements UpdateAttributeInterface
{
    /**
     * @var InfoProveedorChecker
     */
    private $checker;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var RuleResource
     */
    private $ruleResource;

    /**
     * @param InfoProveedorChecker  $checker
     * @param StoreManagerInterface $storeManager
     * @param RuleResource          $ruleResource
     */
    public function __construct(
        InfoProveedorChecker $checker,
        StoreManagerInterface $storeManager,
        RuleResource $ruleResource
    ) {
        $this->checker = $checker;
        $this->storeManager = $storeManager;
        $this->ruleResource = $ruleResource;
    }

    /**
     * Update row
     *
     * @param array $row
     *
     * @return array
     */
    public function update(array $row): array
    {
        $row['info_proveedor'] = $this->buildInfoProveedor($row);
        if ($this->checker->check($row)) {
            $row['info_proveedor'] = $this->updateAttribute(
                (int)$row['entity_id'],
                $row['store_code'],
                $row['info_proveedor']
            );
        }

        return $row;
    }

    /**
     * Update attribute value
     *
     * @param int    $productId
     * @param string $storeCode
     * @param string $infoProveedor
     *
     * @return string
     */
    private function updateAttribute(int $productId, string $storeCode, string $infoProveedor): string
    {
        $storeId = $this->getStoreId($storeCode);
        if ($storeId) {
            $websiteId = $this->storeManager->getStore($storeId)->getWebsiteId();
            $rules = $this->ruleResource->getRulesFromProduct(time(), $websiteId, 0, $productId);
            $discount = count($rules) ? reset($rules)['action_amount'] : '';
            $replacement = ' ' . (float)$discount . " % MSRP | " . date('Y-m-d H:i:s');
            $infoProveedorNew = \preg_replace('/([^\|]+$)/', $replacement, $infoProveedor);
        }

        return $infoProveedorNew ?? '';
    }

    /**
     * Build attribute value
     *
     * @param $row
     *
     * @return string
     */
    private function buildInfoProveedor($row): string
    {
        $proveedor = $row['proveedor'] ?? '';
        $info = $row['info'] ?? '';
        if ($proveedor && $info) {
            $result = $row['proveedor'] . ' ' . $row['info'] . ' | ' . date('d-m-Y H:i');
        }

        return $result ?? '';
    }

    /**
     * Get store id
     *
     * @param string $storeCode
     *
     * @return int|null
     */
    private function getStoreId(string $storeCode): ?int
    {
        try {
            $store = $this->storeManager->getStore($storeCode);
        } catch (NoSuchEntityException $e) {
            $store = null;
        }

        return $store ? (int)$store->getId() : null;
    }
}
