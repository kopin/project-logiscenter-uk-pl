<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\PriceStockImport\Model\Attribute\Update;

use Logiscenter\PriceStockImport\Api\UpdateAttributeInterface;
use Logiscenter\PriceStockImport\Model\Attribute\Update\Checker\ExcludeDiscountGroup as ExcludeDiscountGroupChecker;
use Magento\Catalog\Api\ProductAttributeRepositoryInterface;

/**
 * Class to update exclude_discount_group attribute value
 */
class ExcludeDiscountGroup implements UpdateAttributeInterface
{
    /**
     * @var ExcludeDiscountGroupChecker
     */
    private $excludeDiscountGroupChecker;

    /**
     * @var ProductAttributeRepositoryInterface
     */
    private $productAttributeRepository;

    /**
     * @param ExcludeDiscountGroupChecker         $excludeDiscountGroupChecker
     * @param ProductAttributeRepositoryInterface $productAttributeRepository
     */
    public function __construct(
        ExcludeDiscountGroupChecker $excludeDiscountGroupChecker,
        ProductAttributeRepositoryInterface $productAttributeRepository
    ) {
        $this->excludeDiscountGroupChecker = $excludeDiscountGroupChecker;
        $this->productAttributeRepository = $productAttributeRepository;
    }

    /**
     * Update exclude_discount_group value
     *
     * @param array $row
     *
     * @return array
     */
    public function update(array $row): array
    {
        $attribute = $this->productAttributeRepository->get('exclude_discount_group');
        if ($this->excludeDiscountGroupChecker->check($row)) {
            $row['exclude_discount_group'] = $attribute->getSource()->getIndexOptionText(1);
        }

        return $row;
    }
}
