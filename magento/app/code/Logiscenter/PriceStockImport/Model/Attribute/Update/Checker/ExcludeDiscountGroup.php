<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\PriceStockImport\Model\Attribute\Update\Checker;

/**
 * Class check if need to update exclude discount group attribute
 */
class ExcludeDiscountGroup
{
    /**
     * @var array
     */
    private $checkers;

    /**
     * ExcludeDiscountGroup constructor.
     *
     * @param array $checkers
     */
    public function __construct(array $checkers = [])
    {
        $this->checkers = $checkers;
    }

    /**
     * @return bool
     */
    public function check(array $row): bool
    {
        foreach ($this->checkers as $checker) {
            $result = $checker->check($row);
            if (true === $result) {
                break;
            }
        }

        return $result ?? false;
    }
}
