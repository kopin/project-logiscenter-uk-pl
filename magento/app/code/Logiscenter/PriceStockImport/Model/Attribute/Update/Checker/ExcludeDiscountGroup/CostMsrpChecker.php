<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\PriceStockImport\Model\Attribute\Update\Checker\ExcludeDiscountGroup;

/**
 * Checks cost and msrp condition
 */
class CostMsrpChecker
{
    /**
     * @param array $row
     *
     * @return bool
     */
    public function check(array $row): bool
    {
        return isset($row['coste_nuevo'])
            && isset($row['msrp_nuevo'])
            && (float)$row['coste_nuevo'] > 0
            && $row['coste_nuevo'] === $row['msrp_nuevo'];
    }
}
