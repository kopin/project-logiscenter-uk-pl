<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\PriceStockImport\Model\Attribute\Update\Checker\ExcludeDiscountGroup;

/**
 * Checks info condition
 */
class InfoChecker
{
    /**
     * Check condition
     *
     * @param array $row
     *
     * @return bool
     */
    public function check(array $row): bool
    {
        $result = false;

        if (isset($row['info']) && $row['info'] !== '') {
            $result = (bool)\preg_match('/DG(.*)fixed(.*)/', $row['info']);
        }

        return $result;
    }
}
