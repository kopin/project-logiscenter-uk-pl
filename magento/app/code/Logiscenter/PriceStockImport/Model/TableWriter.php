<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\PriceStockImport\Model;

use Magento\Framework\Filesystem\DriverInterface;
use Symfony\Component\Console\Helper\TableFactory;
use Symfony\Component\Console\Helper\TableSeparator;
use Symfony\Component\Console\Output\StreamOutputFactory;

class TableWriter
{
    /**
     * @var array
     */
    private static $storage = [];

    /**
     * @var TableSeparator
     */
    private $tableSeparator;

    /**
     * @var TableFactory
     */
    private $tableFactory;

    /**
     * @var StreamOutputFactory
     */
    private $streamOutputFactory;

    /**
     * @var DriverInterface
     */
    private $fileDriver;

    /**
     * Log constructor.
     *
     * @param TableSeparator      $tableSeparator
     * @param TableFactory        $tableFactory
     * @param StreamOutputFactory $streamOutputFactory
     * @param DriverInterface     $fileDriver
     */
    public function __construct(
        TableSeparator $tableSeparator,
        TableFactory $tableFactory,
        StreamOutputFactory $streamOutputFactory,
        DriverInterface $fileDriver
    ) {
        $this->tableSeparator = $tableSeparator;
        $this->tableFactory = $tableFactory;
        $this->streamOutputFactory = $streamOutputFactory;
        $this->fileDriver = $fileDriver;
    }

    /**
     * Add product row to storage
     *
     * @param array $product
     *
     * @return void
     */
    public function addProduct(array $product): void
    {
        self::$storage[$product['sku']] = $product;
    }

    /**
     * @return array
     */
    public function getStorage(): array
    {
        return self::$storage;
    }

    /**
     * Render table into provided resource
     *
     * @param string $filePath
     *
     * @return void
     */
    public function renderTable(string $filePath): void
    {
        $headers = ['SKU', 'Name', 'Manufacturer', 'Stock Nuevo', 'Coste Nuevo', 'Proveedor', 'Info'];
        foreach (self::$storage as $product) {
            $data[] = [
                \wordwrap(
                    $product['sku'],
                    40,
                    "\n"
                ),
                \wordwrap(
                    $product['product_name'],
                    40,
                    "\n"
                ),
                \wordwrap(
                    $product['manufacturer'] ?? '',
                    40,
                    "\n"
                ),
                \wordwrap(
                    $product['stock_nuevo'] ?? '',
                    40,
                    "\n"
                ),
                \wordwrap(
                    $product['coste_nuevo'] ?? '',
                    40,
                    "\n"
                ),
                \wordwrap(
                    $product['proveedor'] ?? '',
                    40,
                    "\n"
                ),
                \wordwrap(
                    $product['info'] ?? '',
                    40,
                    "\n"
                ),
            ];
        }
        $data[] = $this->tableSeparator;

        \array_pop($data);
        $data = \array_filter($data);
        $stream = $this->fileDriver->fileOpen($filePath, 'a');
        $output = $this->streamOutputFactory->create(['stream' => $stream]);
        $table = $this->tableFactory->create(['output' => $output]);
        $table->setHeaders($headers)->setRows($data);
        $table->render();
    }
}
