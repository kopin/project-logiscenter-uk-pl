<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\PriceStockImport\Model;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;

/**
 * Class to handle log file
 */
class LogFileLocator
{
    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var string
     */
    private static $fileName;

    /**
     * LogFileLocator constructor.
     *
     * @param Filesystem $filesystem
     */
    public function __construct(Filesystem $filesystem)
    {
        $this->filesystem = $filesystem;
        if (!isset(self::$fileName)) {
            $this->generateLogFileName();
        }
    }

    /**
     * Generate log file name
     *
     * @return void
     */
    public function generateLogFileName(): void
    {
        self::$fileName = 'price_stock_import' . '-' . \date('Y-m-d-H-i-s');
    }

    /**
     * Get log file absolute path
     *
     * @return string
     */
    public function getLogFileAbsolutePath(): string
    {
        $dir = $this->filesystem->getDirectoryRead(DirectoryList::VAR_DIR);

        return $dir->getAbsolutePath(
            'log' . DIRECTORY_SEPARATOR . 'debug' . DIRECTORY_SEPARATOR . self::$fileName . '.log'
        );
    }

    /**
     * Get log file name
     *
     * @return string
     */
    public function getLogFileName(): string
    {
        return self::$fileName;
    }
}
