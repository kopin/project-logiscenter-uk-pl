<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\PriceStockImport\Service\File;

use Logiscenter\PriceStockImportConfig\Api\Config\PriceStockImportConfigInterface;
use Magento\Framework\App\Filesystem\DirectoryList as ApplicationDirectoryList;
use Magento\Framework\Filesystem\DirectoryList;
use Magento\Framework\Filesystem\Driver\File;
use Magento\Framework\Filesystem\Glob;
use Magento\Framework\Filesystem\Io\File as IoFile;

/**
 * Managing the import files
 */
class Manager
{
    private const SFTP_FOLDER = 'sftp';

    /**
     * @var PriceStockImportConfigInterface
     */
    private $priceStockImportConfig;

    /**
     * @var DirectoryList
     */
    private $dir;

    /**
     * @var File
     */
    private $fileManager;

    /**
     * @var IoFile
     */
    private $io;

    /**
     * @param PriceStockImportConfigInterface $priceStockImportConfig
     * @param DirectoryList                   $dir
     * @param File                            $fileManager
     * @param IoFile                          $io
     */
    public function __construct(
        PriceStockImportConfigInterface $priceStockImportConfig,
        DirectoryList $dir,
        File $fileManager,
        IoFile $io
    ) {
        $this->priceStockImportConfig = $priceStockImportConfig;
        $this->dir = $dir;
        $this->fileManager = $fileManager;
        $this->io = $io;
    }

    /**
     * Get file which should be processed
     *
     * @return string
     */
    public function getFileToProcess(): string
    {
        $files = $this->getFileList();
        if (count($files) > 1) {
            $ignoreFiles = $this->getFilesToIgnore();
            $processFile = array_diff($files, $ignoreFiles);

            return reset($processFile);
        }

        return !empty($files) ? reset($files) : '';
    }

    /**
     * Get list of files which should be ignored
     *
     * @return array
     */
    public function getFilesToIgnore(): array
    {
        $files = $this->getFileList();

        if (\count($files) > 1) {
            $sortedFiles = [];
            foreach ($files as $file) {
                $sortedFiles[\filemtime($file)] = $file;
            }
            \krsort($sortedFiles);

            $result = \array_slice($sortedFiles, 1);
        }

        return $result ?? [];
    }

    /**
     * Process success file
     *
     * @param string $filePath
     *
     * @return bool
     */
    public function processSuccess(string $filePath): bool
    {
        $fileName = $this->getBaseName($filePath);
        $processDir = $this->getProcessedDir();
        $newFilePath = $processDir . $fileName;
        $this->fileManager->createDirectory($processDir);

        return $this->fileManager->rename($filePath, $newFilePath);
    }

    /**
     * Process error file
     *
     * @param string $filePath
     *
     * @return bool
     */
    public function processError(string $filePath): bool
    {
        $fileName = $this->getBaseName($filePath);
        $errorDir = $this->getErrorDir();
        $newFilePath = $errorDir . $fileName;
        $this->fileManager->createDirectory($errorDir);

        return $this->fileManager->rename($filePath, $newFilePath);
    }

    /**
     * Get list of all .csv files from import directory
     *
     * @return array
     */
    private function getFileList(): array
    {
        $pattern = $this->getImportDir() . "*.csv";

        return Glob::glob($pattern);
    }

    /**
     * Get import directory path
     *
     * @return string
     */
    private function getImportDir(): string
    {
        return $this->dir->getPath(ApplicationDirectoryList::VAR_DIR)
            . DIRECTORY_SEPARATOR
            . self::SFTP_FOLDER
            . DIRECTORY_SEPARATOR
            . $this->priceStockImportConfig->getImportFolder()
            . DIRECTORY_SEPARATOR;
    }

    /**
     * Get processed files directory path
     *
     * @return string
     */
    private function getProcessedDir(): string
    {
        return $this->dir->getPath(ApplicationDirectoryList::VAR_DIR)
            . DIRECTORY_SEPARATOR
            . self::SFTP_FOLDER
            . DIRECTORY_SEPARATOR
            . $this->priceStockImportConfig->getProcessedFolder()
            . DIRECTORY_SEPARATOR;
    }

    /**
     * Get error files directory path
     *
     * @return string
     */
    private function getErrorDir(): string
    {
        return $this->dir->getPath(ApplicationDirectoryList::VAR_DIR)
            . DIRECTORY_SEPARATOR
            . self::SFTP_FOLDER
            . DIRECTORY_SEPARATOR
            . $this->priceStockImportConfig->getErrorFolder()
            . DIRECTORY_SEPARATOR;
    }

    /**
     * Get file base name
     *
     * @param string $filePath
     *
     * @return string
     */
    private function getBaseName(string $filePath): string
    {
        $pathInfo = $this->io->getPathInfo($filePath);

        return $pathInfo['basename'] ?? '';
    }
}
