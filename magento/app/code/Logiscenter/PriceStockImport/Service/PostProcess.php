<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\PriceStockImport\Service;

use Interactiv4\ImportExport\Model\Bootstrap\Bootstrap;
use Logiscenter\PriceStockImport\Model\LogFileLocator;
use Logiscenter\PriceStockImport\Service\GroupedQty\Count;

/**
 * Price/stock import post process class
 */
class PostProcess
{
    /**
     * @var Bootstrap
     */
    private $bootstrapImport;

    /**
     * @var Count
     */
    private $countGroupedQty;

    /**
     * @var LogFileLocator
     */
    private $logFileLocator;

    /**
     * @param Bootstrap         $bootstrapImport
     * @param Count             $countGroupedQty
     * @param LogFileLocator    $logFileLocator
     */
    public function __construct(
        Bootstrap         $bootstrapImport,
        Count             $countGroupedQty,
        LogFileLocator    $logFileLocator
    ) {
        $this->bootstrapImport = $bootstrapImport;
        $this->countGroupedQty = $countGroupedQty;
        $this->logFileLocator = $logFileLocator;
    }

    /**
     * @return void
     */
    public function execute(): void
    {
        $this->countGroupedQty();
    }

    /**
     * Count grouped qty
     *
     * @return void
     */
    private function countGroupedQty(): void
    {
        $this->log('Starting grouped qty count...');
        try {
            $this->countGroupedQty->execute();
        } catch (\Exception $e) {
            $this->log(
                sprintf(
                    'Something went wrong while counting grouped_qty, see error message: %s',
                    $e->getMessage()
                )
            );
        }

        $this->log('Grouped qty count is finished');
    }

    /**
     * @param string $msg
     */
    private function log(string $msg): void
    {
        $this->bootstrapImport->getConfig()->log($this->logFileLocator->getLogFileName(), $msg . PHP_EOL);
    }
}
