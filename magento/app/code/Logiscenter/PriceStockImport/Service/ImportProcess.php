<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\PriceStockImport\Service;

use Interactiv4\ImportExport\Model\Bootstrap\Bootstrap;
use Logiscenter\PriceStockImport\Api\PriceStockImportInterface;
use Logiscenter\PriceStockImport\Model\LogFileLocator;
use Logiscenter\PriceStockImport\Model\TableWriter;
use Logiscenter\PriceStockImport\Service\File\Manager;
use Logiscenter\PriceStockImportConfig\Api\Config\PriceStockImportModuleStatusInterface;
use Magento\ImportExport\Model\Import;

/**
 * Class represent price stock import process.
 */
class ImportProcess implements PriceStockImportInterface
{
    public const INTEGRATION_CODE = 'logiscenter_pricestockimport_integration';

    /**
     * @var Bootstrap
     */
    private $bootstrapImport;

    /**
     * @var PriceStockImportModuleStatusInterface
     */
    private $moduleStatus;

    /**
     * @var Manager
     */
    private $fileManager;

    /**
     * @var LogFileLocator
     */
    private $logFileLocator;

    /**
     * @var TableWriter
     */
    private $tableWriter;

    /**
     * @var PostProcess
     */
    private $postProcess;

    /**
     * @param Bootstrap                             $bootstrapImport
     * @param PriceStockImportModuleStatusInterface $moduleStatus
     * @param Manager                               $fileManager
     * @param TableWriter                           $tableWriter
     * @param LogFileLocator                        $logFileLocator
     * @param PostProcess                           $postProcess
     */
    public function __construct(
        Bootstrap                             $bootstrapImport,
        PriceStockImportModuleStatusInterface $moduleStatus,
        File\Manager                          $fileManager,
        TableWriter                           $tableWriter,
        LogFileLocator                        $logFileLocator,
        PostProcess                           $postProcess
    ) {
        $this->bootstrapImport = $bootstrapImport;
        $this->moduleStatus = $moduleStatus;
        $this->fileManager = $fileManager;
        $this->tableWriter = $tableWriter;
        $this->logFileLocator = $logFileLocator;
        $this->postProcess = $postProcess;
    }

    /**
     * @return bool
     */
    public function execute(): bool
    {
        $result = false;

        if (!$this->moduleStatus->isEnabled()) {
            return $result;
        }

        $init = $this->bootstrapImport->init(self::INTEGRATION_CODE);
        if ($init) {
            $this->log('Looking for files to ignore...');
            $this->processIgnoredFiles();

            $this->log('Looking for files which should be processed...');
            $file = $this->fileManager->getFileToProcess();

            if (!$file) {
                $this->log('There is no file for import');

                return false;
            }

            try {
                $this->log(sprintf('Running import process using file %s', $file));
                $result = $this->runImportProcess($file);
                if ($result) {
                    $this->log(sprintf('Moving %s file to processed files folder', $file));
                    $this->fileManager->processSuccess($file);
                    $this->postProcess->execute();
                }
            } catch (\Exception $e) {
                $this->log(
                    'Something went wrong during the import process, see error:' . PHP_EOL . $e->getMessage()
                );
                $this->fileManager->processError($file);
            }
        }

        return $result;
    }

    /**
     * Run import process
     *
     * @param string $file
     *
     * @return bool
     */
    private function runImportProcess(string $file): bool
    {
        $this->bootstrapImport->getConfig()->setInData(Import::BEHAVIOR_APPEND, 'importMode');
        $this->bootstrapImport->getConfig()->setInData($file, 'filePath');
        $this->bootstrapImport->getConfig()->setInData($this->logFileLocator->getLogFileName(), 'logFile');
        $result = $this->bootstrapImport->getLauncher()->run();
        $this->renderDiscontinuedProductsTable();

        return $result ?? false;
    }

    /**
     * Process files which should be ignored
     *
     * @return void
     */
    private function processIgnoredFiles(): void
    {
        $filesToIgnore = $this->fileManager->getFilesToIgnore();

        if (!empty($filesToIgnore)) {
            $this->log(sprintf('%d file(s) will be ignored', count($filesToIgnore)));
            foreach ($filesToIgnore as $fileToIgnore) {
                $this->log(sprintf('Moving %s file to ignored files folder', $fileToIgnore));
                $this->fileManager->processError($fileToIgnore);
            }
        }
    }

    /**
     * @param string $msg
     *
     * @return void
     */
    private function log(string $msg): void
    {
        $this->bootstrapImport->getConfig()->log($this->logFileLocator->getLogFileName(), $msg . PHP_EOL);
    }

    /**
     * @return void
     */
    private function renderDiscontinuedProductsTable(): void
    {
        if (!empty($this->tableWriter->getStorage())) {
            $this->log('These products are discontinued but they have stock and price:');
            $this->tableWriter->renderTable($this->logFileLocator->getLogFileAbsolutePath());
        }
    }
}
