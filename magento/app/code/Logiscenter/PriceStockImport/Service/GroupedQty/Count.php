<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\PriceStockImport\Service\GroupedQty;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\Eav\Model\EntityFactory;
use Magento\Framework\App\ResourceConnection;
use Magento\GroupedProduct\Model\Product\Type\Grouped;

/**
 * Service to count grouped qty for provided product sku
 */
class Count
{
    /**
     * @var CollectionFactory
     */
    private $productCollectionFactory;

    /**
     * @var Status
     */
    private $productStatus;

    /**
     * @var EntityFactory
     */
    private $eavEntityFactory;

    /**
     * @var ResourceConnection
     */
    private $resource;

    /**
     * @param CollectionFactory  $productCollectionFactory
     * @param Status             $productStatus
     * @param EntityFactory      $eavEntityFactory
     * @param ResourceConnection $resource
     */
    public function __construct(
        CollectionFactory $productCollectionFactory,
        Status $productStatus,
        EntityFactory $eavEntityFactory,
        ResourceConnection $resource
    ) {
        $this->productCollectionFactory = $productCollectionFactory;
        $this->productStatus = $productStatus;
        $this->eavEntityFactory = $eavEntityFactory;
        $this->resource = $resource;
    }

    /**
     * Recalculate grouped_qty attribute value for grouped products
     *
     * @return void
     */
    public function execute(): void
    {
        $collection = $this->productCollectionFactory->create();
        $collection->addAttributeToFilter('type_id', 'grouped');
        $collection->addAttributeToFilter('status', ['in' => $this->productStatus->getVisibleStatusIds()]);
        $eavEntity = $this->eavEntityFactory->create();
        $eavEntity->setType(Product::ENTITY);

        foreach ($collection as $item) {
            $groupedQty = $this->calculateGroupedQty($item);
            $item->setGroupedQty($groupedQty);
            $eavEntity->saveAttribute($item, 'grouped_qty');
        }
    }

    /**
     * Calculate the product qty
     *
     * @param ProductInterface $product
     *
     * @return int
     */
    private function calculateGroupedQty(ProductInterface $product): int
    {
        /** @var Grouped $typeInstance */
        $typeInstance = $product->getTypeInstance();
        $associatedProducts = $typeInstance->getAssociatedProductCollection($product);
        $associatedIds = $associatedProducts->getColumnValues('entity_id');
        $productQtys = $this->getProductQty($associatedIds);
        $groupedQty = 0;
        foreach ($associatedProducts as $product) {
            $groupedQty += isset($productQtys[$product->getId()])
                ? (int)$productQtys[$product->getId()]
                : 0;
        }

        return $groupedQty;
    }

    /**
     * Get qty from stock table
     *
     * @param array $productIds
     *
     * @return array
     */
    private function getProductQty(array $productIds): array
    {
        $connect = $this->resource->getConnection();
        $select =
            $connect->select()
                ->from(['csi' => $connect->getTableName('cataloginventory_stock_item')], ['product_id', 'qty'])
                ->where('csi.product_id IN(?)', $productIds);

        return $connect->fetchPairs($select);
    }
}
