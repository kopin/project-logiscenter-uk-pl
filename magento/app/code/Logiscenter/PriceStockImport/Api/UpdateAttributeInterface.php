<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\PriceStockImport\Api;

/**
 * Interface for update attributes services
 */
interface UpdateAttributeInterface
{
    /**
     * Accept full row, update attribute and return row
     *
     * @param array $row
     *
     * @return array
     */
    public function update(array $row): array;
}
