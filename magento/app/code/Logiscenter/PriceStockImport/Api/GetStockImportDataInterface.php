<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\PriceStockImport\Api;

/**
 * Return catalog data for filling stock_import table
 */
interface GetStockImportDataInterface
{
    /**
     * Get catalog data for stock import
     *
     * @return array
     */
    public function execute(): array;
}
