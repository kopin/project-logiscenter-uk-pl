<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\PriceStockImport\Api;

/**
 * Price stock import process interface
 */
interface PriceStockImportInterface
{
    public function execute(): bool;
}
