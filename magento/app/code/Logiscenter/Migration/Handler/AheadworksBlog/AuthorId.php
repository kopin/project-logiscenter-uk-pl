<?php
declare(strict_types = 1);

namespace Logiscenter\Migration\Handler\AheadworksBlog;

use Aheadworks\Blog\Model\Post\Author\Resolver;
use Migration\ResourceModel\Record;
use Migration\Handler\AbstractHandler;
use Migration\Handler\HandlerInterface;

/**
 * Class AuthorId
 * @package Migration\Handler\AheadworksBlog
 */
class AuthorId extends AbstractHandler implements HandlerInterface
{
    /**
     * @var Resolver
     */
    private $resolver;

    /**
     * @param Resolver $resolver
     */
    public function __construct(
        Resolver $resolver
    ) {
        $this->resolver = $resolver;
    }

    /**
     * {@inheritdoc}
     */
    public function handle(Record $recordToHandle, Record $oppositeRecord)
    {
        $this->validate($recordToHandle);
        $authorId = $this->resolver->resolveId($recordToHandle->getData(), $this->field);
        $recordToHandle->setValue($this->field, $authorId);
    }
}
