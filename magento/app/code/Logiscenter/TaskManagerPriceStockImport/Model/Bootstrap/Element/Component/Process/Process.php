<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\TaskManagerPriceStockImport\Model\Bootstrap\Element\Component\Process;

use Interactiv4\TaskManager\Model\Bootstrap\Element\Component\Process\Process as ProcessAbstract;
use Interactiv4\TaskManager\Model\Config\Db\General\GeneralInterface as BaseGeneralConfig;
use Interactiv4\TaskManager\Model\Config\Db\Notification\NotificationInterface as BaseNotificationConfig;
use Logiscenter\PriceStockImport\Api\PriceStockImportInterface;
use Magento\Framework\Event\ManagerInterface;

/**
 * Triggers price and stock import process
 */
class Process extends ProcessAbstract
{
    /**
     * @var PriceStockImportInterface
     */
    private $importProcess;

    /**
     * @param ManagerInterface          $eventManager
     * @param BaseNotificationConfig    $baseNotificationConfig
     * @param BaseGeneralConfig         $baseGeneralConfig
     * @param PriceStockImportInterface $importProcess
     */
    public function __construct(
        ManagerInterface $eventManager,
        BaseNotificationConfig $baseNotificationConfig,
        BaseGeneralConfig $baseGeneralConfig,
        PriceStockImportInterface $importProcess
    ) {
        parent::__construct($eventManager, $baseNotificationConfig, $baseGeneralConfig);
        $this->importProcess = $importProcess;
    }

    /**
     * @inheritdoc
     */
    protected function doRun()
    {
        $this->debug('Process ' . $this->getProcessCode() . ' started...');
        $this->importProcess->execute();
        $this->debug('Process ' . $this->getProcessCode() . ' ended');
    }
}
