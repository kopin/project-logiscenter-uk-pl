<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\TaskManagerPriceStockImport\Model\Bootstrap\Element\Component\Notification;

use Interactiv4\TaskManager\Model\Bootstrap\Element\Component\Notification\Notification as ParentNotification;
use Interactiv4\TaskManager\Model\Config\Db\General\GeneralInterface as BaseGeneralConfig;
use Interactiv4\TaskManager\Model\Config\Db\Notification\NotificationInterface as BaseNotificationConfig;
use Logiscenter\PriceStockImport\Model\LogFileLocator;
use Logiscenter\PriceStockImportConfig\Api\Config\PriceStockImportModuleStatusInterface;
use Magento\Framework\App\State;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\Filesystem\Driver\File;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\Translate\Inline\StateInterface;

/**
 * Adds possibility to add attachment to the email
 */
class Notification extends ParentNotification
{
    /**
     * @var File
     */
    private $fileDriver;

    /**
     * @var LogFileLocator
     */
    private $logFileLocator;

    /**
     * @var PriceStockImportModuleStatusInterface
     */
    private $moduleStatus;

    /**
     * @param ManagerInterface                      $eventManager
     * @param TransportBuilder                      $transportBuilder
     * @param StateInterface                        $inlineTranslation
     * @param State                                 $appState
     * @param BaseNotificationConfig                $baseNotificationConfig
     * @param BaseGeneralConfig                     $baseGeneralConfig
     * @param File                                  $fileDriver
     * @param LogFileLocator                        $logFileLocator
     * @param PriceStockImportModuleStatusInterface $moduleStatus
     */
    public function __construct(
        ManagerInterface $eventManager,
        TransportBuilder $transportBuilder,
        StateInterface $inlineTranslation,
        State $appState,
        BaseNotificationConfig $baseNotificationConfig,
        BaseGeneralConfig $baseGeneralConfig,
        File $fileDriver,
        LogFileLocator $logFileLocator,
        PriceStockImportModuleStatusInterface $moduleStatus
    ) {
        parent::__construct(
            $eventManager,
            $transportBuilder,
            $inlineTranslation,
            $appState,
            $baseNotificationConfig,
            $baseGeneralConfig
        );

        $this->fileDriver = $fileDriver;
        $this->logFileLocator = $logFileLocator;
        $this->moduleStatus = $moduleStatus;
    }

    /**
     * Send email notification.
     *
     * @param string $type
     * @param array|string $content
     * @return void
     */
    public function notify(
        $type = self::TYPE_SUCCESS,
        $content = []
    ) {
        if ($this->moduleStatus->isEnabled()) {
            $this->addAttachment();
        }

        parent::notify($type, $content);
    }

    /**
     * Add attachment to the email
     *
     * @return void
     */
    private function addAttachment(): void
    {
        $filePath = $this->logFileLocator->getLogFileAbsolutePath();

        $this->transportBuilder->addAttachment(
            $this->fileDriver->fileGetContents($filePath),
            $this->logFileLocator->getLogFileName()
        );
    }
}
