<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\StoreUrlPathSeoBase\Plugin;

use Interactiv4\StoreUrlPathConfig\Api\Config\StoreUrlPathModuleStatusInterface;
use Magento\Framework\UrlInterface;
use Magento\Store\Model\StoreManagerInterface;
use MageWorx\SeoBase\Helper\StoreUrl;

/**
 * Mageworx base url plugin class.
 */
class UpdateStoreBaseUrl
{
    /**
     * @var StoreUrlPathModuleStatusInterface
     */
    private $urlPathModuleStatus;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @param StoreUrlPathModuleStatusInterface $urlPathModuleStatus
     * @param StoreManagerInterface             $storeManager
     */
    public function __construct(
        StoreUrlPathModuleStatusInterface $urlPathModuleStatus,
        StoreManagerInterface $storeManager
    ) {
        $this->urlPathModuleStatus = $urlPathModuleStatus;
        $this->storeManager = $storeManager;
    }

    /**
     * Use default magento logic for store url initialization to make it work with store url path.
     *
     * @param StoreUrl $helper
     * @param callable $proceed
     * @param null     $storeId
     * @param string   $type
     *
     * @return string
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function aroundGetStoreBaseUrl(
        StoreUrl $helper,
        callable $proceed,
        $storeId = null,
        $type = UrlInterface::DEFAULT_URL_TYPE
    ) {
        if (!$this->urlPathModuleStatus->isEnabled($storeId)) {
            return $proceed($storeId, $type);
        }
        return rtrim($this->storeManager->getStore($storeId)->getBaseUrl($type), '/') . '/';
    }
}
