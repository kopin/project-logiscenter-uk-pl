<?php
declare(strict_types=1);
/**
 * @author    Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Logiscenter\DiscontinuedProducts\ViewModel;

use Logiscenter\DiscontinuedProductsConfig\Api\Config\DiscontinuedProductsModuleStatusInterface;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Helper\Data;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Element\Block\ArgumentInterface;

/**
 * Provides discontinued products data for current product
 */
class DiscontinuedProducts implements ArgumentInterface
{
    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var Data
     */
    private $productHelper;

    /**
     * @var DiscontinuedProductsModuleStatusInterface
     */
    private $statusConfig;

    /**
     * @param Data                                      $productHelper
     * @param ProductRepositoryInterface                $productRepository
     * @param DiscontinuedProductsModuleStatusInterface $statusConfig
     */
    public function __construct(
        Data $productHelper,
        ProductRepositoryInterface $productRepository,
        DiscontinuedProductsModuleStatusInterface $statusConfig
    ) {
        $this->productHelper = $productHelper;
        $this->productRepository = $productRepository;
        $this->statusConfig = $statusConfig;
    }

    /**
     * Get array of data required for discontinued product
     *
     * @return array
     */
    public function getDiscontinuedMessageData(): array
    {
        if (!$this->statusConfig->isEnabled()) {
            return [];
        }

        $product = $this->productHelper->getProduct();

        if ($product->getId()) {
            $data = [
                'message' => $this->getMessage($product),
                'replaced_product_data' => $this->getReplacedProductData($product)
            ];
        }

        return $data ?? [];
    }

    /**
     * Return appropriate message depends on conditions
     *
     * @param ProductInterface $product
     *
     * @return string
     */
    private function getMessage(ProductInterface $product): string
    {
        if ($this->isProductDiscontinued($product)) {
            $message = 'This product is currently discontinued.';

            if ($this->getReplacedProduct($product)) {
                $message = 'This product is currently discontinued - It has been replaced by:';
            }
        }

        return isset($message) ? (string)__($message) : '';
    }

    /**
     * Get replaced product data if such product exist
     *
     * @param ProductInterface $product
     *
     * @return array
     */
    private function getReplacedProductData(ProductInterface $product): array
    {
        if ($newProduct = $this->getReplacedProduct($product)) {
            $data = [
                'product_sku' => $newProduct->getSku(),
                'product_name' => $newProduct->getName(),
                'url_key' => $newProduct->getUrlKey(),
            ];
        }

        return $data ?? [];
    }

    /**
     * Retrieve replaced product if it exist
     *
     * @param ProductInterface $product
     *
     * @return ProductInterface|null
     */
    private function getReplacedProduct(ProductInterface $product): ?ProductInterface
    {
        $newProductSku = $product->getData('sustituto');
        if ($newProductSku) {
            try {
                $newProduct = $this->productRepository->get($newProductSku);
            } catch (NoSuchEntityException $e) {
                $newProduct = null;
            }
        }

        return $newProduct ?? null;
    }

    /**
     * Checks if product marked as discontinued
     *
     * @param ProductInterface $product
     *
     * @return bool
     */
    private function isProductDiscontinued(ProductInterface $product): bool
    {
        return (bool)$product->getData('descatalogado');
    }
}
