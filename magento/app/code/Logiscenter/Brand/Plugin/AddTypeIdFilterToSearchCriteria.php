<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\Brand\Plugin;

use Amasty\Shopby\Helper\Data;
use Amasty\Shopby\Model\Layer\Filter\Attribute;
use Amasty\Shopby\Model\Request;
use Logiscenter\Brand\Service\IsShowGroupedOnlyForBrand;
use Logiscenter\BrandConfig\Api\Config\BrandModuleStatusInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Store\Model\ScopeInterface;

/**
 * Add type_id filter to search criteria on brand page
 */
class AddTypeIdFilterToSearchCriteria
{
    /**
     * @var IsShowGroupedOnlyForBrand
     */
    private $isShowGroupedOnlyForBrand;

    /**
     * @var Data
     */
    private $brandHelper;

    /**
     * @var BrandModuleStatusInterface
     */
    private $statusConfig;

    /**
     * @var Request
     */
    private $shopbyRequest;

    /**
     * @var string
     */
    private $brandCode;

    /**
     * @param IsShowGroupedOnlyForBrand  $isShowGroupedOnlyForBrand
     * @param Data                       $brandHelper
     * @param BrandModuleStatusInterface $statusConfig
     * @param ScopeConfigInterface       $config
     * @param Request                    $shopbyRequest
     */
    public function __construct(
        IsShowGroupedOnlyForBrand $isShowGroupedOnlyForBrand,
        Data $brandHelper,
        BrandModuleStatusInterface $statusConfig,
        ScopeConfigInterface $config,
        Request $shopbyRequest
    ) {
        $this->isShowGroupedOnlyForBrand = $isShowGroupedOnlyForBrand;
        $this->brandHelper = $brandHelper;
        $this->statusConfig = $statusConfig;
        $this->shopbyRequest = $shopbyRequest;
        $this->brandCode = $config->getValue('amshopby_brand/general/attribute_code', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @param Attribute        $subject
     * @param RequestInterface $request
     *
     * @return Attribute
     */
    public function afterApply(
        Attribute $subject,
        Attribute $result
    ) {
        if ($subject->getAttributeModel()->getAttributeCode() !== $this->brandCode) {
            return $result;
        }
        $optionId = (int)$this->shopbyRequest->getFilterParam($subject);
        if ($this->showOnlyGroupedProducts($optionId)) {
            $subject->getLayer()->getProductCollection()->addFieldToFilter('type_id', 'grouped');
        }

        return $result;
    }

    /**
     * Checks if need to to filter grouped products
     *
     * @param int $brandOptionId
     *
     * @return bool
     */
    private function showOnlyGroupedProducts(int $brandOptionId): bool
    {
        return $this->statusConfig->isEnabled() &&
            $this->brandHelper->isBrandPage() &&
            $this->isShowGroupedOnlyForBrand->execute($brandOptionId);
    }
}
