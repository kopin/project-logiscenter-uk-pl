<?php

/**
 * @author    Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\Brand\Plugin\Catalog\Block\Product\View;

use Amasty\ShopbyBase\Model\OptionSetting;
use Amasty\ShopbyBase\Model\ResourceModel\OptionSetting\CollectionFactory;
use Amasty\ShopbyBrand\Helper\Data;
use Amasty\ShopbyBrand\Plugin\Catalog\Block\Product\View\BlockHtmlTitlePlugin as AmastyBlockHtmlTitlePlugin;
use Logiscenter\Brand\Service\Resource\GetProductActiveCategoryIds;
use Logiscenter\BrandConfig\Api\Config\BrandModuleStatusInterface;
use Magento\Catalog\Helper\Data as ProductData;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\BlockFactory;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Override of Amasty Plugin to add category filter url params
 */
class BlockHtmlTitlePlugin extends AmastyBlockHtmlTitlePlugin
{
    /**
     * @var ProductData
     */
    private $productHelper;

    /**
     * @var GetProductActiveCategoryIds
     */
    private $getActiveCategoryIds;

    /**
     * @var BrandModuleStatusInterface
     */
    private $moduleStatus;

    /**
     * @param CollectionFactory           $optCollectionFactory
     * @param Registry                    $registry
     * @param BlockFactory                $blockFactory
     * @param StoreManagerInterface       $storeManager
     * @param Configurable                $configurableType
     * @param Data                        $brandHelper
     * @param ProductData                 $productHelper
     * @param GetProductActiveCategoryIds $getActiveCategoryIds
     * @param BrandModuleStatusInterface  $moduleStatus
     * @param array                       $data
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        CollectionFactory $optCollectionFactory,
        Registry $registry,
        BlockFactory $blockFactory,
        StoreManagerInterface $storeManager,
        Configurable $configurableType,
        Data $brandHelper,
        ProductData $productHelper,
        GetProductActiveCategoryIds $getActiveCategoryIds,
        BrandModuleStatusInterface $moduleStatus,
        $data = []
    ) {
        parent::__construct(
            $optCollectionFactory,
            $registry,
            $blockFactory,
            $storeManager,
            $configurableType,
            $brandHelper,
            $data
        );
        $this->productHelper = $productHelper;
        $this->getActiveCategoryIds = $getActiveCategoryIds;
        $this->moduleStatus = $moduleStatus;
    }

    /**
     * @inheritDoc
     */
    protected function getOptionSettingUrl(OptionSetting $setting)
    {
        $url = parent::getOptionSettingUrl($setting);
        if (!$this->moduleStatus->isEnabled()) {
            return $url;
        }
        $product = $this->productHelper->getProduct();
        $categoryIds = $this->getActiveCategoryIds->execute($product);

        if (empty($categoryIds)) {
            return $url;
        }

        return $this->buildUrlWithCategoryFilter($url, $categoryIds);
    }

    /**
     * Add category filter parameter to URL
     *
     * @param string $url
     * @param array  $categoryIds
     *
     * @return string
     */
    protected function buildUrlWithCategoryFilter(string $url, array $categoryIds): string
    {
        $paramsConnector = strpos($url, '?') ? '&' : '?';
        $params = http_build_query(['cat' => implode(',', $categoryIds)]);

        return $url . $paramsConnector . $params;
    }
}
