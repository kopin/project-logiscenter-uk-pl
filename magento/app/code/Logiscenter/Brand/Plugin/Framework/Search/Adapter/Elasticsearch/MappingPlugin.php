<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\Brand\Plugin\Framework\Search\Adapter\Elasticsearch;

use Magento\Elasticsearch7\Model\Client\Elasticsearch;

/**
 * Adds custom mapping for specified attributes
 */
class MappingPlugin
{
    /**
     * @var string[]
     */
    private $keywordFields = [
        'type_id',
    ];

    /**
     * @param Elasticsearch $subject
     * @param array         $fields
     * @param string        $index
     * @param string        $entityType
     *
     * @return array
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function beforeAddFieldsMapping(
        Elasticsearch $subject,
        array         $fields,
        string        $index,
        string        $entityType
    ): array {
        foreach ($this->keywordFields as $field) {
            $fields[$field] = [
                'type' => 'keyword',
            ];
        }

        return [$fields, $index, $entityType];
    }
}
