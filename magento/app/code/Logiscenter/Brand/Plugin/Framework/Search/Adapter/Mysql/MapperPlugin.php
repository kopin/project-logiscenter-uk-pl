<?php

/**
 * @author Amasty Team
 * @copyright Copyright (c) 2020 Amasty (https://www.amasty.com)
 * @package Amasty_Shopby
 */

declare(strict_types=1);

namespace Logiscenter\Brand\Plugin\Framework\Search\Adapter\Mysql;

use Amasty\Shopby\Helper\Data;
use Amasty\ShopbyBrand\Helper\Data as ShopByBrandHelper;
use Logiscenter\Brand\Service\IsShowGroupedOnlyForBrand;
use Logiscenter\BrandConfig\Api\Config\BrandModuleStatusInterface;
use Magento\Catalog\Model\ResourceModel\Product as ProductResource;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\DB\Select;
use Magento\Framework\Search\Adapter\Mysql\Mapper;
use Magento\Framework\Search\RequestInterface as SearchRequestInterface;
use Magento\GroupedProduct\Model\Product\Type\Grouped;

/**
 * Plugin to filter groped products for navigation count results
 */
class MapperPlugin
{
    /**
     * @var Data
     */
    private $brandHelper;

    /**
     * @var RequestInterface;
     */
    private $request;

    /**
     * @var IsShowGroupedOnlyForBrand
     */
    private $isShowGroupedOnlyForBrand;

    /**
     * @var ShopByBrandHelper
     */
    private $shopByBrandHelper;

    /**
     * @var ProductResource
     */
    private $productResource;

    /**
     * @var BrandModuleStatusInterface
     */
    private $statusConfig;

    /**
     * @param Data                       $brandHelper
     * @param RequestInterface           $request
     * @param IsShowGroupedOnlyForBrand  $isShowGroupedOnlyForBrand
     * @param ShopByBrandHelper          $shopByBrandHelper
     * @param ProductResource            $productResource
     * @param BrandModuleStatusInterface $statusConfig
     */
    public function __construct(
        Data $brandHelper,
        RequestInterface $request,
        IsShowGroupedOnlyForBrand $isShowGroupedOnlyForBrand,
        ShopByBrandHelper $shopByBrandHelper,
        ProductResource $productResource,
        BrandModuleStatusInterface $statusConfig
    ) {
        $this->brandHelper = $brandHelper;
        $this->request = $request;
        $this->isShowGroupedOnlyForBrand = $isShowGroupedOnlyForBrand;
        $this->shopByBrandHelper = $shopByBrandHelper;
        $this->productResource = $productResource;
        $this->statusConfig = $statusConfig;
    }

    /**
     * Filter groped products
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     *
     * @param Mapper                 $subject
     * @param Select                 $select
     * @param SearchRequestInterface $request
     *
     * @return Select
     */
    public function afterBuildQuery(Mapper $subject, Select $select, SearchRequestInterface $request): Select
    {
        $brandCode = $this->shopByBrandHelper->getBrandAttributeCode();
        $brandOptionId = $this->request->getParam($brandCode);

        if ($brandOptionId && $this->shouldFilterGroupedProducts((int)$brandOptionId)) {
            $subSelect = $select->getPart(Select::FROM)['main_select']['tableName'] ?? null;

            if ($subSelect instanceof Select) {
                $condition = sprintf(
                    "search_index.entity_id = product_entity_table.entity_id AND product_entity_table.type_id = '%s'",
                    Grouped::TYPE_CODE
                );
                $subSelect->joinInner(
                    ['product_entity_table' => $this->productResource->getTable('catalog_product_entity')],
                    $condition,
                    []
                );
            }
        }

        return $select;
    }

    /**
     * Checks if need to to filter grouped products
     *
     * @param int $brandOptionId
     *
     * @return bool
     */
    private function shouldFilterGroupedProducts(int $brandOptionId): bool
    {
        return $this->statusConfig->isEnabled()
            && $this->brandHelper->isBrandPage()
            && $this->isShowGroupedOnlyForBrand->execute($brandOptionId);
    }
}
