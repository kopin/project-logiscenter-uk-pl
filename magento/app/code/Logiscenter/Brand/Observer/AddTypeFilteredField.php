<?php

/**
 * @author    Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\Brand\Observer;

use Magento\Config\Model\Config\Source\Yesno;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

/**
 * Adds product filter fieldset to brand configuration page
 */
class AddTypeFilteredField implements ObserverInterface
{
    /**
     * @var Yesno
     */
    private $yesNoSource;

    /**
     * @param Yesno $yesNoSource
     */
    public function __construct(
        Yesno $yesNoSource
    ) {
        $this->yesNoSource = $yesNoSource;
    }

    /**
     * Adds needed fieldset and field to form
     *
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        $form = $observer->getData('form');
        $filterFieldset =
            $form->addFieldset('filter_fieldset', ['legend' => __('Product Filter'), 'class' => 'form-inline']);
        $filterFieldset->addField(
            'type_filtered',
            'select',
            [
                'name'   => 'type_filtered',
                'label'  => __('Show only grouped'),
                'title'  => __('Show only grouped'),
                'values' => $this->yesNoSource->toOptionArray(),
                'note'   => __('Is brand product listing page filtered by grouped product type'),
            ]
        );
    }
}
