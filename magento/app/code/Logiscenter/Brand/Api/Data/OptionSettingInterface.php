<?php

/**
 * @author    Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\Brand\Api\Data;

use Amasty\ShopbyBase\Api\Data\OptionSettingInterface as AmastyOptionSettingInterface;

/**
 * Represent extended Option setting interface
 */
interface OptionSettingInterface extends AmastyOptionSettingInterface
{
    const TYPE_FILTERED = 'type_filtered';

    /**
     * @return bool
     */
    public function isTypeFiltered(): bool;

    /**
     * @param bool $typeFiltered
     *
     * @return $this
     */
    public function setTypeFiltered(bool $typeFiltered): self;
}
