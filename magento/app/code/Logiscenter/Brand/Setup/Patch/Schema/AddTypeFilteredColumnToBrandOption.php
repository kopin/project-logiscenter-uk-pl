<?php

/**
 * @author    Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\Brand\Setup\Patch\Schema;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\Patch\SchemaPatchInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Add new column to brand options table schema patch class
 */
class AddTypeFilteredColumnToBrandOption implements SchemaPatchInterface
{
    /**
     * @var SchemaSetupInterface
     */
    private $schemaSetup;

    /**
     * @inheritDoc
     */
    public function __construct(
        SchemaSetupInterface $schemaSetup
    ) {
        $this->schemaSetup = $schemaSetup;
    }

    /**
     * @inheritDoc
     */
    public function apply()
    {
        $installer = $this->schemaSetup;
        $installer->startSetup();
        $tableName = $installer->getTable('amasty_amshopby_option_setting');
        $columnName = 'type_filtered';
        if ($installer->getConnection()->isTableExists($tableName)
            && !$installer->getConnection()->tableColumnExists($tableName, $columnName)) {
            $installer->getConnection()->addColumn(
                $tableName,
                $columnName,
                [
                    'type'     => Table::TYPE_BOOLEAN,
                    'nullable' => false,
                    'comment'  => 'Is Brand filterable by product type on PLP',
                    'default'  => 0,
                ]
            );
        }
        $installer->endSetup();
    }

    /**
     * @inheritDoc
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public static function getDependencies()
    {
        return [];
    }
}
