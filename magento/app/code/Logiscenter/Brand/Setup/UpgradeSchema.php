<?php

/**
 * @author Interactiv4 Team
 * @copyright  Copyright Â© Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\Brand\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Class to add index to amasty_amshopby_option_setting table
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * Upgrade amasty schemas
     *
     * @param SchemaSetupInterface   $setup
     * @param ModuleContextInterface $context
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context): void
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.0.1', '<')) {
            $this->addIdxToOptionSettingsTable($setup);
        }

        $setup->endSetup();
    }

    /**
     * Add index to amasty option settings table to improve queries
     *
     * @param SchemaSetupInterface $setup
     */
    private function addIdxToOptionSettingsTable(SchemaSetupInterface $setup): void
    {
        $setup->getConnection()->addIndex(
            $setup->getTable('amasty_amshopby_option_setting'),
            $setup->getIdxName('logiscenter', ['filter_code', 'store_id', 'url_alias']),
            ['filter_code', 'store_id', 'url_alias']
        );
    }
}
