<?php

/**
 * @author    Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\Brand\Model;

use Amasty\ShopbyBase\Model\OptionSetting as AmastyOptionSetting;
use Logiscenter\Brand\Api\Data\OptionSettingInterface;

/**
 * Extended option setting model class
 */
class OptionSetting extends AmastyOptionSetting implements OptionSettingInterface
{
    /**
     * @inheritDoc
     */
    public function isTypeFiltered(): bool
    {
        return (bool)$this->getData(self::TYPE_FILTERED);
    }

    /**
     * @inheritDoc
     */
    public function setTypeFiltered(bool $typeFiltered): OptionSettingInterface
    {
        $this->setData(self::TYPE_FILTERED, $typeFiltered);

        return $this;
    }
}
