<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\Brand\Model\Adapter\BatchDataMapper;

use Magento\AdvancedSearch\Model\Adapter\DataMapper\AdditionalFieldsProviderInterface;
use Magento\Catalog\Model\ResourceModel\Product;

/**
 * Product entity type id provider class
 */
class ProductTypeDataProvider implements AdditionalFieldsProviderInterface
{
    /**
     * @var Product
     */
    private $resource;

    /**
     * @param Product $resource
     */
    public function __construct(Product $resource)
    {
        $this->resource = $resource;
    }


    /**
     * @inheritdoc
     */
    public function getFields(array $productIds, $storeId)
    {
        $fields = [];

        $select = $this->resource->getConnection()->select()->from(
            $this->resource->getEntityTable(),
            ['entity_id', 'type_id']
        )->where('entity_id IN (?)', implode(',', $productIds));

        $data = $this->resource->getConnection()->fetchAssoc($select);
        foreach ($productIds as $productId) {
            $fields[$productId] = ['type_id' => $data[$productId]['type_id']];
        }

        return $fields;
    }
}
