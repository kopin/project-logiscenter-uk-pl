<?php

/**
 * @author    Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\Brand\Service\Resource;

use Logiscenter\BrandConfig\Api\Config\BrandModuleStatusInterface;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory as CategoryCollectionFactory;
use Magento\Framework\Exception\LocalizedException;
use Zend_Db_Select;

/**
 * Get product category ids service resource class
 */
class GetProductActiveCategoryIds
{
    /**
     * @var BrandModuleStatusInterface
     */
    private $moduleStatus;

    /**
     * @var CategoryCollectionFactory
     */
    private $collectionFactory;

    /**
     * @var array
     */
    private $categoryIds;

    /**
     * @param BrandModuleStatusInterface $moduleStatus
     * @param CategoryCollectionFactory  $collectionFactory
     */
    public function __construct(
        BrandModuleStatusInterface $moduleStatus,
        CategoryCollectionFactory $collectionFactory
    ) {
        $this->moduleStatus = $moduleStatus;
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * Return product active category ids list
     *
     * @param ProductInterface $product
     *
     * @return array
     */
    public function execute(ProductInterface $product): array
    {
        if (!$this->moduleStatus->isEnabled()) {
            return [];
        }
        if (isset($this->categoryIds[$product->getId()])) {
            return $this->categoryIds[$product->getId()];
        }
        try {
            $this->categoryIds[$product->getId()] = $this->getActiveCategories($product);
        } catch (LocalizedException $exception) {
            $this->categoryIds[$product->getId()] = [];
        }

        return $this->categoryIds[$product->getId()];
    }

    /**
     * Retrieves product active category entities
     *
     * @param ProductInterface $product
     *
     * @return array
     */
    private function getActiveCategories(ProductInterface $product): array
    {
        $categoryIds = $product->getCategoryIds();
        $categoryCollection = $this->collectionFactory->create();
        $categoryCollection->addAttributeToFilter('is_active', '1');
        $categoryCollection->addFieldToSelect('entity_id');
        $categoryCollection->addIdFilter($categoryIds);
        $select = $categoryCollection->getSelect();
        $select->reset(Zend_Db_Select::COLUMNS)->columns(['entity_id']);

        return $select->getConnection()->fetchCol($select);
    }
}
