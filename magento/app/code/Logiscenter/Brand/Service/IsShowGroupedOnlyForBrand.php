<?php

/**
 * @author    Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\Brand\Service;

use Amasty\ShopbyBase\Api\Data\OptionSettingRepositoryInterface;
use Logiscenter\Brand\Api\Data\OptionSettingInterface;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Check if can show only grouped products for provided brand option id
 */
class IsShowGroupedOnlyForBrand
{
    /**
     * @var OptionSettingRepositoryInterface
     */
    private $optionSettingRepository;

    /**
     * @var bool
     */
    private $showGroupedOnly;

    /**
     * @param OptionSettingRepositoryInterface $optionSettingRepository
     */
    public function __construct(OptionSettingRepositoryInterface $optionSettingRepository)
    {
        $this->optionSettingRepository = $optionSettingRepository;
    }

    /**
     * Check if need show only grouped products
     *
     * @param int $brandOptionId
     *
     * @return bool
     */
    public function execute(int $brandOptionId): bool
    {
        if (isset($this->showGroupedOnly)) {
            return $this->showGroupedOnly;
        }
        try {
            /** @var OptionSettingInterface $option */
            $option = $this->optionSettingRepository->get($brandOptionId, OptionSettingInterface::VALUE);
            $this->showGroupedOnly =  $option->isTypeFiltered();
        } catch (NoSuchEntityException $e) {
            $this->showGroupedOnly = false;
        }

        return $this->showGroupedOnly;
    }
}
