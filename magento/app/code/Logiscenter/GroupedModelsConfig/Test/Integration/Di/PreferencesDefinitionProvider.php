<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\GroupedModelsConfig\Test\Integration\Di;

use Interactiv4\Framework\Api\Di\Definition\DefinitionPoolInterface;
use Interactiv4\Framework\Api\Di\Definition\DefinitionPoolProviderInterface;
use Interactiv4\Framework\Di\Definition\Definition;
use Interactiv4\Framework\Di\Definition\DefinitionPool;
use Interactiv4\Framework\Di\Definition\DefinitionName;
use Interactiv4\Framework\Di\Definition\DefinitionValue;

/**
 * Class PreferencesDefinitionProvider.
 *
 * Preferences definition as they appear in di configuration files.
 */
class PreferencesDefinitionProvider implements DefinitionPoolProviderInterface
{
    /**
     * @var DefinitionPoolInterface
     */
    private $definitionPool;

    /**
     * {@inheritdoc}
     */
    public function getDefinitionPool(): DefinitionPoolInterface
    {
        if (!isset($this->definitionPool)) {
            $this->definitionPool = new DefinitionPool(
                new Definition(
                    new DefinitionName(
                        'Logiscenter\\GroupedModelsConfig\\Api\\Config\\GroupedModelsModuleStatusInterface'
                    ),
                    new DefinitionValue(
                        \Logiscenter\GroupedModelsConfig\Api\Config\GroupedModelsModuleStatusInterface::class,
                        \Logiscenter\GroupedModelsConfig\Config\GroupedModelsModuleStatus::class,
                        (string) \Magento\Framework\App\Area::AREA_GLOBAL,
                        false,
                        null,
                        null
                    )
                ),
                new Definition(
                    new DefinitionName(
                        'Logiscenter\\GroupedModelsConfig\\Api\\Config\\GroupedModelsStatusConfigInterface'
                    ),
                    new DefinitionValue(
                        \Logiscenter\GroupedModelsConfig\Api\Config\GroupedModelsStatusConfigInterface::class,
                        \Logiscenter\GroupedModelsConfig\Config\GroupedModelsStatusConfig::class,
                        (string) \Magento\Framework\App\Area::AREA_GLOBAL,
                        false,
                        null,
                        null
                    )
                ),
                new Definition(
                    new DefinitionName(
                        'Logiscenter\\GroupedModelsConfig\\Api\\Config\\GroupedModelsEnvironmentConfigInterface'
                    ),
                    new DefinitionValue(
                        \Logiscenter\GroupedModelsConfig\Api\Config\GroupedModelsEnvironmentConfigInterface::class,
                        \Logiscenter\GroupedModelsConfig\Config\GroupedModelsEnvironmentConfig::class,
                        (string) \Magento\Framework\App\Area::AREA_GLOBAL,
                        false,
                        null,
                        null
                    )
                ),
                new Definition(
                    new DefinitionName(
                        'Logiscenter\\GroupedModelsConfig\\Api\\Config\\GroupedModelsConfigInterface'
                    ),
                    new DefinitionValue(
                        \Logiscenter\GroupedModelsConfig\Api\Config\GroupedModelsConfigInterface::class,
                        \Logiscenter\GroupedModelsConfig\Config\GroupedModelsConfig::class,
                        (string) \Magento\Framework\App\Area::AREA_GLOBAL,
                        false,
                        null,
                        null
                    )
                )
            );
        }

        return $this->definitionPool;
    }
}
