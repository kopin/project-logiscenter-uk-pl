<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\GroupedModelsConfig\Test\Integration\Di;

use Interactiv4\Framework\Api\Di\Definition\DefinitionPoolInterface;
use Interactiv4\Framework\Api\Di\Definition\DefinitionPoolProviderInterface;
use Interactiv4\Framework\Di\Definition\Definition;
use Interactiv4\Framework\Di\Definition\DefinitionPool;
use Interactiv4\Framework\Di\Definition\DefinitionName;
use Interactiv4\Framework\Di\Definition\DefinitionValue;

/**
 * Class ArgumentsDefinitionProvider.
 *
 * Arguments definition as they appear in di configuration files.
 */
class ArgumentsDefinitionProvider implements DefinitionPoolProviderInterface
{
    /**
     * @var DefinitionPoolInterface
     */
    private $definitionPool;

    /**
     * {@inheritdoc}
     */
    public function getDefinitionPool(): DefinitionPoolInterface
    {
        if (!isset($this->definitionPool)) {
            $this->definitionPool = new DefinitionPool(
                new Definition(
                    new DefinitionName(
                        'Logiscenter\\GroupedModelsConfig\\Plugin\\Api\\Status\\CheckGroupedModelsModuleStatusPlugin',
                        'booleanStatusRead'
                    ),
                    new DefinitionValue(
                        \Logiscenter\GroupedModelsConfig\Plugin\Api\Status\CheckGroupedModelsModuleStatusPlugin::class,
                        \Logiscenter\GroupedModelsConfig\Api\Config\GroupedModelsModuleStatusInterface::class,
                        (string) \Magento\Framework\App\Area::AREA_GLOBAL,
                        false,
                        null,
                        null
                    )
                ),
                new Definition(
                    new DefinitionName(
                        'Logiscenter\\GroupedModelsConfig\\Plugin\\Api\\Status\\CheckGroupedModelsEnvironmentPlugin',
                        'allowedEnvironmentsAware'
                    ),
                    new DefinitionValue(
                        \Logiscenter\GroupedModelsConfig\Plugin\Api\Status\CheckGroupedModelsEnvironmentPlugin::class,
                        \Logiscenter\GroupedModelsConfig\Api\Config\GroupedModelsEnvironmentConfigInterface::class,
                        (string) \Magento\Framework\App\Area::AREA_GLOBAL,
                        false,
                        null,
                        null
                    )
                ),
                new Definition(
                    new DefinitionName(
                        'Interactiv4\\BaseConfig\\Pool\\ModuleStatusPool',
                        'objects',
                        'Logiscenter_GroupedModelsConfig'
                    ),
                    new DefinitionValue(
                        'Interactiv4\\BaseConfig\\Pool\\ModuleStatusPool',
                        \Logiscenter\GroupedModelsConfig\Api\Config\GroupedModelsModuleStatusInterface::class,
                        (string) \Magento\Framework\App\Area::AREA_GLOBAL,
                        false,
                        null,
                        null
                    )
                ),
                new Definition(
                    new DefinitionName(
                        'Logiscenter\\GroupedModelsConfig\\Observer\\RefreshDataUponConfigChangeObserver',
                        'refreshCacheStrategy'
                    ),
                    new DefinitionValue(
                        \Logiscenter\GroupedModelsConfig\Observer\RefreshDataUponConfigChangeObserver::class,
                        \Interactiv4\Framework\Cache\RefreshCacheAppStateDependantStrategy::class,
                        (string) \Magento\Framework\App\Area::AREA_ADMINHTML,
                        false,
                        null,
                        null
                    )
                ),
                new Definition(
                    new DefinitionName(
                        'Logiscenter\\GroupedModelsConfig\\Observer\\RefreshDataUponConfigChangeObserver',
                        'cacheTypes',
                        'config'
                    ),
                    new DefinitionValue(
                        \Logiscenter\GroupedModelsConfig\Observer\RefreshDataUponConfigChangeObserver::class,
                        (string) \Magento\Framework\App\Cache\Type\Config::TYPE_IDENTIFIER,
                        (string) \Magento\Framework\App\Area::AREA_ADMINHTML,
                        false,
                        null,
                        null
                    )
                ),
                new Definition(
                    new DefinitionName(
                        'Logiscenter\\GroupedModelsConfig\\Observer\\RefreshDataUponConfigChangeObserver',
                        'cacheTypes',
                        'block_html'
                    ),
                    new DefinitionValue(
                        \Logiscenter\GroupedModelsConfig\Observer\RefreshDataUponConfigChangeObserver::class,
                        (string) \Magento\Framework\App\Cache\Type\Block::TYPE_IDENTIFIER,
                        (string) \Magento\Framework\App\Area::AREA_ADMINHTML,
                        false,
                        null,
                        null
                    )
                ),
                new Definition(
                    new DefinitionName(
                        'Logiscenter\\GroupedModelsConfig\\Observer\\RefreshDataUponConfigChangeObserver',
                        'cacheTypes',
                        'full_page'
                    ),
                    new DefinitionValue(
                        \Logiscenter\GroupedModelsConfig\Observer\RefreshDataUponConfigChangeObserver::class,
                        (string) \Magento\PageCache\Model\Cache\Type::TYPE_IDENTIFIER,
                        (string) \Magento\Framework\App\Area::AREA_ADMINHTML,
                        false,
                        null,
                        null
                    )
                )
            );
        }

        return $this->definitionPool;
    }
}
