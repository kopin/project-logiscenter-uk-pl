<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\GroupedModelsConfig\Config\Db;

use Interactiv4\ConfigHelper\Api\Config\Db\DbConfigHelperInterface;
use Interactiv4\ConfigHelper\Api\Config\Db\Provider\DbConfigGroupPathProviderTrait;
use Interactiv4\ConfigHelper\Api\Config\Db\Provider\DbConfigSectionPathProviderTrait;
use Interactiv4\ConfigHelper\Model\Config\Db\DbConfigHelper;
use Logiscenter\GroupedModelsConfig\Api\Config\Db\Provider\GroupedModelsGroupPathProviderInterface;
use Logiscenter\GroupedModelsConfig\Api\Config\Db\Provider\GroupedModelsSectionPathProviderInterface;

/**
 * Class DbGroupedModelsConfig.
 *
 * This is not an api class, implementation may change at any time.
 * @see \Logiscenter\GroupedModelsConfig\Api\Config\GroupedModelsConfigInterface,
 * use this to retrieve config.
 */
class DbGroupedModelsConfig extends DbConfigHelper implements
    GroupedModelsSectionPathProviderInterface,
    GroupedModelsGroupPathProviderInterface,
    DbConfigHelperInterface
{
    use DbConfigSectionPathProviderTrait;
    use DbConfigGroupPathProviderTrait;
}
