<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\GroupedModelsConfig\Config;

use Logiscenter\GroupedModelsConfig\Api\Config\GroupedModelsConfigInterface;
use Logiscenter\GroupedModelsConfig\Config\Db\DbGroupedModelsConfig;

/**
 * Class GroupedModelsConfig.
 *
 * This is not an api class, implementation may change at any time.
 * @see GroupedModelsConfigInterface, use this to retrieve config.
 *
 * For example, it now relies on DbGroupedModelsConfig to read information from database,
 * but this implementation can be changed when needed, without prior notice.
 */
class GroupedModelsConfig extends DbGroupedModelsConfig implements
    GroupedModelsConfigInterface
{
}
