<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\GroupedModelsConfig\Config;

use Logiscenter\GroupedModelsConfig\Api\Config\GroupedModelsModuleStatusInterface;
use Logiscenter\GroupedModelsConfig\Api\Config\GroupedModelsStatusConfigInterface;

/**
 * Class GroupedModelsModuleStatus.
 *
 * This is not an api class, implementation may change at any time.
 * @see GroupedModelsModuleStatusInterface, use this to retrieve config.
 *
 * For example, it now relies on GroupedModelsStatusConfigInterface to read config,
 * but this implementation can be changed when needed, without prior notice.
 */
class GroupedModelsModuleStatus implements GroupedModelsModuleStatusInterface
{
    /**
     * @var GroupedModelsStatusConfigInterface
     */
    private $statusConfig;

    /**
     * GroupedModelsModuleStatus constructor.
     * @param GroupedModelsStatusConfigInterface $statusConfig
     */
    public function __construct(
        GroupedModelsStatusConfigInterface $statusConfig
    ) {
        $this->statusConfig = $statusConfig;
    }

    /**
     * Compare boolean status, using optionally supplied data
     *
     * @param bool $expectedStatusValue
     * @param int|string|null $storeId
     * @return bool
     */
    public function isBooleanStatus(
        bool $expectedStatusValue,
        $storeId = null
    ): bool {
        return $this->statusConfig->isBooleanStatus($expectedStatusValue, $storeId);
    }

    /**
     * Read boolean status, using optionally supplied data
     *
     * @param int|string|null $storeId
     * @return bool
     */
    public function isEnabled($storeId = null): bool
    {
        return $this->statusConfig->isEnabled($storeId);
    }
}
