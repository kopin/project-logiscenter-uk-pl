<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\GroupedModelsConfig\Api\Config;

use Interactiv4\ConfigHelper\Api\Config\Status\ModuleStatusInterface;

/**
 * Interface GroupedModelsModuleStatusInterface.
 *
 * Module status provider interface.
 *
 * @api
 */
interface GroupedModelsModuleStatusInterface extends ModuleStatusInterface
{
}
