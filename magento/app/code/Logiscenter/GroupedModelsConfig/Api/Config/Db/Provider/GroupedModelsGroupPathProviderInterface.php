<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\GroupedModelsConfig\Api\Config\Db\Provider;

use Interactiv4\ConfigHelper\Api\Config\Db\Provider\DbConfigGroupPathProviderInterface;

/**
 * Interface GroupedModelsGroupPathProviderInterface.
 *
 * Group path provider for groupedmodels group.
 *
 * @api
 */
interface GroupedModelsGroupPathProviderInterface extends DbConfigGroupPathProviderInterface
{
    public const GROUP_PATH = 'groupedmodels';
}
