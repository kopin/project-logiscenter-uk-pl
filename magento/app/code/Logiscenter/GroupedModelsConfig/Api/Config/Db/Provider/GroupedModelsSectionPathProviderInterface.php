<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\GroupedModelsConfig\Api\Config\Db\Provider;

use Interactiv4\ConfigHelper\Api\Config\Db\Provider\DbConfigSectionPathProviderInterface;

/**
 * Interface GroupedModelsSectionPathProviderInterface.
 *
 * Section path provider for logiscenter_groupedmodelsconfig section.
 *
 * @api
 */
interface GroupedModelsSectionPathProviderInterface extends DbConfigSectionPathProviderInterface
{
    public const SECTION_PATH = 'logiscenter_groupedmodelsconfig';
}
