<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\GroupedModelsConfig\Api\Config;

/**
 * Interface GroupedModelsConfigInterface.
 *
 * Config provider interface.
 *
 * @api
 */
interface GroupedModelsConfigInterface
{
}
