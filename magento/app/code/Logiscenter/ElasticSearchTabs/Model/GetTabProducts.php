<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ElasticSearchTabs\Model;

use Logiscenter\ElasticSearch\Model\Criteria\Criteria;
use Logiscenter\ElasticSearch\Model\Criteria\CriteriaBuilderFactory;
use Logiscenter\ElasticSearchTabs\Model\Persistence\ElasticSearchProductTabsRepository;

/**
 * Generate id for document for ES
 */
class GetTabProducts
{
    /**
     * @var ElasticSearchProductTabsRepository
     */
    private $repository;

    /**
     * @var CriteriaBuilderFactory
     */
    private $criteriaBuilderFactory;

    /**
     * @var array
     */
    private $cache;

    /**
     * @param ElasticSearchProductTabsRepository $repository
     * @param CriteriaBuilderFactory               $criteriaBuilderFactory
     */
    public function __construct(
        ElasticSearchProductTabsRepository $repository,
        CriteriaBuilderFactory               $criteriaBuilderFactory
    ) {
        $this->repository = $repository;
        $this->criteriaBuilderFactory = $criteriaBuilderFactory;
    }

    /**
     * @param int    $productId
     * @param string $tabCode
     *
     * @return array
     */
    public function getProducts(int $productId, string $tabCode): array
    {
        $cacheKey = $this->getCacheKey($productId, $tabCode);
        if (isset($this->cache[$cacheKey])) {
            return $this->cache[$cacheKey];
        }

        $this->cache[$cacheKey] = $this->repository->searchByQuery($this->buildCriteria($productId, $tabCode));

        return $this->cache[$cacheKey];
    }

    /**
     * @param int    $productId
     * @param string $tabCode
     *
     * @return Criteria
     */
    private function buildCriteria(int $productId, string $tabCode): Criteria
    {
        $criteriaBuilder = $this->criteriaBuilderFactory->create();
        $criteriaBuilder->addFilter('product_id', (string)$productId);
        $criteriaBuilder->addFilter('tab_code', $tabCode);

        return $criteriaBuilder->create();
    }

    /**
     * @param int    $productId
     * @param string $tabCode
     *
     * @return string
     */
    private function getCacheKey(int $productId, string $tabCode): string
    {
        return \sprintf('%s_%s', $productId, $tabCode);
    }
}
