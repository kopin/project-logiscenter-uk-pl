<?php

declare(strict_types=1);

namespace Logiscenter\ElasticSearchTabs\Model\Persistence;

use Logiscenter\ElasticSearch\Model\Criteria\Criteria;
use Logiscenter\ElasticSearch\Model\Persistence\ElasticsearchRepository;

/**
 * Class ElasticSearchPdvCatalogRepository
 *
 * Repository to persist data to elasticsearch
 */
class ElasticSearchProductTabsRepository extends ElasticsearchRepository
{
    /**
     * Aggregate name
     *
     * @return string
     */
    protected function aggregateName(): string
    {
        return 'product_tabs';
    }

    /**
     * Search all documents
     *
     * @return array
     */
    public function searchAll(): array
    {
        return $this->searchAllInElastic();
    }

    /**
     * Search documents by query
     *
     * @param Criteria $matching
     *
     * @return array
     */
    public function searchByQuery(Criteria $matching): array
    {
        return $this->searchByCriteria($matching);
    }
}
