<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ElasticSearchTabs\Model\DocumentsCollector;

use Logiscenter\ElasticSearch\Model\Criteria\Criteria;
use Logiscenter\ElasticSearch\Model\Criteria\CriteriaBuilderFactory;
use Logiscenter\ElasticSearchTabs\Model\Persistence\ElasticSearchProductTabsRepository;

/**
 * Get documents where provider product exists
 */
class GetAffectedDocuments
{
    /**
     * @var ElasticSearchProductTabsRepository
     */
    private $repository;

    /**
     * @var CriteriaBuilderFactory
     */
    private $criteriaBuilderFactory;

    /**
     * @param ElasticSearchProductTabsRepository $repository
     * @param CriteriaBuilderFactory             $criteriaBuilderFactory
     */
    public function __construct(
        ElasticSearchProductTabsRepository $repository,
        CriteriaBuilderFactory             $criteriaBuilderFactory
    ) {
        $this->repository = $repository;
        $this->criteriaBuilderFactory = $criteriaBuilderFactory;
    }

    /**
     * @param int $productId
     *
     * @return array
     */
    public function execute(int $productId): array
    {
        return $this->repository->searchByQuery($this->buildCriteria($productId));
    }

    /**
     * @param int $productId
     *
     * @return Criteria
     */
    private function buildCriteria(int $productId): Criteria
    {
        $criteriaBuilder = $this->criteriaBuilderFactory->create();
        $criteriaBuilder->addFilter('products', (string)$productId);

        return $criteriaBuilder->create();
    }
}
