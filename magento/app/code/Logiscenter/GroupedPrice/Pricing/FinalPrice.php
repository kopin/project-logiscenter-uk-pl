<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\GroupedPrice\Pricing;

use Logiscenter\GroupedPriceConfig\Api\Config\GroupedPriceModuleStatusInterface;
use Magento\Catalog\Model\Product;
use Magento\Framework\Pricing\Adjustment\CalculatorInterface;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Framework\Pricing\PriceInfoInterface;
use Magento\Framework\Pricing\SaleableInterface;
use Magento\GroupedProduct\Pricing\Price\FinalPrice as CoreFinalPrice;

/**
 * Excludes zero product price on minimum price calculation
 */
class FinalPrice extends CoreFinalPrice
{
    /**
     * @var GroupedPriceModuleStatusInterface
     */
    private $statusConfig;

    /**
     * @param SaleableInterface                 $saleableItem
     * @param float                             $quantity
     * @param CalculatorInterface               $calculator
     * @param PriceCurrencyInterface            $priceCurrency
     * @param GroupedPriceModuleStatusInterface $statusConfig
     */
    public function __construct(
        SaleableInterface $saleableItem,
        $quantity,
        CalculatorInterface $calculator,
        PriceCurrencyInterface $priceCurrency,
        GroupedPriceModuleStatusInterface $statusConfig
    ) {
        parent::__construct($saleableItem, $quantity, $calculator, $priceCurrency);
        $this->statusConfig = $statusConfig;
    }

    /**
     * @var Product
     */
    protected $minProduct;

    /**
     * Skip min product and min price assignment if product price equals to 0
     *
     * @return Product
     */
    public function getMinProduct()
    {
        if (!$this->statusConfig->isEnabled()) {
            return parent::getMinProduct();
        }
        if (null === $this->minProduct) {
            $products = $this->product->getTypeInstance()->getAssociatedProducts($this->product);
            $minPrice = null;
            foreach ($products as $item) {
                $productItem = clone $item;
                $productItem->setQty(PriceInfoInterface::PRODUCT_QUANTITY_DEFAULT);
                $price = $productItem->getPriceInfo()->getPrice(FinalPrice::PRICE_CODE)->getValue();
                if ($price > 0 && ($price <= ($minPrice === null ? $price : $minPrice))) {
                    $this->minProduct = $productItem;
                    $minPrice = $price;
                }
            }
            if ($this->minProduct === null && isset($productItem)) {
                $this->minProduct = $productItem;
            }
        }

        return $this->minProduct;
    }
}
