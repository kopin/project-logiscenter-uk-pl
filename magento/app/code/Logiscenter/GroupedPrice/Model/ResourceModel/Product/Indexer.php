<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\GroupedPrice\Model\ResourceModel\Product;

use DomainException;
use Exception;
use Logiscenter\GroupedPriceConfig\Api\Config\GroupedPriceModuleStatusInterface;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Model\Indexer\Product\Price\TableMaintainer;
use Magento\Catalog\Model\ResourceModel\Product\Indexer\Price\IndexTableStructureFactory;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\DB\Select;
use Magento\Framework\EntityManager\MetadataPool;
use Magento\GroupedProduct\Model\Product\Type\Grouped as GroupedType;
use Magento\GroupedProduct\Model\ResourceModel\Product\Indexer\Price\Grouped;
use Magento\GroupedProduct\Model\ResourceModel\Product\Link;
use Traversable;
use Zend_Db_Expr;

/**
 * Calculate minimal and maximal prices for Grouped products
 *
 * Exclude products with price 0
 * Rewrite for core indexer
 */
class Indexer extends Grouped
{
    /**
     * @var IndexTableStructureFactory
     */
    private $indexTableStructureFactory;

    /**
     * @var TableMaintainer
     */
    private $tableMaintainer;

    /**
     * @var MetadataPool
     */
    private $metadataPool;

    /**
     * @var ResourceConnection
     */
    private $resource;

    /**
     * @var string
     */
    private $connectionName;

    /**
     * @var AdapterInterface
     */
    private $connection;

    /**
     * @var bool
     */
    private $fullReindexAction;

    /**
     * @var GroupedPriceModuleStatusInterface
     */
    private $statusConfig;


    /**
     * @param IndexTableStructureFactory        $indexTableStructureFactory
     * @param TableMaintainer                   $tableMaintainer
     * @param MetadataPool                      $metadataPool
     * @param ResourceConnection                $resource
     * @param GroupedPriceModuleStatusInterface $statusConfig
     * @param string                            $connectionName
     * @param bool                              $fullReindexAction
     */
    public function __construct(
        IndexTableStructureFactory $indexTableStructureFactory,
        TableMaintainer $tableMaintainer,
        MetadataPool $metadataPool,
        ResourceConnection $resource,
        GroupedPriceModuleStatusInterface $statusConfig,
        $connectionName = 'indexer',
        $fullReindexAction = false
    ) {
        parent::__construct(
            $indexTableStructureFactory,
            $tableMaintainer,
            $metadataPool,
            $resource,
            $connectionName,
            $fullReindexAction
        );
        $this->indexTableStructureFactory = $indexTableStructureFactory;
        $this->tableMaintainer = $tableMaintainer;
        $this->connectionName = $connectionName;
        $this->metadataPool = $metadataPool;
        $this->resource = $resource;
        $this->fullReindexAction = $fullReindexAction;
        $this->statusConfig = $statusConfig;
    }

    /**
     * @inheritDoc
     */
    public function executeByDimensions(array $dimensions, Traversable $entityIds)
    {
        if (!$this->statusConfig->isEnabled()) {
            parent::executeByDimensions($dimensions, $entityIds);

            return;
        }

        $temporaryPriceTable = $this->indexTableStructureFactory->create(
            [
                'tableName'          => $this->tableMaintainer->getMainTmpTable($dimensions),
                'entityField'        => 'entity_id',
                'customerGroupField' => 'customer_group_id',
                'websiteField'       => 'website_id',
                'taxClassField'      => 'tax_class_id',
                'originalPriceField' => 'price',
                'finalPriceField'    => 'final_price',
                'minPriceField'      => 'min_price',
                'maxPriceField'      => 'max_price',
                'tierPriceField'     => 'tier_price',
            ]
        );
        $select = $this->prepareGroupedProductPriceDataSelect($dimensions, iterator_to_array($entityIds));
        $this->tableMaintainer->insertFromSelect($select, $temporaryPriceTable->getTableName(), []);
    }

    /**
     * Prepare data index select for Grouped products prices
     *
     * @param array $dimensions
     * @param array $entityIds the parent entity ids limitation
     *
     * @return Select
     * @throws Exception
     */
    private function prepareGroupedProductPriceDataSelect(array $dimensions, array $entityIds)
    {
        $select = $this->getConnection()->select();

        $zeroPriceGroupedIds = $this->getZeroPriceGroupedProductIds($dimensions, $entityIds);

        $select->from(
            ['e' => $this->getTable('catalog_product_entity')],
            'entity_id'
        );

        $linkField = $this->metadataPool->getMetadata(ProductInterface::class)->getLinkField();
        $select->joinLeft(
            ['l' => $this->getTable('catalog_product_link')],
            'e.' . $linkField . ' = l.product_id AND l.link_type_id=' . Link::LINK_TYPE_GROUPED,
            []
        );
        //additional information about inner products
        $select->joinLeft(
            ['le' => $this->getTable('catalog_product_entity')],
            'le.entity_id = l.linked_product_id',
            []
        );
        $select->columns(
            [
                'i.customer_group_id',
                'i.website_id',
            ]
        );
        $taxClassId = $this->getConnection()->getCheckSql('MIN(i.tax_class_id) IS NULL', '0', 'MIN(i.tax_class_id)');
        $minCheckSql = $this->getConnection()->getCheckSql('le.required_options = 0', 'i.min_price', 0);
        $maxCheckSql = $this->getConnection()->getCheckSql('le.required_options = 0', 'i.max_price', 0);
        $select->join(
            ['i' => $this->getMainTable($dimensions)],
            $this->getPriceJoinCondition($zeroPriceGroupedIds),
            [
                'tax_class_id' => $taxClassId,
                'price'        => new Zend_Db_Expr('NULL'),
                'final_price'  => new Zend_Db_Expr('NULL'),
                'min_price'    => new Zend_Db_Expr('MIN(' . $minCheckSql . ')'),
                'max_price'    => new Zend_Db_Expr('MAX(' . $maxCheckSql . ')'),
                'tier_price'   => new Zend_Db_Expr('NULL'),
            ]
        );
        $select->group(
            ['e.entity_id', 'i.customer_group_id', 'i.website_id']
        );
        $select->where(
            'e.type_id=?',
            GroupedType::TYPE_CODE
        );

        if ($entityIds !== null) {
            $select->where('e.entity_id IN(?)', $entityIds);
        }

        return $select;
    }

    /**
     * Get main table
     *
     * @param array $dimensions
     *
     * @return string
     */
    private function getMainTable($dimensions)
    {
        if ($this->fullReindexAction) {
            return $this->tableMaintainer->getMainReplicaTable($dimensions);
        }

        return $this->tableMaintainer->getMainTableByDimensions($dimensions);
    }

    /**
     * Get connection
     *
     * @return AdapterInterface
     * @throws DomainException
     */
    private function getConnection(): AdapterInterface
    {
        if ($this->connection === null) {
            $this->connection = $this->resource->getConnection($this->connectionName);
        }

        return $this->connection;
    }

    /**
     * Get table
     *
     * @param string $tableName
     *
     * @return string
     */
    private function getTable($tableName)
    {
        return $this->resource->getTableName($tableName, $this->connectionName);
    }

    /**
     * Return grouped product ids that include only children with zero price
     *
     * @param array $dimensions
     * @param array $entityIds
     *
     * @return array
     * @throws Exception
     */
    private function getZeroPriceGroupedProductIds(array $dimensions, array $entityIds)
    {
        $select = $this->getConnection()->select();

        $linkField = $this->metadataPool->getMetadata(ProductInterface::class)->getLinkField();
        $select->from(
            ['l' => $this->getTable('catalog_product_link')],
            ['l.product_id']
        );
        $select->join(
            ['i' => $this->getMainTable($dimensions)],
            "i.$linkField = l.linked_product_id",
            [
                'grouped_max_price' => new Zend_Db_Expr('MAX(i.max_price)'),
            ]
        );
        $select->where('l.link_type_id=' . Link::LINK_TYPE_GROUPED);
        if ($entityIds !== null) {
            $select->where('l.product_id IN(?)', $entityIds);
        }
        $select->group(
            ['l.product_id']
        );

        return $this->connection->fetchCol($this->connection->select()->from($select)->where('grouped_max_price = 0'));
    }

    /**
     * Creates price table join condition according to zero price product ids
     *
     * @param array $productIds
     *
     * @return string
     */
    private function getPriceJoinCondition(array $productIds): string
    {
        $condition = 'i.entity_id = l.linked_product_id AND ';
        $condition .= empty($productIds)
            ? 'i.min_price > 0'
            : '(i.min_price > 0 OR l.product_id IN (' . implode(',', $productIds) . '))';

        return $condition;
    }
}
