<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ElasticSearch\Model\Client;

use Magento\Store\Model\StoreManagerInterface;

/**
 * Client to get appropriate index name
 */
class IndexNameResolver
{
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(StoreManagerInterface $storeManager)
    {
        $this->storeManager = $storeManager;
    }

    /**
     * @param string   $index
     * @param string   $prefix
     * @param int|null $storeId
     *
     * @return string
     */
    public function resolve(string $index, string $prefix, ?int $storeId = null): string
    {
        $storeId = $storeId ?: $this->storeManager->getStore()->getId();

        return sprintf('%s_%s_%s', $prefix, $index, $storeId);
    }
}
