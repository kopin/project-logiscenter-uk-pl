<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ElasticSearch\Model\Persistence;

use Logiscenter\ElasticSearch\Model\Criteria\Criteria;

/**
 * Class ElasticsearchCriteriaConverter
 *
 * Convert criteria request to elasticsearch request
 */
class ElasticsearchCriteriaConverter
{
    /**
     * Convert criteria to elasticsearch query
     *
     * @param Criteria $criteria
     *
     * @return array
     */
    public function convert(Criteria $criteria): array
    {
        return $this->formatQuery($criteria);
    }

    /**
     * Format query
     *
     * @param Criteria $criteria
     *
     * @return array|\array[][]
     */
    private function formatQuery(Criteria $criteria): array
    {
        if ($criteria->hasFilters()) {
            return [
                'body' => array_merge(
                    [
                        'from' => $criteria->offset() ?: 0,
                        'size' => $criteria->limit() ?: 1000
                    ],
                    [
                        'query' => [
                            'bool' => $this->getQueryFilters($criteria)
                        ]
                    ]
                )
            ];
        }

        return [];
    }

    /**
     * Get query filters
     *
     * @param Criteria $criteria
     *
     * @return array
     */
    private function getQueryFilters(Criteria $criteria): array
    {
        $filters = [];
        foreach ($criteria->filters()->getFilters() as $filter) {
            if (is_array($filter) && count($filter)) {
                foreach ($filter as $filterItem) {
                    $filterGroup[] = [
                        'match' => [
                            $filterItem->key() => $filterItem->value(),
                        ],
                    ];
                }
                $filters['must'][] = ['bool' => ['should' => $filterGroup]];
                continue;
            }
            $filters[$filter->operator()][] = [
                'match' => [
                    $filter->key() => $filter->value(),
                ],
            ];
        }

        return $filters;
    }
}
