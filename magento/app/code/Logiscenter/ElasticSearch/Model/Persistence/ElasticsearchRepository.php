<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ElasticSearch\Model\Persistence;

use Elasticsearch\Common\Exceptions\Missing404Exception;
use Logiscenter\ElasticSearch\Model\Client\IndexNameResolver;
use Logiscenter\ElasticSearch\Model\Criteria\Criteria;
use Logiscenter\ElasticSearch\Model\ElasticsearchClient;

/**
 * Class ElasticsearchRepository
 *
 * Abstract ElasticSearch Repository
 */
abstract class ElasticsearchRepository
{
    /**
     * @var ElasticsearchClient
     */
    private $client;

    /**
     * @var IndexNameResolver
     */
    private $indexNameResolver;

    /**
     * ElasticsearchRepository constructor.
     *
     * @param ElasticsearchClient $client
     * @param IndexNameResolver   $indexNameResolver
     */
    public function __construct(
        ElasticsearchClient $client,
        IndexNameResolver   $indexNameResolver
    ) {
        $this->client = $client;
        $this->indexNameResolver = $indexNameResolver;
    }

    /**
     * Aggregate name (Index)
     *
     * @return string
     */
    abstract protected function aggregateName(): string;

    /**
     * Search all documents
     *
     * @return array
     */
    protected function searchAllInElastic(): array
    {
        return $this->searchRawElasticsearchQuery([]);
    }

    /**
     * Search in elasticsearch by criteria
     *
     * @param Criteria $matching
     *
     * @return array
     */
    public function searchByCriteria(Criteria $matching): array
    {
        $criteriaConverter = new ElasticsearchCriteriaConverter();
        $elasticRequest = $criteriaConverter->convert($matching);

        return $this->searchRawElasticsearchQuery($elasticRequest);
    }

    /**
     * @param array $documents
     *
     * @return void
     */
    public function bulkAddDocuments(array $documents, $storeId = null): void
    {
        $this->client->addDocs($documents, $this->aggregateName(), $storeId);
    }

    /**
     * @param array $documents
     *
     * @return void
     */
    public function bulkDeleteDocuments(array $documents, $storeId = null): void
    {
        $this->client->deleteDocs($documents, $this->aggregateName(), $storeId);
    }

    /**
     * Search documents by params
     *
     * @param array $params
     *
     * @return array
     */
    protected function searchRawElasticsearchQuery(array $params): array
    {
        try {
            $result = $this->client->client()->search(
                array_merge(
                    ['index' => $this->indexName()],
                    $params
                )
            );
        } catch (Missing404Exception $missing404Exception) {
            return [];
        }

        return $this->elasticValuesExtractor($result);
    }

    /**
     * Extract documents from array
     *
     * @param array $params
     *
     * @return array
     */
    private function elasticValuesExtractor(array $params): array
    {
        $elasticValues = [];
        $hits = $params['hits']['hits'];

        foreach ($hits as $hit) {
            $elasticValues[] = $hit['_source'];
        }

        return $elasticValues;
    }

    /**
     * Get prefixed index name
     *
     * @return string
     */
    protected function indexName(): string
    {
        return $this->indexNameResolver->resolve($this->aggregateName(), $this->client->indexPrefix());
    }
}
