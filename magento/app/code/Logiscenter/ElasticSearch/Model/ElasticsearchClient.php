<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ElasticSearch\Model;

use Elasticsearch\Client;
use Elasticsearch\ClientBuilder;
use Logiscenter\ElasticSearch\Model\Client\IndexNameResolver;
use Logiscenter\ElasticSearchConfig\Api\Config\ElasticSearchConfigInterface;
use Magento\Elasticsearch\Model\Adapter\Elasticsearch;

/**
 * Class ElasticsearchClient
 *
 * Client to connect with elasticsearch
 */
class ElasticsearchClient
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @var IndexNameResolver
     */
    private $indexNameResolver;

    /**
     * @var string
     */
    private $indexPrefix;

    /**
     * ElasticsearchClient constructor.
     *
     * @param ElasticSearchConfigInterface $elasticSearchConfig
     * @param IndexNameResolver            $indexNameResolver
     */
    public function __construct(
        ElasticSearchConfigInterface $elasticSearchConfig,
        IndexNameResolver $indexNameResolver
    ) {
        $this->indexPrefix = $elasticSearchConfig->getIndexPrefix();
        $client = $this->configure($elasticSearchConfig);
        $this->client = $client;
        $this->indexNameResolver = $indexNameResolver;
    }

    /**
     * @param ElasticSearchConfigInterface $elasticSearchConfig
     *
     * @return Client
     */
    private function configure(
        ElasticSearchConfigInterface $elasticSearchConfig
    ): Client {
        $client = ClientBuilder::create()
            ->setHosts([$elasticSearchConfig->getHost()]);
        if ($elasticSearchConfig->isAuthenticationRequired()) {
            $client->setBasicAuthentication(
                $elasticSearchConfig->getUsername(),
                $elasticSearchConfig->getPassword()
            );
        }

        return $client->build();
    }

    /**
     * @param array    $documents
     * @param string   $index
     * @param int|null $storeId
     *
     * @return void
     */
    public function addDocs(array $documents, string $index, ?int $storeId): void
    {
        $bulkDocuments = $this->getDocsArrayInBulkIndexFormat($documents, $this->indexName($index, $storeId));
        $this->client()->bulk($bulkDocuments);
    }

    /**
     * @param array    $documents
     * @param string   $index
     * @param int|null $storeId
     *
     * @return void
     */
    public function deleteDocs(array $documents, string $index, ?int $storeId): void
    {
        $bulkDocuments = $this->getDocsArrayInBulkIndexFormat(
            $documents,
            $this->indexName($index, $storeId),
            Elasticsearch::BULK_ACTION_DELETE
        );

        $this->client()->bulk($bulkDocuments);
    }

    /**
     * Create new elasticsearch index
     *
     * @param string $index
     */
    public function createIndex(string $index): void
    {
        $this->client->indices()->create(
            [
                'index' => $this->indexName($index)
            ]
        );
    }

    /**
     * @param string $index
     *
     * @return void
     */
    public function deleteIndex(string $index): void
    {
        if ($this->indexExists($index)) {
            $this->client->indices()->delete(['index' => $this->indexName($index)]);
        }
    }

    /**
     * Checks whether Elasticsearch index exists
     *
     * @param string $indexName
     * @return bool
     */
    public function indexExists(string $indexName): bool
    {
        return $this->client->indices()->exists(['index' => $indexName]);
    }

    /**
     * Get client
     *
     * @return Client
     */
    public function client(): Client
    {
        return $this->client;
    }

    /**
     * Get index prefix
     *
     * @return string
     */
    public function indexPrefix(): string
    {
        return $this->indexPrefix;
    }

    /**
     * Reformat documents array to bulk format
     *
     * @param array $documents
     * @param string $indexName
     * @param string $action
     * @return array
     */
    protected function getDocsArrayInBulkIndexFormat(
        array $documents,
        string $indexName,
        string $action = Elasticsearch::BULK_ACTION_INDEX
    ): array {
        $bulkArray = [
            'index' => $indexName,
            'body' => [],
            'refresh' => true,
        ];

        foreach ($documents as $id => $document) {
            $bulkArray['body'][] = [
                $action => [
                    '_id' => $id,
                    '_index' => $indexName
                ]
            ];

            if ($action == Elasticsearch::BULK_ACTION_INDEX) {
                $bulkArray['body'][] = $document;
            }
        }

        return $bulkArray;
    }

    /**
     * @param string   $index
     * @param int|null $storeId
     *
     * @return string
     */
    protected function indexName(string $index, ?int $storeId = null): string
    {
        return $this->indexNameResolver->resolve($index, $this->indexPrefix, $storeId);
    }
}
