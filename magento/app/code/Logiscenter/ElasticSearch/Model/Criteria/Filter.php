<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ElasticSearch\Model\Criteria;

/**
 * Class Filter
 *
 * Filter for elasticsearch
 */
class Filter
{
    /**
     * @var string
     */
    private $key;

    /**
     * @var string
     */
    private $value;

    /**
     * @var string|null
     */
    private $operator;

    /**
     * Filter constructor.
     *
     * @param string      $key
     * @param string      $value
     * @param string|null $operator
     */
    public function __construct(
        string $key,
        string $value,
        ?string $operator = null
    ) {
        $this->key = $key;
        $this->value = $value;
        $this->operator = $operator ?? 'must';
    }

    /**
     * @return string
     */
    public function key(): string
    {
        return $this->key;
    }

    /**
     * @return string
     */
    public function value(): string
    {
        return $this->value;
    }

    /**
     * @return string
     */
    public function operator(): string
    {
        return $this->operator;
    }
}
