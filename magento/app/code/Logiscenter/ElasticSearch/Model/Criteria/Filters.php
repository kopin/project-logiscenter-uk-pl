<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ElasticSearch\Model\Criteria;

/**
 * Class Filters
 *
 * Filter for elasticsearch
 */
class Filters
{
    /**
     * @var array
     */
    private $filters;

    /**
     * Add filters
     *
     * @param Filter $filter
     */
    public function addFilter(Filter $filter): void
    {
        $this->filters[] = $filter;
    }

    /**
     * Add filters
     *
     * @param Filter ...$filters
     */
    public function addFilters(Filter ...$filters): void
    {
        foreach ($filters as $filter) {
            $this->filters[] = $filter;
        }
    }

    /**
     * Add filters
     *
     * @param array $filters
     */
    public function addFiltersGroup(array $filters): void
    {
        $this->filters[] = $filters;
    }

    /**
     * @return Filter[]
     */
    public function getFilters(): array
    {
        return $this->filters;
    }

    /**
     * @return int
     */
    public function count(): int
    {
        return count($this->items());
    }

    /**
     * @return array
     */
    protected function items(): array
    {
        return $this->filters;
    }
}
