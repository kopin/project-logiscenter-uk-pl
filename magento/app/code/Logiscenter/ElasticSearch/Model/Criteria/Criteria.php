<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ElasticSearch\Model\Criteria;

/**
 * Class Filter
 *
 * Criteria builder
 */
class Criteria
{
    /**
     * @var Filters
     */
    private $filters;

    /**
     * @var int|null
     */
    private $offset;

    /**
     * @var int|null
     */
    private $limit;

    /**
     * Criteria constructor.
     *
     * @param Filters  $filters
     * @param int|null $offset
     * @param int|null $limit
     */
    public function __construct(
        Filters $filters,
        int $offset = 0,
        int $limit = 0
    ) {
        $this->filters = $filters;
        $this->offset = $offset;
        $this->limit = $limit;
    }

    /**
     * Check if request has filters
     *
     * @return bool
     */
    public function hasFilters(): bool
    {
        return $this->filters->count() > 0;
    }

    /**
     * Get filters
     *
     * @return Filters
     */
    public function filters(): Filters
    {
        return $this->filters;
    }

    /**
     * Request offset
     *
     * @return int|null
     */
    public function offset(): ?int
    {
        return $this->offset;
    }

    /**
     * Request limit
     *
     * @return int|null
     */
    public function limit(): ?int
    {
        return $this->limit;
    }
}
