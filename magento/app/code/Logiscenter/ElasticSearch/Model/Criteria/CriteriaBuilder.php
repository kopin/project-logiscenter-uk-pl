<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ElasticSearch\Model\Criteria;

/**
 * Class CriteriaBuilder
 *
 * Builder pattern for criteria
 */
class CriteriaBuilder
{
    /**
     * @var Filters
     */
    private $filters;

    /**
     * @var CriteriaFactory
     */
    private $criteria;

    /**
     * @var int
     */
    private $offset;

    /**
     * @var int
     */
    private $limit;

    /**
     * CriteriaBuilder constructor.
     *
     * @param FiltersFactory  $filtersFactory
     * @param CriteriaFactory $criteriaFactory
     */
    public function __construct(
        FiltersFactory $filtersFactory,
        CriteriaFactory $criteriaFactory
    ) {
        $this->filters = $filtersFactory->create();
        $this->criteria = $criteriaFactory;
    }

    /**
     * Create criteria
     *
     * @return Criteria
     */
    public function create(): Criteria
    {
        return $this->criteria->create(
            [
                   'filters' => $this->filters,
                   'offset' => $this->offset ?? 0,
                   'limit' => $this->limit ?? 0
               ]
        );
    }

    /**
     * Add new filter
     *
     * @param string      $key
     * @param string      $value
     * @param string|null $operator
     */
    public function addFilter(
        string $key,
        string $value,
        ?string $operator = null
    ): void {
        $this->filters->addFilter(
            new Filter($key, $value, $operator)
        );
    }

    /**
     * Define offset
     *
     * @param int $offset
     */
    public function offset(int $offset = 1)
    {
        $this->offset = $offset;
    }

    /**
     * Define limit
     *
     * @param int $limit
     */
    public function limit(int $limit = 10)
    {
        $this->limit = $limit;
    }
}
