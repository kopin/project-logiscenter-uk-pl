<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ElasticSearchConfig\Api\Config;

/**
 * Interface ElasticSearchConfigInterface.
 *
 * Config provider interface
 *
 * @api
 */
interface ElasticSearchConfigInterface
{
    /**
     * @param int|string|null $storeId
     *
     * @return string
     */
    public function getHost($storeId = null): string;

    /**
     * @param int|string|null $storeId
     *
     * @return string
     */
    public function getIndexPrefix($storeId = null): string;

    /**
     * @param int|string|null $storeId
     *
     * @return bool
     */
    public function isAuthenticationRequired($storeId = null): bool;

    /**
     * @param int|string|null $storeId
     *
     * @return string
     */
    public function getUsername($storeId = null): string;

    /**
     * @param int|string|null $storeId
     *
     * @return string
     */
    public function getPassword($storeId = null): string;
}
