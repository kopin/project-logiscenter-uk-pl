<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ElasticSearchConfig\Test\Integration\Di;

use Interactiv4\Framework\Api\Di\Definition\DefinitionPoolInterface;
use Interactiv4\Framework\Api\Di\Definition\DefinitionPoolProviderInterface;
use Interactiv4\Framework\Di\Definition\Definition;
use Interactiv4\Framework\Di\Definition\DefinitionPool;
use Interactiv4\Framework\Di\Definition\DefinitionName;
use Interactiv4\Framework\Di\Definition\DefinitionValue;

/**
 * Class PreferencesDefinitionProvider.
 *
 * Preferences definition as they appear in di configuration files.
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class PreferencesDefinitionProvider implements DefinitionPoolProviderInterface
{
    /**
     * @var DefinitionPoolInterface
     */
    private $definitionPool;

    /**
     * {@inheritdoc}
     *
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function getDefinitionPool(): DefinitionPoolInterface
    {
        if (!isset($this->definitionPool)) {
            $this->definitionPool = new DefinitionPool(
	new Definition(
	new DefinitionName(
	'Logiscenter\\ElasticSearchConfig\\Api\\Config\\ElasticSearchModuleStatusInterface'
	),
		new DefinitionValue(
	\Logiscenter\ElasticSearchConfig\Api\Config\ElasticSearchModuleStatusInterface::class,
		\Logiscenter\ElasticSearchConfig\Config\ElasticSearchModuleStatus::class,
		(string) \Magento\Framework\App\Area::AREA_GLOBAL,
		false,
		null,
		null
	)
	),
		new Definition(
	new DefinitionName(
	'Logiscenter\\ElasticSearchConfig\\Api\\Config\\ElasticSearchStatusConfigInterface'
	),
		new DefinitionValue(
	\Logiscenter\ElasticSearchConfig\Api\Config\ElasticSearchStatusConfigInterface::class,
		\Logiscenter\ElasticSearchConfig\Config\ElasticSearchStatusConfig::class,
		(string) \Magento\Framework\App\Area::AREA_GLOBAL,
		false,
		null,
		null
	)
	),
		new Definition(
	new DefinitionName(
	'Logiscenter\\ElasticSearchConfig\\Api\\Config\\ElasticSearchEnvironmentConfigInterface'
	),
		new DefinitionValue(
	\Logiscenter\ElasticSearchConfig\Api\Config\ElasticSearchEnvironmentConfigInterface::class,
		\Logiscenter\ElasticSearchConfig\Config\ElasticSearchEnvironmentConfig::class,
		(string) \Magento\Framework\App\Area::AREA_GLOBAL,
		false,
		null,
		null
	)
	),
		new Definition(
	new DefinitionName(
	'Logiscenter\\ElasticSearchConfig\\Api\\Config\\ElasticSearchConfigInterface'
	),
		new DefinitionValue(
	\Logiscenter\ElasticSearchConfig\Api\Config\ElasticSearchConfigInterface::class,
		\Logiscenter\ElasticSearchConfig\Config\ElasticSearchConfig::class,
		(string) \Magento\Framework\App\Area::AREA_GLOBAL,
		false,
		null,
		null
	)
	)
	);
        }

        return $this->definitionPool;
    }
}
