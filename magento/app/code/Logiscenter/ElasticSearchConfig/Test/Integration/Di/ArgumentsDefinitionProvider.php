<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ElasticSearchConfig\Test\Integration\Di;

use Interactiv4\Framework\Api\Di\Definition\DefinitionPoolInterface;
use Interactiv4\Framework\Api\Di\Definition\DefinitionPoolProviderInterface;
use Interactiv4\Framework\Di\Definition\Definition;
use Interactiv4\Framework\Di\Definition\DefinitionPool;
use Interactiv4\Framework\Di\Definition\DefinitionName;
use Interactiv4\Framework\Di\Definition\DefinitionValue;

/**
 * Class ArgumentsDefinitionProvider.
 *
 * Arguments definition as they appear in di configuration files.
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class ArgumentsDefinitionProvider implements DefinitionPoolProviderInterface
{
    /**
     * @var DefinitionPoolInterface
     */
    private $definitionPool;

    /**
     * {@inheritdoc}
     *
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function getDefinitionPool(): DefinitionPoolInterface
    {
        if (!isset($this->definitionPool)) {
            $this->definitionPool = new DefinitionPool(
	new Definition(
	new DefinitionName(
	'Logiscenter\\ElasticSearchConfig\\Plugin\\Api\\Status\\CheckElasticSearchModuleStatusPlugin',
		'booleanStatusRead'
	),
		new DefinitionValue(
	\Logiscenter\ElasticSearchConfig\Plugin\Api\Status\CheckElasticSearchModuleStatusPlugin::class,
		\Logiscenter\ElasticSearchConfig\Api\Config\ElasticSearchModuleStatusInterface::class,
		(string) \Magento\Framework\App\Area::AREA_GLOBAL,
		false,
		null,
		null
	)
	),
		new Definition(
	new DefinitionName(
	'Logiscenter\\ElasticSearchConfig\\Plugin\\Api\\Status\\CheckElasticSearchEnvironmentPlugin',
		'allowedEnvironmentsAware'
	),
		new DefinitionValue(
	\Logiscenter\ElasticSearchConfig\Plugin\Api\Status\CheckElasticSearchEnvironmentPlugin::class,
		\Logiscenter\ElasticSearchConfig\Api\Config\ElasticSearchEnvironmentConfigInterface::class,
		(string) \Magento\Framework\App\Area::AREA_GLOBAL,
		false,
		null,
		null
	)
	),
		new Definition(
	new DefinitionName(
	'Interactiv4\\BaseConfig\\Pool\\ModuleStatusPool',
		'objects',
		'Logiscenter_ElasticSearchConfig'
	),
		new DefinitionValue(
	'Interactiv4\\BaseConfig\\Pool\\ModuleStatusPool',
		\Logiscenter\ElasticSearchConfig\Api\Config\ElasticSearchModuleStatusInterface::class,
		(string) \Magento\Framework\App\Area::AREA_GLOBAL,
		false,
		null,
		null
	)
	),
		new Definition(
	new DefinitionName(
	'Logiscenter\\ElasticSearchConfig\\Observer\\RefreshDataUponConfigChangeObserver',
		'refreshCacheStrategy'
	),
		new DefinitionValue(
	\Logiscenter\ElasticSearchConfig\Observer\RefreshDataUponConfigChangeObserver::class,
		\Interactiv4\Framework\Cache\RefreshCacheAppStateDependantStrategy::class,
		(string) \Magento\Framework\App\Area::AREA_ADMINHTML,
		false,
		null,
		null
	)
	),
		new Definition(
	new DefinitionName(
	'Logiscenter\\ElasticSearchConfig\\Observer\\RefreshDataUponConfigChangeObserver',
		'cacheTypes',
		'config'
	),
		new DefinitionValue(
	\Logiscenter\ElasticSearchConfig\Observer\RefreshDataUponConfigChangeObserver::class,
		(string) \Magento\Framework\App\Cache\Type\Config::TYPE_IDENTIFIER,
		(string) \Magento\Framework\App\Area::AREA_ADMINHTML,
		false,
		null,
		null
	)
	),
		new Definition(
	new DefinitionName(
	'Logiscenter\\ElasticSearchConfig\\Observer\\RefreshDataUponConfigChangeObserver',
		'cacheTypes',
		'block_html'
	),
		new DefinitionValue(
	\Logiscenter\ElasticSearchConfig\Observer\RefreshDataUponConfigChangeObserver::class,
		(string) \Magento\Framework\App\Cache\Type\Block::TYPE_IDENTIFIER,
		(string) \Magento\Framework\App\Area::AREA_ADMINHTML,
		false,
		null,
		null
	)
	),
		new Definition(
	new DefinitionName(
	'Logiscenter\\ElasticSearchConfig\\Observer\\RefreshDataUponConfigChangeObserver',
		'cacheTypes',
		'full_page'
	),
		new DefinitionValue(
	\Logiscenter\ElasticSearchConfig\Observer\RefreshDataUponConfigChangeObserver::class,
		(string) \Magento\PageCache\Model\Cache\Type::TYPE_IDENTIFIER,
		(string) \Magento\Framework\App\Area::AREA_ADMINHTML,
		false,
		null,
		null
	)
	)
	);
        }

        return $this->definitionPool;
    }
}
