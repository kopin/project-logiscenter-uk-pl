<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ElasticSearchConfig\Config\Db;

use Interactiv4\ConfigHelper\Api\Config\Db\DbConfigHelperInterface;
use Interactiv4\ConfigHelper\Api\Config\Db\Provider\DbConfigGroupPathProviderTrait;
use Interactiv4\ConfigHelper\Api\Config\Db\Provider\DbConfigSectionPathProviderTrait;
use Interactiv4\ConfigHelper\Model\Config\Db\DbConfigHelper;
use Logiscenter\ElasticSearchConfig\Api\Config\Db\Provider\ElasticSearchGroupPathProviderInterface;
use Logiscenter\ElasticSearchConfig\Api\Config\Db\Provider\ElasticSearchSectionPathProviderInterface;

/**
 * Class DbElasticSearchConfig.
 *
 * This is not an api class, implementation may change at any time.
 * @see \Logiscenter\ElasticSearchConfig\Api\Config\ElasticSearchConfigInterface,
 * use this to retrieve config.
 */
class DbElasticSearchConfig extends DbConfigHelper implements
    ElasticSearchSectionPathProviderInterface,
    ElasticSearchGroupPathProviderInterface,
    DbConfigHelperInterface
{
    use DbConfigSectionPathProviderTrait;
    use DbConfigGroupPathProviderTrait;

    private const XML_FIELD_ELASTICSEARCH_HOST      = 'host';
    private const XML_FIELD_ELASTICSEARCH_PREFIX    = 'indexPrefix';
    private const XML_FIELD_ELASTICSEARCH_AUTHENTICATE = 'authentication_required';
    private const XML_FIELD_ELASTICSEARCH_USERNAME  = 'username';
    private const XML_FIELD_ELASTICSEARCH_PASSWORD  = 'password';

    /**
     * @param int|string|null $storeId
     *
     * @return string
     */
    public function getHost($storeId = null): string
    {
        return (string)$this->getConfig(
            self::XML_FIELD_ELASTICSEARCH_HOST,
            $storeId
        );
    }

    /**
     * @param int|string|null $storeId
     *
     * @return string
     */
    public function getIndexPrefix($storeId = null): string
    {
        return (string)$this->getConfig(
            self::XML_FIELD_ELASTICSEARCH_PREFIX,
            $storeId
        );
    }

    /**
     * @param int|string|null $storeId
     *
     * @return bool
     */
    public function isAuthenticationRequired($storeId = null): bool
    {
        return $this->getConfigFlag(
            self::XML_FIELD_ELASTICSEARCH_AUTHENTICATE,
            $storeId
        );
    }

    /**
     * @param int|string|null $storeId
     *
     * @return string
     */
    public function getUsername($storeId = null): string
    {
        return (string)$this->getConfig(
            self::XML_FIELD_ELASTICSEARCH_USERNAME,
            $storeId
        );
    }

    /**
     * @param int|string|null $storeId
     *
     * @return string
     */
    public function getPassword($storeId = null): string
    {
        return (string)$this->getConfig(
            self::XML_FIELD_ELASTICSEARCH_PASSWORD,
            $storeId
        );
    }
}
