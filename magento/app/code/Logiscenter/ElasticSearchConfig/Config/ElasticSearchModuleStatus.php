<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ElasticSearchConfig\Config;

use Logiscenter\ElasticSearchConfig\Api\Config\ElasticSearchModuleStatusInterface;
use Logiscenter\ElasticSearchConfig\Api\Config\ElasticSearchStatusConfigInterface;

/**
 * Class ElasticSearchModuleStatus.
 *
 * This is not an api class, implementation may change at any time.
 * @see ElasticSearchModuleStatusInterface, use this to retrieve config.
 *
 * For example, it now relies on ElasticSearchStatusConfigInterface to read config,
 * but this implementation can be changed when needed, without prior notice.
 */
class ElasticSearchModuleStatus implements ElasticSearchModuleStatusInterface
{
    /**
     * @var ElasticSearchStatusConfigInterface
     */
    private $statusConfig;

    /**
     * ElasticSearchModuleStatus constructor.
     * @param ElasticSearchStatusConfigInterface $statusConfig
     */
    public function __construct(
        ElasticSearchStatusConfigInterface $statusConfig
    ) {
        $this->statusConfig = $statusConfig;
    }

    /**
     * Compare boolean status, using optionally supplied data
     *
     * @param bool $expectedStatusValue
     * @param int|string|null $storeId
     * @return bool
     */
    public function isBooleanStatus(
        bool $expectedStatusValue,
        $storeId = null
    ): bool {
        return $this->statusConfig->isBooleanStatus($expectedStatusValue, $storeId);
    }

    /**
     * Read boolean status, using optionally supplied data
     *
     * @param int|string|null $storeId
     * @return bool
     */
    public function isEnabled($storeId = null): bool
    {
        return $this->statusConfig->isEnabled($storeId);
    }
}
