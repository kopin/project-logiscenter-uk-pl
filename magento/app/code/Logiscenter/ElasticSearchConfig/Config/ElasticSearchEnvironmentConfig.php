<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ElasticSearchConfig\Config;

use Logiscenter\ElasticSearchConfig\Api\Config\ElasticSearchEnvironmentConfigInterface;
use Logiscenter\ElasticSearchConfig\Config\Db\DbElasticSearchEnvironmentConfig;

/**
 * Class ElasticSearchEnvironmentConfig.
 *
 * This is not an api class, implementation may change at any time.
 * @see ElasticSearchEnvironmentConfigInterface, use this to retrieve config.
 *
 * For example, it now relies on DbElasticSearchEnvironmentConfig to read information from database,
 * but this implementation can be changed when needed, without prior notice.
 */
class ElasticSearchEnvironmentConfig extends DbElasticSearchEnvironmentConfig implements
    ElasticSearchEnvironmentConfigInterface
{
}
