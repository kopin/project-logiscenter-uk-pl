<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ElasticSearchConfig\Config;

use Logiscenter\ElasticSearchConfig\Api\Config\ElasticSearchConfigInterface;
use Logiscenter\ElasticSearchConfig\Config\Db\DbElasticSearchConfig;

/**
 * Class ElasticSearchConfig.
 *
 * This is not an api class, implementation may change at any time.
 * @see ElasticSearchConfigInterface, use this to retrieve config.
 *
 * For example, it now relies on DbElasticSearchConfig to read information from database,
 * but this implementation can be changed when needed, without prior notice.
 */
class ElasticSearchConfig extends DbElasticSearchConfig implements
    ElasticSearchConfigInterface
{
}
