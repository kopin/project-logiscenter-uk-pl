<?php

/**
 * @author    Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\Price\Model\Price;

use Logiscenter\PriceConfig\Api\Config\PriceConfigInterface;
use Logiscenter\PriceConfig\Api\Config\PriceModuleStatusInterface;
use Logiscenter\ZeroPriceRestriction\Model\PriceRestriction;
use Magento\Framework\Pricing\SaleableInterface;

/**
 * Class to check if need to show product prices
 */
class Config
{
    /**
     * @var PriceModuleStatusInterface
     */
    private $statusConfig;

    /**
     * @var PriceConfigInterface
     */
    private $priceConfig;

    /**
     * @var PriceRestriction
     */
    private $priceRestriction;

    /**
     * @param PriceModuleStatusInterface $statusConfig
     * @param PriceConfigInterface       $priceConfig
     * @param PriceRestriction           $priceRestriction
     */
    public function __construct(
        PriceModuleStatusInterface $statusConfig,
        PriceConfigInterface $priceConfig,
        PriceRestriction $priceRestriction
    ) {
        $this->statusConfig = $statusConfig;
        $this->priceConfig = $priceConfig;
        $this->priceRestriction = $priceRestriction;
    }

    /**
     * Checks if can show msrp price
     *
     * @param SaleableInterface $product
     *
     * @return bool
     */
    public function canShowMsrpPrice(SaleableInterface $product): bool
    {
        return $this->statusConfig->isEnabled()
            && $this->priceConfig->showMsrpPriceOnPdp()
            && $this->priceRestriction->canShowPrice($product);
    }

    /**
     * Checks if can show price including tax
     *
     * @param SaleableInterface $product
     *
     * @return bool
     */
    public function canShowFinalPriceInclTax(SaleableInterface $product): bool
    {
        return $this->statusConfig->isEnabled()
            && $this->priceConfig->showInclTaxPriceOnPdp()
            && $this->priceRestriction->canShowPrice($product);
    }
}
