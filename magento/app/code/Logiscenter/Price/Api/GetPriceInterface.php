<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Logiscenter\Price\Api;

use Magento\Framework\Pricing\SaleableInterface;

/**
 * Get price service interface
 */
interface GetPriceInterface
{
    /**
     * Method to retrieve price
     *
     * @param SaleableInterface $product
     *
     * @return string
     */
    public function execute(SaleableInterface $product): string;
}
