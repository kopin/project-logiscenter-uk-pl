<?php

/**
 * @author    Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\Price\Service\Price;

use Logiscenter\Price\Api\GetPriceInterface;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Framework\Pricing\SaleableInterface;

/**
 * Service to retrieve product final price
 */
class GetFinalPrice implements GetPriceInterface
{
    /**
     * @var PriceCurrencyInterface
     */
    private $priceCurrency;

    /**
     * @param PriceCurrencyInterface $priceCurrency
     */
    public function __construct(PriceCurrencyInterface $priceCurrency)
    {
        $this->priceCurrency = $priceCurrency;
    }

    /**
     * Get converted product's final price
     *
     * @param SaleableInterface $product
     *
     * @return string
     */
    public function execute(SaleableInterface $product): string
    {
        if ($product->isComposite()) {
            return '';
        }
        $amount = $product->getFinalPrice();

        return $this->priceCurrency->convertAndFormat($amount);
    }
}
