<?php

/**
 * @author    Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\Price\Service\Price;

use Logiscenter\Price\Api\GetPriceInterface;
use Magento\Customer\Api\Data\AddressInterface;
use Magento\Customer\Api\Data\AddressInterfaceFactory;
use Magento\Customer\Api\Data\RegionInterfaceFactory;
use Magento\Customer\Api\GroupRepositoryInterface;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Framework\Pricing\SaleableInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Tax\Api\Data\QuoteDetailsInterfaceFactory;
use Magento\Tax\Api\Data\QuoteDetailsItemInterfaceFactory;
use Magento\Tax\Api\Data\TaxClassKeyInterface;
use Magento\Tax\Api\Data\TaxClassKeyInterfaceFactory;
use Magento\Tax\Api\TaxCalculationInterface;

/**
 * Service to retrieve product final price including tax
 */
class GetFinalPriceWithTax implements GetPriceInterface
{
    /**
     * @var PriceCurrencyInterface
     */
    private $priceCurrency;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var CustomerSession
     */
    private $customerSession;

    /**
     * @var TaxClassKeyInterfaceFactory
     */
    private $taxClassKeyFactory;

    /**
     * @var GroupRepositoryInterface
     */
    private $customerGroupRepository;

    /**
     * @var QuoteDetailsItemInterfaceFactory
     */
    private $quoteDetailsItemFactory;

    /**
     * @var QuoteDetailsInterfaceFactory
     */
    private $quoteDetailsFactory;

    /**
     * @var TaxCalculationInterface
     */
    private $taxCalculationService;

    /**
     * @var AddressInterfaceFactory
     */
    private $addressFactory;

    /**
     * @var RegionInterfaceFactory
     */
    private $regionFactory;

    /**
     * @var array
     */
    private $priceBySku;

    /**
     * @param PriceCurrencyInterface           $priceCurrency
     * @param StoreManagerInterface            $storeManager
     * @param CustomerSession                  $_customerSession
     * @param TaxClassKeyInterfaceFactory      $_taxClassKeyFactory
     * @param GroupRepositoryInterface         $customerGroupRepository
     * @param QuoteDetailsItemInterfaceFactory $_quoteDetailsItemFactory
     * @param QuoteDetailsInterfaceFactory     $_quoteDetailsFactory
     * @param TaxCalculationInterface          $_taxCalculationService
     * @param AddressInterfaceFactory          $addressFactory
     * @param RegionInterfaceFactory           $regionFactory
     */
    public function __construct(
        PriceCurrencyInterface $priceCurrency,
        StoreManagerInterface $storeManager,
        CustomerSession $_customerSession,
        TaxClassKeyInterfaceFactory $_taxClassKeyFactory,
        GroupRepositoryInterface $customerGroupRepository,
        QuoteDetailsItemInterfaceFactory $_quoteDetailsItemFactory,
        QuoteDetailsInterfaceFactory $_quoteDetailsFactory,
        TaxCalculationInterface $_taxCalculationService,
        AddressInterfaceFactory $addressFactory,
        RegionInterfaceFactory $regionFactory
    ) {
        $this->priceCurrency = $priceCurrency;
        $this->storeManager = $storeManager;
        $this->customerSession = $_customerSession;
        $this->taxClassKeyFactory = $_taxClassKeyFactory;
        $this->customerGroupRepository = $customerGroupRepository;
        $this->quoteDetailsItemFactory = $_quoteDetailsItemFactory;
        $this->quoteDetailsFactory = $_quoteDetailsFactory;
        $this->taxCalculationService = $_taxCalculationService;
        $this->addressFactory = $addressFactory;
        $this->regionFactory = $regionFactory;
    }

    /**
     * Get product's final price with tax calculated
     *
     * @param SaleableInterface $product
     *
     * @return string
     */
    public function execute(SaleableInterface $product): string
    {
        if ($product->isComposite()) {
            return '';
        }

        $productSku = $product->getSku();

        if ($productSku && isset($this->priceBySku[$productSku])) {
            return $this->priceBySku[$productSku];
        }

        $amount = $product->getFinalPrice();
        $amountWithTax = $this->calculateTax($product, (float)$amount);
        $this->priceBySku[$productSku] = $this->priceCurrency->convertAndFormat($amountWithTax);

        return $this->priceBySku[$productSku];
    }

    /**
     * Calculate taxes for provided product
     *
     * @param SaleableInterface $product
     * @param float             $price
     *
     * @return float
     */
    private function calculateTax(SaleableInterface $product, float $price): float
    {
        $shippingAddressDataObject =
            $this->convertDefaultTaxAddress($this->customerSession->getDefaultTaxShippingAddress());
        $billingAddressDataObject =
            $this->convertDefaultTaxAddress($this->customerSession->getDefaultTaxBillingAddress());

        $taxClassKey = $this->taxClassKeyFactory->create();
        $taxClassKey->setType(TaxClassKeyInterface::TYPE_ID)->setValue($product->getTaxClassId());

        $ctc = null;
        if ($this->customerSession->getCustomerGroupId() != null) {
            $ctc =
                $this->customerGroupRepository->getById($this->customerSession->getCustomerGroupId())->getTaxClassId();
        }

        $customerTaxClassKey = $this->taxClassKeyFactory->create();
        $customerTaxClassKey->setType(TaxClassKeyInterface::TYPE_ID)->setValue($ctc);

        $item = $this->quoteDetailsItemFactory->create();
        $item->setQuantity(1)
            ->setCode($product->getSku())
            ->setShortDescription($product->getShortDescription())
            ->setTaxClassKey($taxClassKey)
            ->setIsTaxIncluded(false)
            ->setType('product')
            ->setUnitPrice($price);

        $quoteDetails = $this->quoteDetailsFactory->create();
        $quoteDetails->setShippingAddress($shippingAddressDataObject)
            ->setBillingAddress($billingAddressDataObject)
            ->setCustomerTaxClassKey($customerTaxClassKey)
            ->setItems([$item])
            ->setCustomerId($this->customerSession->getCustomerId());

        $store = $this->storeManager->getStore();
        $storeId = $store ? $store->getId() : null;

        $taxDetails = $this->taxCalculationService->calculateTax($quoteDetails, $storeId, true);
        $items = $taxDetails->getItems();
        $taxDetailsItem = array_shift($items);

        return (float)$taxDetailsItem->getPriceInclTax();
    }

    /**
     * Convert tax address array to address data object with country id and postcode
     *
     * @param array|null $taxAddress
     *
     * @return AddressInterface|null
     */
    private function convertDefaultTaxAddress(array $taxAddress = null): ?AddressInterface
    {
        if (empty($taxAddress)) {
            return null;
        }

        $addressDataObject = $this->addressFactory->create()->setCountryId($taxAddress['country_id'])->setPostcode(
            $taxAddress['postcode']
        );

        if (isset($taxAddress['region_id'])) {
            $addressDataObject->setRegion($this->regionFactory->create()->setRegionId($taxAddress['region_id']));
        }

        return $addressDataObject;
    }
}
