<?php

/**
 * @author    Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\Price\Service\Price;

use Logiscenter\Price\Api\GetPriceInterface;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Framework\Pricing\SaleableInterface;
use Magento\Msrp\Pricing\MsrpPriceCalculatorInterface;

/**
 * Service to retrieve product MSRP price
 */
class GetMsrpPrice implements GetPriceInterface
{
    /** @var PriceCurrencyInterface */
    private $priceCurrency;

    /** @var MsrpPriceCalculatorInterface */
    private $msrpCalculator;

    /**
     * @param PriceCurrencyInterface       $priceCurrency
     * @param MsrpPriceCalculatorInterface $msrpCalculator
     */
    public function __construct(
        PriceCurrencyInterface $priceCurrency,
        MsrpPriceCalculatorInterface $msrpCalculator
    ) {
        $this->priceCurrency = $priceCurrency;
        $this->msrpCalculator = $msrpCalculator;
    }

    /**
     * Get product's msrp price
     *
     * @param SaleableInterface $product
     *
     * @return string
     */
    public function execute(SaleableInterface $product): string
    {
        if (!$this->canShowMsrpPrice($product)) {
            return '';
        }
        $amount = $this->msrpCalculator->getMsrpPriceValue($product);

        return $this->priceCurrency->convertAndFormat($amount);
    }

    /**
     * Checks if allowed to show msrp price
     *
     * @param SaleableInterface $product
     *
     * @return bool
     */
    private function canShowMsrpPrice(SaleableInterface $product): bool
    {
        $msrp = $product->getMsrp();

        return $msrp
            && !$product->isComposite()
            && (float)$product->getFinalPrice() < (float)$msrp;
    }
}
