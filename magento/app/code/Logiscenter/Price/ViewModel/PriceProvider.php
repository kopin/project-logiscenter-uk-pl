<?php

/**
 * @author    Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\Price\ViewModel;

use Logiscenter\Price\Model\Price\Config;
use Logiscenter\Price\Service\Price\GetFinalPrice;
use Logiscenter\Price\Service\Price\GetFinalPriceWithTax;
use Logiscenter\Price\Service\Price\GetMsrpPrice;
use Logiscenter\PriceConfig\Api\Config\PriceModuleStatusInterface;
use Logiscenter\ZeroPriceRestriction\Model\PriceRestriction;
use Magento\Catalog\Helper\Data;
use Magento\Framework\View\Element\Block\ArgumentInterface;

/**
 * View model to provide prices
 */
class PriceProvider implements ArgumentInterface
{
    /**
     * @var GetFinalPrice
     */
    private $getFinalPrice;

    /**
     * @var GetFinalPriceWithTax
     */
    private $getFinalPriceWithTax;

    /**
     * @var GetMsrpPrice
     */
    private $getMsrpPrice;

    /**
     * @var Data
     */
    private $productHelper;

    /**
     * @var PriceModuleStatusInterface
     */
    private $statusConfig;

    /**
     * @var PriceRestriction
     */
    private $priceRestriction;

    /**
     * @var Config
     */
    private $priceConfig;

    /**
     * @param Data                       $productHelper
     * @param PriceModuleStatusInterface $statusConfig
     * @param GetFinalPrice              $getFinalPrice
     * @param GetFinalPriceWithTax       $getFinalPriceWithTax
     * @param GetMsrpPrice               $getMsrpPrice
     * @param PriceRestriction           $priceRestriction
     * @param Config                     $priceConfig
     */
    public function __construct(
        Data $productHelper,
        PriceModuleStatusInterface $statusConfig,
        GetFinalPrice $getFinalPrice,
        GetFinalPriceWithTax $getFinalPriceWithTax,
        GetMsrpPrice $getMsrpPrice,
        PriceRestriction $priceRestriction,
        Config $priceConfig
    ) {
        $this->productHelper = $productHelper;
        $this->statusConfig = $statusConfig;
        $this->getFinalPrice = $getFinalPrice;
        $this->getFinalPriceWithTax = $getFinalPriceWithTax;
        $this->getMsrpPrice = $getMsrpPrice;
        $this->priceRestriction = $priceRestriction;
        $this->priceConfig = $priceConfig;
    }

    /**
     * Returns msrp product price if allowed
     *
     * @return string
     */
    public function getMsrpPrice($product = null): string
    {
        if ($product == null) {
            $product = $this->productHelper->getProduct();
        }

        if (!$product || !$this->priceConfig->canShowMsrpPrice($product)) {
            return '';
        }

        return $this->getMsrpPrice->execute($product);
    }

    /**
     * Returns final product price
     *
     * @return string
     */
    public function getFinalPrice($product = null): string
    {
        if ($product == null) {
            $product = $this->productHelper->getProduct();
        }

        if (!$product || !$this->statusConfig->isEnabled() || !$this->priceRestriction->canShowPrice($product)) {
            return '';
        }

        return $this->getFinalPrice->execute($product);
    }

    /**
     * Returns final price with tax calculated
     *
     * @return string
     */
    public function getFinalPriceWithTax($product = null): string
    {
        if ($product == null) {
            $product = $this->productHelper->getProduct();
        }

        if (!$product || !$this->priceConfig->canShowFinalPriceInclTax($product)) {
            return '';
        }

        return $this->getFinalPriceWithTax->execute($product);
    }
}
