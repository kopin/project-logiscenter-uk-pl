<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ErpInvoice\Ui\Component\Listing;

use Logiscenter\ErpInvoice\Model\ResourceModel\Invoice\Collection as GridCollection;
use Logiscenter\ErpInvoice\Model\ResourceModel\Invoice\CollectionFactory;
use Logiscenter\ErpInvoiceConfig\Api\Config\ErpInvoiceModuleStatusInterface;
use Magento\Framework\Api\Filter;
use Magento\Framework\App\RequestInterface;
use Magento\Ui\DataProvider\AbstractDataProvider;

/**
 * Custom DataProvider for customer invoice listing
 */
class DataProvider extends AbstractDataProvider
{
    /**
     * @var RequestInterface $request,
     */
    private $request;

    /**
     * @var ErpInvoiceModuleStatusInterface
     */
    private $statusConfig;

    /**
     * @param string                          $name
     * @param string                          $primaryFieldName
     * @param string                          $requestFieldName
     * @param CollectionFactory               $collectionFactory
     * @param RequestInterface                $request
     * @param ErpInvoiceModuleStatusInterface $statusConfig
     * @param array                           $meta
     * @param array                           $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $collectionFactory,
        RequestInterface $request,
        ErpInvoiceModuleStatusInterface $statusConfig,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->collection = $collectionFactory->create();
        $this->request = $request;
        $this->statusConfig = $statusConfig;
    }

    /**
     * Prepare data for invoice listing
     *
     * @return array
     */
    public function getData(): array
    {
        /** @var GridCollection $collection */
        $collection = $this->getCollection();
        $data['items'] = [];
        if ($this->request->getParam('customer_id') && $this->statusConfig->isEnabled()) {
            $collection->addFieldToFilter('customer_id', $this->request->getParam('customer_id'));
            $data = $collection->toArray();
        }

        return $data;
    }

    /**
     * Add full text search filter to collection
     *
     * @param Filter $filter
     * @return void
     */
    public function addFilter(Filter $filter): void
    {
        /** @var GridCollection $collection */
        $collection = $this->getCollection();
        if ($filter->getField() === 'fulltext') {
            $collection->addFullTextFilter(trim($filter->getValue()));
            return;
        }
        $collection->addFieldToFilter(
            $filter->getField(),
            [$filter->getConditionType() => $filter->getValue()]
        );
    }
}
