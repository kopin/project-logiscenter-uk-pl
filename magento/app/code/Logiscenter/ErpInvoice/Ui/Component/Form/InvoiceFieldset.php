<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ErpInvoice\Ui\Component\Form;

use Logiscenter\ErpInvoiceConfig\Api\Config\ErpInvoiceModuleStatusInterface;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\ComponentVisibilityInterface;
use Magento\Ui\Component\Form\Fieldset;

/**
 * Erp invoice fieldset class
 */
class InvoiceFieldset extends Fieldset implements ComponentVisibilityInterface
{
    /**
     * @var ErpInvoiceModuleStatusInterface
     */
    private $statusConfig;

    /**
     * @param ContextInterface                $context
     * @param ErpInvoiceModuleStatusInterface $statusConfig
     * @param array                           $components
     * @param array                           $data
     */
    public function __construct(
        ContextInterface $context,
        ErpInvoiceModuleStatusInterface $statusConfig,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($context, $components, $data);
        $this->context = $context;
        $this->statusConfig = $statusConfig;
    }

    /**
     * Can show invoice tab in tabs or not
     *
     * Will return false for not registered customer in a case when admin user created new customer account.
     * Needed to hide invoice tab from create new customer page
     *
     * @return boolean
     */
    public function isComponentVisible(): bool
    {
        $customerId = (bool)$this->context->getRequestParam('id');
        return $customerId && $this->statusConfig->isEnabled();
    }
}
