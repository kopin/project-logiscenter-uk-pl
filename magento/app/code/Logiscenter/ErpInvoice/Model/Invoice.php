<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ErpInvoice\Model;

use Logiscenter\ErpInvoice\Api\Data\InvoiceInterface;
use Logiscenter\ErpInvoice\Model\ResourceModel\Invoice as InvoiceResource;
use Magento\Framework\Model\AbstractModel;

/**
 * Erp invoice entity model class
 */
class Invoice extends AbstractModel implements InvoiceInterface
{
    /**
     * Initialize corresponding resource model
     *
     * @return void
     * @codingStandardsIgnoreStart
     */
    protected function _construct()
    {
        // @codingStandardsIgnoreEnd
        $this->_init(InvoiceResource::class);
    }

    /**
     * @inheritDoc
     */
    public function getInvoiceId(): string
    {
        return $this->getData(self::FIELD_ID);
    }

    /**
     * @inheritDoc
     */
    public function setInvoiceId(string $invoiceId): InvoiceInterface
    {
        return $this->setData(self::FIELD_ID, $invoiceId);
    }

    /**
     * @inheritDoc
     */
    public function getCustomerId(): int
    {
        return (int)$this->getData(self::FIELD_CUSTOMER_ID);
    }

    /**
     * @inheritDoc
     */
    public function setCustomerId(int $customerId): InvoiceInterface
    {
        return $this->setData(self::FIELD_CUSTOMER_ID, $customerId);
    }

    /**
     * @inheritDoc
     */
    public function getTracking(): string
    {
        return $this->getData(self::FIELD_TRACKING);
    }

    /**
     * @inheritDoc
     */
    public function setTracking(string $tracking): InvoiceInterface
    {
        return $this->setData(self::FIELD_TRACKING, $tracking);
    }

    /**
     * @inheritDoc
     */
    public function getFile(): string
    {
        return (string)$this->getData(self::FIELD_FILE);
    }

    /**
     * @inheritDoc
     */
    public function setFile(string $file): InvoiceInterface
    {
        return $this->setData(self::FIELD_FILE, $file);
    }

    /**
     * @inheritDoc
     */
    public function getDate(): string
    {
        return (string)$this->getData(self::FIELD_DATE);
    }

    /**
     * @inheritDoc
     */
    public function setDate(string $date): InvoiceInterface
    {
        return $this->setData(self::FIELD_DATE, $date);
    }

    /**
     * @inheritDoc
     */
    public function getSendEmail(): bool
    {
        return (bool)$this->getData(self::FIELD_SEND_EMAIL);
    }

    /**
     * @inheritDoc
     */
    public function setSendEmail(bool $sendEmail): InvoiceInterface
    {
        return $this->setData(self::FIELD_SEND_EMAIL, $sendEmail);
    }
}
