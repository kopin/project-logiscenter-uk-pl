<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ErpInvoice\Model\Invoice;

use Logiscenter\ErpInvoice\Model\Invoice;
use Logiscenter\ErpInvoice\Model\ResourceModel\Invoice\CollectionFactory;
use Logiscenter\ErpInvoiceConfig\Api\Config\ErpInvoiceFilesInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\File\Mime;
use Magento\Framework\Filesystem\Directory\Write;
use Magento\Framework\Filesystem\Directory\WriteFactory;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Ui\DataProvider\AbstractDataProvider;

class DataProvider extends AbstractDataProvider
{
    /**
     * @var array
     */
    private $loadedData;

    /**
     * @var ContextInterface
     */
    private $context;

    /**
     * @var Write
     */
    private $uploadDir;

    /**
     * @var Mime
     */
    private $mime;

    /**
     * DataProvider constructor.
     *
     * @param string                   $name
     * @param string                   $primaryFieldName
     * @param string                   $requestFieldName
     * @param CollectionFactory        $collectionFactory
     * @param ContextInterface         $context
     * @param WriteFactory             $writeFactory
     * @param DirectoryList            $directoryList
     * @param ErpInvoiceFilesInterface $filesConfig
     * @param Mime                     $mime
     * @param array                    $meta
     * @param array                    $data
     *
     * @throws FileSystemException
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $collectionFactory,
        ContextInterface $context,
        WriteFactory $writeFactory,
        DirectoryList $directoryList,
        ErpInvoiceFilesInterface $filesConfig,
        Mime $mime,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->collection = $collectionFactory->create();
        $this->context = $context;
        $this->uploadDir = $writeFactory->create(
            $directoryList->getPath(DirectoryList::VAR_DIR) . DIRECTORY_SEPARATOR . $filesConfig->getUploadPath()
        );
        $this->mime = $mime;
    }

    /**
     * Get invoice data
     *
     * @return array
     */
    public function getData(): array
    {
        if (null !== $this->loadedData) {
            return $this->loadedData;
        }
        $items = $this->collection->getItems();
        /** @var Invoice $item */
        foreach ($items as $item) {
            $invoiceId = $item->getEntityId();
            $this->loadedData[$invoiceId] = $item->getData();
            $this->loadedData[$invoiceId]['file'] = $this->overrideFileUploaderData($this->loadedData[$invoiceId]);
        }

        if (null === $this->loadedData) {
            $this->loadedData[''] = $this->getDefaultData();
        }

        return $this->loadedData;
    }

    /**
     * Get default customer data for adding new invoice
     *
     * @return array
     */
    private function getDefaultData(): array
    {
        $customerId = $this->context->getRequestParam('customer_id');

        return [
            'customer_id' => $customerId,
        ];
    }

    /**
     * Prepare file data for uploader
     *
     * @return array
     */
    private function overrideFileUploaderData(array $data): array
    {
        $fileName = $data['file'] ?? null;
        $filePath = $this->uploadDir->getAbsolutePath($data['customer_id'] . DIRECTORY_SEPARATOR . $data['file']);
        if (!empty($fileName) && $this->uploadDir->isExist($filePath)) {
            $stat = $this->uploadDir->stat($filePath);

            return [
                [
                    'file' => $fileName,
                    'size' => null !== $stat
                        ? $stat['size']
                        : 0,
                    'url'  => '',
                    // phpcs:ignore Magento2.Functions.DiscouragedFunction
                    'name' => basename($fileName),
                    'type' => $this->mime->getMimeType($filePath),
                ],
            ];
        }

        return [];
    }
}
