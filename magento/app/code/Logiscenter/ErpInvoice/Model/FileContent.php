<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ErpInvoice\Model;

use Logiscenter\ErpInvoice\Api\FileContentInterface;
use Logiscenter\ErpInvoiceConfig\Api\Config\ErpInvoiceFilesInterface;
use Logiscenter\ErpInvoiceConfig\Api\Config\ErpInvoiceModuleStatusInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;

/**
 * Erp invoice file content manage class
 */
class FileContent implements FileContentInterface
{
    /**
     * @var Filesystem\Directory\ReadInterface
     */
    private $varDir;

    /**
     * @var ErpInvoiceFilesInterface
     */
    private $filesConfig;

    /**
     * @var ErpInvoiceModuleStatusInterface
     */
    private $statusConfig;

    /**
     * @param Filesystem                      $filesystem
     * @param ErpInvoiceFilesInterface        $filesConfig
     * @param ErpInvoiceModuleStatusInterface $statusConfig
     */
    public function __construct(
        Filesystem $filesystem,
        ErpInvoiceFilesInterface $filesConfig,
        ErpInvoiceModuleStatusInterface $statusConfig
    ) {
        $this->varDir = $filesystem->getDirectoryRead(DirectoryList::VAR_DIR);
        $this->filesConfig = $filesConfig;
        $this->statusConfig = $statusConfig;
    }

    /**
     * @inheridoc
     */
    public function getByFileNameAndCustomerId(string $name, int $customerId): string
    {
        if (!$this->statusConfig->isEnabled()) {
            return '';
        }
        $uploadPath = $this->filesConfig->getUploadPath();
        $filePath = $this->varDir->getAbsolutePath(
            $uploadPath . DIRECTORY_SEPARATOR . $customerId . DIRECTORY_SEPARATOR . $name
        );
        if ($this->varDir->isExist($filePath)) {
            return $this->varDir->readFile($filePath);
        }

        return '';
    }
}
