<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ErpInvoice\Model;

use Exception;
use Logiscenter\ErpInvoice\Api\Data\InvoiceInterface;
use Logiscenter\ErpInvoice\Api\InvoiceRepositoryInterface;
use Logiscenter\ErpInvoice\Model\ResourceModel\Invoice as InvoiceResource;
use Logiscenter\ErpInvoice\Model\ResourceModel\Invoice\CollectionFactory;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Api\SearchResultsInterfaceFactory;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\StateException;

/**
 * Erp invoice repository class
 */
class InvoiceRepository implements InvoiceRepositoryInterface
{
    /**
     * @var InvoiceFactory
     */
    private $invoiceFactory;

    /**
     * @var InvoiceResource
     */
    private $invoiceResource;

    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var CollectionProcessorInterface
     */
    private $collectionProcessor;

    /**
     * @var SearchResultsInterfaceFactory
     */
    private $searchResultsFactory;

    /**
     * @param CustomerRepositoryInterface   $customerRepository
     * @param InvoiceFactory                $invoiceFactory
     * @param InvoiceResource               $invoiceResource
     * @param CollectionFactory             $collectionFactory
     * @param CollectionProcessorInterface  $collectionProcessor
     * @param SearchResultsInterfaceFactory $searchResultsFactory
     */
    public function __construct(
        CustomerRepositoryInterface $customerRepository,
        InvoiceFactory $invoiceFactory,
        InvoiceResource $invoiceResource,
        CollectionFactory $collectionFactory,
        CollectionProcessorInterface $collectionProcessor,
        SearchResultsInterfaceFactory $searchResultsFactory
    ) {
        $this->customerRepository = $customerRepository;
        $this->invoiceFactory = $invoiceFactory;
        $this->invoiceResource = $invoiceResource;
        $this->collectionFactory = $collectionFactory;
        $this->collectionProcessor = $collectionProcessor;
        $this->searchResultsFactory = $searchResultsFactory;
    }


    /**
     * @inheritDoc
     */
    public function save(InvoiceInterface $invoice): InvoiceInterface
    {
        try {
            $this->customerRepository->getById($invoice->getCustomerId());
        } catch (NoSuchEntityException $exception) {
            throw NoSuchEntityException::singleField('customer_id', $invoice->getCustomerId());
        }

        if ($invoice->getId()) {
            /** @var Invoice $existingInvoice */
            $existingInvoice = $this->invoiceFactory->create();
            $this->invoiceResource->load($existingInvoice, $invoice->getId());

            if (!$invoice->getId()) {
                throw NoSuchEntityException::singleField('entity_id', $invoice->getId());
            }
            if ($existingInvoice->getCustomerId() != $invoice->getCustomerId()) {
                throw new StateException(
                    __("The invoice doesn't belong to the provided customer.")
                );
            }
        }

        try {
            $this->invoiceResource->save($invoice);
        } catch (Exception $e) {
            throw new StateException(__("The invoice can't be saved."));
        }

        return $invoice;
    }

    /**
     * @inheritDoc
     */
    public function get(int $invoiceId): InvoiceInterface
    {
        /** @var Invoice $invoice */
        $invoice = $this->invoiceFactory->create();
        $this->invoiceResource->load($invoice, $invoiceId);
        if (!$invoice->getId()) {
            throw new NoSuchEntityException(
                __('The invoice with the "%1" ID doesn\'t exist. Verify the ID and try again.', $invoiceId)
            );
        }

        return $invoice;
    }

    /**
     * @inheritDoc
     */
    public function getList(SearchCriteriaInterface $searchCriteria): SearchResultsInterface
    {
        $collection = $this->collectionFactory->create();
        $this->collectionProcessor->process($searchCriteria, $collection);

        /** @var SearchResultsInterface $searchResult */
        $searchResult = $this->searchResultsFactory->create();
        $searchResult->setSearchCriteria($searchCriteria);
        $searchResult->setItems($collection->getItems());
        $searchResult->setTotalCount($collection->getSize());

        return $searchResult;
    }

    /**
     * @inheritDoc
     */
    public function deleteById(int $invoiceId): bool
    {
        $this->delete(
            $this->get($invoiceId)
        );
        return true;
    }

    /**
     * @inheritDoc
     */
    public function delete(InvoiceInterface $invoice): bool
    {
        try {
            $this->invoiceResource->delete($invoice);
        } catch (\Exception $e) {
            throw new StateException(
                __(
                    'The invoice with id "%1" can\'t be deleted.',
                    $invoice->getId()
                ),
                $e
            );
        }
        return true;
    }
}
