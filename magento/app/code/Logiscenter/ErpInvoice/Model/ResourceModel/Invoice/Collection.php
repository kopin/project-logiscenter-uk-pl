<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ErpInvoice\Model\ResourceModel\Invoice;

use Logiscenter\ErpInvoice\Api\Data\InvoiceInterface;
use Logiscenter\ErpInvoice\Model\Invoice;
use Logiscenter\ErpInvoice\Model\ResourceModel\Invoice as InvoiceResource;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Erp invoice collection class
 */
class Collection extends AbstractCollection
{
    /**
     * List of fields to fulltext search
     */
    private const FIELDS_TO_FULLTEXT_SEARCH = [
        InvoiceInterface::FIELD_ID,
        InvoiceInterface::FIELD_CUSTOMER_ID,
        InvoiceInterface::FIELD_TRACKING,
        InvoiceInterface::FIELD_FILE,
        InvoiceInterface::FIELD_DATE,
    ];

    /**
     * Init models for collection
     *
     * @return void
     * @codingStandardsIgnoreStart
     */
    protected function _construct()
    {
        // @codingStandardsIgnoreEnd
        $this->_init(Invoice::class, InvoiceResource::class);
    }

    /**
     * Add fulltext filter
     *
     * @param string $value
     * @return $this
     */
    public function addFullTextFilter(string $value): self
    {
        $fields = self::FIELDS_TO_FULLTEXT_SEARCH;
        $whereCondition = '';
        foreach ($fields as $key => $field) {
            $field = 'main_table.' . $field;
            $condition = $this->_getConditionSql(
                $this->getConnection()->quoteIdentifier($field),
                ['like' => "%$value%"]
            );
            $whereCondition .= ($key === 0 ? '' : ' OR ') . $condition;
        }
        if ($whereCondition) {
            $this->getSelect()->where($whereCondition);
        }

        return $this;
    }
}
