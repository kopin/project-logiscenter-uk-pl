<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ErpInvoice\Model\ResourceModel\Invoice;

use Interactiv4\Contracts\Factory\Api\FactoryInterface;
use Interactiv4\Contracts\Factory\Api\ObjectFactoryInterface;

/**
 * Factory class for @see \Logiscenter\ErpInvoice\Model\ResourceModel\Invoice\Collection.
 */
class CollectionFactory implements FactoryInterface
{
    /**
     * Object factory instance.
     *
     * @var ObjectFactoryInterface
     */
    private $objectFactory;

    /**
     * Instance name to create.
     *
     * @var string
     */
    private $instanceName = '\\Logiscenter\\ErpInvoice\\Model\\ResourceModel\\Invoice\\Collection';

    /**
     * Factory constructor.
     *
     * @param ObjectFactoryInterface $objectFactory
     * @param string|null            $instanceName
     */
    public function __construct(ObjectFactoryInterface $objectFactory, string $instanceName = null)
    {
        $this->objectFactory = $objectFactory;
        $this->instanceName = $instanceName ?? $this->instanceName;
    }

    /**
     * Create class instance with specified parameters.
     *
     * @param array $arguments
     * @return Collection
     */
    public function create(array $arguments = [])
    {
        return $this->objectFactory->create($this->instanceName, $arguments);
    }
}
