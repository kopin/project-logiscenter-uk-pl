<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ErpInvoice\Model\ResourceModel;

use Logiscenter\ErpInvoice\Api\Data\InvoiceInterface;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Erp invoice resource model class
 */
class Invoice extends AbstractDb
{
    /**
     * @inheritDoc
     * @codingStandardsIgnoreStart
     */
    protected function _construct()
    {
        // @codingStandardsIgnoreEnd
        $this->_init(InvoiceInterface::INVOICE_TABLE_NAME, InvoiceInterface::FIELD_ENTITY_ID);
    }
}
