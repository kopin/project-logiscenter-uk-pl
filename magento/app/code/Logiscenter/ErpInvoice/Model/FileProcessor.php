<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ErpInvoice\Model;

use Exception;
use Logiscenter\ErpInvoiceConfig\Api\Config\ErpInvoiceFilesInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\File\Uploader;
use Magento\Framework\File\UploaderFactory;
use Magento\Framework\Filesystem;
use Magento\Framework\Filesystem\Directory\WriteInterface;

class FileProcessor
{
    private const ALLOWED_EXTENSIONS = [
        'pdf',
    ];

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var UploaderFactory
     */
    private $uploaderFactory;

    private $tmpDirectory;

    /**
     * @var ErpInvoiceFilesInterface
     */
    private $erpInvoiceFiles;

    /**
     * @var WriteInterface
     */
    private $varDirectory;

    /**
     * @param Filesystem               $filesystem
     * @param UploaderFactory          $uploaderFactory
     * @param ErpInvoiceFilesInterface $erpInvoiceFiles
     */
    public function __construct(
        Filesystem $filesystem,
        UploaderFactory $uploaderFactory,
        ErpInvoiceFilesInterface $erpInvoiceFiles
    ) {
        $this->filesystem = $filesystem;
        $this->uploaderFactory = $uploaderFactory;
        $this->erpInvoiceFiles = $erpInvoiceFiles;
    }

    /**
     * @throws FileSystemException
     */
    public function uploadToTmpDir()
    {
        $result = ['file' => '', 'size' => '', 'type' => '', 'cssWidth' => '', 'cssHeight' => ''];
        $varTmpDirectory = $this->getTmpDirectory()->getAbsolutePath();

        /** @var \Magento\MediaStorage\Model\File\Uploader $uploader */
        $uploader = $this->uploaderFactory->create(['fileId' => 'file']);
        $uploader->setAllowRenameFiles(true)->setFilesDispersion(false)->setAllowedExtensions(
            self::ALLOWED_EXTENSIONS
        );

        return array_intersect_key($uploader->save($varTmpDirectory), $result);
    }

    /**
     * @throws FileSystemException
     * @throws LocalizedException
     */
    public function moveFromTmpToBaseDir($fileName, $customerId)
    {
        if (empty($fileName)) {
            return null;
        }
        $baseTmpPath = $this->getTmpDirectory()->getAbsolutePath();
        $basePath = $this->getBasePath($customerId);

        $baseFilePath = $basePath . Uploader::getNewFileName(
            $this->varDirectory->getAbsolutePath(
                $basePath . $fileName
            )
        );
        $baseTmpFilePath = $baseTmpPath . $fileName;

        try {
            if (file_exists($baseTmpFilePath)) {
                $this->varDirectory->renameFile(
                    $baseTmpFilePath,
                    $baseFilePath
                );
            }
        } catch (Exception $e) {
            throw new LocalizedException(
                __('Something went wrong while saving the file(s).')
            );
        }

        return $fileName;
    }

    /**
     * @param string $fileName
     * @param int    $customerId
     *
     * @throws FileSystemException
     */
    public function deleteExistingInvoice(string $fileName, int $customerId)
    {
        $filePath = $this->getBasePath($customerId) . $fileName;
        if ($this->varDirectory->isFile($filePath)) {
            $this->varDirectory->delete($filePath);
        }
    }

    /**
     * @return WriteInterface
     * @throws FileSystemException
     */
    private function getTmpDirectory()
    {
        if ($this->tmpDirectory === null) {
            $this->tmpDirectory = $this->filesystem->getDirectoryWrite(DirectoryList::TMP);
        }

        return $this->tmpDirectory;
    }

    /**
     * @return WriteInterface
     * @throws FileSystemException
     */
    private function getVarDirectory()
    {
        if ($this->varDirectory === null) {
            $this->varDirectory = $this->filesystem->getDirectoryWrite(DirectoryList::VAR_DIR);
        }

        return $this->varDirectory;
    }

    /**
     * @param $customerId
     *
     * @return string
     * @throws FileSystemException
     */
    private function getBasePath($customerId): string
    {
        $basePath = $this->getVarDirectory()->getAbsolutePath(
            $this->erpInvoiceFiles->getUploadPath() . $customerId . DIRECTORY_SEPARATOR
        );

        return $basePath;
    }
}
