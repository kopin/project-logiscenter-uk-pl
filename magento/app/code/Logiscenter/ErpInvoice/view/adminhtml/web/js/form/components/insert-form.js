/**
 * @author    Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

define([
    'Magento_Ui/js/form/components/insert-form'
], function (Insert) {
    'use strict';

    return Insert.extend({
        defaults: {
            listens: {
                responseData: 'onResponse'
            },
            modules: {
                erpinvoiceListing: '${ $.erpinvoiceListingProvider }',
                erpinvoiceModal: '${ $.erpinvoiceModalProvider }'
            }
        },

        /**
         * Close modal, reload customer invoice listing and save customer invoice
         *
         * @param {Object} responseData
         */
        onResponse: function (responseData) {
            var data;

            if (!responseData.error) {
                this.erpinvoiceModal().closeModal();
                this.erpinvoiceListing().reload({
                    refresh: true
                });
                data = this.externalSource().get('data');
                this.saveErpinvoice(responseData, data);
            }
        },

        /**
         * Save customer invoice to customer form data source
         *
         * @param {Object} responseData
         * @param {Object} data - customer erp invoice
         */
        saveErpinvoice: function (responseData, data) {
            data['entity_id'] = responseData.data['entity_id'];
        },
    });
});
