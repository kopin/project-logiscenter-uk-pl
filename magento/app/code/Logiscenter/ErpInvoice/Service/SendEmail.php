<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ErpInvoice\Service;

use Logiscenter\ErpInvoiceConfig\Api\Config\ErpInvoiceModuleStatusInterface;
use Logiscenter\ErpInvoiceConfig\Api\Config\ErpInvoiceNotificationInterface;
use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Customer\Helper\View;
use Magento\Customer\Model\CustomerRegistry;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\MailException;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\Reflection\DataObjectProcessor;

/**
 * Erp invoice send email service class
 */
class SendEmail
{
    /**
     * @var ErpInvoiceNotificationInterface
     */
    private $notificationConfig;

    /**
     * @var TransportBuilder
     */
    private $transportBuilder;

    /**
     * @var View
     */
    private $customerViewHelper;

    /**#@-*/
    private $customerRegistry;

    /**
     * @var DataObjectProcessor
     */
    protected $dataProcessor;

    /**
     * @var ErpInvoiceModuleStatusInterface
     */
    private $statusConfig;

    /**
     * @param ErpInvoiceNotificationInterface $notificationConfig
     * @param TransportBuilder                $transportBuilder
     * @param View                            $customerViewHelper
     * @param CustomerRegistry                $customerRegistry
     * @param DataObjectProcessor             $dataProcessor
     * @param ErpInvoiceModuleStatusInterface $statusConfig
     */
    public function __construct(
        ErpInvoiceNotificationInterface $notificationConfig,
        TransportBuilder $transportBuilder,
        View $customerViewHelper,
        CustomerRegistry $customerRegistry,
        DataObjectProcessor $dataProcessor,
        ErpInvoiceModuleStatusInterface $statusConfig
    ) {
        $this->notificationConfig = $notificationConfig;
        $this->transportBuilder = $transportBuilder;
        $this->customerViewHelper = $customerViewHelper;
        $this->customerRegistry = $customerRegistry;
        $this->dataProcessor = $dataProcessor;
        $this->statusConfig = $statusConfig;
    }

    /**
     * Send customer erp invoice email
     *
     * @param CustomerInterface $customer
     *
     * @throws MailException
     * @throws LocalizedException
     */
    public function execute(CustomerInterface $customer): void
    {
        if (!$this->statusConfig->isEnabled()) {
            return;
        }
        $storeId = $customer->getStoreId();
        $templateId = $this->notificationConfig->getEmailTemplate($storeId);
        $email = $customer->getEmail();

        $customerEmailData = $this->getFullCustomerObject($customer);

        $from = $this->notificationConfig->getEmailSender($storeId);

        $transport = $this->transportBuilder
            ->setTemplateIdentifier($templateId)
            ->setTemplateOptions(['area' => 'frontend', 'store' => $storeId])
            ->setTemplateVars(['customer' => $customerEmailData])
            ->setFromByScope($from)
            ->addTo($email, $this->customerViewHelper->getCustomerName($customer))
            ->getTransport();

        $transport->sendMessage();
    }

    /**
     * Create an object with data merged from Customer and CustomerSecure
     *
     * @param CustomerInterface $customer
     * @return \Magento\Customer\Model\Data\CustomerSecure
     */
    private function getFullCustomerObject($customer)
    {
        // No need to flatten the custom attributes or nested objects since the only usage is for email templates and
        // object passed for events
        $mergedCustomerData = $this->customerRegistry->retrieveSecureData($customer->getId());
        $customerData = $this->dataProcessor
            ->buildOutputDataArray($customer, \Magento\Customer\Api\Data\CustomerInterface::class);
        $mergedCustomerData->addData($customerData);
        $mergedCustomerData->setData('name', $this->customerViewHelper->getCustomerName($customer));
        return $mergedCustomerData;
    }
}
