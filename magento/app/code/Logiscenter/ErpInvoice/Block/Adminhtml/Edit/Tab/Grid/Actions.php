<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ErpInvoice\Block\Adminhtml\Edit\Tab\Grid;

use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;

/**
 * Prepare actions column for customer erp invoice grid
 */
class Actions extends Column
{
    const ERPINVOICE_PATH_DELETE = 'erpinvoice/invoice/delete';

    /**
     * @var UrlInterface
     */
    private $urlBuilder;

    /**
     * @param ContextInterface   $context
     * @param UiComponentFactory $uiComponentFactory
     * @param UrlInterface       $urlBuilder
     * @param array              $components
     * @param array              $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        UrlInterface $urlBuilder,
        array $components = [],
        array $data = []
    ) {
        $this->urlBuilder = $urlBuilder;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     *
     * @return array
     */
    public function prepareDataSource(array $dataSource): array
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$item) {
                $name = $this->getData('name');
                if (isset($item['entity_id'])) {
                    $item[$name]['edit'] = [
                        'callback' => [
                            [
                                'provider' => 'customer_form.areas.erpinvoice.erpinvoice' .
                                    '.erpinvoice_update_modal.update_erpinvoice_form_loader',
                                'target'   => 'destroyInserted',
                            ],
                            [
                                'provider' => 'customer_form.areas.erpinvoice.erpinvoice' . '.erpinvoice_update_modal',
                                'target'   => 'openModal',
                            ],
                            [
                                'provider' => 'customer_form.areas.erpinvoice.erpinvoice' .
                                    '.erpinvoice_update_modal.update_erpinvoice_form_loader',
                                'target'   => 'render',
                                'params'   => [
                                    'entity_id' => $item['entity_id'],
                                ],
                            ],
                        ],
                        'href'     => '#',
                        'label'    => __('Edit'),
                        'hidden'   => false,
                    ];
                    $item[$name]['delete'] = [
                        'href'    => $this->urlBuilder->getUrl(
                            self::ERPINVOICE_PATH_DELETE,
                            ['customer_id' => $item['customer_id'], 'id' => $item['entity_id']]
                        ),
                        'label'   => __('Delete'),
                        'isAjax'  => true,
                        'confirm' => [
                            'title'   => __('Delete invoice'),
                            'message' => __('Are you sure you want to delete the customer erp invoice?'),
                        ],
                    ];
                }
            }
        }

        return $dataSource;
    }
}
