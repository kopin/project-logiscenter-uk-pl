<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ErpInvoice\Api\Data;

/**
 * Erp Invoice interface
 */
interface InvoiceInterface
{
    public const INVOICE_TABLE_NAME = 'logiscenter_invoice';
    public const FIELD_ENTITY_ID    = 'entity_id';
    public const FIELD_ID           = 'invoice_id';
    public const FIELD_CUSTOMER_ID  = 'customer_id';
    public const FIELD_TRACKING     = 'tracking';
    public const FIELD_FILE         = 'file';
    public const FIELD_DATE         = 'date';
    public const FIELD_SEND_EMAIL   = 'send_email';

    /**
     * @return int
     */
    public function getId();

    /**
     * @param int $id
     *
     * @return $this
     */
    public function setId(int $id);

    /**
     * @return string
     */
    public function getInvoiceId(): string;

    /**
     * @param string $invoiceId
     *
     * @return $this
     */
    public function setInvoiceId(string $invoiceId): self;

    /**
     * @return int
     */
    public function getCustomerId(): int;

    /**
     * @param int $customerId
     *
     * @return $this
     */
    public function setCustomerId(int $customerId): self;

    /**
     * @return string
     */
    public function getTracking(): string;

    /**
     * @param string $tracking
     *
     * @return $this
     */
    public function setTracking(string $tracking): self;

    /**
     * @return string
     */
    public function getFile(): string;

    /**
     * @param string $file
     *
     * @return $this
     */
    public function setFile(string $file): self;

    /**
     * @return string
     */
    public function getDate(): string;

    /**
     * @param string $date
     *
     * @return $this
     */
    public function setDate(string $date): self;

    /**
     * @return bool
     */
    public function getSendEmail(): bool;

    /**
     * @param bool $sendEmail
     *
     * @return $this
     */
    public function setSendEmail(bool $sendEmail): self;
}
