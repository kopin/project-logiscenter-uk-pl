<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ErpInvoice\Api;

/**
 * Invoice file content manage interface
 */
interface FileContentInterface
{
    /**
     * Return file content by customer id and file name
     *
     * @param string $name
     * @param int    $customerId
     *
     * @return string
     */
    public function getByFileNameAndCustomerId(string $name, int $customerId): string;
}
