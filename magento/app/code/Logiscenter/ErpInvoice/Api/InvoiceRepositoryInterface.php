<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ErpInvoice\Api;

use Logiscenter\ErpInvoice\Api\Data\InvoiceInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\StateException;

/**
 * Erp invoice repository interface
 */
interface InvoiceRepositoryInterface
{
    /**
     * Save invoice
     *
     * @param InvoiceInterface $invoice
     *
     * @return InvoiceInterface
     * @throws NoSuchEntityException
     * @throws StateException
     */
    public function save(InvoiceInterface $invoice): InvoiceInterface;

    /**
     * Retrieve invoice
     *
     * @param int $invoiceId
     *
     * @return InvoiceInterface
     * @throws NoSuchEntityException
     */
    public function get(int $invoiceId): InvoiceInterface;

    /**
     * Retrieve list of invoices
     *
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return SearchResultsInterface
     * @throws NoSuchEntityException
     */
    public function getList(SearchCriteriaInterface $searchCriteria): SearchResultsInterface;

    /**
     * Remove invoice by id
     *
     * @param int $invoiceId
     *
     * @return bool
     * @throws NoSuchEntityException
     * @throws StateException
     */
    public function deleteById(int $invoiceId): bool;

    /**
     * Remove invoice
     *
     * @param InvoiceInterface $invoice
     *
     * @return bool
     * @throws NoSuchEntityException
     * @throws StateException
     */
    public function delete(InvoiceInterface $invoice): bool;
}
