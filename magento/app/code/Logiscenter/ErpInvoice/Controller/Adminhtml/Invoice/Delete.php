<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ErpInvoice\Controller\Adminhtml\Invoice;

use Exception;
use Logiscenter\ErpInvoice\Api\InvoiceRepositoryInterface;
use Logiscenter\ErpInvoice\Model\FileProcessor;
use Logiscenter\ErpInvoiceConfig\Api\Config\ErpInvoiceModuleStatusInterface;
use Magento\Backend\App\Action;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Psr\Log\LoggerInterface;

/**
 * Action class for deleting of erp invoice
 */
class Delete extends Action implements HttpPostActionInterface
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    public const ADMIN_RESOURCE = 'Magento_Customer::manage';

    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var JsonFactory
     */
    private $resultJsonFactory;

    /**
     * @var InvoiceRepositoryInterface
     */
    private $invoiceRepository;

    /**
     * @var FileProcessor
     */
    private $fileProcessor;

    /**
     * @var ErpInvoiceModuleStatusInterface
     */
    private $statusConfig;

    /**
     * @param Action\Context                  $context
     * @param CustomerRepositoryInterface     $customerRepository
     * @param JsonFactory                     $resultJsonFactory
     * @param LoggerInterface                 $logger
     * @param InvoiceRepositoryInterface      $invoiceRepository
     * @param FileProcessor                   $fileProcessor
     * @param ErpInvoiceModuleStatusInterface $statusConfig
     */
    public function __construct(
        Action\Context $context,
        CustomerRepositoryInterface $customerRepository,
        JsonFactory $resultJsonFactory,
        LoggerInterface $logger,
        InvoiceRepositoryInterface $invoiceRepository,
        FileProcessor $fileProcessor,
        ErpInvoiceModuleStatusInterface $statusConfig
    ) {
        parent::__construct($context);
        $this->customerRepository = $customerRepository;
        $this->logger = $logger;
        $this->invoiceRepository = $invoiceRepository;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->fileProcessor = $fileProcessor;
        $this->statusConfig = $statusConfig;
    }

    /**
     * Delete customer invoice action
     *
     * @return Json
     */
    public function execute(): Json
    {
        $customerId = (int)$this->getRequest()->getParam('customer_id', false);
        $invoiceId = (int)$this->getRequest()->getParam('id', false);

        $error = false;
        try {
            if (!$this->statusConfig->isEnabled()) {
                throw new LocalizedException(__('Module is disabled in config'));
            }
            $this->customerRepository->getById($customerId);
            if ($invoiceId) {
                $invoice = $this->invoiceRepository->get($invoiceId);
                $this->invoiceRepository->delete($invoice);
                $this->fileProcessor->deleteExistingInvoice($invoice->getFile(), $invoice->getCustomerId());
            }
            $messages[] = __('You deleted the invoice.');
        } catch (NoSuchEntityException $e) {
            $this->logger->critical($e);
            $error = true;
            $messages = [__('There is no customer with such id.')];
        } catch (LocalizedException $e) {
            $error = true;
            $messages = [__($e->getMessage())];
            $this->logger->critical($e);
        } catch (Exception $e) {
            $error = true;
            $messages = [__('We can\'t delete invoice right now.')];
            $this->logger->critical($e);
        }

        $resultJson = $this->resultJsonFactory->create();
        $resultJson->setData(
            [
                'messages' => $messages,
                'error'    => $error,
            ]
        );

        return $resultJson;
    }
}
