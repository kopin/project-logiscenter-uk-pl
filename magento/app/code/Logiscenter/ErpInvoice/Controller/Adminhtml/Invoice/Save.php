<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ErpInvoice\Controller\Adminhtml\Invoice;

use Exception;
use Logiscenter\ErpInvoice\Api\InvoiceRepositoryInterface;
use Logiscenter\ErpInvoice\Model\FileProcessor;
use Logiscenter\ErpInvoice\Model\InvoiceFactory;
use Logiscenter\ErpInvoice\Service\SendEmail;
use Logiscenter\ErpInvoiceConfig\Api\Config\ErpInvoiceModuleStatusInterface;
use Magento\Backend\App\Action;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Psr\Log\LoggerInterface;

/**
 * Class for saving of erp invoice
 */
class Save extends Action implements HttpPostActionInterface
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    public const ADMIN_RESOURCE = 'Magento_Customer::manage';

    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var JsonFactory
     */
    private $resultJsonFactory;

    /**
     * @var InvoiceFactory
     */
    private $invoiceFactory;

    /**
     * @var InvoiceRepositoryInterface
     */
    private $invoiceRepository;

    /**
     * @var FileProcessor
     */
    private $fileProcessor;

    /**
     * @var SendEmail
     */
    private $sendEmail;

    /**
     * @var ErpInvoiceModuleStatusInterface
     */
    private $statusConfig;

    /**
     * @param Action\Context                  $context
     * @param CustomerRepositoryInterface     $customerRepository
     * @param LoggerInterface                 $logger
     * @param JsonFactory                     $resultJsonFactory
     * @param InvoiceFactory                  $invoiceFactory
     * @param InvoiceRepositoryInterface      $invoiceRepository
     * @param FileProcessor                   $fileProcessor
     * @param SendEmail                       $sendEmail
     * @param ErpInvoiceModuleStatusInterface $statusConfig
     */
    public function __construct(
        Action\Context $context,
        CustomerRepositoryInterface $customerRepository,
        LoggerInterface $logger,
        JsonFactory $resultJsonFactory,
        InvoiceFactory $invoiceFactory,
        InvoiceRepositoryInterface $invoiceRepository,
        FileProcessor $fileProcessor,
        SendEmail $sendEmail,
        ErpInvoiceModuleStatusInterface $statusConfig
    ) {
        parent::__construct($context);
        $this->customerRepository = $customerRepository;
        $this->logger = $logger;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->invoiceFactory = $invoiceFactory;
        $this->invoiceRepository = $invoiceRepository;
        $this->fileProcessor = $fileProcessor;
        $this->sendEmail = $sendEmail;
        $this->statusConfig = $statusConfig;
    }

    /**
     * Save customer invoice action
     *
     * @return Json
     */
    public function execute(): Json
    {
        $customerId = $this->getRequest()->getParam('customer_id', false);

        $error = false;
        try {
            if (!$this->statusConfig->isEnabled()) {
                throw new LocalizedException(__('Module is disabled in config'));
            }
            $customer = $this->customerRepository->getById($customerId);
            $data = $this->getRequest()->getPostValue();
            $data['file'] = $this->fileProcessor->moveFromTmpToBaseDir($data['file'][0]['file'] ?? [], $customerId);
            if (empty($data['file'])) {
                throw new LocalizedException(__('File is required field.'));
            }
            $model = $this->invoiceFactory->create();
            $id = (int)$this->getRequest()->getParam('entity_id');
            if ($id) {
                try {
                    $model = $this->invoiceRepository->get($id);
                    if ($model->getFile() && empty($data['file'])) {
                        $this->fileProcessor->deleteExistingInvoice($model->getFile(), $model->getCustomerId());
                    }
                } catch (FileSystemException $e) {
                    $messages[] = __('You saved the invoice from filesystem.');
                } catch (NoSuchEntityException $e) {
                    throw new LocalizedException(__('This invoice no longer exists.'));
                }
            }
            if (empty($id)) {
                $data['entity_id'] = null;
            }
            $model->setData($data);
            $this->invoiceRepository->save($model);
            $messages[] = __('You saved the invoice.');
            if ($model->getSendEmail()) {
                try {
                    $this->sendEmail->execute($customer);
                    $messages[] = __('Email successfully sent.');
                } catch (Exception $exception) {
                    $this->logger->critical($exception);
                    $messages[] = __('Unable to send email.');
                }
            }
        } catch (NoSuchEntityException $e) {
            $this->logger->critical($e);
            $error = true;
            $messages = [__('There is no customer with such id.')];
        } catch (LocalizedException $e) {
            $error = true;
            $messages = [__($e->getMessage())];
            $this->logger->critical($e);
        } catch (Exception $e) {
            $error = true;
            $messages = [__('We can\'t save invoice right now.')];
            $this->logger->critical($e);
        }

        $invoiceId = $id ?? null;
        $resultJson = $this->resultJsonFactory->create();
        $resultJson->setData(
            [
                'messages' => $messages,
                'error'    => $error,
                'data'     => [
                    'entity_id' => $invoiceId,
                ],
            ]
        );

        return $resultJson;
    }
}
