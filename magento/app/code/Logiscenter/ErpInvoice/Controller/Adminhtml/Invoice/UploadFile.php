<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ErpInvoice\Controller\Adminhtml\Invoice;

use Exception;
use Logiscenter\ErpInvoice\Model\FileProcessor;
use Logiscenter\ErpInvoiceConfig\Api\Config\ErpInvoiceModuleStatusInterface;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;

/**
 * Upload invoice file action class
 */
class UploadFile extends Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    public const ADMIN_RESOURCE = 'Magento_Customer::manage';

    /**
     * @var FileProcessor
     */
    private $uploadFile;

    /**
     * @var ErpInvoiceModuleStatusInterface
     */
    private $statusConfig;

    /**
     * @param Context                         $context
     * @param FileProcessor                   $uploadFile
     * @param ErpInvoiceModuleStatusInterface $statusConfig
     */
    public function __construct(
        Context $context,
        FileProcessor $uploadFile,
        ErpInvoiceModuleStatusInterface $statusConfig
    ) {
        parent::__construct($context);
        $this->uploadFile = $uploadFile;
        $this->statusConfig = $statusConfig;
    }

    /**
     * Image upload action
     *
     * @return ResultInterface
     */
    public function execute(): ResultInterface
    {
        /** @var Json $resultJson */
        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        try {
            if (!$this->statusConfig->isEnabled()) {
                throw new LocalizedException(__('Module is disabled in config'));
            }
            $result = $this->uploadFile->uploadToTmpDir();
        } catch (Exception $e) {
            $result = [
                'error'     => $e->getMessage(),
                'errorcode' => $e->getCode(),
            ];
        }

        return $resultJson->setData($result);
    }
}
