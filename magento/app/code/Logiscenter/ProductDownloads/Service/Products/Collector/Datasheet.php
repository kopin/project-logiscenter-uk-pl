<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ProductDownloads\Service\Products\Collector;

use Logiscenter\ProductDownloads\Api\Product\CollectorInterface;
use Logiscenter\ProductDownloads\Model\Datasheet as DatasheetModel;
use Magento\Catalog\Model\ResourceModel\Product\Collection;

/**
 * Class for datasheet collector
 */
class Datasheet extends AbstractProductsCollector implements CollectorInterface
{
    /**
     * @inheritdoc
     */
    protected function addAdditionalAttributes(Collection $productCollection): void
    {
        $productCollection->addAttributeToSelect('manufacturer');
        $productCollection->addAttributeToSelect('name');
        $productCollection->addAttributeToSelect('modelo');

        parent::addAdditionalAttributes($productCollection);
    }

    /**
     * @inheritdoc
     */
    protected function getAttributeCode(): string
    {
        return DatasheetModel::ATTRIBUTE_CODE;
    }
}
