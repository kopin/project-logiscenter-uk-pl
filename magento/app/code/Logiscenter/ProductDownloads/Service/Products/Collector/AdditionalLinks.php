<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ProductDownloads\Service\Products\Collector;

use Logiscenter\ProductDownloads\Api\Product\CollectorInterface;
use Logiscenter\ProductDownloads\Model\AdditionalLinks as AdditionalLinksModel;

/**
 * Class for additional links collector
 */
class AdditionalLinks extends AbstractProductsCollector implements CollectorInterface
{
    /**
     * @inheritdoc
     */
    protected function getAttributeCode(): string
    {
        return AdditionalLinksModel::ATTRIBUTE_CODE;
    }
}
