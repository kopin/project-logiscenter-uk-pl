<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ProductDownloads\Service\Products\Collector;

use Logiscenter\ProductDownloads\Api\Product\CollectorInterface;
use Magento\Catalog\Model\ResourceModel\Product\Collection;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Consist of basic logic for products collectors
 */
abstract class AbstractProductsCollector implements CollectorInterface
{
    /**
     * @var CollectionFactory
     */
    private $productCollectionFactory;

    /**
     * @var array
     */
    private $cache = [];

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @param CollectionFactory     $productCollectionFactory
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        CollectionFactory     $productCollectionFactory,
        StoreManagerInterface $storeManager
    ) {
        $this->productCollectionFactory = $productCollectionFactory;
        $this->storeManager = $storeManager;
    }

    /**
     * @inheritdoc
     */
    public function collectProducts(int $storeId, array $productIds): array
    {
        $websiteId = $storeId ? $this->getWebsitesId($storeId) : null;
        $cacheKey = $storeId;
        if (isset($this->cache[$this->getAttributeCode()][$cacheKey])) {
            return $this->cache[$this->getAttributeCode()][$cacheKey];
        }

        $productCollection = $this->productCollectionFactory->create();
        $productCollection->addStoreFilter($storeId);
        if (!empty($websiteId)) {
            $productCollection->addWebsiteFilter($websiteId);
        }

        if (!empty($productIds)) {
            $productCollection->addAttributeToFilter('entity_id', ['in' => $productIds]);
        }

        $this->addAdditionalAttributes($productCollection);
        $attributeCode = $this->getAttributeCode();
        $productCollection->addAttributeToSelect($attributeCode);
        $productCollection->addAttributeToFilter($attributeCode, ['neq' => null]);
        $this->cache[$this->getAttributeCode()][$cacheKey] = $productCollection->getItems();

        return $this->cache[$this->getAttributeCode()][$cacheKey];
    }

    /**
     * @param int $storeId
     *
     * @return int
     */
    private function getWebsitesId(int $storeId): ?int
    {
        $store = $this->storeManager->getStore($storeId);

        return $store->getId() ? (int)$store->getWebsiteId() : null;
    }

    /**
     * Adds additional filters to collection
     *
     * @param Collection $productCollection
     */
    protected function addAdditionalAttributes(Collection $productCollection): void
    {
        //should be implemented in child classes if needed
    }

    /**
     * Returns attribute code by which products should be collected
     *
     * @return string
     */
    abstract protected function getAttributeCode(): string;
}
