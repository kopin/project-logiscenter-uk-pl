<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ProductDownloads\Service\Files;

use Logiscenter\ProductDownloads\Api\Files\CopierInterface;
use Logiscenter\ProductDownloadsConfig\Api\Config\ProductDownloadsModuleStatusInterface;

/**
 * Class perform files coping for each registered copier
 */
class Copier
{
    /**
     * @var array
     */
    private $copiersPool;

    /**
     * @var ProductDownloadsModuleStatusInterface
     */
    private $moduleStatus;

    /**
     * @param ProductDownloadsModuleStatusInterface $moduleStatus
     * @param array                                 $copiersPool
     */
    public function __construct(
        ProductDownloadsModuleStatusInterface $moduleStatus,
        array $copiersPool = []
    ) {
        $this->copiersPool = $copiersPool;
        $this->moduleStatus = $moduleStatus;
    }

    /**
     * Perform file coping
     *
     * @param int $storeId
     * @param array $productIds
     *
     * @return void
     */
    public function execute(int $storeId, array $productIds): void
    {
        if (!$this->moduleStatus->isEnabled()) {
            return;
        }

        foreach ($this->copiersPool as $copier) {
            if (!$copier instanceof CopierInterface) {
                $message = sprintf('The copier should be instance of %s', CopierInterface::class);
                throw new \InvalidArgumentException($message);
            }

            $copier->execute($storeId, $productIds);
        }
    }
}
