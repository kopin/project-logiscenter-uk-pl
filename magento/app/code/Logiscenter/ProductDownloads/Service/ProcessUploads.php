<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ProductDownloads\Service;

use Logiscenter\ProductDownloads\Api\UploadsMigrationInterface;
use Logiscenter\ProductDownloads\Service\Data\Attachments\Migrate;
use Logiscenter\ProductDownloads\Service\Data\Attachments\RemoveAttachments;
use Logiscenter\ProductDownloads\Service\Files\Copier;
use Logiscenter\ProductDownloadsConfig\Api\Config\ProductDownloadsModuleStatusInterface;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Service to process product uploads migration
 */
class ProcessUploads implements UploadsMigrationInterface
{
    /**
     * @var RemoveAttachments
     */
    private $removeAttachments;

    /**
     * @var Copier
     */
    private $copier;

    /**
     * @var Migrate
     */
    private $migrate;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var ProductDownloadsModuleStatusInterface
     */
    private $moduleStatus;

    /**
     * @param RemoveAttachments                     $removeAttachments
     * @param Copier                                $copier
     * @param Migrate                               $migrate
     * @param StoreManagerInterface                 $storeManager
     * @param ProductDownloadsModuleStatusInterface $moduleStatus
     */
    public function __construct(
        RemoveAttachments $removeAttachments,
        Copier $copier,
        Migrate $migrate,
        StoreManagerInterface $storeManager,
        ProductDownloadsModuleStatusInterface $moduleStatus
    ) {
        $this->removeAttachments = $removeAttachments;
        $this->copier = $copier;
        $this->migrate = $migrate;
        $this->storeManager = $storeManager;
        $this->moduleStatus = $moduleStatus;
    }

    /**
     * Execute migration process
     *
     * @param array $storeCodes
     * @param array $productIds
     *
     * @return void
     */
    public function execute(array $storeCodes = [], array $productIds = []): void
    {
        if (!$this->moduleStatus->isEnabled()) {
            return;
        }
        $storeIds = $this->getStoreIds($storeCodes);
        if (empty($storeIds)) {
            $stores = $this->storeManager->getStores();
            $storeIds = array_keys($stores);
        }
        foreach ($storeIds as $storeId) {
            $this->removeAttachments->execute($storeId, $productIds);
            $this->copier->execute($storeId, $productIds);
            $this->migrate->execute($storeId, $productIds);
        }
    }

    /**
     * Get store ids instead of store codes
     *
     * @param array $storeCodes
     *
     * @return array
     */
    private function getStoreIds(array $storeCodes): array
    {
        foreach ($storeCodes as $storeCode) {
            $storeIds[] = (int)$this->storeManager->getStore($storeCode)->getId();
        }

        return $storeIds ?? [];
    }
}
