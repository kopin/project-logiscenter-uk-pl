<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ProductDownloads\Service\Data\Attachments;

use Logiscenter\ProductDownloads\Api\Data\MigrateInterface;
use Logiscenter\ProductDownloadsConfig\Api\Config\ProductDownloadsModuleStatusInterface;

/**
 * Class perform data migration for each registered migration service
 */
class Migrate
{
    /**
     * @var array
     */
    private $migrationsPool;

    /**
     * @var ProductDownloadsModuleStatusInterface
     */
    private $moduleStatus;

    /**
     * @param ProductDownloadsModuleStatusInterface $moduleStatus
     * @param array                                 $migrationsPool
     */
    public function __construct(
        ProductDownloadsModuleStatusInterface $moduleStatus,
        array                                 $migrationsPool = []
    ) {
        $this->migrationsPool = $migrationsPool;
        $this->moduleStatus = $moduleStatus;
    }

    /**
     * Perform data migration
     *
     * @param int   $storeId
     * @param array $productIds
     *
     * @return void
     */
    public function execute(int $storeId, array $productIds = []): void
    {
        if (!$this->moduleStatus->isEnabled()) {
            return;
        }

        foreach ($this->migrationsPool as $migration) {
            if (!$migration instanceof MigrateInterface) {
                $message = sprintf('The migration service should be instance of %s', MigrateInterface::class);
                throw new \InvalidArgumentException($message);
            }

            $migration->execute($storeId, $productIds);
        }
    }
}
