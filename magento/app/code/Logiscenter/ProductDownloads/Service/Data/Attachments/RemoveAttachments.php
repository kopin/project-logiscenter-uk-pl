<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ProductDownloads\Service\Data\Attachments;

use Logiscenter\ProductDownloads\Service\Products\Collector\AdditionalLinks;
use Logiscenter\ProductDownloads\Service\Products\Collector\Datasheet;
use Logiscenter\ProductDownloadsConfig\Api\Config\ProductDownloadsModuleStatusInterface;
use Magento\Framework\Filesystem\Driver\File;
use MageWorx\Downloads\Model\Attachment\Link as AttachmentLinkModel;
use MageWorx\Downloads\Model\ResourceModel\Attachment as AttachmentResource;
use MageWorx\Downloads\Model\ResourceModel\Attachment\Collection;
use MageWorx\Downloads\Model\ResourceModel\Attachment\CollectionFactory;

/**
 * Service to cleanup existing product uploads data
 */
class RemoveAttachments
{
    /**
     * @var AdditionalLinks
     */
    private $additionalLinksProductsCollector;

    /**
     * @var Datasheet
     */
    private $dataSheetProductsCollector;

    /**
     * @var AttachmentResource
     */
    private $attachmentResource;

    /**
     * @var CollectionFactory
     */
    private $attachmentCollectionFactory;

    /**
     * @var File
     */
    private $file;

    /**
     * @var AttachmentLinkModel
     */
    private $attachmentLinkModel;

    /**
     * @var array
     */
    private $relatedTables;

    /**
     * @var ProductDownloadsModuleStatusInterface
     */
    private $moduleStatus;

    /**
     * @param AdditionalLinks                       $additionalLinksProductsCollector
     * @param Datasheet                             $dataSheetProductsCollector
     * @param AttachmentResource                    $attachmentResource
     * @param CollectionFactory                     $attachmentCollectionFactory
     * @param File                                  $file
     * @param AttachmentLinkModel                   $attachmentLinkModel
     * @param ProductDownloadsModuleStatusInterface $moduleStatus
     * @param array                                 $relatedTables
     */
    public function __construct(
        AdditionalLinks                       $additionalLinksProductsCollector,
        Datasheet                             $dataSheetProductsCollector,
        AttachmentResource                    $attachmentResource,
        CollectionFactory                     $attachmentCollectionFactory,
        File                                  $file,
        AttachmentLinkModel                   $attachmentLinkModel,
        ProductDownloadsModuleStatusInterface $moduleStatus,
        array                                 $relatedTables = []
    ) {
        $this->additionalLinksProductsCollector = $additionalLinksProductsCollector;
        $this->dataSheetProductsCollector = $dataSheetProductsCollector;
        $this->attachmentResource = $attachmentResource;
        $this->attachmentCollectionFactory = $attachmentCollectionFactory;
        $this->file = $file;
        $this->attachmentLinkModel = $attachmentLinkModel;
        $this->relatedTables = $relatedTables;
        $this->moduleStatus = $moduleStatus;
    }

    /**
     * Perform delete existing data
     *
     * @param int   $storeIds
     * @param array $productIds
     *
     * @return void
     */
    public function execute(int $storeId, array $productIds): void
    {
        if (!$this->moduleStatus->isEnabled()) {
            return;
        }

        $attachments = $this->getAttachments($storeId, $this->getProductIds($storeId, $productIds));
        $this->removeRelatedFiles($attachments);
        $this->removeAttachmentData($attachments);
    }

    /**
     * Get products ids
     *
     * @param int   $storeId
     * @param array $productIds
     *
     * @return array
     */
    private function getProductIds(int $storeId, array $productIds): array
    {
        $dataSheetProducts = $this->dataSheetProductsCollector->collectProducts($storeId, $productIds);
        $additionalLinksProducts = $this->additionalLinksProductsCollector->collectProducts($storeId, $productIds);

        $products = array_merge(array_keys($dataSheetProducts), array_keys($additionalLinksProducts));

        return array_unique($products);
    }

    /**
     * Get related attachments
     *
     * @param array $storeIds
     * @param array $productIds
     *
     * @return Collection
     */
    protected function getAttachments(int $storeIds, array $productIds): Collection
    {
        $collection = $this->attachmentCollectionFactory->create();
        $collection->addProductsFilter($productIds);
        $collection->addStoreFilter($storeIds);

        return $collection;
    }

    /**
     * Remove related data
     *
     * @param Collection $attachments
     *
     * @return void
     */
    private function removeAttachmentData(Collection $attachments): void
    {
        $attachmentIds = $attachments->getAllIds();

        if (empty($attachmentIds)) {
            return;
        }

        $connect = $this->attachmentResource->getConnection();
        foreach ($this->relatedTables as $tableName) {
            $tableName = $connect->getTableName($tableName);
            $connect->delete($tableName, $connect->quoteInto('attachment_id IN(?)', $attachmentIds));
        }
    }

    /**
     * Remove related files
     *
     * @param Collection $attachments
     *
     * @return void
     */
    private function removeRelatedFiles(Collection $attachments): void
    {
        foreach ($attachments as $attachment) {
            $file = $attachment->getFilename();
            if ($file) {
                $path = rtrim($this->attachmentLinkModel->getBaseDir(), '/') . '/' . ltrim($file, '/');

                if ($this->file->isExists($path)) {
                    $this->file->deleteFile($path);
                }
            }
        }
    }
}
