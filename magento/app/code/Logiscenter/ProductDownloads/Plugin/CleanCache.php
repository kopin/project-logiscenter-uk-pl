<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ProductDownloads\Plugin;

use Logiscenter\ProductDownloads\Api\UploadsMigrationInterface;
use Magento\Framework\App\Cache\TypeListInterface;
use Magento\PageCache\Model\Cache\Type;
use Magento\PageCache\Model\Config;

/**
 * Plugin to clean the page cache after migration is done
 */
class CleanCache
{
    /**
     * @var Config
     */
    protected $config;

    /**
     * @var TypeListInterface
     */
    protected $typeList;

    /**
     * @param Config            $config
     * @param TypeListInterface $typeList
     */
    public function __construct(Config $config, TypeListInterface $typeList)
    {
        $this->config = $config;
        $this->typeList = $typeList;
    }

    /**
     * Clean cache after migration done
     *
     * @param UploadsMigrationInterface $subject
     *
     * @return void
     */
    public function afterExecute(UploadsMigrationInterface $subject): void
    {
        if ($this->config->isEnabled()) {
            $this->typeList->cleanType(Type::TYPE_IDENTIFIER);
        }
    }
}
