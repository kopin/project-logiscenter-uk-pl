<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ProductDownloads\Console\Command;

use Interactiv4\Framework\Console\AbstractCommand;
use Interactiv4\Framework\Console\Command\Context;
use Logiscenter\ProductDownloads\Api\UploadsMigrationInterface;
use Magento\Framework\Console\Cli;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Api\Data\WebsiteInterface;
use Magento\Store\Model\StoreManagerInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Perform product downloads migration from eav to mageworx extension
 */
class ProcessUploads extends AbstractCommand
{
    private const ARGUMENT_WEBSITE = 'website-code';

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var UploadsMigrationInterface
     */
    private $migrationService;

    /**
     * @param Context                   $context
     * @param StoreManagerInterface     $storeManager
     * @param UploadsMigrationInterface $migrationService
     */
    public function __construct(
        Context $context,
        StoreManagerInterface $storeManager,
        UploadsMigrationInterface $migrationService
    ) {
        parent::__construct($context);
        $this->storeManager = $storeManager;
        $this->migrationService = $migrationService;
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setName('logiscenter:product-downloads:process');
        $this->setAliases(['downloads:process']);
        $this->setDescription(
            'Product Downloads: Migrate products downloads from eav to mageworx extension'
        );
        $this->addArgument(
            self::ARGUMENT_WEBSITE,
            InputArgument::REQUIRED,
            'website code for which migration should be performed'
        );

        parent::configure();
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $websiteCode = $input->getArgument(self::ARGUMENT_WEBSITE);
        $website = $this->getWebsite($websiteCode);

        if ($website === null) {
            $this->error(sprintf('The website with %s code does not exist', $websiteCode));

            return Cli::RETURN_FAILURE;
        }

        $stores = $website->getStoreCodes();
        $this->migrationService->execute($stores);

        return Cli::RETURN_SUCCESS;
    }

    /**
     * Get website by code
     *
     * @param string $code
     *
     * @return WebsiteInterface|null
     */
    private function getWebsite(string $code): ?WebsiteInterface
    {
        try {
            $website = $this->storeManager->getWebsite($code);
        } catch (NoSuchEntityException $e) {
            $website = null;
        }

        return $website;
    }
}
