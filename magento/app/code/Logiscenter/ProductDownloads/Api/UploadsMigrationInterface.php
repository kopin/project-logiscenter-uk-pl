<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ProductDownloads\Api;

/**
 * Interface for migration processor
 */
interface UploadsMigrationInterface
{
    /**
     * Perform process of downloads migration
     *
     * @param array $storeCodes
     * @param array $productIds
     *
     * @return void
     */
    public function execute(array $storeCodes = [], array $productIds = []): void;
}
