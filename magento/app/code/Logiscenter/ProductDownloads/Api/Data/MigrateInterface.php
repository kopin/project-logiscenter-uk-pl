<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ProductDownloads\Api\Data;

/**
 * Migration service interface
 */
interface MigrateInterface
{
    /**
     * Migrate related data into appropriate table
     *
     * @param int   $storeId
     * @param array $productIds
     *
     * @return void
     */
    public function execute(int $storeId, array $productIds): void;
}
