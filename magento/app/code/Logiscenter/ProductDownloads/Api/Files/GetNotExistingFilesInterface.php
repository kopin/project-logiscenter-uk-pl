<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ProductDownloads\Api\Files;

/**
 * Interface for retrieving not existing files
 */
interface GetNotExistingFilesInterface
{
    /**
     * Get list of not existing files specified as attribute value
     *
     * @param string $attributeValue
     *
     * @return array
     */
    public function execute(string $attributeValue): array;
}
