<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ProductDownloads\Api\Files;

/**
 * Copy service interface
 */
interface CopierInterface
{
    /**
     * Copy related files into appropriate folder
     *
     * @param int   $storeId
     * @param array $productIds
     *
     * @return void
     */
    public function execute(int $storeId, array $productIds): void;
}
