<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ProductDownloads\Api\Product;

/**
 * Products collector interface
 */
interface CollectorInterface
{
    /**
     * Collect products
     *
     * @param int $storeId
     * @param array $productIds
     *
     * @return array
     */
    public function collectProducts(int $storeId, array $productIds): array;
}
