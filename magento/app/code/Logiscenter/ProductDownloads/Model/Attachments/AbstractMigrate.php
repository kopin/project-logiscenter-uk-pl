<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ProductDownloads\Model\Attachments;

use Logiscenter\ProductDownloads\Api\Data\MigrateInterface;
use Logiscenter\ProductDownloads\Api\Product\CollectorInterface;
use Logiscenter\ProductDownloadsConfig\Api\Config\ProductDownloadsConfigInterface;
use Magento\Customer\Model\ResourceModel\Group\CollectionFactory;
use MageWorx\Downloads\Model\AttachmentFactory;
use MageWorx\Downloads\Model\ResourceModel\Attachment as AttachmentResource;

/**
 * Consist of basic logic for product uploads data migration process
 */
abstract class AbstractMigrate implements MigrateInterface
{
    /**
     * @var ProductDownloadsConfigInterface
     */
    protected $config;

    /**
     * @var AttachmentResource
     */
    private $attachmentResource;

    /**
     * @var AttachmentFactory
     */
    private $attachmentFactory;

    /**
     * @var CollectionFactory
     */
    private $customerGroupCollectionFactory;

    /**
     * @param ProductDownloadsConfigInterface $config
     * @param AttachmentResource              $attachmentResource
     * @param AttachmentFactory               $attachmentFactory
     * @param CollectionFactory               $customerGroupCollectionFactory
     */
    public function __construct(
        ProductDownloadsConfigInterface $config,
        AttachmentResource $attachmentResource,
        AttachmentFactory $attachmentFactory,
        CollectionFactory $customerGroupCollectionFactory
    ) {
        $this->config = $config;
        $this->attachmentResource = $attachmentResource;
        $this->attachmentFactory = $attachmentFactory;
        $this->customerGroupCollectionFactory = $customerGroupCollectionFactory;
    }

    /**
     * @inheritdoc
     */
    public function execute(int $storeId, array $productIds): void
    {
        $products = $this->getCollector()->collectProducts($storeId, $productIds);
        $this->processSave($products, $storeId);
    }

    /**
     * Saves attachment with provided data
     *
     * @param array $data
     *
     * @return void
     */
    protected function saveAttachment(array $data): void
    {
        $attachment = $this->attachmentFactory->create();
        $attachment->addData($data);
        $this->attachmentResource->save($attachment);
    }

    /**
     * Get list of all customer groups ids
     *
     * @return array
     */
    protected function getAllCustomerGroupsIds(): array
    {
        $customerGroupCollection = $this->customerGroupCollectionFactory->create();

        return $customerGroupCollection->getAllIds();
    }

    /**
     * Populate attachments with appropriate data and save
     *
     * @param array $products
     * @param int   $storeId
     *
     * @return void
     */
    abstract protected function processSave(array $products, int $storeId): void;

    /**
     * Return appropriate collector
     *
     * @return CollectorInterface
     */
    abstract protected function getCollector(): CollectorInterface;
}
