<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ProductDownloads\Model\Attachments\AdditionalLinks;

use Logiscenter\ProductDownloads\Api\Product\CollectorInterface;
use Logiscenter\ProductDownloads\Model\AdditionalLinks as AdditionalLinksModel;
use Logiscenter\ProductDownloads\Model\Attachments\AbstractMigrate;
use Logiscenter\ProductDownloads\Model\Files\FileMapper;
use Logiscenter\ProductDownloads\Service\Products\Collector\AdditionalLinks as AdditionalLinksCollector;
use Logiscenter\ProductDownloadsConfig\Api\Config\ProductDownloadsConfigInterface;
use Magento\Customer\Model\ResourceModel\Group\CollectionFactory;
use MageWorx\Downloads\Model\Attachment\Source\ContentType;
use MageWorx\Downloads\Model\AttachmentFactory;
use MageWorx\Downloads\Model\ResourceModel\Attachment as AttachmentResource;

/**
 * Class to migrate additional links attachments
 */
class Migrate extends AbstractMigrate
{
    /**
     * @var AdditionalLinksCollector
     */
    private $additionalLinksCollector;

    /**
     * @var AdditionalLinksModel
     */
    private $additionalLinks;

    /**
     * @param ProductDownloadsConfigInterface $config
     * @param AttachmentResource              $attachmentResource
     * @param AttachmentFactory               $attachmentFactory
     * @param CollectionFactory               $customerGroupCollectionFactory
     * @param AdditionalLinksCollector        $additionalLinksCollector
     * @param AdditionalLinksModel            $additionalLinks
     */
    public function __construct(
        ProductDownloadsConfigInterface $config,
        AttachmentResource $attachmentResource,
        AttachmentFactory $attachmentFactory,
        CollectionFactory $customerGroupCollectionFactory,
        AdditionalLinksCollector $additionalLinksCollector,
        AdditionalLinksModel $additionalLinks
    ) {
        parent::__construct($config, $attachmentResource, $attachmentFactory, $customerGroupCollectionFactory);

        $this->additionalLinksCollector = $additionalLinksCollector;
        $this->additionalLinks = $additionalLinks;
    }

    /**
     * @inheritdoc
     */
    protected function processSave(array $products, int $storeId): void
    {
        foreach ($products as $product) {
            foreach ($this->additionalLinks->getAttachmentsByProduct($product) as $productAttachment) {
                $newFileName = FileMapper::getNewFileName(
                    $storeId,
                    $this->additionalLinks->getPath($productAttachment)
                );

                if ($newFileName !== null) {
                    $data = [
                        'name'               => $this->additionalLinks->getName($productAttachment),
                        'filename'           => $newFileName,
                        'section_id'         => $this->config->getSection($storeId),
                        'stores'             => [$storeId],
                        'products_data'      => [$product->getId()],
                        'customer_group_ids' => $this->getAllCustomerGroupsIds(),
                        'content_type'       => ContentType::CONTENT_FILE,
                        'is_active'          => 1,
                    ];

                    $this->saveAttachment($data);
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    protected function getCollector(): CollectorInterface
    {
        return $this->additionalLinksCollector;
    }
}
