<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ProductDownloads\Model\Attachments\Datasheet;

use Logiscenter\ProductDownloads\Api\Product\CollectorInterface;
use Logiscenter\ProductDownloads\Model\Attachments\AbstractMigrate;
use Logiscenter\ProductDownloads\Model\Datasheet as DatasheetModel;
use Logiscenter\ProductDownloads\Model\Files\FileMapper;
use Logiscenter\ProductDownloads\Service\Products\Collector\Datasheet;
use Logiscenter\ProductDownloadsConfig\Api\Config\ProductDownloadsConfigInterface;
use Magento\Customer\Model\ResourceModel\Group\CollectionFactory;
use MageWorx\Downloads\Model\Attachment\Source\ContentType;
use MageWorx\Downloads\Model\AttachmentFactory;
use MageWorx\Downloads\Model\ResourceModel\Attachment as AttachmentResource;

/**
 * Class to migrate datasheet attachments
 */
class Migrate extends AbstractMigrate
{
    /**
     * @var DatasheetModel
     */
    private $datasheetModel;

    /**
     * @var Datasheet
     */
    private $datasheetCollector;

    /**
     * @param ProductDownloadsConfigInterface $config
     * @param AttachmentResource              $attachmentResource
     * @param AttachmentFactory               $attachmentFactory
     * @param CollectionFactory               $customerGroupCollectionFactory
     * @param DatasheetModel                  $datasheetModel
     * @param Datasheet                       $datasheetCollector
     */
    public function __construct(
        ProductDownloadsConfigInterface $config,
        AttachmentResource $attachmentResource,
        AttachmentFactory $attachmentFactory,
        CollectionFactory $customerGroupCollectionFactory,
        DatasheetModel $datasheetModel,
        Datasheet $datasheetCollector
    ) {
        parent::__construct($config, $attachmentResource, $attachmentFactory, $customerGroupCollectionFactory);
        $this->datasheetModel = $datasheetModel;
        $this->datasheetCollector = $datasheetCollector;
    }

    /**
     * @inheritdoc
     */
    protected function processSave(array $products, int $storeId): void
    {
        foreach ($products as $product) {
            $newFileName = FileMapper::getNewFileName($storeId, $product->getData(DatasheetModel::ATTRIBUTE_CODE));
            if ($newFileName !== null) {
                $data = [
                    'name'               => $this->datasheetModel->generateName($product, $storeId),
                    'filename'           => $newFileName,
                    'section_id'         => $this->config->getSection($storeId),
                    'stores'             => [$storeId],
                    'products_data'      => [$product->getId()],
                    'customer_group_ids' => $this->getAllCustomerGroupsIds(),
                    'content_type'       => ContentType::CONTENT_FILE,
                    'is_active'          => 1,
                ];

                $this->saveAttachment($data);
            }
        }
    }

    /**
     * @inheritdoc
     */
    protected function getCollector(): CollectorInterface
    {
        return $this->datasheetCollector;
    }
}
