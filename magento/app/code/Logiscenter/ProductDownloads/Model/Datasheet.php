<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ProductDownloads\Model;

use Logiscenter\ProductDownloadsConfig\Api\Config\ProductDownloadsConfigInterface;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Model\ResourceModel\Product as ProductResource;

/**
 * Class consist of datasheet logic
 */
class Datasheet
{
    public const ATTRIBUTE_CODE = 'datasheet';

    /**
     * @var ProductDownloadsConfigInterface
     */
    private $config;

    /**
     * @var ProductResource
     */
    private $productResource;

    /**
     * @param ProductDownloadsConfigInterface $config
     */
    public function __construct(ProductDownloadsConfigInterface $config, ProductResource $productResource)
    {
        $this->config = $config;
        $this->productResource = $productResource;
    }

    /**
     * Generate appropriate path
     *
     * @param string $attrValue
     *
     * @return string
     */
    public function buildFilePath(string $attrValue): string
    {
        return $this->config->getDocumentFolder()
            . DIRECTORY_SEPARATOR
            . 'media'
            . DIRECTORY_SEPARATOR
            . 'pdf'
            . DIRECTORY_SEPARATOR
            . $attrValue;
    }

    /**
     * @param string $attrValue
     *
     * @return string
     */
    public function getFileExt(string $attrValue): string
    {
        $arr = explode('.', $attrValue);

        return end($arr);
    }

    /**
     * Generate appropriate name
     *
     * @param ProductInterface $product
     *
     * @return string
     */
    public function generateName(ProductInterface $product, $storeId): string
    {
        return ' '
            . $this->config->getDatasheetPrefix($storeId)
            . ' '
            . $this->getOptionText($product, 'manufacturer')
            . ' '
            . $this->getOptionText($product, 'modelo')
            . ' '
            . '('
            . strtoupper($this->getFileExt($product->getDatasheet()))
            . ')';
    }

    /**
     * Get attribute option label
     *
     * @param ProductInterface $product
     * @param string           $attributeCode
     *
     * @return string
     */
    private function getOptionText(ProductInterface $product, string $attributeCode): string
    {
        $attribute = $this->productResource->getAttribute($attributeCode);

        return (string)$attribute->getSource()->getOptionText($product->getData($attributeCode));
    }
}
