<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ProductDownloads\Model;

use Logiscenter\ProductDownloadsConfig\Api\Config\ProductDownloadsConfigInterface;
use Magento\Catalog\Api\Data\ProductInterface;

/**
 * Class consist of additional links logic
 */
class AdditionalLinks
{
    public const ATTRIBUTE_CODE = 'additional_links';

    /**
     * @var ProductDownloadsConfigInterface
     */
    private $config;

    /**
     * @param ProductDownloadsConfigInterface $config
     */
    public function __construct(ProductDownloadsConfigInterface $config)
    {
        $this->config = $config;
    }

    /**
     * Get all product related attachments
     *
     * @param ProductInterface $product
     *
     * @return array
     */
    public function getAttachmentsByProduct(ProductInterface $product): array
    {
        $attributeValue = $product->getData(self::ATTRIBUTE_CODE);

        return $this->getAttachmentsByAttributeValue($attributeValue);
    }

    /**
     * Get all attachments from provided attribute value
     *
     * @param string $attributeValue
     *
     * @return array
     */
    public function getAttachmentsByAttributeValue(string $attributeValue): array
    {
        return $this->cleanupBlank(explode('#', $attributeValue));
    }

    /**
     * Cleanup blank flag, we dont need it after migration
     *
     * @param array $attachments
     *
     * @return array
     */
    public function cleanupBlank(array $attachments)
    {
        $cleanAttachments = [];
        foreach ($attachments as $attachment) {
            $cleanAttachments[] = str_replace('|blank', '', $attachment);
        }

        return $cleanAttachments;
    }

    /**
     * Generate appropriate path
     *
     * @param string $attachment
     *
     * @return string
     */
    public function buildFilePath(string $attachment): string
    {
        return $this->config->getDocumentFolder() . DIRECTORY_SEPARATOR . $this->getPath($attachment);
    }

    /**
     * Get path from single attachment value
     *
     * @param string $attachment
     *
     * @return string
     */
    public function getPath(string $attachment): string
    {
        $parts = explode('|', $attachment);

        return end($parts);
    }

    /**
     * Get name from single attachment value
     *
     * @param string $attachment
     *
     * @return string
     */
    public function getFileName(string $attachment): string
    {
        $currentPath = $this->getPath($attachment);
        $parts = explode(DIRECTORY_SEPARATOR, $currentPath);

        return end($parts);
    }

    /**
     * Get name from single attachment value
     *
     * @param string $attachment
     *
     * @return string
     */
    public function getName(string $attachment): string
    {
        $parts = explode('|', $attachment);

        return reset($parts);
    }
}
