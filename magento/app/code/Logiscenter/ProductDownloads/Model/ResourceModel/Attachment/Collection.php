<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ProductDownloads\Model\ResourceModel\Attachment;

use MageWorx\Downloads\Model\ResourceModel\Attachment\Collection as AttachmentCollection;

/**
 * Add additional methods to attachments collection
 */
class Collection extends AttachmentCollection
{
    /**
     * Add products filter to collection
     *
     * @param array $productIds
     *
     * @return void
     */
    public function addProductsFilter(array $productIds): void
    {
        if (!$this->joinProductFlag) {
            $this->joinProduct();
        }

        $this->getSelect()->reset(\Zend_Db_Select::GROUP);
        $this->getSelect()->where('product_relation_table.product_id IN(?)', $productIds);
    }
}
