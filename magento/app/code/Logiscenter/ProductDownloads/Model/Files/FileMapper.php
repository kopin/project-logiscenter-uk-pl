<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ProductDownloads\Model\Files;

/**
 * Class to save old - new file path relations
 */
class FileMapper
{
    /**
     * @var array
     */
    private static $storage = [];

    /**
     * Get all storage
     *
     * @return array
     */
    public static function getStorage()
    {
        return self::$storage;
    }

    /**
     * Put new file into storage
     *
     * @param string $oldFileName
     * @param string $newFileName
     *
     * @return void
     */
    public static function mapFile(int $storeId, string $oldFileName, string $newFileName): void
    {
        self::$storage[$storeId][$oldFileName] = $newFileName;
    }

    /**
     * Get new file by old file name
     *
     * @param int    $storeId
     * @param string $oldFileName
     *
     * @return string|null
     */
    public static function getNewFileName(int $storeId, string $oldFileName): ?string
    {
        return self::$storage[$storeId][$oldFileName] ?? null;
    }

    /**
     * Clear storage
     *
     * @return void
     */
    public static function clearStorage(): void
    {
        self::$storage = [];
    }
}
