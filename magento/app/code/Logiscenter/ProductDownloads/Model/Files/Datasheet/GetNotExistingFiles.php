<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ProductDownloads\Model\Files\Datasheet;

use Logiscenter\ProductDownloads\Api\Files\GetNotExistingFilesInterface;
use Logiscenter\ProductDownloads\Model\Datasheet;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;

/**
 * Returns list of not existing documents for additional links attribute
 */
class GetNotExistingFiles implements GetNotExistingFilesInterface
{
    /**
     * @var Datasheet
     */
    private $datasheet;

    /**
     * @var Filesystem
     */
    protected $fileSystem;

    /**
     * @param Datasheet  $datasheet
     * @param Filesystem $fileSystem
     */
    public function __construct(Datasheet $datasheet, Filesystem $fileSystem)
    {
        $this->datasheet = $datasheet;
        $this->fileSystem = $fileSystem;
    }

    /**
     * Get not existing files
     *
     * @param string $attributeValue
     *
     * @return array
     */
    public function execute(string $attributeValue): array
    {
        $varDir = $this->fileSystem->getDirectoryWrite(DirectoryList::VAR_DIR);
        $fileName = $this->datasheet->buildFilePath($attributeValue);

        return $varDir->isExist($fileName) ? [] : [$varDir->getRelativePath($fileName)];
    }
}
