<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ProductDownloads\Model\Files\AdditionalLinks;

use Logiscenter\ProductDownloads\Api\Files\GetNotExistingFilesInterface;
use Logiscenter\ProductDownloads\Model\AdditionalLinks;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;

/**
 * Returns list of not existing documents for additional links attribute
 */
class GetNotExistingFiles implements GetNotExistingFilesInterface
{
    /**
     * @var AdditionalLinks
     */
    private $additionalLinks;

    /**
     * @var Filesystem
     */
    protected $fileSystem;

    /**
     * @param AdditionalLinks $additionalLinks
     * @param Filesystem      $fileSystem
     */
    public function __construct(AdditionalLinks $additionalLinks, Filesystem $fileSystem)
    {
        $this->additionalLinks = $additionalLinks;
        $this->fileSystem = $fileSystem;
    }

    /**
     * Get not existing files
     *
     * @param string $attributeValue
     *
     * @return array
     */
    public function execute(string $attributeValue): array
    {
        $notExistingFiles = [];
        $attachments = $this->additionalLinks->getAttachmentsByAttributeValue($attributeValue);

        $varDir = $this->fileSystem->getDirectoryWrite(DirectoryList::VAR_DIR);
        foreach ($attachments as $attachment) {
            $path = $this->additionalLinks->buildFilePath($attachment);

            if (!$varDir->isExist($path)) {
                $notExistingFiles[] = $varDir->getRelativePath($path);
            }
        }

        return $notExistingFiles;
    }
}
