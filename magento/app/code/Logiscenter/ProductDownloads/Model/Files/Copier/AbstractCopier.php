<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ProductDownloads\Model\Files\Copier;

use Logiscenter\ProductDownloads\Api\Files\CopierInterface;
use Logiscenter\ProductDownloads\Api\Product\CollectorInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;
use Magento\Framework\Filesystem\Directory\WriteInterface;
use MageWorx\Downloads\Model\Attachment\Link;
use MageWorx\DownloadsImportExport\Model\Copy;

/**
 * Class consist of base copier logic
 */
abstract class AbstractCopier implements CopierInterface
{
    /**
     * @var Copy
     */
    private $copyModel;

    /**
     * @var Filesystem
     */
    private $fileSystem;

    /**
     * @var Link
     */
    private $attachmentLink;

    /**
     * @var WriteInterface
     */
    private $tmpDir;

    /**
     * @var WriteInterface
     */
    protected $varDir;

    /**
     * @param Copy       $copyModel
     * @param Filesystem $fileSystem
     * @param Link       $attachmentLink
     */
    public function __construct(Copy $copyModel, Filesystem $fileSystem, Link $attachmentLink)
    {
        $this->copyModel = $copyModel;
        $this->fileSystem = $fileSystem;
        $this->attachmentLink = $attachmentLink;
        $this->varDir = $this->fileSystem->getDirectoryWrite(DirectoryList::VAR_DIR);
        $this->tmpDir = $this->fileSystem->getDirectoryWrite(DirectoryList::TMP);
    }

    /**
     * Perform coping
     *
     * @param int $storeId
     * @param array $productIds
     *
     * @return void
     */
    public function execute(int $storeId, array $productIds): void
    {
        $products = $this->getCollector()->collectProducts($storeId, $productIds);

        $this->processFileCopy($storeId, $products);
    }

    /**
     * Copy file from source folder to destination folder
     *
     * @param $filePath
     * @param $fileName
     *
     * @return array
     */
    protected function copyFile($filePath, $fileName)
    {
        $absoluteFilePath = $this->varDir->getAbsolutePath($filePath);
        $absoluteTmpFilePath = $this->tmpDir->getAbsolutePath($fileName);

        if (!$this->tmpDir->isExist($fileName)) {
            $this->tmpDir->touch($fileName);
        }

        $this->tmpDir->getDriver()->copy($absoluteFilePath, $absoluteTmpFilePath);

        return $this->moveTmpFile($fileName, $absoluteTmpFilePath);
    }

    /**
     * Move tmp file to destination folder
     *
     * @param string $fileName
     * @param string $oldAbsolutePath
     *
     * @return array
     */
    private function moveTmpFile(string $fileName, string $oldAbsolutePath): array
    {
        $data = [
            'multifile' => [
                'name'     => $fileName,
                'tmp_name' => $oldAbsolutePath,
            ],
        ];

        return $this->copyModel->copyFileAndGetName($this->attachmentLink->getBaseDir(), $data);
    }

    /**
     * Process coping files
     *
     * @param int $storeId
     * @param array $products
     *
     * @return void
     */
    abstract protected function processFileCopy(int $storeId, array $products): void;

    /**
     * returns appropriate collector
     *
     * @return CollectorInterface
     */
    abstract protected function getCollector(): CollectorInterface;
}
