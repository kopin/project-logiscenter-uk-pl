<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ProductDownloads\Model\Files\Copier\Datasheet;

use Logiscenter\ProductDownloads\Api\Files\CopierInterface;
use Logiscenter\ProductDownloads\Api\Product\CollectorInterface;
use Logiscenter\ProductDownloads\Model\Datasheet as DatasheetModel;
use Logiscenter\ProductDownloads\Model\Files\Copier\AbstractCopier;
use Logiscenter\ProductDownloads\Model\Files\FileMapper;
use Logiscenter\ProductDownloads\Service\Products\Collector\Datasheet as DatasheetCollector;
use Magento\Framework\Filesystem;
use MageWorx\Downloads\Model\Attachment\Link;
use MageWorx\DownloadsImportExport\Model\Copy;

class Copier extends AbstractCopier implements CopierInterface
{
    /**
     * @var DatasheetCollector
     */
    private $datasheetCollector;

    /**
     * @var DatasheetModel
     */
    private $dataSheet;

    public function __construct(
        Copy $copyModel,
        Filesystem $fileSystem,
        Link $attachmentLink,
        DatasheetCollector $datasheetCollector,
        DatasheetModel $dataSheet
    ) {
        parent::__construct($copyModel, $fileSystem, $attachmentLink);

        $this->datasheetCollector = $datasheetCollector;
        $this->dataSheet = $dataSheet;
    }

    /**
     * @inheritdoc
     */
    protected function processFileCopy(int $storeId, array $products): void
    {
        foreach ($products as $product) {
            $fileName = $product->getData(DatasheetModel::ATTRIBUTE_CODE);
            $filePath = $this->dataSheet->buildFilePath($fileName);
            if ($this->varDir->isFile($filePath)) {
                $result = $this->copyFile($filePath, $fileName);

                if (isset($result['file'])) {
                    FileMapper::mapFile($storeId, $fileName, $result['file']);
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    protected function getCollector(): CollectorInterface
    {
        return $this->datasheetCollector;
    }
}
