<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ProductDownloads\Model\Files\Copier\AdditionalLinks;

use Logiscenter\ProductDownloads\Api\Files\CopierInterface;
use Logiscenter\ProductDownloads\Api\Product\CollectorInterface;
use Logiscenter\ProductDownloads\Model\AdditionalLinks as AdditionalLinksModel;
use Logiscenter\ProductDownloads\Model\Files\Copier\AbstractCopier;
use Logiscenter\ProductDownloads\Model\Files\FileMapper;
use Logiscenter\ProductDownloads\Service\Products\Collector\AdditionalLinks as AdditionalLinksCollector;
use Magento\Framework\Filesystem;
use MageWorx\Downloads\Model\Attachment\Link;
use MageWorx\DownloadsImportExport\Model\Copy;

class Copier extends AbstractCopier implements CopierInterface
{
    /**
     * @var AdditionalLinksCollector
     */
    private $additionalLinksCollector;

    /**
     * @var AdditionalLinksModel
     */
    private $additionalLink;

    /**
     * @param Copy                     $copyModel
     * @param Filesystem               $fileSystem
     * @param Link                     $attachmentLink
     * @param AdditionalLinksCollector $additionalLinksCollector
     * @param AdditionalLinksModel     $additionalLink
     */
    public function __construct(
        Copy $copyModel,
        Filesystem $fileSystem,
        Link $attachmentLink,
        AdditionalLinksCollector $additionalLinksCollector,
        AdditionalLinksModel $additionalLink
    ) {
        parent::__construct($copyModel, $fileSystem, $attachmentLink);

        $this->additionalLinksCollector = $additionalLinksCollector;
        $this->additionalLink = $additionalLink;
    }

    /**
     * @inheritdoc
     */
    protected function processFileCopy(int $storeId, array $products): void
    {
        foreach ($products as $product) {
            foreach ($this->additionalLink->getAttachmentsByProduct($product) as $attachment) {
                $filePath = $this->additionalLink->buildFilePath($attachment);
                if ($this->varDir->isFile($filePath)) {
                    $fileName = $this->additionalLink->getFileName($attachment);
                    $result = $this->copyFile($filePath, $fileName);

                    if (isset($result['file'])) {
                        FileMapper::mapFile($storeId, $this->additionalLink->getPath($attachment), $result['file']);
                    }
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    protected function getCollector(): CollectorInterface
    {
        return $this->additionalLinksCollector;
    }
}
