<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\GroupedRelatedConfig\Block\Adminhtml\System\Config\TabList;

use Logiscenter\GroupedRelatedConfig\Model\Source\RelatedTabs as Options;
use Magento\Framework\View\Element\Context;
use Magento\Framework\View\Element\Html\Select;

/**
 * Class RelatedTabRendered.
 *
 * Related tab renderer for map
 *
 */
class RelatedTabRendered extends Select
{
    /**
     * @var Options
     */
    private $options;

    /**
     * @param Options $options
     * @param Context $context
     * @param array   $data
     */
    public function __construct(
        Options $options,
        Context $context,
        array $data = []
    ) {
        $this->options = $options;
        parent::__construct(
            $context,
            $data
        );
    }

    /**
     * Set "name" for <select> element.
     *
     * @param string $value
     *
     * @return $this
     */
    public function setInputName($value)
    {
        return $this->setName($value);
    }

    /**
     * Set "id" for <select> element.
     *
     * @param $value
     *
     * @return $this
     */
    public function setInputId($value)
    {
        return $this->setId($value);
    }

    /**
     * Render block HTML.
     *
     * @return string
     * @codingStandardsIgnoreStart
     */
    public function _toHtml(): string
    {
        // @codingStandardsIgnoreEnd
        if (!$this->getOptions()) {
            $this->setOptions($this->options->toOptionArray());
        }

        return parent::_toHtml();
    }
}
