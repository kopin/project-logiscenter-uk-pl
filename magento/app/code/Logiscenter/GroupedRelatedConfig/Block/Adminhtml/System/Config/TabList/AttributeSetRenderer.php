<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\GroupedRelatedConfig\Block\Adminhtml\System\Config\TabList;

use Logiscenter\GroupedRelatedConfig\Model\Source\RelatedTabs;
use Magento\Catalog\Model\Product\AttributeSet\Options;
use Magento\Framework\View\Element\Context;
use Magento\Framework\View\Element\Html\Select;

/**
 * Class AttributeSetRenderer.
 *
 * Tabs attribute set renderer for map
 *
 */
class AttributeSetRenderer extends Select
{
    /**
     * @var Options
     */
    private $options;

    /**
     * @param Options $options
     * @param Context $context
     * @param array   $data
     */
    public function __construct(
        Options $options,
        Context $context,
        array $data = []
    ) {
        $this->options = $options;
        parent::__construct(
            $context,
            $data
        );
    }

    /**
     * Set "name" for <select> element.
     *
     * @param string $value
     *
     * @return $this
     */
    public function setInputName($value)
    {
        return $this->setName($value);
    }

    /**
     * Set "id" for <select> element.
     *
     * @param $value
     *
     * @return $this
     */
    public function setInputId($value)
    {
        return $this->setId($value);
    }

    /**
     * Render block HTML.
     *
     * @return string
     * @codingStandardsIgnoreStart
     */
    public function _toHtml(): string
    {
        // @codingStandardsIgnoreEnd
        if (!$this->getOptions()) {
            $options = $this->options->toOptionArray();
            $options[] = [
                'value' => RelatedTabs::ATTRIBUTE_SET_VALUE_FOR_DEFAULT_TAB,
                'label' => 'NON CONFIGURED ATTRIBUTE SETS',
            ];
            $this->setOptions($options);
        }

        return parent::_toHtml();
    }
}
