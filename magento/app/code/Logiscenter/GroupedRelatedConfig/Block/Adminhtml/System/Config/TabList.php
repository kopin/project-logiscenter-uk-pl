<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\GroupedRelatedConfig\Block\Adminhtml\System\Config;

use Logiscenter\GroupedRelatedConfig\Api\Config\GroupedRelatedConfigInterface;
use Logiscenter\GroupedRelatedConfig\Block\Adminhtml\System\Config\TabList\AttributeSetRenderer;
use Logiscenter\GroupedRelatedConfig\Block\Adminhtml\System\Config\TabList\RelatedTabRendered;
use Logiscenter\GroupedRelatedConfig\Block\Adminhtml\System\Config\TabList\YesNoRenderer;

use Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray;
use Magento\Framework\DataObject;
use Magento\Framework\View\Element\BlockInterface;

/**
 * Template mapping block config class
 */
class TabList extends AbstractFieldArray
{
    /**
     * @var BlockInterface|YesNoRenderer
     */
    private $yesNoRenderer;

    /**
     * @var BlockInterface|AttributeSetRenderer
     */
    private $attributeSetRenderer;

    /**
     * @var BlockInterface|RelatedTabRendered
     */
    private $relatedTabRenderer;

    /**
     * @inheritdoc
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     */
    // @codingStandardsIgnoreStart
    protected function _prepareToRender()
    {
        // @codingStandardsIgnoreEnd
        $this->addColumn(
            GroupedRelatedConfigInterface::TAB_CODE,
            [
                'label'    => __('Related Tab'),
                'renderer' => $this->getRelatedTabRenderer()
            ]
        );
        $this->addColumn(
            GroupedRelatedConfigInterface::ATTRIBUTE_SET,
            [
                'label'    => __('Attribute set'),
                'renderer' => $this->getAttributeSetRenderer()
            ]
        );
        $this->addColumn(
            GroupedRelatedConfigInterface::IS_ENABLED,
            [
                'label'    => __('Enabled'),
                'renderer' => $this->getYesNoRenderer()
            ]
        );
        $this->_addAfter       = true;
        $this->_addButtonLabel = __('Add more');
        parent::_construct();
    }

    private function getYesNoRenderer()
    {
        if (!$this->yesNoRenderer) {
            $this->yesNoRenderer = $this->getLayout()->createBlock(
                YesNoRenderer::class,
                '',
                ['data' => ['is_render_to_js_template' => true]]
            );
        }

        return $this->yesNoRenderer;
    }

    private function getAttributeSetRenderer()
    {
        if (!$this->attributeSetRenderer) {
            $this->attributeSetRenderer = $this->getLayout()->createBlock(
                AttributeSetRenderer::class,
                '',
                ['data' => ['is_render_to_js_template' => true]]
            );
        }

        return $this->attributeSetRenderer;
    }

    private function getRelatedTabRenderer()
    {
        if (!$this->relatedTabRenderer) {
            $this->relatedTabRenderer = $this->getLayout()->createBlock(
                RelatedTabRendered::class,
                '',
                ['data' => ['is_render_to_js_template' => true]]
            );
        }

        return $this->relatedTabRenderer;
    }

    /**
     * @param DataObject $row
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     */
    // @codingStandardsIgnoreStart
    protected function _prepareArrayRow(DataObject $row)
    {
        // @codingStandardsIgnoreEnd
        $optionExtraAttr = [];

        $optionExtraAttr['option_' . $this->getYesNoRenderer()
            ->calcOptionHash($row->getData(GroupedRelatedConfigInterface::IS_ENABLED))]
            = 'selected="selected"';

        $optionExtraAttr['option_' . $this->getAttributeSetRenderer()
            ->calcOptionHash($row->getData(GroupedRelatedConfigInterface::ATTRIBUTE_SET))]
            = 'selected="selected"';

        $optionExtraAttr['option_' . $this->getRelatedTabRenderer()
            ->calcOptionHash($row->getData(GroupedRelatedConfigInterface::TAB_CODE))]
            = 'selected="selected"';

        $row->setData(
            'option_extra_attrs',
            $optionExtraAttr
        );
    }
}
