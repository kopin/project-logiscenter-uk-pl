<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\GroupedRelatedConfig\Api\Config;

use Interactiv4\ConfigHelper\Api\Config\Status\ModuleStatusInterface;

/**
 * Interface GroupedRelatedModuleStatusInterface.
 *
 * Module status provider interface.
 *
 * @api
 */
interface GroupedRelatedModuleStatusInterface extends ModuleStatusInterface
{
}
