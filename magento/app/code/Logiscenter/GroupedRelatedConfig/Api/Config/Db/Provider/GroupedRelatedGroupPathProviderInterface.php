<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\GroupedRelatedConfig\Api\Config\Db\Provider;

use Interactiv4\ConfigHelper\Api\Config\Db\Provider\DbConfigGroupPathProviderInterface;

/**
 * Interface GroupedRelatedGroupPathProviderInterface.
 *
 * Group path provider for groupedrelated group.
 *
 * @api
 */
interface GroupedRelatedGroupPathProviderInterface extends DbConfigGroupPathProviderInterface
{
    public const GROUP_PATH = 'groupedrelated';
}
