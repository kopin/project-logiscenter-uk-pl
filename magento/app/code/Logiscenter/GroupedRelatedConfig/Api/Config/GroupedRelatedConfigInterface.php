<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\GroupedRelatedConfig\Api\Config;

/**
 * Interface GroupedRelatedConfigInterface.
 *
 * Config provider interface
 *
 * @api
 */
interface GroupedRelatedConfigInterface
{
    const ATTRIBUTE_SET = 'attribute_set';
    const TAB_CODE = 'tab_code';
    const IS_ENABLED = 'is_enabled';

    /**
     * @param null $storeId
     *
     * @return array
     */
    public function getAttributeSetsGroupedByTabs($storeId = null): array;

    /**
     * @param string $tabCode
     * @param null   $storeId
     *
     * @return array
     */
    public function getAttributeSetsByTabCode(string $tabCode, $storeId = null): array;

    /**
     * @param string $tabCode
     * @param null   $storeId
     *
     * @return array
     */
    public function getAttributeSetsIds($storeId = null): array;

    /**
     * @param string $tabCode
     * @param null   $storeId
     *
     * @return bool
     */
    public function isDefaultTab(string $tabCode, $storeId = null): bool;
}
