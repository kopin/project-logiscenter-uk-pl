<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\GroupedRelatedConfig\Test\Unit\Plugin\Api\Status;

use Logiscenter\GroupedRelatedConfig\Plugin\Api\Status\CheckGroupedRelatedModuleStatusPlugin;
use Interactiv4\Framework\Test\Unit\Plugin\Api\Status\CheckBooleanStatusPluginTestTrait;
use PHPUnit\Framework\TestCase;

/**
 * Class CheckGroupedRelatedModuleStatusPlugin.
 *
 * Test plugin that allows to intercept module status.
 *
 * @group config
 *
 * @internal
 */
class CheckGroupedRelatedModuleStatusPluginTest extends TestCase
{
    use CheckBooleanStatusPluginTestTrait;

    /**
     * {@inheritdoc}
     */
    protected function getPluginClass(): string
    {
        return CheckGroupedRelatedModuleStatusPlugin::class;
    }
}
