<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\GroupedRelatedConfig\Config;

use Logiscenter\GroupedRelatedConfig\Api\Config\GroupedRelatedEnvironmentConfigInterface;
use Logiscenter\GroupedRelatedConfig\Config\Db\DbGroupedRelatedEnvironmentConfig;

/**
 * Class GroupedRelatedEnvironmentConfig.
 *
 * This is not an api class, implementation may change at any time.
 * @see GroupedRelatedEnvironmentConfigInterface, use this to retrieve config.
 *
 * For example, it now relies on DbGroupedRelatedEnvironmentConfig to read information from database,
 * but this implementation can be changed when needed, without prior notice.
 */
class GroupedRelatedEnvironmentConfig extends DbGroupedRelatedEnvironmentConfig implements
    GroupedRelatedEnvironmentConfigInterface
{
}
