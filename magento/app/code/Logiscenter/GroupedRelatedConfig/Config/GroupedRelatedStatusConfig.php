<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\GroupedRelatedConfig\Config;

use Logiscenter\GroupedRelatedConfig\Api\Config\GroupedRelatedStatusConfigInterface;
use Logiscenter\GroupedRelatedConfig\Config\Db\DbGroupedRelatedStatusConfig;

/**
 * Class GroupedRelatedStatusConfig.
 *
 * This is not an api class, implementation may change at any time.
 * @see GroupedRelatedStatusConfigInterface, use this to retrieve config.
 *
 * For example, it now relies on DbGroupedRelatedStatusConfig to read information from database,
 * but this implementation can be changed when needed, without prior notice.
 */
class GroupedRelatedStatusConfig implements GroupedRelatedStatusConfigInterface
{
    /**
     * @var DbGroupedRelatedStatusConfig
     */
    private $dbStatusConfig;

    /**
     * GroupedRelatedStatusConfig constructor.
     *
     * @param DbGroupedRelatedStatusConfig $dbStatusConfig
     */
    public function __construct(
        DbGroupedRelatedStatusConfig $dbStatusConfig
    ) {
        $this->dbStatusConfig = $dbStatusConfig;
    }

    /**
     * Compare boolean status, using optionally supplied data
     *
     * @param bool $expectedStatusValue
     * @param int|string|null $storeId
     * @return bool
     */
    public function isBooleanStatus(
        bool $expectedStatusValue,
        $storeId = null
    ): bool {
        return $this->dbStatusConfig->isBooleanStatus($expectedStatusValue, $storeId);
    }

    /**
     * Read boolean status, using optionally supplied data
     *
     * @param int|string|null $storeId
     * @return bool
     */
    public function isEnabled($storeId = null): bool
    {
        return $this->dbStatusConfig->isEnabled($storeId);
    }

    /**
     * Enable status, using optionally supplied data
     *
     * @param int|string|null $storeId
     * @return void
     */
    public function enable($storeId = null): void
    {
        $this->dbStatusConfig->enable($storeId);
    }

    /**
     * Disable status, using optionally supplied data
     *
     * @param int|string|null $storeId
     * @return void
     */
    public function disable($storeId = null): void
    {
        $this->dbStatusConfig->disable($storeId);
    }
}
