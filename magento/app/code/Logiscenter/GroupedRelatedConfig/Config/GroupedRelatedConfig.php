<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\GroupedRelatedConfig\Config;

use Logiscenter\GroupedRelatedConfig\Api\Config\GroupedRelatedConfigInterface;
use Logiscenter\GroupedRelatedConfig\Config\Db\DbGroupedRelatedConfig;
use Logiscenter\GroupedRelatedConfig\Model\Source\RelatedTabs as RelatedTabsSource;

/**
 * Class GroupedRelatedConfig.
 *
 * This is not an api class, implementation may change at any time.
 * @see GroupedRelatedConfigInterface, use this to retrieve config.
 *
 * For example, it now relies on DbGroupedRelatedConfig to read information from database,
 * but this implementation can be changed when needed, without prior notice.
 */
class GroupedRelatedConfig extends DbGroupedRelatedConfig implements
    GroupedRelatedConfigInterface
{
    /**
     * {@inheritdoc}
     */
    public function getAttributeSetsGroupedByTabs($storeId = null): array
    {
        $list = parent::getTabList($storeId);
        $list = $this->filterEnabledTabs($list);
        return $this->groupListByTab($list);
    }

    /**
     * {@inheritdoc}
     */
    public function getAttributeSetsByTabCode(string $tabCode, $storeId = null): array
    {
        $fullList = $this->getAttributeSetsGroupedByTabs($storeId);

        return array_key_exists($tabCode, $fullList) ? $fullList[$tabCode] : [];
    }

    /**
     * {@inheritdoc}
     */
    public function getAttributeSetsIds($storeId = null): array
    {
        $list = parent::getTabList($storeId);
        $list = $this->filterEnabledTabs($list);
        $attributeSetIds = [];
        foreach ($list as $tab) {
            if (empty($tab[self::TAB_CODE]) || empty($tab[self::ATTRIBUTE_SET])) {
                continue;
            }
            $attributeSetIds[] = $tab[self::ATTRIBUTE_SET];
        }
        return array_unique($attributeSetIds);
    }


    /**
     * {@inheritdoc}
     */
    public function isDefaultTab(string $tabCode, $storeId = null): bool
    {
        $fullList = $this->getAttributeSetsGroupedByTabs($storeId);
        if (empty($fullList) || !key_exists($tabCode, $fullList)) {
            return false;
        }
        if (in_array(RelatedTabsSource::ATTRIBUTE_SET_VALUE_FOR_DEFAULT_TAB, $fullList[$tabCode])) {
            return true;
        }
        return false;
    }

    /**
     * @param $tabList
     *
     * @return array
     */
    private function filterEnabledTabs($tabList): array
    {
        // Filter by enabled tabs
        return \array_filter(
            $tabList,
            function (array $value): bool {
                return
                    $value[self::IS_ENABLED] != 0;
            }
        );
    }

    /**
     * @param $tabList
     *
     * @return array
     */
    private function groupListByTab($tabList) : array
    {
        $processedTabs = [];
        foreach ($tabList as $tab) {
            if (empty($tab[self::TAB_CODE]) || empty($tab[self::ATTRIBUTE_SET])) {
                continue;
            }
            $processedTabs[$tab[self::TAB_CODE]][] = $tab[self::ATTRIBUTE_SET];
        }
        return $processedTabs;
    }
}
