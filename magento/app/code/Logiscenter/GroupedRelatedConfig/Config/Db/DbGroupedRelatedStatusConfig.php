<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\GroupedRelatedConfig\Config\Db;

use Interactiv4\BaseConfig\Api\Config\Db\Provider\StatusGroupPathProviderInterface;
use Interactiv4\ConfigHelper\Api\Config\Db\DbConfigHelperInterface;
use Interactiv4\ConfigHelper\Api\Config\Db\DbStatusTrait;
use Interactiv4\ConfigHelper\Api\Config\Db\Provider\DbConfigGroupPathProviderTrait;
use Interactiv4\ConfigHelper\Api\Config\Db\Provider\DbConfigSectionPathProviderTrait;
use Interactiv4\ConfigHelper\Api\Config\Db\DbStatusInterface;
use Interactiv4\ConfigHelper\Model\Config\Db\DbConfigHelper;
use Logiscenter\GroupedRelatedConfig\Api\Config\Db\Provider\GroupedRelatedSectionPathProviderInterface;

/**
 * Class DbGroupedRelatedStatusConfig.
 *
 * This is not an api class, implementation may change at any time.
 * @see \Logiscenter\GroupedRelatedConfig\Api\Config\GroupedRelatedStatusConfigInterface,
 * use this to retrieve config.
 */
class DbGroupedRelatedStatusConfig extends DbConfigHelper implements
    GroupedRelatedSectionPathProviderInterface,
    StatusGroupPathProviderInterface,
    DbStatusInterface,
    DbConfigHelperInterface
{
    use DbConfigSectionPathProviderTrait;
    use DbConfigGroupPathProviderTrait;
    use DbStatusTrait;
}
