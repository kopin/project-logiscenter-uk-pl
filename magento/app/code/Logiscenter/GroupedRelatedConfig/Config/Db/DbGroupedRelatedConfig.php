<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\GroupedRelatedConfig\Config\Db;

use Interactiv4\ConfigHelper\Api\Config\Db\DbConfigHelperInterface;
use Interactiv4\ConfigHelper\Api\Config\Db\Provider\DbConfigGroupPathProviderTrait;
use Interactiv4\ConfigHelper\Api\Config\Db\Provider\DbConfigSectionPathProviderTrait;
use Interactiv4\ConfigHelper\Model\Config\Db\DbConfigHelper;
use Logiscenter\GroupedRelatedConfig\Api\Config\Db\Provider\GroupedRelatedGroupPathProviderInterface;
use Logiscenter\GroupedRelatedConfig\Api\Config\Db\Provider\GroupedRelatedSectionPathProviderInterface;

use Logiscenter\CountrySwitcherConfig\Api\Config\CountrySwitcherConfigInterface;
use Logiscenter\CountrySwitcherConfig\Api\Config\Db\Provider\CountrySwitcherGroupPathProviderInterface;
use Logiscenter\CountrySwitcherConfig\Api\Config\Db\Provider\GeneralSectionPathProviderInterface;
use Magento\Config\Model\ResourceModel\Config as ResourceConfig;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Serialize\SerializerInterface;

/**
 * Class DbGroupedRelatedConfig.
 *
 * This is not an api class, implementation may change at any time.
 *
 * @see \Logiscenter\GroupedRelatedConfig\Api\Config\GroupedRelatedConfigInterface,
 * use this to retrieve config.
 */
class DbGroupedRelatedConfig extends DbConfigHelper implements
    GroupedRelatedSectionPathProviderInterface,
    GroupedRelatedGroupPathProviderInterface,
    DbConfigHelperInterface
{
    use DbConfigSectionPathProviderTrait;
    use DbConfigGroupPathProviderTrait;

    private const XML_FIELD_TAB_LIST = 'tab_list';

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @param ScopeConfigInterface $scopeConfig
     * @param ResourceConfig       $resourceConfig
     * @param SerializerInterface  $serializer
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        ResourceConfig $resourceConfig,
        SerializerInterface $serializer
    ) {
        parent::__construct($scopeConfig, $resourceConfig);
        $this->serializer = $serializer;
    }

    /**
     * @param int|string|null $storeId
     *
     * @return string
     */
    public function getTabList($storeId = null): array
    {
        return $this->getConfig(self::XML_FIELD_TAB_LIST, $storeId)
            ? $this->serializer->unserialize($this->getConfig(self::XML_FIELD_TAB_LIST))
            : [];
    }
}
