<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\GroupedRelatedConfig\Config;

use Logiscenter\GroupedRelatedConfig\Api\Config\GroupedRelatedModuleStatusInterface;
use Logiscenter\GroupedRelatedConfig\Api\Config\GroupedRelatedStatusConfigInterface;

/**
 * Class GroupedRelatedModuleStatus.
 *
 * This is not an api class, implementation may change at any time.
 * @see GroupedRelatedModuleStatusInterface, use this to retrieve config.
 *
 * For example, it now relies on GroupedRelatedStatusConfigInterface to read config,
 * but this implementation can be changed when needed, without prior notice.
 */
class GroupedRelatedModuleStatus implements GroupedRelatedModuleStatusInterface
{
    /**
     * @var GroupedRelatedStatusConfigInterface
     */
    private $statusConfig;

    /**
     * GroupedRelatedModuleStatus constructor.
     * @param GroupedRelatedStatusConfigInterface $statusConfig
     */
    public function __construct(
        GroupedRelatedStatusConfigInterface $statusConfig
    ) {
        $this->statusConfig = $statusConfig;
    }

    /**
     * Compare boolean status, using optionally supplied data
     *
     * @param bool $expectedStatusValue
     * @param int|string|null $storeId
     * @return bool
     */
    public function isBooleanStatus(
        bool $expectedStatusValue,
        $storeId = null
    ): bool {
        return $this->statusConfig->isBooleanStatus($expectedStatusValue, $storeId);
    }

    /**
     * Read boolean status, using optionally supplied data
     *
     * @param int|string|null $storeId
     * @return bool
     */
    public function isEnabled($storeId = null): bool
    {
        return $this->statusConfig->isEnabled($storeId);
    }
}
