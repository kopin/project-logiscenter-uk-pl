<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\GroupedRelatedConfig\Model\Source;

use Interactiv4\Framework\Data\AbstractOptionSource;

/**
 * Class RelatedTabs.
 */
class RelatedTabs extends AbstractOptionSource
{
    const ATTRIBUTE_SET_VALUE_FOR_DEFAULT_TAB = 'non-configured';

    /**
     * @var array
     */
    private $relatedTabs;

    /**
     * RelatedTabs constructor.
     *
     * @param array $relatedTabs
     */
    public function __construct(
        array $relatedTabs = []
    ) {
        $this->relatedTabs = $relatedTabs;
    }

    /**
     * To option array.
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options = [];
        foreach ($this->toArray() as $value => $data) {
            $options[] = [
                'value' => $value,
                'label' => __($data['label']),
            ];
        }

        return $options;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return $this->relatedTabs;
    }
}
