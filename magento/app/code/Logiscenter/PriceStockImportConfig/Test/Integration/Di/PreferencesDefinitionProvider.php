<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\PriceStockImportConfig\Test\Integration\Di;

use Interactiv4\Framework\Api\Di\Definition\DefinitionPoolInterface;
use Interactiv4\Framework\Api\Di\Definition\DefinitionPoolProviderInterface;
use Interactiv4\Framework\Di\Definition\Definition;
use Interactiv4\Framework\Di\Definition\DefinitionPool;
use Interactiv4\Framework\Di\Definition\DefinitionName;
use Interactiv4\Framework\Di\Definition\DefinitionValue;

/**
 * Class PreferencesDefinitionProvider.
 *
 * Preferences definition as they appear in di configuration files.
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class PreferencesDefinitionProvider implements DefinitionPoolProviderInterface
{
    /**
     * @var DefinitionPoolInterface
     */
    private $definitionPool;

    /**
     * {@inheritdoc}
     *
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function getDefinitionPool(): DefinitionPoolInterface
    {
        if (!isset($this->definitionPool)) {
            $this->definitionPool = new DefinitionPool(
	new Definition(
	new DefinitionName(
	'Logiscenter\\PriceStockImportConfig\\Api\\Config\\PriceStockImportModuleStatusInterface'
	),
		new DefinitionValue(
	\Logiscenter\PriceStockImportConfig\Api\Config\PriceStockImportModuleStatusInterface::class,
		\Logiscenter\PriceStockImportConfig\Config\PriceStockImportModuleStatus::class,
		(string) \Magento\Framework\App\Area::AREA_GLOBAL,
		false,
		null,
		null
	)
	),
		new Definition(
	new DefinitionName(
	'Logiscenter\\PriceStockImportConfig\\Api\\Config\\PriceStockImportStatusConfigInterface'
	),
		new DefinitionValue(
	\Logiscenter\PriceStockImportConfig\Api\Config\PriceStockImportStatusConfigInterface::class,
		\Logiscenter\PriceStockImportConfig\Config\PriceStockImportStatusConfig::class,
		(string) \Magento\Framework\App\Area::AREA_GLOBAL,
		false,
		null,
		null
	)
	),
		new Definition(
	new DefinitionName(
	'Logiscenter\\PriceStockImportConfig\\Api\\Config\\PriceStockImportEnvironmentConfigInterface'
	),
		new DefinitionValue(
	\Logiscenter\PriceStockImportConfig\Api\Config\PriceStockImportEnvironmentConfigInterface::class,
		\Logiscenter\PriceStockImportConfig\Config\PriceStockImportEnvironmentConfig::class,
		(string) \Magento\Framework\App\Area::AREA_GLOBAL,
		false,
		null,
		null
	)
	),
		new Definition(
	new DefinitionName(
	'Logiscenter\\PriceStockImportConfig\\Api\\Config\\PriceStockImportConfigInterface'
	),
		new DefinitionValue(
	\Logiscenter\PriceStockImportConfig\Api\Config\PriceStockImportConfigInterface::class,
		\Logiscenter\PriceStockImportConfig\Config\PriceStockImportConfig::class,
		(string) \Magento\Framework\App\Area::AREA_GLOBAL,
		false,
		null,
		null
	)
	)
	);
        }

        return $this->definitionPool;
    }
}
