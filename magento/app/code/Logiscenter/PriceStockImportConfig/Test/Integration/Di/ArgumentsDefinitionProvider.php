<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\PriceStockImportConfig\Test\Integration\Di;

use Interactiv4\Framework\Api\Di\Definition\DefinitionPoolInterface;
use Interactiv4\Framework\Api\Di\Definition\DefinitionPoolProviderInterface;
use Interactiv4\Framework\Di\Definition\Definition;
use Interactiv4\Framework\Di\Definition\DefinitionPool;
use Interactiv4\Framework\Di\Definition\DefinitionName;
use Interactiv4\Framework\Di\Definition\DefinitionValue;

/**
 * Class ArgumentsDefinitionProvider.
 *
 * Arguments definition as they appear in di configuration files.
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class ArgumentsDefinitionProvider implements DefinitionPoolProviderInterface
{
    /**
     * @var DefinitionPoolInterface
     */
    private $definitionPool;

    /**
     * {@inheritdoc}
     *
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function getDefinitionPool(): DefinitionPoolInterface
    {
        if (!isset($this->definitionPool)) {
            $this->definitionPool = new DefinitionPool(
	new Definition(
	new DefinitionName(
	'Logiscenter\\PriceStockImportConfig\\Plugin\\Api\\Status\\CheckPriceStockImportModuleStatusPlugin',
		'booleanStatusRead'
	),
		new DefinitionValue(
	\Logiscenter\PriceStockImportConfig\Plugin\Api\Status\CheckPriceStockImportModuleStatusPlugin::class,
		\Logiscenter\PriceStockImportConfig\Api\Config\PriceStockImportModuleStatusInterface::class,
		(string) \Magento\Framework\App\Area::AREA_GLOBAL,
		false,
		null,
		null
	)
	),
		new Definition(
	new DefinitionName(
	'Logiscenter\\PriceStockImportConfig\\Plugin\\Api\\Status\\CheckPriceStockImportEnvironmentPlugin',
		'allowedEnvironmentsAware'
	),
		new DefinitionValue(
	\Logiscenter\PriceStockImportConfig\Plugin\Api\Status\CheckPriceStockImportEnvironmentPlugin::class,
		\Logiscenter\PriceStockImportConfig\Api\Config\PriceStockImportEnvironmentConfigInterface::class,
		(string) \Magento\Framework\App\Area::AREA_GLOBAL,
		false,
		null,
		null
	)
	),
		new Definition(
	new DefinitionName(
	'Interactiv4\\BaseConfig\\Pool\\ModuleStatusPool',
		'objects',
		'Logiscenter_PriceStockImportConfig'
	),
		new DefinitionValue(
	'Interactiv4\\BaseConfig\\Pool\\ModuleStatusPool',
		\Logiscenter\PriceStockImportConfig\Api\Config\PriceStockImportModuleStatusInterface::class,
		(string) \Magento\Framework\App\Area::AREA_GLOBAL,
		false,
		null,
		null
	)
	),
		new Definition(
	new DefinitionName(
	'Logiscenter\\PriceStockImportConfig\\Observer\\RefreshDataUponConfigChangeObserver',
		'refreshCacheStrategy'
	),
		new DefinitionValue(
	\Logiscenter\PriceStockImportConfig\Observer\RefreshDataUponConfigChangeObserver::class,
		\Interactiv4\Framework\Cache\RefreshCacheAppStateDependantStrategy::class,
		(string) \Magento\Framework\App\Area::AREA_ADMINHTML,
		false,
		null,
		null
	)
	),
		new Definition(
	new DefinitionName(
	'Logiscenter\\PriceStockImportConfig\\Observer\\RefreshDataUponConfigChangeObserver',
		'cacheTypes',
		'config'
	),
		new DefinitionValue(
	\Logiscenter\PriceStockImportConfig\Observer\RefreshDataUponConfigChangeObserver::class,
		(string) \Magento\Framework\App\Cache\Type\Config::TYPE_IDENTIFIER,
		(string) \Magento\Framework\App\Area::AREA_ADMINHTML,
		false,
		null,
		null
	)
	),
		new Definition(
	new DefinitionName(
	'Logiscenter\\PriceStockImportConfig\\Observer\\RefreshDataUponConfigChangeObserver',
		'cacheTypes',
		'block_html'
	),
		new DefinitionValue(
	\Logiscenter\PriceStockImportConfig\Observer\RefreshDataUponConfigChangeObserver::class,
		(string) \Magento\Framework\App\Cache\Type\Block::TYPE_IDENTIFIER,
		(string) \Magento\Framework\App\Area::AREA_ADMINHTML,
		false,
		null,
		null
	)
	),
		new Definition(
	new DefinitionName(
	'Logiscenter\\PriceStockImportConfig\\Observer\\RefreshDataUponConfigChangeObserver',
		'cacheTypes',
		'full_page'
	),
		new DefinitionValue(
	\Logiscenter\PriceStockImportConfig\Observer\RefreshDataUponConfigChangeObserver::class,
		(string) \Magento\PageCache\Model\Cache\Type::TYPE_IDENTIFIER,
		(string) \Magento\Framework\App\Area::AREA_ADMINHTML,
		false,
		null,
		null
	)
	)
	);
        }

        return $this->definitionPool;
    }
}
