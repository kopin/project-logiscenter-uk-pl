<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\PriceStockImportConfig\Api\Config;

/**
 * Interface PriceStockImportConfigInterface.
 *
 * Config provider interface
 *
 * @api
 */
interface PriceStockImportConfigInterface
{
    /**
     * @param int|string|null $storeId
     *
     * @return string
     */
    public function getImportFolder($storeId = null): string;

    /**
     * @param int|string|null $storeId
     *
     * @return string
     */
    public function getProcessedFolder($storeId = null): string;

    /**
     * @param int|string|null $storeId
     *
     * @return string
     */
    public function getErrorFolder($storeId = null): string;
}
