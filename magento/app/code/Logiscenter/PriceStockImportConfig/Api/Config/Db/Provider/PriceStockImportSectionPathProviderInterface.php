<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\PriceStockImportConfig\Api\Config\Db\Provider;

use Interactiv4\ConfigHelper\Api\Config\Db\Provider\DbConfigSectionPathProviderInterface;

/**
 * Interface PriceStockImportSectionPathProviderInterface.
 *
 * Section path provider for logiscenter_pricestockimportconfig section.
 *
 * @api
 */
interface PriceStockImportSectionPathProviderInterface extends DbConfigSectionPathProviderInterface
{
    public const SECTION_PATH = 'logiscenter_pricestockimportconfig';
}
