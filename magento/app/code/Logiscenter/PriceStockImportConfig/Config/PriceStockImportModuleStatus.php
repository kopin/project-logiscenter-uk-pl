<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\PriceStockImportConfig\Config;

use Logiscenter\PriceStockImportConfig\Api\Config\PriceStockImportModuleStatusInterface;
use Logiscenter\PriceStockImportConfig\Api\Config\PriceStockImportStatusConfigInterface;

/**
 * Class PriceStockImportModuleStatus.
 *
 * This is not an api class, implementation may change at any time.
 * @see PriceStockImportModuleStatusInterface, use this to retrieve config.
 *
 * For example, it now relies on PriceStockImportStatusConfigInterface to read config,
 * but this implementation can be changed when needed, without prior notice.
 */
class PriceStockImportModuleStatus implements PriceStockImportModuleStatusInterface
{
    /**
     * @var PriceStockImportStatusConfigInterface
     */
    private $statusConfig;

    /**
     * PriceStockImportModuleStatus constructor.
     * @param PriceStockImportStatusConfigInterface $statusConfig
     */
    public function __construct(
        PriceStockImportStatusConfigInterface $statusConfig
    ) {
        $this->statusConfig = $statusConfig;
    }

    /**
     * Compare boolean status, using optionally supplied data
     *
     * @param bool $expectedStatusValue
     * @param int|string|null $storeId
     * @return bool
     */
    public function isBooleanStatus(
        bool $expectedStatusValue,
        $storeId = null
    ): bool {
        return $this->statusConfig->isBooleanStatus($expectedStatusValue, $storeId);
    }

    /**
     * Read boolean status, using optionally supplied data
     *
     * @param int|string|null $storeId
     * @return bool
     */
    public function isEnabled($storeId = null): bool
    {
        return $this->statusConfig->isEnabled($storeId);
    }
}
