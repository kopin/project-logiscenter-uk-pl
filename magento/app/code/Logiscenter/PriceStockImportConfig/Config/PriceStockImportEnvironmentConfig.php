<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\PriceStockImportConfig\Config;

use Logiscenter\PriceStockImportConfig\Api\Config\PriceStockImportEnvironmentConfigInterface;
use Logiscenter\PriceStockImportConfig\Config\Db\DbPriceStockImportEnvironmentConfig;

/**
 * Class PriceStockImportEnvironmentConfig.
 *
 * This is not an api class, implementation may change at any time.
 * @see PriceStockImportEnvironmentConfigInterface, use this to retrieve config.
 *
 * For example, it now relies on DbPriceStockImportEnvironmentConfig to read information from database,
 * but this implementation can be changed when needed, without prior notice.
 */
class PriceStockImportEnvironmentConfig extends DbPriceStockImportEnvironmentConfig implements
    PriceStockImportEnvironmentConfigInterface
{
}
