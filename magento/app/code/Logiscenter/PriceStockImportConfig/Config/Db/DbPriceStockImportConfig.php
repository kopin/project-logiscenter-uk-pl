<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\PriceStockImportConfig\Config\Db;

use Interactiv4\ConfigHelper\Api\Config\Db\DbConfigHelperInterface;
use Interactiv4\ConfigHelper\Api\Config\Db\Provider\DbConfigGroupPathProviderTrait;
use Interactiv4\ConfigHelper\Api\Config\Db\Provider\DbConfigSectionPathProviderTrait;
use Interactiv4\ConfigHelper\Model\Config\Db\DbConfigHelper;
use Logiscenter\PriceStockImportConfig\Api\Config\Db\Provider\PriceStockImportGroupPathProviderInterface;
use Logiscenter\PriceStockImportConfig\Api\Config\Db\Provider\PriceStockImportSectionPathProviderInterface;

/**
 * Class DbPriceStockImportConfig.
 *
 * This is not an api class, implementation may change at any time.
 * @see \Logiscenter\PriceStockImportConfig\Api\Config\PriceStockImportConfigInterface,
 * use this to retrieve config.
 */
class DbPriceStockImportConfig extends DbConfigHelper implements
    PriceStockImportSectionPathProviderInterface,
    PriceStockImportGroupPathProviderInterface,
    DbConfigHelperInterface
{
    use DbConfigSectionPathProviderTrait;
    use DbConfigGroupPathProviderTrait;

    private const XML_FIELD_IMPORT_FOLDER = 'import_folder';

    private const XML_FIELD_PROCESSED_FOLDER = 'processed_folder';

    private const XML_FIELD_ERROR_FOLDER = 'error_folder';

    /**
     * @param int|string|null $storeId
     *
     * @return string
     */
    public function getImportFolder($storeId = null): string
    {
        return $this->getConfig(self::XML_FIELD_IMPORT_FOLDER, $storeId);
    }

    /**
     * @param int|string|null $storeId
     *
     * @return string
     */
    public function getProcessedFolder($storeId = null): string
    {
        return (string)$this->getConfig(self::XML_FIELD_PROCESSED_FOLDER, $storeId);
    }

    /**
     * @param int|string|null $storeId
     *
     * @return string
     */
    public function getErrorFolder($storeId = null): string
    {
        return (string)$this->getConfig(self::XML_FIELD_ERROR_FOLDER, $storeId);
    }
}
