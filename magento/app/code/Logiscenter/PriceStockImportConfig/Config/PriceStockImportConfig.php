<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\PriceStockImportConfig\Config;

use Logiscenter\PriceStockImportConfig\Api\Config\PriceStockImportConfigInterface;
use Logiscenter\PriceStockImportConfig\Config\Db\DbPriceStockImportConfig;

/**
 * Class PriceStockImportConfig.
 *
 * This is not an api class, implementation may change at any time.
 * @see PriceStockImportConfigInterface, use this to retrieve config.
 *
 * For example, it now relies on DbPriceStockImportConfig to read information from database,
 * but this implementation can be changed when needed, without prior notice.
 */
class PriceStockImportConfig extends DbPriceStockImportConfig implements
    PriceStockImportConfigInterface
{
}
