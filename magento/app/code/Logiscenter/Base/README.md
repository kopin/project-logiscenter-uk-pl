# Interactiv4 Module Base

Description
-----------
Module base


Versioning
----------
This package follows semver for versioning.


Minimum Compatibility
---------------------
- PHP: 7.0.2|7.0.4|~7.0.6|~7.1.0

- Magento: ~2.2.7, as previous Magento versions do not generate proxies with appropriate strict type return value.


Installation Instructions
-------------------------
You can install this package using composer by adding it to your composer file using following command:

`composer require logiscenter/module-base --update-with-all-dependencies`

Finally, run setup upgrade to enable new modules:

`php magento/bin/magento setup:upgrade --keep-generated`


Support
-------
Refer to [technical contacts](https://bitbucket.org/interactiv4/project-develop/wiki/Technical%20Contacts) for further information and support.


Credits
---------
Supported and maintained by Interactiv4 Team.


Contribution
------------
Any contribution is highly appreciated.
The best way to contribute code is to open a [pull request on Bitbucket](https://bitbucket.org/logiscenter/module-base/pull-requests/new).


License
-------
Respect the [Magento OSL license](https://raw.githubusercontent.com/magento/magento2/2.2/LICENSE.txt).

Do not distribute or share this code unless you are authorized to do so.


Copyright
---------
Copyright (c) 2019 Interactiv4 S.L.