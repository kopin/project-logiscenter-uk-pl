/*
 * @author Interactiv4 Team
 * @copyright  Copyright © Interactiv4 (https://www.interactiv4.com)
 *
 */

define([
    'jquery'
], function($) {
    'use strict';

    $.widget('logiscenter.authors', {

        show : function () {
            var accountManager      = [''];
            var projectManager      = ['Jorge Clavería'];
            var developerLead       = [''];
            var technicalLead       = ['Rebeca Martinez'];
            var frontendLead        = [''];

            console.log('********************************************************************************');
            console.log('********************************************************************************');
            console.log('************************* INTERACTIV4 TEAM *************************************');
            console.log('');
            console.log('Account Manager: ' + accountManager.join(', '));
            console.log('Project Manager: ' + projectManager.join(', '));
            console.log('Lead Developer: ' + developerLead.join(', '));
            console.log('Technical Lead: ' + technicalLead.join(', '));
            console.log('Frontend Lead: ' + frontendLead.join(', '));
            console.log('');
            console.log('********************************************************************************');
            console.log('********************************************************************************');
        }

    });

    return $.logiscenter.authors;
});