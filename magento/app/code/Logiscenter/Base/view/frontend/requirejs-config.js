/*
 * @author Interactiv4 Team
 * @copyright  Copyright © Interactiv4 (https://www.interactiv4.com)
 *
 */

var config = {
    deps: [
        "jquery"
    ],
    'paths': {
        'authors' : 'Logiscenter_Base/js/authors'
    },
    map: {
        '*': {
            'authors'  : 'Logiscenter_Base/js/authors'
        }
    }
};