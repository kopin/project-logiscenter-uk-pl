<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\RestrictedProductInfoConfig\Config;

use Logiscenter\RestrictedProductInfoConfig\Api\Config\RestrictedProductInfoEnvironmentConfigInterface;
use Logiscenter\RestrictedProductInfoConfig\Config\Db\DbRestrictedProductInfoEnvironmentConfig;

/**
 * Class RestrictedProductInfoEnvironmentConfig.
 *
 * This is not an api class, implementation may change at any time.
 * @see RestrictedProductInfoEnvironmentConfigInterface, use this to retrieve config.
 *
 * For example, it now relies on DbRestrictedProductInfoEnvironmentConfig to read information from database,
 * but this implementation can be changed when needed, without prior notice.
 */
class RestrictedProductInfoEnvironmentConfig extends DbRestrictedProductInfoEnvironmentConfig implements
    RestrictedProductInfoEnvironmentConfigInterface
{
}
