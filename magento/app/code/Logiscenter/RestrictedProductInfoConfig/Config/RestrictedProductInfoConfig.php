<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\RestrictedProductInfoConfig\Config;

use Logiscenter\RestrictedProductInfoConfig\Api\Config\RestrictedProductInfoConfigInterface;
use Logiscenter\RestrictedProductInfoConfig\Config\Db\DbRestrictedProductInfoConfig;

/**
 * Class RestrictedProductInfoConfig.
 *
 * This is not an api class, implementation may change at any time.
 * @see RestrictedProductInfoConfigInterface, use this to retrieve config.
 *
 * For example, it now relies on DbRestrictedProductInfoConfig to read information from database,
 * but this implementation can be changed when needed, without prior notice.
 */
class RestrictedProductInfoConfig extends DbRestrictedProductInfoConfig implements
    RestrictedProductInfoConfigInterface
{
}
