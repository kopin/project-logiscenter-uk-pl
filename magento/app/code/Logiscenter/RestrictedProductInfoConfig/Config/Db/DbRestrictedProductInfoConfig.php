<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\RestrictedProductInfoConfig\Config\Db;

use Interactiv4\ConfigHelper\Api\Config\Db\DbConfigHelperInterface;
use Interactiv4\ConfigHelper\Api\Config\Db\Provider\DbConfigGroupPathProviderTrait;
use Interactiv4\ConfigHelper\Api\Config\Db\Provider\DbConfigSectionPathProviderTrait;
use Interactiv4\ConfigHelper\Model\Config\Db\DbConfigHelper;
use Logiscenter\RestrictedProductInfoConfig\Api\Config\Db\Provider\RestrictedProductInfoGroupPathProviderInterface;
use Logiscenter\RestrictedProductInfoConfig\Api\Config\Db\Provider\RestrictedProductInfoSectionPathProviderInterface;
use Logiscenter\RestrictedProductInfoConfig\Api\Config\RestrictedProductInfoConfigInterface;
use Magento\Config\Model\ResourceModel\Config as ResourceConfig;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Serialize\SerializerInterface;

/**
 * Class DbRestrictedProductInfoConfig.
 *
 * This is not an api class, implementation may change at any time.
 *
 * @see \Logiscenter\RestrictedProductInfoConfig\Api\Config\RestrictedProductInfoConfigInterface,
 * use this to retrieve config.
 */
class DbRestrictedProductInfoConfig extends DbConfigHelper implements
    RestrictedProductInfoSectionPathProviderInterface,
    RestrictedProductInfoGroupPathProviderInterface,
    DbConfigHelperInterface
{
    use DbConfigSectionPathProviderTrait;
    use DbConfigGroupPathProviderTrait;

    private const ALLOWED_IDS = 'ip_whitelisted';

    /**
     * @var SerializerInterface
     */
    private $serializer;

    public function __construct(
        ScopeConfigInterface $scopeConfig,
        ResourceConfig $resourceConfig,
        SerializerInterface $serializer
    ) {
        parent::__construct($scopeConfig, $resourceConfig);
        $this->serializer = $serializer;
    }

    /**
     * @param int|string|null $storeId
     *
     * @return array
     */
    public function getWhitelistedIps($storeId = null): array
    {
        $result = [];
        if (!$this->getConfig(self::ALLOWED_IDS, $storeId)) {
            return $result;
        }

        $data = $this->serializer->unserialize($this->getConfig(self::ALLOWED_IDS));
        foreach ($data as $row) {
            $result[] = $row[RestrictedProductInfoConfigInterface::IP_WHITELISTED];
        }

        return $result;
    }
}
