<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\RestrictedProductInfoConfig\Test\Integration\Di;

use Interactiv4\Framework\Api\Di\Definition\DefinitionPoolInterface;
use Interactiv4\Framework\Api\Di\Definition\DefinitionPoolProviderInterface;
use Interactiv4\Framework\Di\Definition\Definition;
use Interactiv4\Framework\Di\Definition\DefinitionPool;
use Interactiv4\Framework\Di\Definition\DefinitionName;
use Interactiv4\Framework\Di\Definition\DefinitionValue;

/**
 * Class PreferencesDefinitionProvider.
 *
 * Preferences definition as they appear in di configuration files.
 */
class PreferencesDefinitionProvider implements DefinitionPoolProviderInterface
{
    /**
     * @var DefinitionPoolInterface
     */
    private $definitionPool;

    /**
     * {@inheritdoc}
     */
    public function getDefinitionPool(): DefinitionPoolInterface
    {
        if (!isset($this->definitionPool)) {
            $this->definitionPool = new DefinitionPool(
                new Definition(
                    new DefinitionName(
                        'Logiscenter\\RestrictedProductInfoConfig\\Api\\Config\\RestrictedProductInfoModuleStatusInterface'
                    ),
                    new DefinitionValue(
                        \Logiscenter\RestrictedProductInfoConfig\Api\Config\RestrictedProductInfoModuleStatusInterface::class,
                        \Logiscenter\RestrictedProductInfoConfig\Config\RestrictedProductInfoModuleStatus::class,
                        (string) \Magento\Framework\App\Area::AREA_GLOBAL,
                        false,
                        null,
                        null
                    )
                ),
                new Definition(
                    new DefinitionName(
                        'Logiscenter\\RestrictedProductInfoConfig\\Api\\Config\\RestrictedProductInfoStatusConfigInterface'
                    ),
                    new DefinitionValue(
                        \Logiscenter\RestrictedProductInfoConfig\Api\Config\RestrictedProductInfoStatusConfigInterface::class,
                        \Logiscenter\RestrictedProductInfoConfig\Config\RestrictedProductInfoStatusConfig::class,
                        (string) \Magento\Framework\App\Area::AREA_GLOBAL,
                        false,
                        null,
                        null
                    )
                ),
                new Definition(
                    new DefinitionName(
                        'Logiscenter\\RestrictedProductInfoConfig\\Api\\Config\\RestrictedProductInfoEnvironmentConfigInterface'
                    ),
                    new DefinitionValue(
                        \Logiscenter\RestrictedProductInfoConfig\Api\Config\RestrictedProductInfoEnvironmentConfigInterface::class,
                        \Logiscenter\RestrictedProductInfoConfig\Config\RestrictedProductInfoEnvironmentConfig::class,
                        (string) \Magento\Framework\App\Area::AREA_GLOBAL,
                        false,
                        null,
                        null
                    )
                ),
                new Definition(
                    new DefinitionName(
                        'Logiscenter\\RestrictedProductInfoConfig\\Api\\Config\\RestrictedProductInfoConfigInterface'
                    ),
                    new DefinitionValue(
                        \Logiscenter\RestrictedProductInfoConfig\Api\Config\RestrictedProductInfoConfigInterface::class,
                        \Logiscenter\RestrictedProductInfoConfig\Config\RestrictedProductInfoConfig::class,
                        (string) \Magento\Framework\App\Area::AREA_GLOBAL,
                        false,
                        null,
                        null
                    )
                )
            );
        }

        return $this->definitionPool;
    }
}
