<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\RestrictedProductInfoConfig\Api\Config;

/**
 * Interface RestrictedProductInfoConfigInterface.
 *
 * Config provider interface
 *
 * @api
 */
interface RestrictedProductInfoConfigInterface
{
    public const IP_WHITELISTED = 'ip_whitelisted';

    /**
     * @param int|string|null $storeId
     *
     * @return array
     */
    public function getWhitelistedIps($storeId = null): array;
}
