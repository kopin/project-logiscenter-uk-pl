<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\RestrictedProductInfoConfig\Api\Config;

use Interactiv4\ConfigHelper\Api\Config\Status\ModuleStatusInterface;

/**
 * Interface RestrictedProductInfoModuleStatusInterface.
 *
 * Module status provider interface.
 *
 * @api
 */
interface RestrictedProductInfoModuleStatusInterface extends ModuleStatusInterface
{
}
