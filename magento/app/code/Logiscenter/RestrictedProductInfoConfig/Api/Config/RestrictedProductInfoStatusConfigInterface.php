<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\RestrictedProductInfoConfig\Api\Config;

use Interactiv4\Contracts\SPL\BooleanStatus\Api\BooleanStatusCompareInterface;
use Interactiv4\Contracts\SPL\BooleanStatus\Api\BooleanStatusPutInterface;
use Interactiv4\Contracts\SPL\BooleanStatus\Api\BooleanStatusReadInterface;

/**
 * Interface RestrictedProductInfoStatusConfigInterface.
 *
 * Status provider interface.
 *
 * @api
 */
interface RestrictedProductInfoStatusConfigInterface extends
    BooleanStatusReadInterface,
    BooleanStatusCompareInterface,
    BooleanStatusPutInterface
{
}
