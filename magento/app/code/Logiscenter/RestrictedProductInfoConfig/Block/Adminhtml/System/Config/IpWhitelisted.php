<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\RestrictedProductInfoConfig\Block\Adminhtml\System\Config;

use Logiscenter\RestrictedProductInfoConfig\Api\Config\RestrictedProductInfoConfigInterface;
use Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray;

/**
 * Class IpWhitelisted
 */
class IpWhitelisted extends AbstractFieldArray
{
    /**
     * @inheritdoc
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     */
    // @codingStandardsIgnoreStart
    protected function _prepareToRender()
    {
        // @codingStandardsIgnoreEnd
        $this->addColumn(
            RestrictedProductInfoConfigInterface::IP_WHITELISTED,
            [
                'label'    => __('IP Whitelisted'),
            ]
        );
        $this->_addAfter       = true;
        $this->_addButtonLabel = __('Add more');
        parent::_construct();
    }
}
