<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Logiscenter\PriceConfig\Config;

use Logiscenter\PriceConfig\Api\Config\PriceModuleStatusInterface;
use Logiscenter\PriceConfig\Api\Config\PriceStatusConfigInterface;

/**
 * Class PriceModuleStatus.
 *
 * This is not an api class, implementation may change at any time.
 * @see PriceModuleStatusInterface, use this to retrieve config.
 *
 * For example, it now relies on PriceStatusConfigInterface to read config,
 * but this implementation can be changed when needed, without prior notice.
 */
class PriceModuleStatus implements PriceModuleStatusInterface
{
    /**
     * @var PriceStatusConfigInterface
     */
    private $statusConfig;

    /**
     * PriceModuleStatus constructor.
     * @param PriceStatusConfigInterface $statusConfig
     */
    public function __construct(
        PriceStatusConfigInterface $statusConfig
    ) {
        $this->statusConfig = $statusConfig;
    }

    /**
     * Compare boolean status, using optionally supplied data
     *
     * @param bool $expectedStatusValue
     * @param int|string|null $storeId
     * @return bool
     */
    public function isBooleanStatus(
        bool $expectedStatusValue,
        $storeId = null
    ): bool {
        return $this->statusConfig->isBooleanStatus($expectedStatusValue, $storeId);
    }

    /**
     * Read boolean status, using optionally supplied data
     *
     * @param int|string|null $storeId
     * @return bool
     */
    public function isEnabled($storeId = null): bool
    {
        return $this->statusConfig->isEnabled($storeId);
    }
}
