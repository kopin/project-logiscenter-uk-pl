<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Logiscenter\PriceConfig\Config\Db;

use Interactiv4\ConfigHelper\Api\Config\Db\DbConfigHelperInterface;
use Interactiv4\ConfigHelper\Api\Config\Db\Provider\DbConfigGroupPathProviderTrait;
use Interactiv4\ConfigHelper\Api\Config\Db\Provider\DbConfigSectionPathProviderTrait;
use Interactiv4\ConfigHelper\Model\Config\Db\DbConfigHelper;
use Logiscenter\PriceConfig\Api\Config\Db\Provider\PriceGroupPathProviderInterface;
use Logiscenter\PriceConfig\Api\Config\Db\Provider\PriceSectionPathProviderInterface;

/**
 * Class DbPriceConfig.
 *
 * This is not an api class, implementation may change at any time.
 * @see \Logiscenter\PriceConfig\Api\Config\PriceConfigInterface,
 * use this to retrieve config.
 */
class DbPriceConfig extends DbConfigHelper implements
    PriceSectionPathProviderInterface,
    PriceGroupPathProviderInterface,
    DbConfigHelperInterface
{
    use DbConfigSectionPathProviderTrait;
    use DbConfigGroupPathProviderTrait;

    private const SHOW_MSRP_ON_PDP = 'show_msrp_on_pdp';
    private const SHOW_INCL_TAX_PRICE_ON_PDP = 'show_incl_tax_on_pdp';

    /**
     * @param int|string|null $storeId
     *
     * @return bool
     */
    public function showMsrpPriceOnPdp($storeId = null): bool
    {
        return $this->getConfigFlag(self::SHOW_MSRP_ON_PDP, $storeId);
    }

    /**
     * @param int|string|null $storeId
     *
     * @return bool
     */
    public function showInclTaxPriceOnPdp($storeId = null): bool
    {
        return $this->getConfigFlag(self::SHOW_INCL_TAX_PRICE_ON_PDP, $storeId);
    }
}
