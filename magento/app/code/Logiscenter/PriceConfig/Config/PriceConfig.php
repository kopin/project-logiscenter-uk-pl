<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Logiscenter\PriceConfig\Config;

use Logiscenter\PriceConfig\Api\Config\PriceConfigInterface;
use Logiscenter\PriceConfig\Config\Db\DbPriceConfig;

/**
 * Class PriceConfig.
 *
 * This is not an api class, implementation may change at any time.
 * @see PriceConfigInterface, use this to retrieve config.
 *
 * For example, it now relies on DbPriceConfig to read information from database,
 * but this implementation can be changed when needed, without prior notice.
 */
class PriceConfig extends DbPriceConfig implements
    PriceConfigInterface
{
}
