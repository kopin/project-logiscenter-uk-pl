<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Logiscenter\PriceConfig\Config;

use Logiscenter\PriceConfig\Api\Config\PriceEnvironmentConfigInterface;
use Logiscenter\PriceConfig\Config\Db\DbPriceEnvironmentConfig;

/**
 * Class PriceEnvironmentConfig.
 *
 * This is not an api class, implementation may change at any time.
 * @see PriceEnvironmentConfigInterface, use this to retrieve config.
 *
 * For example, it now relies on DbPriceEnvironmentConfig to read information from database,
 * but this implementation can be changed when needed, without prior notice.
 */
class PriceEnvironmentConfig extends DbPriceEnvironmentConfig implements
    PriceEnvironmentConfigInterface
{
}
