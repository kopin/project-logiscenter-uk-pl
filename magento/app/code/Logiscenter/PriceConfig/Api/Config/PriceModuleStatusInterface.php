<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Logiscenter\PriceConfig\Api\Config;

use Interactiv4\ConfigHelper\Api\Config\Status\ModuleStatusInterface;

/**
 * Interface PriceModuleStatusInterface.
 *
 * Module status provider interface.
 *
 * @api
 */
interface PriceModuleStatusInterface extends ModuleStatusInterface
{
}
