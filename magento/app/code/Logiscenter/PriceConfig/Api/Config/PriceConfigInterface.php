<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Logiscenter\PriceConfig\Api\Config;

/**
 * Interface PriceConfigInterface.
 *
 * Config provider interface
 *
 * @api
 */
interface PriceConfigInterface
{
    /**
     * @param int|string|null $storeId
     *
     * @return bool
     */
    public function showMsrpPriceOnPdp($storeId = null): bool;

    /**
     * @param int|string|null $storeId
     *
     * @return bool
     */
    public function showInclTaxPriceOnPdp($storeId = null): bool;
}
