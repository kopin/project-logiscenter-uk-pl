<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Logiscenter\PriceConfig\Test\Integration\Di;

use Interactiv4\Framework\Api\Di\Definition\DefinitionPoolInterface;
use Interactiv4\Framework\Api\Di\Definition\DefinitionPoolProviderInterface;
use Interactiv4\Framework\Di\Definition\Definition;
use Interactiv4\Framework\Di\Definition\DefinitionPool;
use Interactiv4\Framework\Di\Definition\DefinitionName;
use Interactiv4\Framework\Di\Definition\DefinitionValue;

/**
 * Class ArgumentsDefinitionProvider.
 *
 * Arguments definition as they appear in di configuration files.
 */
class ArgumentsDefinitionProvider implements DefinitionPoolProviderInterface
{
    /**
     * @var DefinitionPoolInterface
     */
    private $definitionPool;

    /**
     * {@inheritdoc}
     */
    public function getDefinitionPool(): DefinitionPoolInterface
    {
        if (!isset($this->definitionPool)) {
            $this->definitionPool = new DefinitionPool(
	new Definition(
	new DefinitionName(
	'Logiscenter\\PriceConfig\\Plugin\\Api\\Status\\CheckPriceModuleStatusPlugin',
		'booleanStatusRead'
	),
		new DefinitionValue(
	\Logiscenter\PriceConfig\Plugin\Api\Status\CheckPriceModuleStatusPlugin::class,
		\Logiscenter\PriceConfig\Api\Config\PriceModuleStatusInterface::class,
		(string) \Magento\Framework\App\Area::AREA_GLOBAL,
		false,
		null,
		null
	)
	),
		new Definition(
	new DefinitionName(
	'Logiscenter\\PriceConfig\\Plugin\\Api\\Status\\CheckPriceEnvironmentPlugin',
		'allowedEnvironmentsAware'
	),
		new DefinitionValue(
	\Logiscenter\PriceConfig\Plugin\Api\Status\CheckPriceEnvironmentPlugin::class,
		\Logiscenter\PriceConfig\Api\Config\PriceEnvironmentConfigInterface::class,
		(string) \Magento\Framework\App\Area::AREA_GLOBAL,
		false,
		null,
		null
	)
	),
		new Definition(
	new DefinitionName(
	'Interactiv4\\BaseConfig\\Pool\\ModuleStatusPool',
		'objects',
		'Logiscenter_PriceConfig'
	),
		new DefinitionValue(
	'Interactiv4\\BaseConfig\\Pool\\ModuleStatusPool',
		\Logiscenter\PriceConfig\Api\Config\PriceModuleStatusInterface::class,
		(string) \Magento\Framework\App\Area::AREA_GLOBAL,
		false,
		null,
		null
	)
	),
		new Definition(
	new DefinitionName(
	'Logiscenter\\PriceConfig\\Observer\\RefreshDataUponConfigChangeObserver',
		'refreshCacheStrategy'
	),
		new DefinitionValue(
	\Logiscenter\PriceConfig\Observer\RefreshDataUponConfigChangeObserver::class,
		\Interactiv4\Framework\Cache\RefreshCacheAppStateDependantStrategy::class,
		(string) \Magento\Framework\App\Area::AREA_ADMINHTML,
		false,
		null,
		null
	)
	),
		new Definition(
	new DefinitionName(
	'Logiscenter\\PriceConfig\\Observer\\RefreshDataUponConfigChangeObserver',
		'cacheTypes',
		'config'
	),
		new DefinitionValue(
	\Logiscenter\PriceConfig\Observer\RefreshDataUponConfigChangeObserver::class,
		(string) \Magento\Framework\App\Cache\Type\Config::TYPE_IDENTIFIER,
		(string) \Magento\Framework\App\Area::AREA_ADMINHTML,
		false,
		null,
		null
	)
	),
		new Definition(
	new DefinitionName(
	'Logiscenter\\PriceConfig\\Observer\\RefreshDataUponConfigChangeObserver',
		'cacheTypes',
		'block_html'
	),
		new DefinitionValue(
	\Logiscenter\PriceConfig\Observer\RefreshDataUponConfigChangeObserver::class,
		(string) \Magento\Framework\App\Cache\Type\Block::TYPE_IDENTIFIER,
		(string) \Magento\Framework\App\Area::AREA_ADMINHTML,
		false,
		null,
		null
	)
	),
		new Definition(
	new DefinitionName(
	'Logiscenter\\PriceConfig\\Observer\\RefreshDataUponConfigChangeObserver',
		'cacheTypes',
		'full_page'
	),
		new DefinitionValue(
	\Logiscenter\PriceConfig\Observer\RefreshDataUponConfigChangeObserver::class,
		(string) \Magento\PageCache\Model\Cache\Type::TYPE_IDENTIFIER,
		(string) \Magento\Framework\App\Area::AREA_ADMINHTML,
		false,
		null,
		null
	)
	)
	);
        }

        return $this->definitionPool;
    }
}
