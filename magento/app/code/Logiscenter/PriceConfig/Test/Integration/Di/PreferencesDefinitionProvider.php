<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Logiscenter\PriceConfig\Test\Integration\Di;

use Interactiv4\Framework\Api\Di\Definition\DefinitionPoolInterface;
use Interactiv4\Framework\Api\Di\Definition\DefinitionPoolProviderInterface;
use Interactiv4\Framework\Di\Definition\Definition;
use Interactiv4\Framework\Di\Definition\DefinitionPool;
use Interactiv4\Framework\Di\Definition\DefinitionName;
use Interactiv4\Framework\Di\Definition\DefinitionValue;

/**
 * Class PreferencesDefinitionProvider.
 *
 * Preferences definition as they appear in di configuration files.
 */
class PreferencesDefinitionProvider implements DefinitionPoolProviderInterface
{
    /**
     * @var DefinitionPoolInterface
     */
    private $definitionPool;

    /**
     * {@inheritdoc}
     */
    public function getDefinitionPool(): DefinitionPoolInterface
    {
        if (!isset($this->definitionPool)) {
            $this->definitionPool = new DefinitionPool(
	new Definition(
	new DefinitionName(
	'Logiscenter\\PriceConfig\\Api\\Config\\PriceModuleStatusInterface'
	),
		new DefinitionValue(
	\Logiscenter\PriceConfig\Api\Config\PriceModuleStatusInterface::class,
		\Logiscenter\PriceConfig\Config\PriceModuleStatus::class,
		(string) \Magento\Framework\App\Area::AREA_GLOBAL,
		false,
		null,
		null
	)
	),
		new Definition(
	new DefinitionName(
	'Logiscenter\\PriceConfig\\Api\\Config\\PriceStatusConfigInterface'
	),
		new DefinitionValue(
	\Logiscenter\PriceConfig\Api\Config\PriceStatusConfigInterface::class,
		\Logiscenter\PriceConfig\Config\PriceStatusConfig::class,
		(string) \Magento\Framework\App\Area::AREA_GLOBAL,
		false,
		null,
		null
	)
	),
		new Definition(
	new DefinitionName(
	'Logiscenter\\PriceConfig\\Api\\Config\\PriceEnvironmentConfigInterface'
	),
		new DefinitionValue(
	\Logiscenter\PriceConfig\Api\Config\PriceEnvironmentConfigInterface::class,
		\Logiscenter\PriceConfig\Config\PriceEnvironmentConfig::class,
		(string) \Magento\Framework\App\Area::AREA_GLOBAL,
		false,
		null,
		null
	)
	),
		new Definition(
	new DefinitionName(
	'Logiscenter\\PriceConfig\\Api\\Config\\PriceConfigInterface'
	),
		new DefinitionValue(
	\Logiscenter\PriceConfig\Api\Config\PriceConfigInterface::class,
		\Logiscenter\PriceConfig\Config\PriceConfig::class,
		(string) \Magento\Framework\App\Area::AREA_GLOBAL,
		false,
		null,
		null
	)
	)
	);
        }

        return $this->definitionPool;
    }
}
