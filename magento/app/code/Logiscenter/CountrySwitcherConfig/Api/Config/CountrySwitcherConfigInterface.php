<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\CountrySwitcherConfig\Api\Config;

/**
 * Interface CountrySwitcherConfigInterface.
 *
 * Config provider interface
 *
 * @api
 */
interface CountrySwitcherConfigInterface
{
    const DOMAIN_URL = 'domain_url';
    const COUNTRY_CODE = 'country_code';
    const IS_ENABLED = 'is_enabled';

    /**
     * @return array
     */
    public function getSitesData(): array;

    /**
     * @param string $domain
     *
     * @return array
     */
    public function getSiteDataByDomain(string $domain): array;
}
