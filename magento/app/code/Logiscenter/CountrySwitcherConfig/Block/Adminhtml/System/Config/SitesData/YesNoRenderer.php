<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\CountrySwitcherConfig\Block\Adminhtml\System\Config\SitesData;

use Magento\Config\Model\Config\Source\Yesno;
use Magento\Framework\View\Element\Context;
use Magento\Framework\View\Element\Html\Select;

/**
 * Class YesNoRenderer.
 *
 * Yes/no renderer for map
 *
 * @api
 */
class YesNoRenderer extends Select
{
    /**
     * @var Yesno
     */
    private $yesNo;

    /**
     * @param Yesno   $yesNo
     * @param Context $context
     * @param array   $data
     */
    public function __construct(
        Yesno $yesNo,
        Context $context,
        array $data = []
    ) {
        $this->yesNo = $yesNo;
        parent::__construct(
            $context,
            $data
        );
    }

    /**
     * Set "name" for <select> element.
     *
     * @param string $value
     *
     * @return $this
     */
    public function setInputName($value)
    {
        return $this->setName($value);
    }

    /**
     * Set "id" for <select> element.
     *
     * @param $value
     *
     * @return $this
     */
    public function setInputId($value)
    {
        return $this->setId($value);
    }

    /**
     * Render block HTML.
     *
     * @return string
     * @codingStandardsIgnoreStart
     */
    public function _toHtml(): string
    {
        // @codingStandardsIgnoreEnd
        if (!$this->getOptions()) {
            $this->setOptions($this->yesNo->toOptionArray());
        }

        return parent::_toHtml();
    }
}
