<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\CountrySwitcherConfig\Block\Adminhtml\System\Config;

use Logiscenter\CountrySwitcherConfig\Api\Config\CountrySwitcherConfigInterface;
use Logiscenter\CountrySwitcherConfig\Block\Adminhtml\System\Config\SitesData\YesNoRenderer;
use Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray;
use Magento\Framework\DataObject;
use Magento\Framework\View\Element\BlockInterface;

/**
 * Template mapping block config class
 */
class SitesData extends AbstractFieldArray
{
    /**
     * @var BlockInterface|YesNoRenderer
     */
    private $renderer;

    /**
     * @inheritdoc
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     */
    // @codingStandardsIgnoreStart
    protected function _prepareToRender()
    {
        // @codingStandardsIgnoreEnd
        $this->addColumn(
            CountrySwitcherConfigInterface::DOMAIN_URL,
            [
                'label'    => __('Domain URL'),
            ]
        );
        $this->addColumn(
            CountrySwitcherConfigInterface::COUNTRY_CODE,
            [
                'label'    => __('Country code'),
            ]
        );
        $this->addColumn(
            CountrySwitcherConfigInterface::IS_ENABLED,
            [
                'label'    => __('Enabled'),
                'renderer' => $this->getRenderer()
            ]
        );
        $this->_addAfter       = true;
        $this->_addButtonLabel = __('Add more');
        parent::_construct();
    }

    private function getRenderer()
    {
        if (!$this->renderer) {
            $this->renderer = $this->getLayout()->createBlock(
                YesNoRenderer::class,
                '',
                ['data' => ['is_render_to_js_template' => true]]
            );
        }

        return $this->renderer;
    }

    /**
     * @param DataObject $row
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     */
    // @codingStandardsIgnoreStart
    protected function _prepareArrayRow(DataObject $row)
    {
        // @codingStandardsIgnoreEnd
        $optionExtraAttr = [];

        $optionExtraAttr['option_' . $this->getRenderer()
            ->calcOptionHash($row->getData(CountrySwitcherConfigInterface::IS_ENABLED))]
            = 'selected="selected"';

        $row->setData(
            'option_extra_attrs',
            $optionExtraAttr
        );
    }
}
