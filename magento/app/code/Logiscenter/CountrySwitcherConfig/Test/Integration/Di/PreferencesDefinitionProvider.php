<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\CountrySwitcherConfig\Test\Integration\Di;

use Interactiv4\Framework\Api\Di\Definition\DefinitionPoolInterface;
use Interactiv4\Framework\Api\Di\Definition\DefinitionPoolProviderInterface;
use Interactiv4\Framework\Di\Definition\Definition;
use Interactiv4\Framework\Di\Definition\DefinitionPool;
use Interactiv4\Framework\Di\Definition\DefinitionName;
use Interactiv4\Framework\Di\Definition\DefinitionValue;

/**
 * Class PreferencesDefinitionProvider.
 *
 * Preferences definition as they appear in di configuration files.
 */
class PreferencesDefinitionProvider implements DefinitionPoolProviderInterface
{
    /**
     * @var DefinitionPoolInterface
     */
    private $definitionPool;

    /**
     * {@inheritdoc}
     */
    public function getDefinitionPool(): DefinitionPoolInterface
    {
        if (!isset($this->definitionPool)) {
            $this->definitionPool = new DefinitionPool(
                new Definition(
                    new DefinitionName(
                        'Logiscenter\\CountrySwitcherConfig\\Api\\Config\\CountrySwitcherModuleStatusInterface'
                    ),
                    new DefinitionValue(
                        \Logiscenter\CountrySwitcherConfig\Api\Config\CountrySwitcherModuleStatusInterface::class,
                        \Logiscenter\CountrySwitcherConfig\Config\CountrySwitcherModuleStatus::class,
                        (string) \Magento\Framework\App\Area::AREA_GLOBAL,
                        false,
                        null,
                        null
                    )
                ),
                new Definition(
                    new DefinitionName(
                        'Logiscenter\\CountrySwitcherConfig\\Api\\Config\\CountrySwitcherStatusConfigInterface'
                    ),
                    new DefinitionValue(
                        \Logiscenter\CountrySwitcherConfig\Api\Config\CountrySwitcherStatusConfigInterface::class,
                        \Logiscenter\CountrySwitcherConfig\Config\CountrySwitcherStatusConfig::class,
                        (string) \Magento\Framework\App\Area::AREA_GLOBAL,
                        false,
                        null,
                        null
                    )
                ),
                new Definition(
                    new DefinitionName(
                        'Logiscenter\\CountrySwitcherConfig\\Api\\Config\\CountrySwitcherEnvironmentConfigInterface'
                    ),
                    new DefinitionValue(
                        \Logiscenter\CountrySwitcherConfig\Api\Config\CountrySwitcherEnvironmentConfigInterface::class,
                        \Logiscenter\CountrySwitcherConfig\Config\CountrySwitcherEnvironmentConfig::class,
                        (string) \Magento\Framework\App\Area::AREA_GLOBAL,
                        false,
                        null,
                        null
                    )
                ),
                new Definition(
                    new DefinitionName(
                        'Logiscenter\\CountrySwitcherConfig\\Api\\Config\\CountrySwitcherConfigInterface'
                    ),
                    new DefinitionValue(
                        \Logiscenter\CountrySwitcherConfig\Api\Config\CountrySwitcherConfigInterface::class,
                        \Logiscenter\CountrySwitcherConfig\Config\CountrySwitcherConfig::class,
                        (string) \Magento\Framework\App\Area::AREA_GLOBAL,
                        false,
                        null,
                        null
                    )
                )
            );
        }

        return $this->definitionPool;
    }
}
