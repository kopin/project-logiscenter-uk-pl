<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\CountrySwitcherConfig\Config;

use Logiscenter\CountrySwitcherConfig\Api\Config\CountrySwitcherModuleStatusInterface;
use Logiscenter\CountrySwitcherConfig\Api\Config\CountrySwitcherStatusConfigInterface;

/**
 * Class CountrySwitcherModuleStatus.
 *
 * This is not an api class, implementation may change at any time.
 * @see CountrySwitcherModuleStatusInterface, use this to retrieve config.
 *
 * For example, it now relies on CountrySwitcherStatusConfigInterface to read config,
 * but this implementation can be changed when needed, without prior notice.
 */
class CountrySwitcherModuleStatus implements CountrySwitcherModuleStatusInterface
{
    /**
     * @var CountrySwitcherStatusConfigInterface
     */
    private $statusConfig;

    /**
     * CountrySwitcherModuleStatus constructor.
     * @param CountrySwitcherStatusConfigInterface $statusConfig
     */
    public function __construct(
        CountrySwitcherStatusConfigInterface $statusConfig
    ) {
        $this->statusConfig = $statusConfig;
    }

    /**
     * Compare boolean status, using optionally supplied data
     *
     * @param bool $expectedStatusValue
     * @param int|string|null $storeId
     * @return bool
     */
    public function isBooleanStatus(
        bool $expectedStatusValue,
        $storeId = null
    ): bool {
        return $this->statusConfig->isBooleanStatus($expectedStatusValue, $storeId);
    }

    /**
     * Read boolean status, using optionally supplied data
     *
     * @param int|string|null $storeId
     * @return bool
     */
    public function isEnabled($storeId = null): bool
    {
        return $this->statusConfig->isEnabled($storeId);
    }
}
