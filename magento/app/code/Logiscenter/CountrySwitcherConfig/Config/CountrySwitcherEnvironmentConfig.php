<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\CountrySwitcherConfig\Config;

use Logiscenter\CountrySwitcherConfig\Api\Config\CountrySwitcherEnvironmentConfigInterface;
use Logiscenter\CountrySwitcherConfig\Config\Db\DbGeneralEnvironmentConfig;

/**
 * Class CountrySwitcherEnvironmentConfig.
 *
 * This is not an api class, implementation may change at any time.
 * @see CountrySwitcherEnvironmentConfigInterface, use this to retrieve config.
 *
 * For example, it now relies on DbCountrySwitcherEnvironmentConfig to read information from database,
 * but this implementation can be changed when needed, without prior notice.
 */
class CountrySwitcherEnvironmentConfig extends DbGeneralEnvironmentConfig implements
    CountrySwitcherEnvironmentConfigInterface
{
}
