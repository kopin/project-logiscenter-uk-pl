<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\CountrySwitcherConfig\Config;

use Logiscenter\CountrySwitcherConfig\Api\Config\CountrySwitcherConfigInterface;
use Logiscenter\CountrySwitcherConfig\Config\Db\DbCountrySwitcherConfig;

/**
 * Class CountrySwitcherConfig.
 *
 * This is not an api class, implementation may change at any time.
 * @see CountrySwitcherConfigInterface, use this to retrieve config.
 *
 * For example, it now relies on DbCountrySwitcherConfig to read information from database,
 * but this implementation can be changed when needed, without prior notice.
 */
class CountrySwitcherConfig extends DbCountrySwitcherConfig implements
    CountrySwitcherConfigInterface
{
}
