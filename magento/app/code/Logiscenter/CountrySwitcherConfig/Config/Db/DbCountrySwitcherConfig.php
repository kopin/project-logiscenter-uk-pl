<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\CountrySwitcherConfig\Config\Db;

use Interactiv4\ConfigHelper\Api\Config\Db\DbConfigHelperInterface;
use Interactiv4\ConfigHelper\Api\Config\Db\Provider\DbConfigGroupPathProviderTrait;
use Interactiv4\ConfigHelper\Api\Config\Db\Provider\DbConfigSectionPathProviderTrait;
use Interactiv4\ConfigHelper\Model\Config\Db\DbConfigHelper;
use Logiscenter\CountrySwitcherConfig\Api\Config\CountrySwitcherConfigInterface;
use Logiscenter\CountrySwitcherConfig\Api\Config\Db\Provider\CountrySwitcherGroupPathProviderInterface;
use Logiscenter\CountrySwitcherConfig\Api\Config\Db\Provider\GeneralSectionPathProviderInterface;
use Magento\Config\Model\ResourceModel\Config as ResourceConfig;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Serialize\SerializerInterface;

/**
 * Class DbCountrySwitcherConfig.
 *
 * This is not an api class, implementation may change at any time.
 *
 * @see \Logiscenter\CountrySwitcherConfig\Api\Config\CountrySwitcherConfigInterface,
 * use this to retrieve config.
 */
class DbCountrySwitcherConfig extends DbConfigHelper implements
    GeneralSectionPathProviderInterface,
    CountrySwitcherGroupPathProviderInterface,
    DbConfigHelperInterface
{
    use DbConfigSectionPathProviderTrait;
    use DbConfigGroupPathProviderTrait;

    private const XML_FIELD_SITES_DATA = 'sites_data';

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @param ScopeConfigInterface $scopeConfig
     * @param ResourceConfig       $resourceConfig
     * @param SerializerInterface  $serializer
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        ResourceConfig $resourceConfig,
        SerializerInterface $serializer
    ) {
        parent::__construct($scopeConfig, $resourceConfig);
        $this->serializer = $serializer;
    }

    /**
     * @return array
     */
    public function getSitesData(): array
    {
        return $this->getConfig(self::XML_FIELD_SITES_DATA)
            ? $this->serializer->unserialize($this->getConfig(self::XML_FIELD_SITES_DATA))
            : [];
    }

    /**
     * Return site config data by domain value
     *
     * @param string $domain
     *
     * @return array
     */
    public function getSiteDataByDomain(string $domain): array
    {
        foreach ($this->getSitesData() as $siteData) {
            if (strpos($siteData[CountrySwitcherConfigInterface::DOMAIN_URL], $domain) !== false) {
                return $siteData;
            }
        }

        return [];
    }
}
