<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\CountrySwitcherConfig\Setup\Patch\Data;

use Interactiv4\Framework\Config\MigrateConfigData;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\Patch\PatchVersionInterface;

/**
 * Class MigrateConfigPath.
 * Migrate config data according to new config path.
 */
class MigrateConfigPath implements DataPatchInterface
{
    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @var MigrateConfigData
     */
    private $migrateConfigData;

    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        MigrateConfigData $migrateConfigData
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->migrateConfigData = $migrateConfigData;
    }

    /**
     * {@inheritdoc}
     */
    public function apply()
    {
        $this->migrateConfigData->migrateConfigPath(
            $this->moduleDataSetup,
            'logiscenter_generalconfig/country_switcher/sites_data',
            'logiscenter_countryswitcherconfig/country_switcher/sites_data'
        );
        $this->migrateConfigData->migrateConfigPath(
            $this->moduleDataSetup,
            'logiscenter_generalconfig/environment/allowed_environments',
            'logiscenter_countryswitcherconfig/environment/allowed_environments'
        );
        $this->migrateConfigData->migrateConfigPath(
            $this->moduleDataSetup,
            'logiscenter_generalconfig/status/enable',
            'logiscenter_countryswitcherconfig/status/enable'
        );
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }
}
