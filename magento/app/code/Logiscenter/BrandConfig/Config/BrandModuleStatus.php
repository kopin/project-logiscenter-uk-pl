<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Logiscenter\BrandConfig\Config;

use Logiscenter\BrandConfig\Api\Config\BrandModuleStatusInterface;
use Logiscenter\BrandConfig\Api\Config\BrandStatusConfigInterface;

/**
 * Class BrandModuleStatus.
 *
 * This is not an api class, implementation may change at any time.
 * @see BrandModuleStatusInterface, use this to retrieve config.
 *
 * For example, it now relies on BrandStatusConfigInterface to read config,
 * but this implementation can be changed when needed, without prior notice.
 */
class BrandModuleStatus implements BrandModuleStatusInterface
{
    /**
     * @var BrandStatusConfigInterface
     */
    private $statusConfig;

    /**
     * BrandModuleStatus constructor.
     * @param BrandStatusConfigInterface $statusConfig
     */
    public function __construct(
        BrandStatusConfigInterface $statusConfig
    ) {
        $this->statusConfig = $statusConfig;
    }

    /**
     * Compare boolean status, using optionally supplied data
     *
     * @param bool $expectedStatusValue
     * @param int|string|null $storeId
     * @return bool
     */
    public function isBooleanStatus(
        bool $expectedStatusValue,
        $storeId = null
    ): bool {
        return $this->statusConfig->isBooleanStatus($expectedStatusValue, $storeId);
    }

    /**
     * Read boolean status, using optionally supplied data
     *
     * @param int|string|null $storeId
     * @return bool
     */
    public function isEnabled($storeId = null): bool
    {
        return $this->statusConfig->isEnabled($storeId);
    }
}
