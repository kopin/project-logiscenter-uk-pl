<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Logiscenter\BrandConfig\Config\Db;

use Interactiv4\BaseConfig\Api\Config\Db\Provider\StatusGroupPathProviderInterface;
use Interactiv4\ConfigHelper\Api\Config\Db\DbConfigHelperInterface;
use Interactiv4\ConfigHelper\Api\Config\Db\DbStatusTrait;
use Interactiv4\ConfigHelper\Api\Config\Db\Provider\DbConfigGroupPathProviderTrait;
use Interactiv4\ConfigHelper\Api\Config\Db\Provider\DbConfigSectionPathProviderTrait;
use Interactiv4\ConfigHelper\Api\Config\Db\DbStatusInterface;
use Interactiv4\ConfigHelper\Model\Config\Db\DbConfigHelper;
use Logiscenter\BrandConfig\Api\Config\Db\Provider\BrandSectionPathProviderInterface;

/**
 * Class DbBrandStatusConfig.
 *
 * This is not an api class, implementation may change at any time.
 * @see \Logiscenter\BrandConfig\Api\Config\BrandStatusConfigInterface,
 * use this to retrieve config.
 */
class DbBrandStatusConfig extends DbConfigHelper implements
    BrandSectionPathProviderInterface,
    StatusGroupPathProviderInterface,
    DbStatusInterface,
    DbConfigHelperInterface
{
    use DbConfigSectionPathProviderTrait;
    use DbConfigGroupPathProviderTrait;
    use DbStatusTrait;
}
