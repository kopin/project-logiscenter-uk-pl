<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Logiscenter\BrandConfig\Config;

use Logiscenter\BrandConfig\Api\Config\BrandEnvironmentConfigInterface;
use Logiscenter\BrandConfig\Config\Db\DbBrandEnvironmentConfig;

/**
 * Class BrandEnvironmentConfig.
 *
 * This is not an api class, implementation may change at any time.
 * @see BrandEnvironmentConfigInterface, use this to retrieve config.
 *
 * For example, it now relies on DbBrandEnvironmentConfig to read information from database,
 * but this implementation can be changed when needed, without prior notice.
 */
class BrandEnvironmentConfig extends DbBrandEnvironmentConfig implements
    BrandEnvironmentConfigInterface
{
}
