<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Logiscenter\BrandConfig\Test\Integration\Di;

use Interactiv4\Framework\Api\Di\Definition\DefinitionPoolInterface;
use Interactiv4\Framework\Api\Di\Definition\DefinitionPoolProviderInterface;
use Interactiv4\Framework\Di\Definition\Definition;
use Interactiv4\Framework\Di\Definition\DefinitionPool;
use Interactiv4\Framework\Di\Definition\DefinitionName;
use Interactiv4\Framework\Di\Definition\DefinitionValue;

/**
 * Class PluginsDefinitionProvider.
 *
 * Plugins definition as they appear in di configuration files.
 */
class PluginsDefinitionProvider implements DefinitionPoolProviderInterface
{
    /**
     * @var DefinitionPoolInterface
     */
    private $definitionPool;

    /**
     * {@inheritdoc}
     */
    public function getDefinitionPool(): DefinitionPoolInterface
    {
        if (!isset($this->definitionPool)) {
            $this->definitionPool = new DefinitionPool(
	new Definition(
	new DefinitionName(
	'Logiscenter\\BrandConfig\\Plugin\\Api\\Status\\CheckBrandEnvironmentPlugin',
		'afterIsEnabled'
	),
		new DefinitionValue(
	\Logiscenter\BrandConfig\Api\Config\BrandModuleStatusInterface::class,
		\Logiscenter\BrandConfig\Plugin\Api\Status\CheckBrandEnvironmentPlugin::class,
		(string) \Magento\Framework\App\Area::AREA_GLOBAL,
		false,
		10,
		null
	)
	)
	);
        }

        return $this->definitionPool;
    }
}
