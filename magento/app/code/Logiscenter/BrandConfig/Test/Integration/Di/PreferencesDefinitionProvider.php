<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Logiscenter\BrandConfig\Test\Integration\Di;

use Interactiv4\Framework\Api\Di\Definition\DefinitionPoolInterface;
use Interactiv4\Framework\Api\Di\Definition\DefinitionPoolProviderInterface;
use Interactiv4\Framework\Di\Definition\Definition;
use Interactiv4\Framework\Di\Definition\DefinitionPool;
use Interactiv4\Framework\Di\Definition\DefinitionName;
use Interactiv4\Framework\Di\Definition\DefinitionValue;

/**
 * Class PreferencesDefinitionProvider.
 *
 * Preferences definition as they appear in di configuration files.
 */
class PreferencesDefinitionProvider implements DefinitionPoolProviderInterface
{
    /**
     * @var DefinitionPoolInterface
     */
    private $definitionPool;

    /**
     * {@inheritdoc}
     */
    public function getDefinitionPool(): DefinitionPoolInterface
    {
        if (!isset($this->definitionPool)) {
            $this->definitionPool = new DefinitionPool(
	new Definition(
	new DefinitionName(
	'Logiscenter\\BrandConfig\\Api\\Config\\BrandModuleStatusInterface'
	),
		new DefinitionValue(
	\Logiscenter\BrandConfig\Api\Config\BrandModuleStatusInterface::class,
		\Logiscenter\BrandConfig\Config\BrandModuleStatus::class,
		(string) \Magento\Framework\App\Area::AREA_GLOBAL,
		false,
		null,
		null
	)
	),
		new Definition(
	new DefinitionName(
	'Logiscenter\\BrandConfig\\Api\\Config\\BrandStatusConfigInterface'
	),
		new DefinitionValue(
	\Logiscenter\BrandConfig\Api\Config\BrandStatusConfigInterface::class,
		\Logiscenter\BrandConfig\Config\BrandStatusConfig::class,
		(string) \Magento\Framework\App\Area::AREA_GLOBAL,
		false,
		null,
		null
	)
	),
		new Definition(
	new DefinitionName(
	'Logiscenter\\BrandConfig\\Api\\Config\\BrandEnvironmentConfigInterface'
	),
		new DefinitionValue(
	\Logiscenter\BrandConfig\Api\Config\BrandEnvironmentConfigInterface::class,
		\Logiscenter\BrandConfig\Config\BrandEnvironmentConfig::class,
		(string) \Magento\Framework\App\Area::AREA_GLOBAL,
		false,
		null,
		null
	)
	),
		new Definition(
	new DefinitionName(
	'Logiscenter\\BrandConfig\\Api\\Config\\BrandConfigInterface'
	),
		new DefinitionValue(
	\Logiscenter\BrandConfig\Api\Config\BrandConfigInterface::class,
		\Logiscenter\BrandConfig\Config\BrandConfig::class,
		(string) \Magento\Framework\App\Area::AREA_GLOBAL,
		false,
		null,
		null
	)
	)
	);
        }

        return $this->definitionPool;
    }
}
