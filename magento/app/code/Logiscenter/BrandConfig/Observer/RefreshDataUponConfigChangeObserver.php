<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Logiscenter\BrandConfig\Observer;

use Interactiv4\Framework\Api\Cache\RefreshCacheStrategyInterface;
use Interactiv4\Framework\Api\Indexer\ReindexStrategyInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

/**
 * Class RefreshDataUponConfigChangeObserver.
 *
 * Refresh cache / reindex data upon config change.
 */
class RefreshDataUponConfigChangeObserver implements ObserverInterface
{
    /**
     * @var RefreshCacheStrategyInterface
     */
    private $refreshCacheStrategy;

    /**
     * @var ReindexStrategyInterface
     */
    private $reindexStrategy;

    /**
     * @var string[]
     */
    private $cacheTypes;

    /**
     * @var string[]
     */
    private $indexerIds;

    /**
     * RefreshDataUponConfigChangeObserver constructor.
     *
     * @param RefreshCacheStrategyInterface $refreshCacheStrategy
     * @param ReindexStrategyInterface $reindexStrategy
     * @param string[] $cacheTypes
     * @param string[] $indexerIds
     */
    public function __construct(
        RefreshCacheStrategyInterface $refreshCacheStrategy,
        ReindexStrategyInterface $reindexStrategy,
        array $cacheTypes = [],
        array $indexerIds = []
    ) {
        $this->refreshCacheStrategy = $refreshCacheStrategy;
        $this->reindexStrategy = $reindexStrategy;
        $this->cacheTypes = $cacheTypes;
        $this->indexerIds = $indexerIds;
    }

    /**
     * Refresh data if something has changed.
     *
     * @inheritdoc
     */
    public function execute(Observer $observer)
    {
        /**
         * @see \Magento\Config\Model\Config::save() for event reference
         *
         * $website = $observer->getWebsite();
         * $store = $observer->getStore();
         * $changedPaths = $observer->getChangedPaths();
         */
        $changedPaths = $observer->getChangedPaths();

        if (!empty($changedPaths)) {
            foreach ($this->indexerIds as $indexerId) {
                $this->reindexStrategy->reindexIndexerId($indexerId);
            }

            foreach ($this->cacheTypes as $cacheType) {
                $this->refreshCacheStrategy->refreshCacheType($cacheType);
            }
        }
    }
}
