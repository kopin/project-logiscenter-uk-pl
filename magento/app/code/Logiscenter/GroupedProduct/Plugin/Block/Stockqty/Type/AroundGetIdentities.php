<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\GroupedProduct\Plugin\Block\Stockqty\Type;

use Logiscenter\GroupedProduct\Api\Config\GroupedProductModuleStatus;
use Magento\GroupedProduct\Block\Stockqty\Type\Grouped;

/**
 * Suppress get grouped children identities for block
 */
class AroundGetIdentities
{
    /**
     * @var GroupedProductModuleStatus
     */
    private $moduleStatus;

    /**
     * @param GroupedProductModuleStatus $moduleStatus
     */
    public function __construct(GroupedProductModuleStatus $moduleStatus)
    {
        $this->moduleStatus = $moduleStatus;
    }

    /**
     * @param Grouped $subject
     * @param callable $proceed
     * @return array
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function aroundGetIdentities(Grouped $subject, callable $proceed): array
    {
        if ($this->moduleStatus->isEnabled()) {
            return [];
        }

        return $proceed();
    }
}
