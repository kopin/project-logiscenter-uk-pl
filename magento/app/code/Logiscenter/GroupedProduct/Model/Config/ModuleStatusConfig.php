<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\GroupedProduct\Model\Config;

use Logiscenter\GroupedProduct\Api\Config\GroupedProductModuleStatus;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;

/**
 * Class to check if module is enabled
 */
class ModuleStatusConfig implements GroupedProductModuleStatus
{
    public const XML_FIELD_IS_MODULE_ENABLED = 'logiscenter_groupedproduct/status/enabled';

    /**
     * @var ScopeConfigInterface
     */
    private $config;

    /**
     * @param ScopeConfigInterface $config
     */
    public function __construct(ScopeConfigInterface $config)
    {
        $this->config = $config;
    }

    /**
     * Compare boolean status, using optionally supplied data.
     *
     * @param bool            $expectedStatusValue
     * @param int|string|null $storeId
     *
     * @return bool
     */
    public function isBooleanStatus(
        bool $expectedStatusValue,
             $storeId = null
    ): bool {
        return $this->isEnabled($storeId) === $expectedStatusValue;
    }

    /**
     * Read boolean status, using optionally supplied data.
     *
     * @param int|string|null $storeId
     *
     * @return bool
     */
    public function isEnabled($storeId = null): bool
    {
        return (bool) $this->config->getValue(
            self::XML_FIELD_IS_MODULE_ENABLED,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }
}
