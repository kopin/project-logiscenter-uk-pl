<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\GroupedRelated\Model\Product;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Model\Config;
use Magento\Catalog\Model\ResourceModel\Product\Link\Product\Collection as LinkProductCollection;
use Magento\Framework\Module\Manager;

/**
 * Class to get related products
 */
class Related
{
    /**
     * @var Manager
     */
    private $moduleManager;

    /**
     * @var Config
     */
    private $catalogConfig;

    /**
     * Related constructor.
     *
     * @param Manager $moduleManager
     * @param Config  $catalogConfig
     */
    public function __construct(
        Manager $moduleManager,
        Config  $catalogConfig
    ) {
        $this->moduleManager = $moduleManager;
        $this->catalogConfig = $catalogConfig;
    }

    /**
     * @param ProductInterface $product
     *
     * @return
     */
    public function getProducts(ProductInterface $product)
    {
        return $this->getCollection($product);
    }

    /**
     * @param ProductInterface $product
     *
     * @return LinkProductCollection
     */
    protected function getCollection(ProductInterface $product): LinkProductCollection
    {
        $collection =
            $product->getRelatedProductCollection()->addAttributeToSelect('required_options')->setPositionOrder();

        $collection->addAttributeToSelect('visibility');

        if ($this->moduleManager->isEnabled('Magento_Checkout')) {
            $this->addProductAttributesAndPrices($collection);
        }

        $collection->getSelect()->order(['stock_status_index.qty DESC', 'final_price DESC']);

        return $collection;
    }

    /**
     * Add all attributes and apply pricing logic to products collection
     * to get correct values in different products lists.
     * E.g. crosssells, upsells, new products, recently viewed
     *
     * @param LinkProductCollection $collection
     *
     * @return LinkProductCollection
     */
    protected function addProductAttributesAndPrices(
        LinkProductCollection $collection
    ): LinkProductCollection {
        return $collection->addMinimalPrice()->addFinalPrice()->addTaxPercents()
            ->addAttributeToSelect($this->catalogConfig->getProductAttributes())
            ->addUrlRewrite();
    }
}
