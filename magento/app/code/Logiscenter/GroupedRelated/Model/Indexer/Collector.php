<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\GroupedRelated\Model\Indexer;

use Logiscenter\GroupedRelatedConfig\Api\Config\GroupedRelatedConfigInterface;
use Logiscenter\GroupedRelatedConfig\Model\Source\RelatedTabs as TabsSource;
use Logiscenter\TabsIndexer\Api\CollectorInterface;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Model\Product\AttributeSet\Options;
use Magento\Catalog\Model\Product\Visibility;
use Magento\Catalog\Model\ResourceModel\Product\Link\Product\Collection as LinkProductCollection;

/**
 * Related tabs products collector
 */
class Collector implements CollectorInterface
{
    /**
     * @var Visibility
     */
    private $catalogProductVisibility;

    /**
     * @var GroupedRelatedConfigInterface
     */
    private $config;

    /**
     * @var Options
     */
    private $attributeSetOptions;

    /**
     * Related constructor.
     *
     * @param Visibility                    $catalogProductVisibility
     * @param GroupedRelatedConfigInterface $config
     * @param Options                       $attributeSetOptions
     */
    public function __construct(
        Visibility                    $catalogProductVisibility,
        GroupedRelatedConfigInterface $config,
        Options                       $attributeSetOptions
    ) {
        $this->catalogProductVisibility = $catalogProductVisibility;
        $this->attributeSetOptions = $attributeSetOptions;
        $this->config = $config;
    }

    /**
     * @param ProductInterface $product
     * @param string|null      $tabCode
     * @return array
     */
    public function getProductsIds(ProductInterface $product, ?string $tabCode = null): array
    {
        $attributeSetIds = $this->getAttributeSetsIdsToFilter($tabCode);
        if (empty($attributeSetIds)) {
            return [];
        }

        return $this->getCollection($product, $attributeSetIds)->load()->getAllIds();
    }

    /**
     * @param ProductInterface $product
     * @param array            $attributeSetIds
     *
     * @return LinkProductCollection
     */
    protected function getCollection(ProductInterface $product, array $attributeSetIds = []): LinkProductCollection
    {
        $collection = $product->getRelatedProductCollection()
                ->addStoreFilter();
        $collection->addAttributeToFilter('attribute_set_id', ['in' => $attributeSetIds]);
        $collection->setVisibility($this->catalogProductVisibility->getVisibleInCatalogIds());

        return $collection;
    }

    /**
     * @param string $tabCode
     *
     * @return array
     */
    private function getAttributeSetsIdsToFilter(string $tabCode): array
    {
        $attributeSetIds = $this->config->getAttributeSetsByTabCode($tabCode);
        $isDefault = $this->config->isDefaultTab($tabCode);
        if (!$isDefault || empty($attributeSetIds)) {
            return $attributeSetIds;
        }
        $key = array_search(TabsSource::ATTRIBUTE_SET_VALUE_FOR_DEFAULT_TAB, $attributeSetIds);
        unset($attributeSetIds[$key]);
        $nonConfiguredAttributeSetIds = $this->getAllNonConfiguredAttributeSetIds();

        return array_merge($nonConfiguredAttributeSetIds, $attributeSetIds);
    }

    /**
     * @return array
     */
    private function getAllNonConfiguredAttributeSetIds(): array
    {
        $allConfiguredAttributeSetId = $this->config->getAttributeSetsIds();
        $allExistingAttributeSet = array_column($this->attributeSetOptions->toOptionArray(), 'value');

        return \array_filter(
            $allExistingAttributeSet,
            function ($value) use ($allConfiguredAttributeSetId): bool {
                return !in_array($value, $allConfiguredAttributeSetId);
            }
        );
    }
}
