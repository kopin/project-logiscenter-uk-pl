<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Logiscenter\CompatibleProductsConfig\Api\Config\Db\Provider;

use Interactiv4\ConfigHelper\Api\Config\Db\Provider\DbConfigGroupPathProviderInterface;

/**
 * Interface CompatibleProductsGroupPathProviderInterface.
 *
 * Group path provider for compatibleproducts group.
 *
 * @api
 */
interface CompatibleProductsGroupPathProviderInterface extends DbConfigGroupPathProviderInterface
{
    public const GROUP_PATH = 'compatibleproducts';
}
