<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Logiscenter\CompatibleProductsConfig\Api\Config\Db\Provider;

use Interactiv4\ConfigHelper\Api\Config\Db\Provider\DbConfigSectionPathProviderInterface;

/**
 * Interface CompatibleProductsSectionPathProviderInterface.
 *
 * Section path provider for logiscenter_compatibleproductsconfig section.
 *
 * @api
 */
interface CompatibleProductsSectionPathProviderInterface extends DbConfigSectionPathProviderInterface
{
    public const SECTION_PATH = 'logiscenter_compatibleproductsconfig';
}
