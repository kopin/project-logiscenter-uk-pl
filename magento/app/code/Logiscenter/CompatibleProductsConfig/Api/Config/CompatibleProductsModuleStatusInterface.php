<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Logiscenter\CompatibleProductsConfig\Api\Config;

use Interactiv4\ConfigHelper\Api\Config\Status\ModuleStatusInterface;

/**
 * Interface CompatibleProductsModuleStatusInterface.
 *
 * Module status provider interface.
 *
 * @api
 */
interface CompatibleProductsModuleStatusInterface extends ModuleStatusInterface
{
}
