<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Logiscenter\CompatibleProductsConfig\Api\Config;

use Interactiv4\Environment\Api\AllowedEnvironmentsAwareInterface;

/**
 * Interface CompatibleProductsEnvironmentConfigInterface.
 *
 * Environment config provider interface.
 *
 * @api
 */
interface CompatibleProductsEnvironmentConfigInterface extends AllowedEnvironmentsAwareInterface
{
}
