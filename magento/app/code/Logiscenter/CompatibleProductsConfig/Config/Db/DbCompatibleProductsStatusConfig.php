<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Logiscenter\CompatibleProductsConfig\Config\Db;

use Interactiv4\BaseConfig\Api\Config\Db\Provider\StatusGroupPathProviderInterface;
use Interactiv4\ConfigHelper\Api\Config\Db\DbConfigHelperInterface;
use Interactiv4\ConfigHelper\Api\Config\Db\DbStatusTrait;
use Interactiv4\ConfigHelper\Api\Config\Db\Provider\DbConfigGroupPathProviderTrait;
use Interactiv4\ConfigHelper\Api\Config\Db\Provider\DbConfigSectionPathProviderTrait;
use Interactiv4\ConfigHelper\Api\Config\Db\DbStatusInterface;
use Interactiv4\ConfigHelper\Model\Config\Db\DbConfigHelper;
use Logiscenter\CompatibleProductsConfig\Api\Config\Db\Provider\CompatibleProductsSectionPathProviderInterface;

/**
 * Class DbCompatibleProductsStatusConfig.
 *
 * This is not an api class, implementation may change at any time.
 * @see \Logiscenter\CompatibleProductsConfig\Api\Config\CompatibleProductsStatusConfigInterface,
 * use this to retrieve config.
 */
class DbCompatibleProductsStatusConfig extends DbConfigHelper implements
    CompatibleProductsSectionPathProviderInterface,
    StatusGroupPathProviderInterface,
    DbStatusInterface,
    DbConfigHelperInterface
{
    use DbConfigSectionPathProviderTrait;
    use DbConfigGroupPathProviderTrait;
    use DbStatusTrait;
}
