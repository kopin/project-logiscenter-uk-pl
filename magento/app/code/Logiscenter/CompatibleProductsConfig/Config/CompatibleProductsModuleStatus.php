<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Logiscenter\CompatibleProductsConfig\Config;

use Logiscenter\CompatibleProductsConfig\Api\Config\CompatibleProductsModuleStatusInterface;
use Logiscenter\CompatibleProductsConfig\Api\Config\CompatibleProductsStatusConfigInterface;

/**
 * Class CompatibleProductsModuleStatus.
 *
 * This is not an api class, implementation may change at any time.
 * @see CompatibleProductsModuleStatusInterface, use this to retrieve config.
 *
 * For example, it now relies on CompatibleProductsStatusConfigInterface to read config,
 * but this implementation can be changed when needed, without prior notice.
 */
class CompatibleProductsModuleStatus implements CompatibleProductsModuleStatusInterface
{
    /**
     * @var CompatibleProductsStatusConfigInterface
     */
    private $statusConfig;

    /**
     * CompatibleProductsModuleStatus constructor.
     * @param CompatibleProductsStatusConfigInterface $statusConfig
     */
    public function __construct(
        CompatibleProductsStatusConfigInterface $statusConfig
    ) {
        $this->statusConfig = $statusConfig;
    }

    /**
     * Compare boolean status, using optionally supplied data
     *
     * @param bool $expectedStatusValue
     * @param int|string|null $storeId
     * @return bool
     */
    public function isBooleanStatus(
        bool $expectedStatusValue,
        $storeId = null
    ): bool {
        return $this->statusConfig->isBooleanStatus($expectedStatusValue, $storeId);
    }

    /**
     * Read boolean status, using optionally supplied data
     *
     * @param int|string|null $storeId
     * @return bool
     */
    public function isEnabled($storeId = null): bool
    {
        return $this->statusConfig->isEnabled($storeId);
    }
}
