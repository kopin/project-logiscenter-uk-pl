<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Logiscenter\CompatibleProductsConfig\Config;

use Logiscenter\CompatibleProductsConfig\Api\Config\CompatibleProductsEnvironmentConfigInterface;
use Logiscenter\CompatibleProductsConfig\Config\Db\DbCompatibleProductsEnvironmentConfig;

/**
 * Class CompatibleProductsEnvironmentConfig.
 *
 * This is not an api class, implementation may change at any time.
 * @see CompatibleProductsEnvironmentConfigInterface, use this to retrieve config.
 *
 * For example, it now relies on DbCompatibleProductsEnvironmentConfig to read information from database,
 * but this implementation can be changed when needed, without prior notice.
 */
class CompatibleProductsEnvironmentConfig extends DbCompatibleProductsEnvironmentConfig implements
    CompatibleProductsEnvironmentConfigInterface
{
}
