<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Logiscenter\CompatibleProductsConfig\Test\Integration\Di;

use Interactiv4\Framework\Api\Di\Definition\DefinitionPoolInterface;
use Interactiv4\Framework\Api\Di\Definition\DefinitionPoolProviderInterface;
use Interactiv4\Framework\Di\Definition\Definition;
use Interactiv4\Framework\Di\Definition\DefinitionPool;
use Interactiv4\Framework\Di\Definition\DefinitionName;
use Interactiv4\Framework\Di\Definition\DefinitionValue;

/**
 * Class PreferencesDefinitionProvider.
 *
 * Preferences definition as they appear in di configuration files.
 */
class PreferencesDefinitionProvider implements DefinitionPoolProviderInterface
{
    /**
     * @var DefinitionPoolInterface
     */
    private $definitionPool;

    /**
     * {@inheritdoc}
     */
    public function getDefinitionPool(): DefinitionPoolInterface
    {
        if (!isset($this->definitionPool)) {
            $this->definitionPool = new DefinitionPool(
	new Definition(
	new DefinitionName(
	'Logiscenter\\CompatibleProductsConfig\\Api\\Config\\CompatibleProductsModuleStatusInterface'
	),
		new DefinitionValue(
	\Logiscenter\CompatibleProductsConfig\Api\Config\CompatibleProductsModuleStatusInterface::class,
		\Logiscenter\CompatibleProductsConfig\Config\CompatibleProductsModuleStatus::class,
		(string) \Magento\Framework\App\Area::AREA_GLOBAL,
		false,
		null,
		null
	)
	),
		new Definition(
	new DefinitionName(
	'Logiscenter\\CompatibleProductsConfig\\Api\\Config\\CompatibleProductsStatusConfigInterface'
	),
		new DefinitionValue(
	\Logiscenter\CompatibleProductsConfig\Api\Config\CompatibleProductsStatusConfigInterface::class,
		\Logiscenter\CompatibleProductsConfig\Config\CompatibleProductsStatusConfig::class,
		(string) \Magento\Framework\App\Area::AREA_GLOBAL,
		false,
		null,
		null
	)
	),
		new Definition(
	new DefinitionName(
	'Logiscenter\\CompatibleProductsConfig\\Api\\Config\\CompatibleProductsEnvironmentConfigInterface'
	),
		new DefinitionValue(
	\Logiscenter\CompatibleProductsConfig\Api\Config\CompatibleProductsEnvironmentConfigInterface::class,
		\Logiscenter\CompatibleProductsConfig\Config\CompatibleProductsEnvironmentConfig::class,
		(string) \Magento\Framework\App\Area::AREA_GLOBAL,
		false,
		null,
		null
	)
	),
		new Definition(
	new DefinitionName(
	'Logiscenter\\CompatibleProductsConfig\\Api\\Config\\CompatibleProductsConfigInterface'
	),
		new DefinitionValue(
	\Logiscenter\CompatibleProductsConfig\Api\Config\CompatibleProductsConfigInterface::class,
		\Logiscenter\CompatibleProductsConfig\Config\CompatibleProductsConfig::class,
		(string) \Magento\Framework\App\Area::AREA_GLOBAL,
		false,
		null,
		null
	)
	)
	);
        }

        return $this->definitionPool;
    }
}
