/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

var config = {
    map: {
        '*': {
            mwGroupedProduct: 'Logiscenter_GroupedModels/js/product/view/type/grouped'
        }
    }
};

