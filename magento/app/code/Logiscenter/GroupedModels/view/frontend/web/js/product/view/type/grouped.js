define([
    'jquery',
    'mage/url'
], function ($, urlBuilder) {
    return {
        init: function () {
            $(document).on('click', '[data-event="see-more-models"]', function(e) {
                e.preventDefault();

                let positionTop = $('.product__extra').position().top - $('.page-header').height();
                $("html, body").animate({
                    scrollTop: positionTop
                });
            });

            $(document).on('change keyup', '[data-event="input-qty-model-product"]', function(e) {
                e.preventDefault();

                var productActions = $(this).closest('.grouped__content-product').find('.grouped__col.actions button');
                var productQty = $(this).val();

                if (productQty <= 0) {
                    productActions.prop('disabled', true);
                }
                else {
                    productActions.prop('disabled', false);
                }
            });

            $(document).on('click', '[data-event="add-to-cart-model-product"]', function(e) {
                e.preventDefault();

                let minicartSelector = '[data-block="minicart"]';
                let messagesSelector = '[data-placeholder="messages"]';
                let productId = $(this).closest('.grouped__content-product').data("product-id");
                let productQty = $(this).closest('.grouped__content-product').find('.grouped__col.qty input').val();
                let formKey = $(this).closest('form').find('input[name="form_key"]').val();

                let addToCartUrl = urlBuilder.build('checkout/cart/add', {});
                $.ajax({
                    url: addToCartUrl,
                    data: {
                        product: productId,
                        qty: productQty,
                        form_key: formKey
                    },
                    type: 'post',
                    dataType: 'json',
                    cache: false,

                    /** @inheritdoc */
                    beforeSend: function () {
                        // we show the loader
                        $('body').trigger('processStart');
                    },

                    /** @inheritdoc */
                    success: function (res) {
                        // we hide the loader
                        $('body').trigger('processStop');

                        if (res.backUrl) {
                            window.location.reload();
                        }

                        if (res.messages) {
                            $(messagesSelector).html(res.messages);
                        }

                        if (res.minicart) {
                            $(minicartSelector).replaceWith(res.minicart);
                            $(minicartSelector).trigger('contentUpdated');
                        }

                        if (res.product && res.product.statusText) {
                            window.location.reload();
                        }
                    },

                    /** @inheritdoc */
                    complete: function (res) {
                        if (res.state() === 'rejected') {
                            window.location.reload();
                        }
                    }
                });
            });
        }
    }
});