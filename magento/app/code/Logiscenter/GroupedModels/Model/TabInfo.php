<?php

/**
 * @author    Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\GroupedModels\Model;

use Logiscenter\AvailabilityInfo\Api\GetAvailabilityInfoDataInterface;
use Logiscenter\ElasticSearchTabs\Model\GetTabProducts;
use Logiscenter\GroupedModels\Model\Product\Associated;
use Logiscenter\GroupedModelsConfig\Api\Config\GroupedModelsModuleStatusInterface;
use Logiscenter\GroupedModels\Model\Source\ModelTabs as TabsSource;
use Logiscenter\Price\Model\Price\Config;
use Logiscenter\Price\Service\Price\GetFinalPriceWithTax;
use Logiscenter\Price\Service\Price\GetMsrpPrice;
use Logiscenter\ProductTabs\Api\Data\TabInfoInterface;
use Logiscenter\ProductTabs\Model\TabInfoAbstract;
use Logiscenter\ProductTabsConfig\Api\Config\ProductTabsConfigInterface;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Helper\Image;
use Magento\Catalog\Model\ResourceModel\Collection\AbstractCollection;
use Magento\Framework\Pricing\PriceCurrencyInterface;

/**
 * Class represent grouped models tab info
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class TabInfo extends TabInfoAbstract implements TabInfoInterface
{
    /**
     * @var Associated
     */
    private $productAssociated;

    /**
     * @param GroupedModelsModuleStatusInterface $statusConfig
     * @param TabsSource                         $tabsSource
     * @param Image                              $imageHelper
     * @param PriceCurrencyInterface             $priceCurrency
     * @param GetMsrpPrice                       $getMsrpPrice
     * @param GetFinalPriceWithTax               $getFinalPriceWithTax
     * @param GetAvailabilityInfoDataInterface   $getAvailabilityInfoData
     * @param Config                             $priceConfig
     * @param GetTabProducts                     $getTabProducts
     * @param ProductTabsConfigInterface         $tabsConfig
     * @param Associated                         $productAssociated
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        GroupedModelsModuleStatusInterface $statusConfig,
        TabsSource                         $tabsSource,
        Image                              $imageHelper,
        PriceCurrencyInterface             $priceCurrency,
        GetMsrpPrice                       $getMsrpPrice,
        GetFinalPriceWithTax               $getFinalPriceWithTax,
        GetAvailabilityInfoDataInterface   $getAvailabilityInfoData,
        Config                             $priceConfig,
        GetTabProducts                     $getTabProducts,
        ProductTabsConfigInterface         $tabsConfig,
        Product\Associated                 $productAssociated
    ) {
        parent::__construct(
            $statusConfig,
            $tabsSource,
            $imageHelper,
            $priceCurrency,
            $getMsrpPrice,
            $getFinalPriceWithTax,
            $getAvailabilityInfoData,
            $priceConfig,
            $getTabProducts,
            $tabsConfig
        );
        $this->productAssociated = $productAssociated;
    }

    /**
     * @param ProductInterface $product
     * @param string           $tabCode
     *
     * @return AbstractCollection
     */
    protected function getProductsCollection(ProductInterface $product, string $tabCode): AbstractCollection
    {
        return $this->productAssociated->getProducts($product);
    }
}
