<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\GroupedModels\Model\Indexer;

use Logiscenter\GroupedModels\Model\Product\Associated;
use Logiscenter\TabsIndexer\Api\CollectorInterface;
use Magento\Catalog\Api\Data\ProductInterface;

/**
 * Collector for models tabs
 */
class Collector implements CollectorInterface
{
    /**
     * @var Associated
     */
    private $associated;

    /**
     * @param Associated $associated
     */
    public function __construct(Associated $associated)
    {
        $this->associated = $associated;
    }

    /**
     * @param ProductInterface $product
     *
     * @return array
     */
    public function getProductsIds(ProductInterface $product, ?string $tabCode = null): array
    {
        $products = $this->associated->getProductsWithStockData($product);

        $i = 0;
        foreach ($products as $product) {
            $i++;
            //don't need to show first two products in tab
            if ($i < 3) {
                continue;
            }

            $result[] = $product->getId();
        }

        return $result ?? [];
    }
}
