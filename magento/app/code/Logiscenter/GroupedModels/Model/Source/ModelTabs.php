<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\GroupedModels\Model\Source;

use Interactiv4\Framework\Data\AbstractOptionSource;

/**
 * Class ModelTabs.
 */
class ModelTabs extends AbstractOptionSource
{
    /**
     * @var array
     */
    private $modelTabs;

    /**
     * RelatedTabs constructor.
     *
     * @param array $modelTabs
     */
    public function __construct(
        array $modelTabs = []
    ) {
        $this->modelTabs = $modelTabs;
    }

    /**
     * To option array.
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options = [];
        foreach ($this->toArray() as $value => $data) {
            $options[] = [
                'value' => $value,
                'label' => __($data['label']),
            ];
        }

        return $options;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return $this->modelTabs;
    }
}
