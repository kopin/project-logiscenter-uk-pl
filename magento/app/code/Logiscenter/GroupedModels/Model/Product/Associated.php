<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\GroupedModels\Model\Product;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Model\ResourceModel\Collection\AbstractCollection;
use Magento\CatalogInventory\Model\ResourceModel\Stock\Status;

/**
 * Class to get associated products
 */
class Associated
{
    private const ATTRIBUTE_TO_SELECT = [
        'name',
        'price',
        'special_price',
        'special_from_date',
        'special_to_date',
        'tax_class_id',
        'image',
        'thumbnail',
        'url_key',
        'short_description',
        'visibility',
    ];

    /**
     * @var Status
     */
    private $stockResource;

    /**
     * @param Status $stockResource
     */
    public function __construct(Status $stockResource)
    {
        $this->stockResource = $stockResource;
    }

    /**
     * @param ProductInterface $product
     *
     * @return AbstractCollection
     */
    public function getProducts(ProductInterface $product): AbstractCollection
    {
        $typeInstance = $product->getTypeInstance();
        $typeInstance->setSaleableStatus($product);
        $collection = $typeInstance->getAssociatedProductCollection($product);
        $collection->addAttributeToSelect(self::ATTRIBUTE_TO_SELECT)
            ->addFilterByRequiredOptions()
            ->setPositionOrder()
            ->addStoreFilter($typeInstance->getStoreFilter($product))
            ->addAttributeToFilter(
                'status',
                ['in' => $typeInstance->getStatusFilters($product)]
            );
        $collection->getSelect()->reset('order');
        $collection->getSelect()->order(['stock_status_index.qty DESC', 'final_price DESC', 'entity_id ASC']);
        $collection->addPriceData();

        return $collection;
    }

    /**
     * @param ProductInterface $product
     *
     * @return AbstractCollection
     */
    public function getProductsWithStockData(ProductInterface $product): AbstractCollection
    {
        $collection = $this->getProducts($product);
        $this->stockResource->addStockDataToCollection($collection, false);
        $collection->getSelect()->columns(['stock_qty' => 'stock_status_index.qty']);

        return $collection;
    }
}
