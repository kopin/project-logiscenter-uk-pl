<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\GroupedModels\ViewModel;

use Magento\Framework\View\Element\Block\ArgumentInterface;
use Logiscenter\GroupedModelsConfig\Api\Config\GroupedModelsModuleStatusInterface;

/**
 * Class Config
 *
 * @package Magento\Catalog\ViewModel\Product
 */
class Config implements ArgumentInterface
{
    private $statusConfig;

    /**
     * Config constructor.
     *
     * @param GroupedModelsModuleStatusInterface $statusConfig
     */
    public function __construct(
        GroupedModelsModuleStatusInterface $statusConfig
    ) {
        $this->statusConfig = $statusConfig;
    }

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->statusConfig->isEnabled();
    }
}
