<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\GroupedModels\Plugin\Model\Product\Type\Grouped;

use Logiscenter\GroupedModelsConfig\Api\Config\GroupedModelsModuleStatusInterface;
use Magento\GroupedProduct\Model\Product\Type\Grouped;

/**
 * Add extra information to associated products
 */
class AddExtraInfoPlugin
{
    private const ATTRIBUTE_TO_SELECT = [
        'name',
        'price',
        'special_price',
        'special_from_date',
        'special_to_date',
        'tax_class_id',
        'image',
        'thumbnail',
        'url_key',
        'short_description',
        'visibility',
    ];

    /**
     * @var GroupedModelsModuleStatusInterface
     */
    private $statusConfig;

    /**
     * Cache key for Associated Products
     *
     * @var string
     */
    protected $keyAssociatedProducts = '_cache_instance_associated_products';

    /**
     * Grouped constructor.
     *
     * @param GroupedModelsModuleStatusInterface $statusConfig
     */
    public function __construct(
        GroupedModelsModuleStatusInterface $statusConfig
    ) {
        $this->statusConfig = $statusConfig;
    }

    /**
     * @param Grouped  $subject
     * @param \Closure $proceed
     * @param          $product
     *
     * @return mixed
     */
    public function aroundGetAssociatedProducts(
        Grouped $subject,
        \Closure $proceed,
        $product
    ) {
        if (!$this->statusConfig->isEnabled()) {
            return $proceed($product);
        }
        if (!$product->hasData($this->keyAssociatedProducts)) {
            $associatedProducts = [];

            $subject->setSaleableStatus($product);
            $collection =
                $subject->getAssociatedProductCollection($product)
                    ->addAttributeToSelect(self::ATTRIBUTE_TO_SELECT)
                    ->addFilterByRequiredOptions()
                    ->setPositionOrder()
                    ->addStoreFilter($subject->getStoreFilter($product))
                    ->addAttributeToFilter(
                        'status',
                        ['in' => $subject->getStatusFilters($product)]
                    )
                    ->setOrder('price', 'DESC');
            $collection->getSelect()->reset('order');
            $collection->getSelect()->order(['stock_status_index.qty DESC', 'final_price DESC']);

            foreach ($collection->getItems() as $item) {
                $associatedProducts[] = $item;
            }
            $product->setData($this->keyAssociatedProducts, $associatedProducts);
        }

        return $product->getData($this->keyAssociatedProducts);
    }
}
