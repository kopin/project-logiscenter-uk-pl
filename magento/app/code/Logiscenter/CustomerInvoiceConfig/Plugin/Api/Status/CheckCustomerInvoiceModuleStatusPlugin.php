<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\CustomerInvoiceConfig\Plugin\Api\Status;

use Interactiv4\Framework\Plugin\Api\Status\CheckBooleanStatusPlugin;

/**
 * Class CheckCustomerInvoiceModuleStatusPlugin.
 *
 * Plugin to check module status. Usage:
 * Attach this plugin to any BooleanStatusReadInterface instance (e.g.: ModuleStatusInterface),
 * to chain this module status check to original isEnabled($additionalData = null) check.
 *
 * @see \Interactiv4\Contracts\SPL\BooleanStatus\Api\BooleanStatusReadInterface
 * @see \Interactiv4\ConfigHelper\Api\Config\Status\ModuleStatusInterface
 *
 * Forced to materialize file, since virtual plugins DO NOT WORK in production mode.
 *
 * @api
 */
class CheckCustomerInvoiceModuleStatusPlugin extends CheckBooleanStatusPlugin
{
}
