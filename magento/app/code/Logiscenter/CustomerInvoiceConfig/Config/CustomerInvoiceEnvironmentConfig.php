<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\CustomerInvoiceConfig\Config;

use Logiscenter\CustomerInvoiceConfig\Api\Config\CustomerInvoiceEnvironmentConfigInterface;
use Logiscenter\CustomerInvoiceConfig\Config\Db\DbCustomerInvoiceEnvironmentConfig;

/**
 * Class CustomerInvoiceEnvironmentConfig.
 *
 * This is not an api class, implementation may change at any time.
 * @see CustomerInvoiceEnvironmentConfigInterface, use this to retrieve config.
 *
 * For example, it now relies on DbCustomerInvoiceEnvironmentConfig to read information from database,
 * but this implementation can be changed when needed, without prior notice.
 */
class CustomerInvoiceEnvironmentConfig extends DbCustomerInvoiceEnvironmentConfig implements
    CustomerInvoiceEnvironmentConfigInterface
{
}
