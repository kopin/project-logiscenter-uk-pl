<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\CustomerInvoiceConfig\Config\Db;

use Interactiv4\ConfigHelper\Api\Config\Db\DbConfigHelperInterface;
use Interactiv4\ConfigHelper\Api\Config\Db\Provider\DbConfigGroupPathProviderTrait;
use Interactiv4\ConfigHelper\Api\Config\Db\Provider\DbConfigSectionPathProviderTrait;
use Interactiv4\ConfigHelper\Model\Config\Db\DbConfigHelper;
use Interactiv4\Environment\Api\Config\Db\AllowedEnvironmentsInterface;
use Interactiv4\Environment\Api\Config\Db\AllowedEnvironmentsTrait;
use Interactiv4\EnvironmentConfig\Api\Config\Db\Provider\EnvironmentGroupPathProviderInterface;
use Logiscenter\CustomerInvoiceConfig\Api\Config\Db\Provider\CustomerInvoiceSectionPathProviderInterface;

/**
 * Class DbCustomerInvoiceEnvironmentConfig.
 *
 * This is not an api class, implementation may change at any time.
 * @see \Logiscenter\CustomerInvoiceConfig\Api\Config\CustomerInvoiceEnvironmentConfigInterface,
 * use this to retrieve config.
 */
class DbCustomerInvoiceEnvironmentConfig extends DbConfigHelper implements
    CustomerInvoiceSectionPathProviderInterface,
    EnvironmentGroupPathProviderInterface,
    AllowedEnvironmentsInterface,
    DbConfigHelperInterface
{
    use DbConfigSectionPathProviderTrait;
    use DbConfigGroupPathProviderTrait;
    use AllowedEnvironmentsTrait;
}
