<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\CustomerInvoiceConfig\Config;

use Logiscenter\CustomerInvoiceConfig\Api\Config\CustomerInvoiceModuleStatusInterface;
use Logiscenter\CustomerInvoiceConfig\Api\Config\CustomerInvoiceStatusConfigInterface;

/**
 * Class CustomerInvoiceModuleStatus.
 *
 * This is not an api class, implementation may change at any time.
 * @see CustomerInvoiceModuleStatusInterface, use this to retrieve config.
 *
 * For example, it now relies on CustomerInvoiceStatusConfigInterface to read config,
 * but this implementation can be changed when needed, without prior notice.
 */
class CustomerInvoiceModuleStatus implements CustomerInvoiceModuleStatusInterface
{
    /**
     * @var CustomerInvoiceStatusConfigInterface
     */
    private $statusConfig;

    /**
     * CustomerInvoiceModuleStatus constructor.
     * @param CustomerInvoiceStatusConfigInterface $statusConfig
     */
    public function __construct(
        CustomerInvoiceStatusConfigInterface $statusConfig
    ) {
        $this->statusConfig = $statusConfig;
    }

    /**
     * Compare boolean status, using optionally supplied data
     *
     * @param bool $expectedStatusValue
     * @param int|string|null $storeId
     * @return bool
     */
    public function isBooleanStatus(
        bool $expectedStatusValue,
        $storeId = null
    ): bool {
        return $this->statusConfig->isBooleanStatus($expectedStatusValue, $storeId);
    }

    /**
     * Read boolean status, using optionally supplied data
     *
     * @param int|string|null $storeId
     * @return bool
     */
    public function isEnabled($storeId = null): bool
    {
        return $this->statusConfig->isEnabled($storeId);
    }
}
