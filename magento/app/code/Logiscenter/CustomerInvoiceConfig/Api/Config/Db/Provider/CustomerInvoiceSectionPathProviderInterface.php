<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\CustomerInvoiceConfig\Api\Config\Db\Provider;

use Interactiv4\ConfigHelper\Api\Config\Db\Provider\DbConfigSectionPathProviderInterface;

/**
 * Interface CustomerInvoiceSectionPathProviderInterface.
 *
 * Section path provider for logiscenter_customerinvoiceconfig section.
 *
 * @api
 */
interface CustomerInvoiceSectionPathProviderInterface extends DbConfigSectionPathProviderInterface
{
    public const SECTION_PATH = 'logiscenter_customerinvoiceconfig';
}
