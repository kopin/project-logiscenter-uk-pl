<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\CustomerInvoiceConfig\Test\Integration\Di;

use Interactiv4\Framework\Api\Di\Definition\DefinitionPoolInterface;
use Interactiv4\Framework\Api\Di\Definition\DefinitionPoolProviderInterface;
use Interactiv4\Framework\Di\Definition\Definition;
use Interactiv4\Framework\Di\Definition\DefinitionPool;
use Interactiv4\Framework\Di\Definition\DefinitionName;
use Interactiv4\Framework\Di\Definition\DefinitionValue;

/**
 * Class PreferencesDefinitionProvider.
 *
 * Preferences definition as they appear in di configuration files.
 */
class PreferencesDefinitionProvider implements DefinitionPoolProviderInterface
{
    /**
     * @var DefinitionPoolInterface
     */
    private $definitionPool;

    /**
     * {@inheritdoc}
     */
    public function getDefinitionPool(): DefinitionPoolInterface
    {
        if (!isset($this->definitionPool)) {
            $this->definitionPool = new DefinitionPool(
                new Definition(
                    new DefinitionName(
                        'Logiscenter\\CustomerInvoiceConfig\\Api\\Config\\CustomerInvoiceModuleStatusInterface'
                    ),
                    new DefinitionValue(
                        \Logiscenter\CustomerInvoiceConfig\Api\Config\CustomerInvoiceModuleStatusInterface::class,
                        \Logiscenter\CustomerInvoiceConfig\Config\CustomerInvoiceModuleStatus::class,
                        (string) \Magento\Framework\App\Area::AREA_GLOBAL,
                        false,
                        null,
                        null
                    )
                ),
                new Definition(
                    new DefinitionName(
                        'Logiscenter\\CustomerInvoiceConfig\\Api\\Config\\CustomerInvoiceStatusConfigInterface'
                    ),
                    new DefinitionValue(
                        \Logiscenter\CustomerInvoiceConfig\Api\Config\CustomerInvoiceStatusConfigInterface::class,
                        \Logiscenter\CustomerInvoiceConfig\Config\CustomerInvoiceStatusConfig::class,
                        (string) \Magento\Framework\App\Area::AREA_GLOBAL,
                        false,
                        null,
                        null
                    )
                ),
                new Definition(
                    new DefinitionName(
                        'Logiscenter\\CustomerInvoiceConfig\\Api\\Config\\CustomerInvoiceEnvironmentConfigInterface'
                    ),
                    new DefinitionValue(
                        \Logiscenter\CustomerInvoiceConfig\Api\Config\CustomerInvoiceEnvironmentConfigInterface::class,
                        \Logiscenter\CustomerInvoiceConfig\Config\CustomerInvoiceEnvironmentConfig::class,
                        (string) \Magento\Framework\App\Area::AREA_GLOBAL,
                        false,
                        null,
                        null
                    )
                ),
                new Definition(
                    new DefinitionName(
                        'Logiscenter\\CustomerInvoiceConfig\\Api\\Config\\CustomerInvoiceConfigInterface'
                    ),
                    new DefinitionValue(
                        \Logiscenter\CustomerInvoiceConfig\Api\Config\CustomerInvoiceConfigInterface::class,
                        \Logiscenter\CustomerInvoiceConfig\Config\CustomerInvoiceConfig::class,
                        (string) \Magento\Framework\App\Area::AREA_GLOBAL,
                        false,
                        null,
                        null
                    )
                )
            );
        }

        return $this->definitionPool;
    }
}
