<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\CustomerInvoiceConfig\Test\Integration\Di;

use Interactiv4\Framework\Api\Di\Definition\DefinitionPoolInterface;
use Interactiv4\Framework\Api\Di\Definition\DefinitionPoolProviderInterface;
use Interactiv4\Framework\Di\Definition\Definition;
use Interactiv4\Framework\Di\Definition\DefinitionPool;
use Interactiv4\Framework\Di\Definition\DefinitionName;
use Interactiv4\Framework\Di\Definition\DefinitionValue;

/**
 * Class ArgumentsDefinitionProvider.
 *
 * Arguments definition as they appear in di configuration files.
 */
class ArgumentsDefinitionProvider implements DefinitionPoolProviderInterface
{
    /**
     * @var DefinitionPoolInterface
     */
    private $definitionPool;

    /**
     * {@inheritdoc}
     */
    public function getDefinitionPool(): DefinitionPoolInterface
    {
        if (!isset($this->definitionPool)) {
            $this->definitionPool = new DefinitionPool(
                new Definition(
                    new DefinitionName(
                        'Logiscenter\\CustomerInvoiceConfig\\Plugin\\Api\\Status\\CheckCustomerInvoiceModuleStatusPlugin',
                        'booleanStatusRead'
                    ),
                    new DefinitionValue(
                        \Logiscenter\CustomerInvoiceConfig\Plugin\Api\Status\CheckCustomerInvoiceModuleStatusPlugin::class,
                        \Logiscenter\CustomerInvoiceConfig\Api\Config\CustomerInvoiceModuleStatusInterface::class,
                        (string) \Magento\Framework\App\Area::AREA_GLOBAL,
                        false,
                        null,
                        null
                    )
                ),
                new Definition(
                    new DefinitionName(
                        'Logiscenter\\CustomerInvoiceConfig\\Plugin\\Api\\Status\\CheckCustomerInvoiceEnvironmentPlugin',
                        'allowedEnvironmentsAware'
                    ),
                    new DefinitionValue(
                        \Logiscenter\CustomerInvoiceConfig\Plugin\Api\Status\CheckCustomerInvoiceEnvironmentPlugin::class,
                        \Logiscenter\CustomerInvoiceConfig\Api\Config\CustomerInvoiceEnvironmentConfigInterface::class,
                        (string) \Magento\Framework\App\Area::AREA_GLOBAL,
                        false,
                        null,
                        null
                    )
                ),
                new Definition(
                    new DefinitionName(
                        'Interactiv4\\BaseConfig\\Pool\\ModuleStatusPool',
                        'objects',
                        'Logiscenter_CustomerInvoiceConfig'
                    ),
                    new DefinitionValue(
                        'Interactiv4\\BaseConfig\\Pool\\ModuleStatusPool',
                        \Logiscenter\CustomerInvoiceConfig\Api\Config\CustomerInvoiceModuleStatusInterface::class,
                        (string) \Magento\Framework\App\Area::AREA_GLOBAL,
                        false,
                        null,
                        null
                    )
                ),
                new Definition(
                    new DefinitionName(
                        'Logiscenter\\CustomerInvoiceConfig\\Observer\\RefreshDataUponConfigChangeObserver',
                        'refreshCacheStrategy'
                    ),
                    new DefinitionValue(
                        \Logiscenter\CustomerInvoiceConfig\Observer\RefreshDataUponConfigChangeObserver::class,
                        \Interactiv4\Framework\Cache\RefreshCacheAppStateDependantStrategy::class,
                        (string) \Magento\Framework\App\Area::AREA_ADMINHTML,
                        false,
                        null,
                        null
                    )
                ),
                new Definition(
                    new DefinitionName(
                        'Logiscenter\\CustomerInvoiceConfig\\Observer\\RefreshDataUponConfigChangeObserver',
                        'cacheTypes',
                        'config'
                    ),
                    new DefinitionValue(
                        \Logiscenter\CustomerInvoiceConfig\Observer\RefreshDataUponConfigChangeObserver::class,
                        (string) \Magento\Framework\App\Cache\Type\Config::TYPE_IDENTIFIER,
                        (string) \Magento\Framework\App\Area::AREA_ADMINHTML,
                        false,
                        null,
                        null
                    )
                ),
                new Definition(
                    new DefinitionName(
                        'Logiscenter\\CustomerInvoiceConfig\\Observer\\RefreshDataUponConfigChangeObserver',
                        'cacheTypes',
                        'block_html'
                    ),
                    new DefinitionValue(
                        \Logiscenter\CustomerInvoiceConfig\Observer\RefreshDataUponConfigChangeObserver::class,
                        (string) \Magento\Framework\App\Cache\Type\Block::TYPE_IDENTIFIER,
                        (string) \Magento\Framework\App\Area::AREA_ADMINHTML,
                        false,
                        null,
                        null
                    )
                ),
                new Definition(
                    new DefinitionName(
                        'Logiscenter\\CustomerInvoiceConfig\\Observer\\RefreshDataUponConfigChangeObserver',
                        'cacheTypes',
                        'full_page'
                    ),
                    new DefinitionValue(
                        \Logiscenter\CustomerInvoiceConfig\Observer\RefreshDataUponConfigChangeObserver::class,
                        (string) \Magento\PageCache\Model\Cache\Type::TYPE_IDENTIFIER,
                        (string) \Magento\Framework\App\Area::AREA_ADMINHTML,
                        false,
                        null,
                        null
                    )
                )
            );
        }

        return $this->definitionPool;
    }
}
