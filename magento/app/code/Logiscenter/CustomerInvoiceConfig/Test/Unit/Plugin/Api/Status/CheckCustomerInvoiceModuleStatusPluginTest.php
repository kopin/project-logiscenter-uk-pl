<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\CustomerInvoiceConfig\Test\Unit\Plugin\Api\Status;

use Logiscenter\CustomerInvoiceConfig\Plugin\Api\Status\CheckCustomerInvoiceModuleStatusPlugin;
use Interactiv4\Framework\Test\Unit\Plugin\Api\Status\CheckBooleanStatusPluginTestTrait;
use PHPUnit\Framework\TestCase;

/**
 * Class CheckCustomerInvoiceModuleStatusPlugin.
 *
 * Test plugin that allows to intercept module status.
 *
 * @group config
 *
 * @internal
 */
class CheckCustomerInvoiceModuleStatusPluginTest extends TestCase
{
    use CheckBooleanStatusPluginTestTrait;

    /**
     * {@inheritdoc}
     */
    protected function getPluginClass(): string
    {
        return CheckCustomerInvoiceModuleStatusPlugin::class;
    }
}
