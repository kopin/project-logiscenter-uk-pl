<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\TabsIndexer\Model;

use Logiscenter\ElasticSearchTabs\Model\DocumentsCollector\GetAffectedDocuments;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class to load documents from ES with store simulation
 */
class GetAffectedDocumentsPerStore
{
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var GetAffectedDocuments
     */
    private $getAffectedDocuments;

    /**
     * @param StoreManagerInterface $storeManager
     * @param GetAffectedDocuments  $getAffectedDocuments
     */
    public function __construct(StoreManagerInterface $storeManager, GetAffectedDocuments $getAffectedDocuments)
    {
        $this->storeManager = $storeManager;
        $this->getAffectedDocuments = $getAffectedDocuments;
    }

    /**
     * @param int $productId
     * @param int $storeId
     *
     * @return array
     */
    public function execute(int $productId, int $storeId): array
    {
        $currentStore = $this->storeManager->getStore()->getId();

        try {
            $this->storeManager->setCurrentStore($storeId);

            return $this->getAffectedDocuments->execute($productId);
        } finally {
            $this->storeManager->setCurrentStore($currentStore);
        }
    }
}
