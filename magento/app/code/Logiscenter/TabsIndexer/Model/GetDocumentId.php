<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\TabsIndexer\Model;

/**
 * Generate id for document for ES
 */
class GetDocumentId
{
    /**
     * @param int    $id
     * @param string $tabCode
     *
     * @return string
     */
    public function execute(int $id, string $tabCode): string
    {
        return sprintf('%s_%s', $id, $tabCode);
    }
}
