<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\TabsIndexer\Model;

use Logiscenter\ElasticSearchTabs\Model\Persistence\ElasticSearchProductTabsRepository;
use Magento\Framework\Indexer\SaveHandler\Batch;

class IndexHandler
{
    /**
     * @var Batch
     */
    private $batch;

    /**
     * @var int
     */
    private $batchSize;

    /**
     * @var ElasticSearchProductTabsRepository
     */
    private $elasticSearchRepository;

    /**
     * @param Batch                              $batch
     * @param ElasticSearchProductTabsRepository $elasticSearchRepository
     * @param int                                $batchSize
     */
    public function __construct(
        Batch                              $batch,
        ElasticSearchProductTabsRepository $elasticSearchRepository,
        int                                $batchSize = 500
    ) {
        $this->batch = $batch;
        $this->batchSize = $batchSize;
        $this->elasticSearchRepository = $elasticSearchRepository;
    }

    /**
     * @param \Traversable $documents
     * @param int          $storeId
     *
     * @return void
     */
    public function saveIndex(\Traversable $documents, int $storeId): void
    {
        foreach ($this->batch->getItems($documents, $this->batchSize) as $batchDocuments) {
            $this->insertDocuments($batchDocuments, $storeId);
        }
    }

    /**
     * @param array  $batchDocuments
     *
     * @return void
     */
    private function insertDocuments(array $batchDocuments, $storeId): void
    {
        $this->elasticSearchRepository->bulkDeleteDocuments($batchDocuments, $storeId);
        $this->elasticSearchRepository->bulkAddDocuments($batchDocuments, $storeId);
    }
}
