<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\TabsIndexer\Model;

use Magento\Framework\Indexer\ActionInterface;
use Magento\Framework\Indexer\DimensionProviderInterface;
use Magento\Framework\Mview\ActionInterface as MviewActionInterface;
use Magento\Store\Model\StoreDimensionProvider;

/**
 * Indexer to reindex product tabs
 */
class Indexer implements ActionInterface, MviewActionInterface
{
    /**
     * @var IndexHandlerFactory
     */
    private $indexHandlerFactory;

    /**
     * @var GetDocuments
     */
    private $getDocuments;

    /**
     * @var GetAffectedDocumentsPerStore
     */
    private $getAffectedDocumentsPerStore;

    /**
     * @var DimensionProviderInterface
     */
    private $dimensionProvider;

    /**
     * @param IndexHandlerFactory $indexHandler
     * @param GetDocuments        $getDocuments
     */
    public function __construct(
        IndexHandlerFactory  $indexHandler,
        GetDocuments         $getDocuments,
        GetAffectedDocumentsPerStore $getAffectedDocumentsPerStore,
        DimensionProviderInterface $dimensionProvider
    ) {
        $this->indexHandlerFactory = $indexHandler;
        $this->getDocuments = $getDocuments;
        $this->getAffectedDocumentsPerStore = $getAffectedDocumentsPerStore;
        $this->dimensionProvider = $dimensionProvider;
    }

    /**
     * @inheritdoc
     */
    public function execute($ids): void
    {
        foreach ($this->dimensionProvider->getIterator() as $dimension) {
            $storeId = (int)$dimension[StoreDimensionProvider::DIMENSION_NAME]->getValue();
            $handler = $this->indexHandlerFactory->create();
            $handler->saveIndex(
                $this->getDocuments->execute($storeId, $this->getIdsToReindex($ids, $storeId)),
                $storeId
            );
        }
    }

    /**
     * @inheritdoc
     */
    public function executeFull(): void
    {
        foreach ($this->dimensionProvider->getIterator() as $dimension) {
            $storeId = (int)$dimension[StoreDimensionProvider::DIMENSION_NAME]->getValue();
            $handler = $this->indexHandlerFactory->create();
            $handler->saveIndex($this->getDocuments->execute($storeId), $storeId);
        }
    }

    /**
     * @inheritdoc
     */
    public function executeList(array $ids): void
    {
        $this->execute($ids);
    }

    /**
     * @inheritdoc
     */
    public function executeRow($id): void
    {
        $this->execute([$id]);
    }

    /**
     * @param int      $id
     * @param int|null $storeId
     *
     * @return array
     */
    private function collectAffectedProductIds(int $id, int $storeId): array
    {
        $docs = $this->getAffectedDocumentsPerStore->execute($id, $storeId);
        foreach ($docs as $doc) {
            $productId = $doc['product_id'] ?? null;
            if (null === $productId) {
                continue;
            }

            $ids[] = $productId;
        }

        return $ids ?? [];
    }

    /**
     * @param array $ids
     * @param int   $storeId
     *
     * @return array
     */
    private function getIdsToReindex(array $ids, int $storeId): array
    {
        $affectedIds = [];
        foreach ($ids as $id) {
            // phpcs:disable Magento2.Performance.ForeachArrayMerge.ForeachArrayMerge
            $affectedIds = array_merge($affectedIds, $this->collectAffectedProductIds($id, $storeId));
        }

        return array_unique(array_merge($ids, $affectedIds));
    }
}
