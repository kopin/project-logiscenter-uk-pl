<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\TabsIndexer\Model\Collector;

use Logiscenter\TabsIndexer\Api\CollectorInterface;
use Magento\Framework\Exception\LocalizedException;

/**
 * Class to choose appropriate collector for tab
 */
class Resolver
{
    /**
     * @var array
     */
    private $mapping;

    /**
     * @param array $mapping
     */
    public function __construct(array $mapping = [])
    {
        $this->mapping = $mapping;
    }

    /**
     * @param string $tabCode
     *
     * @return CollectorInterface
     * @throws LocalizedException
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     */
    public function getCollector(string $tabCode): CollectorInterface
    {
        foreach ($this->mapping as $item) {
            $source = $item['source'] ?? null;

            if (null === $source) {
                throw new \LogicException((string)__('Source is not specified for mapping'));
            }

            foreach ($item['source']->toArray() as $tabName => $tabData) {
                if ($tabName === $tabCode) {
                    $collector = $item['collector'] ?? null;

                    if (!$collector instanceof CollectorInterface) {
                        throw new \InvalidArgumentException(
                            (string)__(
                                'Collector should be an instance of %1',
                                CollectorInterface::class
                            )
                        );
                    }

                    return $collector;
                }
            }
        }

        throw new LocalizedException(__('Collector is not specified for tab %1', $tabCode));
    }
}
