<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\TabsIndexer\Model;

use Logiscenter\TabsIndexer\Model\Collector\Resolver;
use Logiscenter\ProductTabs\Model\TabConfig;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Get documents for ES
 */
class GetDocuments
{
    /**
     * @var ResourceConnection
     */
    private $resourceConnection;

    /**
     * @var TabConfig $tabs
     */
    private $tabs;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var GetDocumentId
     */
    private $getDocumentId;

    /**
     * @var Resolver
     */
    private $collectorResolver;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var int
     */
    private $batchSize;

    /**
     * @param ResourceConnection         $resourceConnection
     * @param TabConfig                  $tabs
     * @param ProductRepositoryInterface $productRepository
     * @param GetDocumentId              $getDocumentId
     * @param Resolver                   $collectorResolver
     * @param StoreManagerInterface      $storeManager
     * @param int                        $batchSize
     */
    public function __construct(
        ResourceConnection         $resourceConnection,
        TabConfig                  $tabs,
        ProductRepositoryInterface $productRepository,
        GetDocumentId              $getDocumentId,
        Resolver                   $collectorResolver,
        StoreManagerInterface      $storeManager,
        int                        $batchSize = 500
    ) {
        $this->resourceConnection = $resourceConnection;
        $this->tabs = $tabs;
        $this->productRepository = $productRepository;
        $this->batchSize = $batchSize;
        $this->getDocumentId = $getDocumentId;
        $this->collectorResolver = $collectorResolver;
        $this->storeManager = $storeManager;
    }

    /**
     * @param array|null $productIds
     *
     * @return \Generator
     */
    public function execute(int $storeId, array $productIds = null): \Generator
    {
        $lastProductId = 0;
        $ids = $this->getProductsBatch($storeId, $lastProductId, $productIds, $this->batchSize);

        while (count($ids) > 0) {
            $tabs = array_keys($this->tabs->getTabs());
            foreach ($tabs as $tab) {
                $collector = $this->collectorResolver->getCollector($tab);
                foreach ($ids as $id) {
                    $id = (int)$id;
                    $lastProductId = $id;
                    $product = $this->getProduct($id);
                    if (null === $product) {
                        continue;
                    }
                    $products = $collector->getProductsIds($product, $tab);
                    $result = $this->populateData($id, $products, $tab);
                    yield $this->getDocumentId->execute($id, $tab) => $result;
                }
            }

            $ids = $this->getProductsBatch($storeId, $lastProductId, $productIds, $this->batchSize);
        }
    }

    /**
     * @param int $lastProductId
     * @param int $batchSize
     *
     * @return array
     */
    private function getProductsBatch(int $storeId, int $lastProductId, $productIds, int $batchSize = 500): array
    {
        $connect = $this->resourceConnection->getConnection();
        $websiteId = (int)$this->storeManager->getStore($storeId)->getWebsiteId();
        $select = $connect->select()->from(
            ['cpe' => $connect->getTableName('catalog_product_entity')],
            'entity_id'
        );

        if ($productIds !== null) {
            $select->where('cpe.entity_id IN (?)', $productIds);
        }

        $select->where('cpe.type_id = ?', 'grouped');
        $select->where('cpe.entity_id > ?', $lastProductId);
        $select->join(
            ['website' => $connect->getTableName('catalog_product_website')],
            $connect->quoteInto('website.product_id = cpe.entity_id AND website.website_id = ?', $websiteId),
            []
        );
        $select->limit($batchSize);

        return $connect->fetchCol($select);
    }

    /**
     * @param int    $id
     * @param array  $ids
     * @param string $tabCode
     *
     * @return array
     */
    private function populateData(int $id, array $ids, string $tabCode): array
    {
        $result = [];
        $result['product_id'] = $id;
        $result['tab_code'] = $tabCode;
        $result['products'] = $ids;

        return $result;
    }

    /**
     * @param int $id
     *
     * @return ProductInterface|null
     */
    private function getProduct(int $id): ?ProductInterface
    {
        try {
            $product = $this->productRepository->getById($id);
            // phpcs:ignore Magento2.CodeAnalysis.EmptyBlock.DetectedCatch
        } catch (NoSuchEntityException $e) {
            //silent fail
        }

        return $product ?? null;
    }
}
