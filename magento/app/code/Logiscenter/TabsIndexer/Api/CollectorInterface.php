<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\TabsIndexer\Api;

use Magento\Catalog\Api\Data\ProductInterface;

/**
 * Interface for collectors objects
 */
interface CollectorInterface
{
    /**
     * @param ProductInterface $product
     * @param string|null      $tabCode
     *
     * @return array
     */
    public function getProductsIds(ProductInterface $product, ?string $tabCode = null): array;
}
