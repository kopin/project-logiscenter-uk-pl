<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\CountrySwitcher\ViewModel;

use Logiscenter\CountrySwitcherConfig\Api\Config\CountrySwitcherConfigInterface;
use Magento\Directory\Model\CountryFactory;
use Magento\Framework\Locale\ResolverInterface;
use Magento\Framework\View\Element\Block\ArgumentInterface;

/**
 * Websites popup view model class
 */
class WebsitesPopup implements ArgumentInterface
{
    /**
     * @var CountrySwitcherConfigInterface
     */
    private $countrySwitcherConfig;

    /**
     * @var CountryFactory
     */
    private $countryFactory;

    /**
     * @var ResolverInterface
     */
    private $localeResolver;

    /**
     * @param CountrySwitcherConfigInterface $countrySwitcherConfig
     * @param CountryFactory                 $countryFactory
     * @param ResolverInterface              $localeResolver
     */
    public function __construct(
        CountrySwitcherConfigInterface $countrySwitcherConfig,
        CountryFactory $countryFactory,
        ResolverInterface $localeResolver
    ) {
        $this->countrySwitcherConfig = $countrySwitcherConfig;
        $this->countryFactory = $countryFactory;
        $this->localeResolver = $localeResolver;
    }

    /**
     * Prepare countries info data for popup
     *
     * @return array
     */
    public function getAllCountriesInfo(): array
    {
        $result = [];
        foreach ($this->countrySwitcherConfig->getSitesData() as $siteData) {
            if ($siteData[CountrySwitcherConfigInterface::IS_ENABLED] !== '1') {
                continue;
            }
            $countryCode = $siteData[CountrySwitcherConfigInterface::COUNTRY_CODE];
            $countryLabel = $countryCode === 'eu'
                ? __('Other EU')
                : $this->countryFactory->create()->loadByCode($countryCode)->getName(
                    $this->localeResolver->getLocale()
                );
            $result[$countryCode] = [
                'label' => $countryLabel,
                'url'   => $siteData[CountrySwitcherConfigInterface::DOMAIN_URL],
            ];
        }

        return $result;
    }
}
