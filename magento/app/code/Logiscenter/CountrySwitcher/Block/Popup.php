<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\CountrySwitcher\Block;

use Logiscenter\CountrySwitcherConfig\Api\Config\CountrySwitcherModuleStatusInterface;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

/**
 * Class Popup.
 */
class Popup extends Template
{
    /**
     * @var CountrySwitcherModuleStatusInterface
     */
    private $countrySwitcherConfig;

    /**
     * Popup constructor.
     *
     * @param CountrySwitcherModuleStatusInterface $countrySwitcherConfig
     * @param Context                  $context
     * @param array                    $data
     */
    public function __construct(
        CountrySwitcherModuleStatusInterface $countrySwitcherConfig,
        Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->countrySwitcherConfig = $countrySwitcherConfig;
    }

    /**
     * @return bool
     */
    public function isEnabled()
    {
        return $this->countrySwitcherConfig->isEnabled();
    }
}
