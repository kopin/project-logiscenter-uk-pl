<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\CountrySwitcher\Controller\Validate;

use Logiscenter\CountrySwitcher\Service\GetCustomerCountryCode;
use Logiscenter\CountrySwitcherConfig\Api\Config\CountrySwitcherConfigInterface;
use Logiscenter\CountrySwitcherConfig\Api\Config\CountrySwitcherModuleStatusInterface;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Store\Model\ScopeInterface;

/**
 * Validate customer store switch
 */
class Ajax extends Action implements HttpPostActionInterface
{
    /**
     * @var JsonFactory
     */
    private $resultJsonFactory;

    /**
     * @var CountrySwitcherConfigInterface
     */
    private $switcherConfig;

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var GetCustomerCountryCode
     */
    private $getCountryCode;

    /**
     * @var CountrySwitcherModuleStatusInterface
     */
    private $statusConfig;

    /**
     * @param Context                              $context
     * @param JsonFactory                          $resultJsonFactory
     * @param CountrySwitcherConfigInterface       $switcherConfig
     * @param GetCustomerCountryCode               $getCountryCode
     * @param ScopeConfigInterface                 $scopeConfig
     * @param CountrySwitcherModuleStatusInterface $statusConfig
     */
    public function __construct(
        Context $context,
        JsonFactory $resultJsonFactory,
        CountrySwitcherConfigInterface $switcherConfig,
        GetCustomerCountryCode $getCountryCode,
        ScopeConfigInterface $scopeConfig,
        CountrySwitcherModuleStatusInterface $statusConfig
    ) {
        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
        $this->switcherConfig = $switcherConfig;
        $this->scopeConfig = $scopeConfig;
        $this->getCountryCode = $getCountryCode;
        $this->statusConfig = $statusConfig;
    }

    /**
     * @inheritDoc
     */
    public function execute()
    {
        if (!$this->getRequest()->isPost() || !$this->getRequest()->isAjax() || !$this->statusConfig->isEnabled()) {
            return null;
        }
        $countryCode = $this->getCountryCode->execute();
        $showModal = !($this->isRedirected() || $this->isRightCountry($countryCode));

        $result = [
            'status'       => 'ok',
            'show_modal'   => $showModal,
            'country_code' => $countryCode,
        ];

        return $this->resultJsonFactory->create()->setData($result);
    }

    /**
     * Consider user is in correct country when:
     * current website country is the same than the Geoip detected country
     * or if country couldn't be detected
     *
     * @param string $countryCode
     * @return bool
     */
    private function isRightCountry(string $countryCode): bool
    {
        return empty($countryCode) || $countryCode === $this->getWebsiteDefaultCountry();
    }

    /**
     * Check if customer changed store and is redirected from same site
     *
     * @return bool
     */
    private function isRedirected(): bool
    {
        $referrer = $this->getRequest()->getParam('previousReferer', false);

        if (!$referrer) {
            return false;
        }
        $host = parse_url($referrer, PHP_URL_HOST);

        return !empty($this->switcherConfig->getSiteDataByDomain($host));
    }

    /**
     * Return default country code for current website
     *
     * @return string
     */
    private function getWebsiteDefaultCountry(): string
    {
        return $this->scopeConfig->getValue(
            'general/country/default',
            ScopeInterface::SCOPE_WEBSITES
        );
    }
}
