/*
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

var config = {
    map: {
        '*': {
            'country_switcher': 'Logiscenter_CountrySwitcher/js/popup'
        }
    }
};
