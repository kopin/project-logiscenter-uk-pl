/*
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

define([
    'jquery',
    'Magento_Ui/js/modal/modal',
    'mage/url',
    'mage/storage',
    'mage/translate',
    'mage/cookies'
], function ($, modal, urlBuilder, storage, $t) {
    'use strict';

    $.widget('logiscenter_store_switcher.popup', {
        /**
         * Options common to all instances of this widget.
         * @type {Object}
         */
        options: {
            title: $t('Select your country'),
            selectInput: '.js-country-switcher-select',
            submitButton: '.js-country-switcher-confirm',
            declineButton: '.js-country-switcher-decline',
            flagItem: '.js-country-switcher-flag-item',
            classContent: '.country-switcher',
            optionsModal: {
                type: 'popup',
                responsive: true,
                innerScroll: true,
                modalClass: 'modal-country-switcher',
                buttons: []
            },
            cookieLifetimeDays: 365,
            cookieName: 'show_country_switcher_modal'
        },

        /** @inheritdoc */
        _create: function () {
            if (!$.mage.cookies.get(this.options.cookieName) && $(this.options.classContent).length > 0) {
                let serviceUrl = urlBuilder.build('country_switcher/validate/ajax', {});
                let formKey = $(this.options.classContent).find('form input[name="form_key"]').val();

                $.ajax({
                    url: serviceUrl,
                    data: {
                        form_key: formKey,
                        previousReferer: document.referrer
                    },
                    type: 'post',
                    dataType: 'json',
                    cache: false,

                    /** @inheritdoc */
                    fail: function (xhr, textStatus, errorThrown) {
                        console.error('Some error happened getting Store Switcher config.');
                    },

                    complete: function (response) {
                        if (typeof response === 'object') {
                            try {
                                if (response.status == 200) {
                                    this._setCookie(false);
                                }
                            } catch (e) {
                                console.error('Some error happened parsing Store Switcher config.');
                            }
                        }
                    }.bind(this),

                    /** @inheritdoc */
                    success: function (response) {
                        if (typeof response === 'object') {
                            try {
                                if (response.status == 'ok' && response.show_modal == true) {
                                    var storeSwitcherModal = modal(this.options.optionsModal, $(this.options.classContent));

                                    this._bind($(this.options.selectInput));
                                    this._bind($(this.options.submitButton));
                                    this._bind($(this.options.flagItem));
                                    this._bind($(this.options.declineButton));

                                    if (response.country_code) {
                                        this._assignSuggestedCountry(response.country_code);
                                    }

                                    this._showModal();
                                }
                            } catch (e) {
                                console.error('Some error happened parsing Store Switcher config.');
                            }
                        }
                    }.bind(this)
                });
            }
        },

        /**
         * Assign the suggested country to the input selector
         *
         * @param {String} isoCode
         * @private
         */
        _assignSuggestedCountry: function (isoCode) {
            let selectedOption = $(this.options.selectInput).find('option[value="'+isoCode+'"]');

            if (selectedOption) {
                let previousLabel = selectedOption.html();

                selectedOption.prop('selected', true);
                selectedOption.html($t('Suggested store:') + ' ' + previousLabel);
            }
        },

        /**
         * Bind the different case events to the elements
         *
         * @param {Object} element
         * @private
         */
        _bind: function (element) {
            if (element.is('select')) { // Select input country
                element.on('change', $.proxy(this._processSelect, this));
            }
            else if (element.is('button')) { // Submit button
                element.on('click', $.proxy(this._processButton, this));
            }
            else if (element.is('li')) { // Flag icon country
                element.on('click', $.proxy(this._processFlag, this));
            }
            else { // Decline button
                element.on('click', $.proxy(this._closeModal, this));
            }
        },

        /**
         * @param {jQuery.Event} event
         * @private
         */
        _processSelect: function (event) {
            this._redirectToStore(
                $(event.currentTarget).find(':selected').data('url')
            );
        },

        /**
         * @param {jQuery.Event} event
         * @private
         */
        _processButton: function (event) {
            event.preventDefault();

            this._redirectToStore(
                $(this.options.selectInput).find(':selected').data('url')
            );
        },

        /**
         * @param {jQuery.Event} event
         * @private
         */
        _processFlag: function (event) {
            let isoCode = $(event.currentTarget).data('store');
            let selectInput = $(this.options.selectInput).find('option[value='+isoCode+']').attr("selected", true);

            this._redirectToStore(
                $(this.options.selectInput).find(':selected').data('url')
            );
        },

        /**
         * @param {Boolean} cookieValue
         * @private
         */
        _setCookie: function (cookieValue) {
            // Default at 365 days.
            let days = this.options.cookieLifetimeDays || 365;

            // Get unix milliseconds at current time plus number of days
            let cookieExpires = new Date(new Date().getTime() + (days * 24 * 60 * 60 * 1000));

            $.mage.cookies.set(this.options.cookieName, JSON.stringify(cookieValue), {
                expires: cookieExpires,
                path: '/'
            });
        },

        /**
         * Redirect to the store selected by the client
         *
         * @param {String} redirectUrl
         * @return {Boolean}
         */
        _redirectToStore: function (redirectUrl) {
            window.location.href = redirectUrl;
        },

        /** @inheritdoc */
        _showModal: function () {
            $(this.options.classContent).modal('openModal').trigger('contentUpdated');
        },

        /** @inheritdoc */
        _closeModal: function () {
            $(this.options.classContent).modal('closeModal');
        }
    });

    return $.logiscenter_store_switcher.popup;
});
