<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\CountrySwitcher\Service;

use Interactiv4\Geoip\Api\GeoProviderInterface;
use Interactiv4\Geoip\Api\IpProviderInterface;
use Logiscenter\CountrySwitcherConfig\Api\Config\CountrySwitcherModuleStatusInterface;

/**
 * Get country code by customer IP service class
 */
class GetCustomerCountryCode
{
    /**
     * @var IpProviderInterface
     */
    private $ipProvider;

    /**
     * @var GeoProviderInterface
     */
    private $geoProvider;

    /**
     * @var CountrySwitcherModuleStatusInterface
     */
    private $statusConfig;

    /**
     * @param IpProviderInterface                  $ipProvider
     * @param GeoProviderInterface                 $geoProvider
     * @param CountrySwitcherModuleStatusInterface $statusConfig
     */
    public function __construct(
        IpProviderInterface $ipProvider,
        GeoProviderInterface $geoProvider,
        CountrySwitcherModuleStatusInterface $statusConfig
    ) {
        $this->ipProvider = $ipProvider;
        $this->geoProvider = $geoProvider;
        $this->statusConfig = $statusConfig;
    }

    /**
     * Return customer ip country code
     *
     * @return string
     */
    public function execute(): string
    {
        if (!$this->statusConfig->isEnabled()) {
            return '';
        }
        $data = $this->geoProvider->getData($this->ipProvider->getIpAddress());

        return strtolower($data['country_code'] ?? '');
    }
}
