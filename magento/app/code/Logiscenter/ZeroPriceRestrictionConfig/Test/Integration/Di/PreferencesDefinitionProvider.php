<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Logiscenter\ZeroPriceRestrictionConfig\Test\Integration\Di;

use Interactiv4\Framework\Api\Di\Definition\DefinitionPoolInterface;
use Interactiv4\Framework\Api\Di\Definition\DefinitionPoolProviderInterface;
use Interactiv4\Framework\Di\Definition\Definition;
use Interactiv4\Framework\Di\Definition\DefinitionPool;
use Interactiv4\Framework\Di\Definition\DefinitionName;
use Interactiv4\Framework\Di\Definition\DefinitionValue;

/**
 * Class PreferencesDefinitionProvider.
 *
 * Preferences definition as they appear in di configuration files.
 */
class PreferencesDefinitionProvider implements DefinitionPoolProviderInterface
{
    /**
     * @var DefinitionPoolInterface
     */
    private $definitionPool;

    /**
     * {@inheritdoc}
     */
    public function getDefinitionPool(): DefinitionPoolInterface
    {
        if (!isset($this->definitionPool)) {
            $this->definitionPool = new DefinitionPool(
	new Definition(
	new DefinitionName(
	'Logiscenter\\ZeroPriceRestrictionConfig\\Api\\Config\\ZeroPriceRestrictionModuleStatusInterface'
	),
		new DefinitionValue(
	\Logiscenter\ZeroPriceRestrictionConfig\Api\Config\ZeroPriceRestrictionModuleStatusInterface::class,
		\Logiscenter\ZeroPriceRestrictionConfig\Config\ZeroPriceRestrictionModuleStatus::class,
		(string) \Magento\Framework\App\Area::AREA_GLOBAL,
		false,
		null,
		null
	)
	),
		new Definition(
	new DefinitionName(
	'Logiscenter\\ZeroPriceRestrictionConfig\\Api\\Config\\ZeroPriceRestrictionStatusConfigInterface'
	),
		new DefinitionValue(
	\Logiscenter\ZeroPriceRestrictionConfig\Api\Config\ZeroPriceRestrictionStatusConfigInterface::class,
		\Logiscenter\ZeroPriceRestrictionConfig\Config\ZeroPriceRestrictionStatusConfig::class,
		(string) \Magento\Framework\App\Area::AREA_GLOBAL,
		false,
		null,
		null
	)
	),
		new Definition(
	new DefinitionName(
	'Logiscenter\\ZeroPriceRestrictionConfig\\Api\\Config\\ZeroPriceRestrictionEnvironmentConfigInterface'
	),
		new DefinitionValue(
	\Logiscenter\ZeroPriceRestrictionConfig\Api\Config\ZeroPriceRestrictionEnvironmentConfigInterface::class,
		\Logiscenter\ZeroPriceRestrictionConfig\Config\ZeroPriceRestrictionEnvironmentConfig::class,
		(string) \Magento\Framework\App\Area::AREA_GLOBAL,
		false,
		null,
		null
	)
	),
		new Definition(
	new DefinitionName(
	'Logiscenter\\ZeroPriceRestrictionConfig\\Api\\Config\\ZeroPriceRestrictionConfigInterface'
	),
		new DefinitionValue(
	\Logiscenter\ZeroPriceRestrictionConfig\Api\Config\ZeroPriceRestrictionConfigInterface::class,
		\Logiscenter\ZeroPriceRestrictionConfig\Config\ZeroPriceRestrictionConfig::class,
		(string) \Magento\Framework\App\Area::AREA_GLOBAL,
		false,
		null,
		null
	)
	)
	);
        }

        return $this->definitionPool;
    }
}
