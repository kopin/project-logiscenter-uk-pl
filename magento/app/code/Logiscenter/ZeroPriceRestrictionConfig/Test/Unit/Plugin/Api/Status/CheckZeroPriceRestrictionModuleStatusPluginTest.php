<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Logiscenter\ZeroPriceRestrictionConfig\Test\Unit\Plugin\Api\Status;

use Logiscenter\ZeroPriceRestrictionConfig\Plugin\Api\Status\CheckZeroPriceRestrictionModuleStatusPlugin;
use Interactiv4\Framework\Test\Unit\Plugin\Api\Status\CheckBooleanStatusPluginTestTrait;
use PHPUnit\Framework\TestCase;

/**
 * Class CheckZeroPriceRestrictionModuleStatusPlugin.
 *
 * Test plugin that allows to intercept module status.
 *
 * @group config
 *
 * @internal
 */
class CheckZeroPriceRestrictionModuleStatusPluginTest extends TestCase
{
    use CheckBooleanStatusPluginTestTrait;

    /**
     * {@inheritdoc}
     */
    protected function getPluginClass(): string
    {
        return CheckZeroPriceRestrictionModuleStatusPlugin::class;
    }
}
