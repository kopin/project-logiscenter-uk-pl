<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Logiscenter\ZeroPriceRestrictionConfig\Config;

use Logiscenter\ZeroPriceRestrictionConfig\Api\Config\ZeroPriceRestrictionEnvironmentConfigInterface;
use Logiscenter\ZeroPriceRestrictionConfig\Config\Db\DbZeroPriceRestrictionEnvironmentConfig;

/**
 * Class ZeroPriceRestrictionEnvironmentConfig.
 *
 * This is not an api class, implementation may change at any time.
 * @see ZeroPriceRestrictionEnvironmentConfigInterface, use this to retrieve config.
 *
 * For example, it now relies on DbZeroPriceRestrictionEnvironmentConfig to read information from database,
 * but this implementation can be changed when needed, without prior notice.
 */
class ZeroPriceRestrictionEnvironmentConfig extends DbZeroPriceRestrictionEnvironmentConfig implements
    ZeroPriceRestrictionEnvironmentConfigInterface
{
}
