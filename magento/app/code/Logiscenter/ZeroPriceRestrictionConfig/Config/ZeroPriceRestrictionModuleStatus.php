<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Logiscenter\ZeroPriceRestrictionConfig\Config;

use Logiscenter\ZeroPriceRestrictionConfig\Api\Config\ZeroPriceRestrictionModuleStatusInterface;
use Logiscenter\ZeroPriceRestrictionConfig\Api\Config\ZeroPriceRestrictionStatusConfigInterface;

/**
 * Class ZeroPriceRestrictionModuleStatus.
 *
 * This is not an api class, implementation may change at any time.
 * @see ZeroPriceRestrictionModuleStatusInterface, use this to retrieve config.
 *
 * For example, it now relies on ZeroPriceRestrictionStatusConfigInterface to read config,
 * but this implementation can be changed when needed, without prior notice.
 */
class ZeroPriceRestrictionModuleStatus implements ZeroPriceRestrictionModuleStatusInterface
{
    /**
     * @var ZeroPriceRestrictionStatusConfigInterface
     */
    private $statusConfig;

    /**
     * ZeroPriceRestrictionModuleStatus constructor.
     * @param ZeroPriceRestrictionStatusConfigInterface $statusConfig
     */
    public function __construct(
        ZeroPriceRestrictionStatusConfigInterface $statusConfig
    ) {
        $this->statusConfig = $statusConfig;
    }

    /**
     * Compare boolean status, using optionally supplied data
     *
     * @param bool $expectedStatusValue
     * @param int|string|null $storeId
     * @return bool
     */
    public function isBooleanStatus(
        bool $expectedStatusValue,
        $storeId = null
    ): bool {
        return $this->statusConfig->isBooleanStatus($expectedStatusValue, $storeId);
    }

    /**
     * Read boolean status, using optionally supplied data
     *
     * @param int|string|null $storeId
     * @return bool
     */
    public function isEnabled($storeId = null): bool
    {
        return $this->statusConfig->isEnabled($storeId);
    }
}
