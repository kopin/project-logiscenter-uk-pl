<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Logiscenter\ZeroPriceRestrictionConfig\Config\Db;

use Interactiv4\ConfigHelper\Api\Config\Db\DbConfigHelperInterface;
use Interactiv4\ConfigHelper\Api\Config\Db\Provider\DbConfigGroupPathProviderTrait;
use Interactiv4\ConfigHelper\Api\Config\Db\Provider\DbConfigSectionPathProviderTrait;
use Interactiv4\ConfigHelper\Model\Config\Db\DbConfigHelper;
use Logiscenter\ZeroPriceRestrictionConfig\Api\Config\Db\Provider\ZeroPriceRestrictionGroupPathProviderInterface;
use Logiscenter\ZeroPriceRestrictionConfig\Api\Config\Db\Provider\ZeroPriceRestrictionSectionPathProviderInterface;

/**
 * Class DbZeroPriceRestrictionConfig.
 *
 * This is not an api class, implementation may change at any time.
 * @see \Logiscenter\ZeroPriceRestrictionConfig\Api\Config\ZeroPriceRestrictionConfigInterface,
 * use this to retrieve config.
 */
class DbZeroPriceRestrictionConfig extends DbConfigHelper implements
    ZeroPriceRestrictionSectionPathProviderInterface,
    ZeroPriceRestrictionGroupPathProviderInterface,
    DbConfigHelperInterface
{
    use DbConfigSectionPathProviderTrait;
    use DbConfigGroupPathProviderTrait;
}
