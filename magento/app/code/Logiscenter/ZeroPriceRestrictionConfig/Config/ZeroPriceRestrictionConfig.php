<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Logiscenter\ZeroPriceRestrictionConfig\Config;

use Logiscenter\ZeroPriceRestrictionConfig\Api\Config\ZeroPriceRestrictionConfigInterface;
use Logiscenter\ZeroPriceRestrictionConfig\Config\Db\DbZeroPriceRestrictionConfig;

/**
 * Class ZeroPriceRestrictionConfig.
 *
 * This is not an api class, implementation may change at any time.
 * @see ZeroPriceRestrictionConfigInterface, use this to retrieve config.
 *
 * For example, it now relies on DbZeroPriceRestrictionConfig to read information from database,
 * but this implementation can be changed when needed, without prior notice.
 */
class ZeroPriceRestrictionConfig extends DbZeroPriceRestrictionConfig implements
    ZeroPriceRestrictionConfigInterface
{
}
