<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Logiscenter\ZeroPriceRestrictionConfig\Api\Config\Db\Provider;

use Interactiv4\ConfigHelper\Api\Config\Db\Provider\DbConfigSectionPathProviderInterface;

/**
 * Interface ZeroPriceRestrictionSectionPathProviderInterface.
 *
 * Section path provider for logiscenter_zeropricerestrictionconfig section.
 *
 * @api
 */
interface ZeroPriceRestrictionSectionPathProviderInterface extends DbConfigSectionPathProviderInterface
{
    public const SECTION_PATH = 'logiscenter_zeropricerestrictionconfig';
}
