<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Logiscenter\ZeroPriceRestrictionConfig\Api\Config;

use Interactiv4\Contracts\SPL\BooleanStatus\Api\BooleanStatusCompareInterface;
use Interactiv4\Contracts\SPL\BooleanStatus\Api\BooleanStatusPutInterface;
use Interactiv4\Contracts\SPL\BooleanStatus\Api\BooleanStatusReadInterface;

/**
 * Interface ZeroPriceRestrictionStatusConfigInterface.
 *
 * Status provider interface.
 *
 * @api
 */
interface ZeroPriceRestrictionStatusConfigInterface extends
    BooleanStatusReadInterface,
    BooleanStatusCompareInterface,
    BooleanStatusPutInterface
{
}
