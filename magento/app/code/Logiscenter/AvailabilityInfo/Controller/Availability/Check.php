<?php
declare(strict_types=1);
/**
 * @author    Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Logiscenter\AvailabilityInfo\Controller\Availability;

use Logiscenter\AvailabilityInfo\Model\Parameters\Checker;
use Logiscenter\AvailabilityInfoConfig\Config\AvailabilityInfoModuleStatus;
use Magento\Contact\Model\MailInterface;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Controller\Result\JsonFactory;

/**
 * Handle check availability requests
 */
class Check extends Action implements HttpPostActionInterface
{
    /** @var JsonFactory  */
    private $resultJsonFactory;

    /** @var AvailabilityInfoModuleStatus */
    private $moduleStatus;

    /** @var MailInterface */
    private $mail;

    /** @var Checker */
    private $paramsChecker;

    /**
     * @param Context                      $context
     * @param JsonFactory                  $resultJsonFactory
     * @param AvailabilityInfoModuleStatus $moduleStatus
     * @param MailInterface                $mail
     * @param Checker                      $paramsChecker
     */
    public function __construct(
        Context $context,
        JsonFactory $resultJsonFactory,
        AvailabilityInfoModuleStatus $moduleStatus,
        MailInterface $mail,
        Checker $paramsChecker
    ) {
        parent::__construct($context);

        $this->resultJsonFactory = $resultJsonFactory;
        $this->moduleStatus = $moduleStatus;
        $this->mail = $mail;
        $this->paramsChecker = $paramsChecker;
    }

    /**
     * @inheritdoc
     */
    public function execute()
    {
        if (!$this->canExecute()) {
            return null;
        }

        $result = [
            'success' => true,
            'message'   => (string)__('Thank you for your request. We\'ll respond to you very soon.'),
        ];
        $params = $this->getRequest()->getParams();

        try {
            $this->paramsChecker->checkParams($params);
            $this->mail->send($params['email'], ['data' => $params]);
        } catch (\Exception $e) {
            $result = [
                'success' => false,
                'message' => $e->getMessage(),
            ];
        }

        return $this->resultJsonFactory->create()->setData($result);
    }

    /**
     * @return bool
     */
    private function canExecute(): bool
    {
        return $this->moduleStatus->isEnabled() && $this->getRequest()->isPost() && $this->getRequest()->isAjax();
    }
}
