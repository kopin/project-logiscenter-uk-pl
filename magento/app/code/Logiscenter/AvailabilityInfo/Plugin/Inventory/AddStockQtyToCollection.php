<?php

/**
 * @author    Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\AvailabilityInfo\Plugin\Inventory;

use Magento\Catalog\Model\ResourceModel\Product\Collection;

/**
 * Add stock qty to the product collection
 */
class AddStockQtyToCollection
{
    /**
     * Add stock qty to collection.
     *
     * @param Collection $productCollection
     * @param bool       $printQuery
     * @param bool       $logQuery
     *
     * @return array
     */
    public function beforeLoad(Collection $productCollection, $printQuery = false, $logQuery = false)
    {
        $select = $productCollection->getSelect();
        if (isset($select->getPart('from')['stock_status_index'])) {
            $select->columns(['stock_qty' => 'stock_status_index.qty']);
        }

        return [$printQuery, $logQuery];
    }
}
