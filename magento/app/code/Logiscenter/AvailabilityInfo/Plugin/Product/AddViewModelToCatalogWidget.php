<?php

/**
 * @author    Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\AvailabilityInfo\Plugin\Product;

use Logiscenter\AvailabilityInfo\ViewModel\AvailabilityData;
use Magento\CatalogWidget\Block\Product\ProductsList;

/**
 * Add availability modelview to the product widget
 */
class AddViewModelToCatalogWidget
{
    /**
     * @var AvailabilityData
     */
    private $availabilityData;

    /**
     *  Initialize viewmodel
     */
    public function __construct(AvailabilityData $availabilityData)
    {
        $this->availabilityData = $availabilityData;
    }

    /**
     * Add viewmodel to widget
     *
     * @param ProductsList $productsList
     */
    public function beforeToHtml(ProductsList $productsList)
    {
        $productsList->setData('view_model_availability_info', $this->availabilityData);
    }
}
