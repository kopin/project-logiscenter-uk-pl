<?php
declare(strict_types=1);
/**
 * @author    Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */
namespace Logiscenter\AvailabilityInfo\ViewModel;

use Logiscenter\AvailabilityInfo\Api\AvailabilityInfoProviderInterface;
use Logiscenter\AvailabilityInfo\Api\GetAvailabilityInfoDataInterface;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Framework\View\Element\Block\ArgumentInterface;

/**
 * View model to provide availability data
 */
class AvailabilityData implements ArgumentInterface
{
    /**
     * @var GetAvailabilityInfoDataInterface
     */
    private $getAvailabilityInfoData;

    /**
     * @param GetAvailabilityInfoDataInterface $getAvailabilityInfoData
     */
    public function __construct(
        GetAvailabilityInfoDataInterface $getAvailabilityInfoData
    ) {
        $this->getAvailabilityInfoData = $getAvailabilityInfoData;
    }

    /**
     * Return availability message
     *
     * @param ProductInterface $product
     * @param bool             $isListing
     *
     * @return string
     */
    public function getAvailabilityMessage(ProductInterface $product, bool $isListing = false): string
    {
        $data = $this->getAvailabilityInfoData->execute($product, $isListing);

        return $data[AvailabilityInfoProviderInterface::KEY_AVAILABILITY_MESSAGE] ?? '';
    }

    /**
     * Return availability tooltip text
     *
     * @param ProductInterface $product
     * @param bool             $isListing
     *
     * @return string
     */
    public function getTooltipText(ProductInterface $product, bool $isListing = false): string
    {
        $data = $this->getAvailabilityInfoData->execute($product, $isListing);

        return $data[AvailabilityInfoProviderInterface::KEY_TOOLTIP_TEXT] ?? '';
    }

    /**
     * Return availability message type
     *
     * @param ProductInterface $product
     * @param bool             $isListing
     *
     * @return string
     */
    public function getAvailabilityMessageType(ProductInterface $product, bool $isListing = false): string
    {
        $data = $this->getAvailabilityInfoData->execute($product, $isListing);

        return $data[AvailabilityInfoProviderInterface::KEY_MESSAGE_TYPE] ?? '';
    }

    /**
     * Checks if can show check availability popup
     *
     * @param ProductInterface $product
     * @param bool             $isListing
     *
     * @return bool
     */
    public function canShowAvailabilityPopup(ProductInterface $product, bool $isListing = false): bool
    {
        $data = $this->getAvailabilityInfoData->execute($product, $isListing);

        return $data[AvailabilityInfoProviderInterface::KEY_CAN_SHOW_CHECK_AVAILABILITY_MODAL] ?? false;
    }
}
