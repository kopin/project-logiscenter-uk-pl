<?php
declare(strict_types=1);
/**
 * @author    Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Logiscenter\AvailabilityInfo\Ui\DataProvider\Product\Listing\Collector;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\Data\ProductRenderExtensionFactory;
use Magento\Catalog\Api\Data\ProductRenderInterface;
use Magento\Catalog\Ui\DataProvider\Product\ProductRenderCollectorInterface;
use Logiscenter\AvailabilityInfo\Api\AvailabilityInfoProviderInterface;
use Magento\Framework\Json\Helper\Data as JsonHelper;

/**
 * Get availability info data for extension attributes
 */
class AvailabilityInfo implements ProductRenderCollectorInterface
{
    /**
     * @var AvailabilityInfoProviderInterface
     */
    private $availabilityInfo;

    /**
     * @var ProductRenderExtensionFactory
     */
    private $productRenderExtensionFactory;

    /**
     * @param AvailabilityInfoProviderInterface     $availabilityInfo
     * @param ProductRenderExtensionFactory         $productRenderExtensionFactory
     * @param JsonHelper                            $jsonHelper
     */
    public function __construct(
        AvailabilityInfoProviderInterface $availabilityInfo,
        ProductRenderExtensionFactory $productRenderExtensionFactory,
        JsonHelper $jsonHelper
    ) {
        $this->availabilityInfo = $availabilityInfo;
        $this->productRenderExtensionFactory = $productRenderExtensionFactory;
        $this->jsonHelper = $jsonHelper;
    }

    /**
     * @inheritdoc
     */
    public function collect(ProductInterface $product, ProductRenderInterface $productRender)
    {
        /** @var ProductRenderExtensionInterface $extensionAttributes */
        $extensionAttributes = $productRender->getExtensionAttributes();

        if (!$extensionAttributes) {
            $extensionAttributes = $this->productRenderExtensionFactory->create();
        }

        $isListing = true;
        if ($availabilityInfo = $this->availabilityInfo->getAvailabilityInfo($product, $isListing)) {
            $extensionAttributes->setAvailabilityInfo($this->jsonHelper->jsonEncode($availabilityInfo));
        }

        if ($product->getSku()) {
            $extensionAttributes->setSku($product->getSku());
        }

        $productRender->setExtensionAttributes($extensionAttributes);
    }
}
