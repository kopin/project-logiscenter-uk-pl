/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'Magento_Ui/js/grid/columns/column',
    'Magento_Catalog/js/product/list/column-status-validator'
], function (Column, columnStatusValidator) {
    'use strict';

    return Column.extend({
        /**
         * @param row
         * @returns {boolean}
         */
        hasAvailabilityInfo: function (row) {
            return "availability_info" in row['extension_attributes'];
        },

        /**
         * @param row
         * @returns {boolean}
         */
        hasSku: function (row) {
            return "sku" in row['extension_attributes'];
        },

        /**
         * @param row
         * @returns {*}
         */
        getAvailabilityInfo: function (row) {
            return JSON.parse(row['extension_attributes']['availability_info']);
        },

        /**
         * @param row
         * @returns {*}
         */
        getSku: function (row) {
            return row['extension_attributes']['sku'];
        },

        /**
         * @param row
         * @returns {*}
         */
        getAvailabilityMessage: function (row) {
            return this.getAvailabilityInfo(row)['availability_message'];
        },

        /**
         * @param row
         * @returns {*}
         */
        getAvailabilityMessageType: function (row) {
            return this.getAvailabilityInfo(row)['message_type'];
        },

        /**
         * @param row
         * @returns {*}
         */
        getUrl: function (row) {
            return row['url'];
        },

        /**
         * @param row
         * @returns {*}
         */
        canShowAvailabilityPopup: function (row) {
            return this.getAvailabilityInfo(row)['allow_check_availability'];
        },

        /**
         * @param row
         * @returns {*|boolean}
         */
        isAllowed: function (row) {
            return (columnStatusValidator.isValid(this.source(), 'availability_info', 'show_attributes') && this.hasAvailabilityInfo(row) );
        }
    });
});
