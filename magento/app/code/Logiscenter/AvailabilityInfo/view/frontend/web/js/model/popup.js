/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'jquery',
    'ko',
    'Magento_Ui/js/modal/modal',
    'mage/url',
    'mage/translate'
], function ($, ko, modal, urlBuilder, $t) {
    'use strict';

    return {
        modalWindow: null,
        isLoading: ko.observable(false),
        formSelector: '#block-availability-info form',

        /**
         * Create popUp window for provided element.
         *
         * @param {HTMLElement} element
         */
        createModal: function (element) {
            let options, self = this;

            this.modalWindow = element;
            options = {
                'type': 'popup',
                'modalClass': 'modal-availability-info',
                'responsive': true,
                'innerScroll': true,
                'trigger': '.show-modal',
                'buttons': []
            };
            modal(options, $(this.modalWindow));
        },

        /**
         *  Show availability popup window
         */
        showModal: function () {
            $(this.modalWindow).modal('openModal');
        },

        /**
         *  Close availability popup window
         */
        closeModal: function () {
            $(this.modalWindow).modal('closeModal');
            $(this.formSelector).trigger('reset');
            $(this.formSelector).find('.message').remove();
        },

        /**
         * Bind the part number to the form
         *
         * @param {string} sku
         */
        attachSku: function (sku) {
            $(this.modalWindow).find('.field.field-sku input').val(sku);
        }
    };
});
