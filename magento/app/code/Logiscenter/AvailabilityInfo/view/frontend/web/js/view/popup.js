/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */
define([
    'jquery',
    'Magento_Ui/js/form/form',
    'Magento_Ui/js/modal/modal',
    'Logiscenter_AvailabilityInfo/js/model/popup',
    'mage/translate',
    'mage/validation',
    'domReady!'
], function (
    $,
    Component,
    modal,
    modelPopup,
    $t
) {
    'use strict';

    return {
        checkAvailabilitySelector: '[data-event="check-availability"]',
        blockSelector: '#block-availability-info',
        modalWindow: null,

        createModal: function() {
            alert('created');
        },

        /** @inheritdoc */
        initialize: function () {
            if ($(this.blockSelector).length > 0) {
                modelPopup.createModal(this.blockSelector);
                this.bindCustomEvents();
            }
        },

        /**
         * This method is used to bind events associated with this widget.
         */
        bindCustomEvents: function () {
            let self = this;
            // Attach a delegated event handler because these elements are added by ajax
            $(document).on('click', this.checkAvailabilitySelector, function(e) {
                let button = e.target,
                    sku = $(button).data('sku');
                e.preventDefault();

                modelPopup.attachSku(sku);
                modelPopup.showModal();
            });
        }
    };
});
