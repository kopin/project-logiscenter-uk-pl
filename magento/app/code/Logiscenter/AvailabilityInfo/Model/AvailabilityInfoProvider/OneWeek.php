<?php
declare(strict_types=1);
/**
 * @author    Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Logiscenter\AvailabilityInfo\Model\AvailabilityInfoProvider;

use Logiscenter\AvailabilityInfoConfig\Api\Config\AvailabilityInfoConfigInterface;
use Magento\Catalog\Api\Data\ProductInterface;
use Logiscenter\AvailabilityInfo\Model\AvailabilityInfoAbstract;

/**
 * One week availability info checker class
 */
class OneWeek extends AvailabilityInfoAbstract
{
    /**
     * @inheritDoc
     */
    protected const AVAILABILITY_MESSAGE = 'Available in one week (or more)';

    /**
     * @inheritDoc
     */
    protected const TOOLTIP_TEXT_CODE = AvailabilityInfoConfigInterface::ONE_WEEK;

    /**
     * @inheritDoc
     */
    protected const AVAILABILITY_MESSAGE_TYPE = 'no-stock';

    /**
     * @inheritDoc
     */
    protected function shouldProcess(ProductInterface $product, bool $isListing = false): bool
    {
        return !$isListing &&
            !$this->isGroupedProduct($product) &&
            $this->getStockQty($product) === 0 &&
            !$this->isFreeShipping($product) &&
            $this->hasPrice($product);
    }
}
