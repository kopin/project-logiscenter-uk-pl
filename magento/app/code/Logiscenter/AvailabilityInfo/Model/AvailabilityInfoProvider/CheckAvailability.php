<?php
declare(strict_types=1);
/**
 * @author    Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Logiscenter\AvailabilityInfo\Model\AvailabilityInfoProvider;

use Logiscenter\AvailabilityInfo\Model\AvailabilityInfoAbstract;
use Logiscenter\AvailabilityInfoConfig\Api\Config\AvailabilityInfoConfigInterface;
use Magento\Catalog\Api\Data\ProductInterface;

/**
 * Out of stock availability checker
 */
class CheckAvailability extends AvailabilityInfoAbstract
{
    /**
     * @inheritDoc
     */
    protected const AVAILABILITY_MESSAGE = 'Check Availability';

    /**
     * @inheritDoc
     */
    protected const TOOLTIP_TEXT_CODE = AvailabilityInfoConfigInterface::CHECK_AVAILABILITY;

    /**
     * @inheritDoc
     */
    protected const CAN_SHOW_CHECK_AVAILABILITY_MODAL = true;

    /**
     * @inheritDoc
     */
    protected const AVAILABILITY_MESSAGE_TYPE = 'action';

    /**
     * @inheritDoc
     */
    protected function shouldProcess(ProductInterface $product, bool $isListing = false): bool
    {
        return (!$isListing && $this->canShowOnPDP($product)) || ($isListing && $this->canShowOnListing($product));
    }

    /**
     * Check show conditions for PDP
     *
     * @param ProductInterface $product
     *
     * @return bool
     */
    private function canShowOnPDP(ProductInterface $product): bool
    {
        return !$this->hasPrice($product);
    }

    /**
     * Check show conditions for listing
     *
     * @param ProductInterface $product
     *
     * @return bool
     */
    private function canShowOnListing(ProductInterface $product): bool
    {
        return $this->isSimpleProduct($product) && (!$this->hasPrice($product) || $product->getData('descatalogado'));
    }
}
