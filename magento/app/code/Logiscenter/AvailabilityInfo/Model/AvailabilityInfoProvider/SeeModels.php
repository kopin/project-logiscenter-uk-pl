<?php
declare(strict_types=1);
/**
 * @author    Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Logiscenter\AvailabilityInfo\Model\AvailabilityInfoProvider;

use Logiscenter\AvailabilityInfo\Model\AvailabilityInfoAbstract;
use Magento\Catalog\Api\Data\ProductInterface;

/**
 * See models availability checker
 */
class SeeModels extends AvailabilityInfoAbstract
{
    /**
     * @inheritDoc
     */
    protected const AVAILABILITY_MESSAGE = 'See models';

    /**
     * @inheritDoc
     */
    protected const AVAILABILITY_MESSAGE_TYPE = 'action';

    /**
     * @inheritDoc
     */
    protected function shouldProcess(ProductInterface $product, bool $isListing = false): bool
    {
        return $isListing && $this->isGroupedProduct($product) && !$this->hasPrice($product);
    }
}
