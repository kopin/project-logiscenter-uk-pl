<?php
declare(strict_types=1);
/**
 * @author    Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Logiscenter\AvailabilityInfo\Model\AvailabilityInfoProvider;

use Logiscenter\AvailabilityInfoConfig\Api\Config\AvailabilityInfoConfigInterface;
use Magento\Catalog\Api\Data\ProductInterface;
use Logiscenter\AvailabilityInfo\Model\AvailabilityInfoAbstract;

/**
 * Ship today availability info checker class
 */
class ShipToday extends AvailabilityInfoAbstract
{
    /**
     * @inheritDoc
     */
    protected const AVAILABILITY_MESSAGE = 'In Stock Ships Today!';

    /**
     * @inheritDoc
     */
    protected const TOOLTIP_TEXT_CODE = AvailabilityInfoConfigInterface::SHIPS_TODAY;

    /**
     * @inheritDoc
     */
    protected const AVAILABILITY_MESSAGE_TYPE = 'in-stock';

    /**
     * @inheritDoc
     */
    protected function shouldProcess(ProductInterface $product, bool $isListing = false): bool
    {
        return !$isListing &&
            !$this->isGroupedProduct($product) &&
            $this->getStockQty($product) > 0 &&
            !$this->isFreeShipping($product) &&
            $this->hasPrice($product);
    }
}
