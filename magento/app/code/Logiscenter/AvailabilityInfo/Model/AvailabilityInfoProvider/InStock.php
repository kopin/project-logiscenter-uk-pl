<?php
declare(strict_types=1);
/**
 * @author    Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Logiscenter\AvailabilityInfo\Model\AvailabilityInfoProvider;

use Magento\Catalog\Api\Data\ProductInterface;
use Logiscenter\AvailabilityInfo\Model\AvailabilityInfoAbstract;

/**
 * In stock availability checker
 */
class InStock extends AvailabilityInfoAbstract
{
    /**
     * @inheritDoc
     */
    protected const AVAILABILITY_MESSAGE = 'In Stock';

    /**
     * @inheritDoc
     */
    protected const AVAILABILITY_MESSAGE_TYPE = 'in-stock';

    /**
     * @inheritdoc
     */
    protected function shouldProcess(ProductInterface $product, bool $isListing = false): bool
    {
        return ($isListing &&
            ($this->canShowForGroupedOnListing($product) || $this->canShowForSimpleOnListing($product)));
    }

    /**
     * Check show conditions for grouped product
     *
     * @param ProductInterface $product
     *
     * @return bool
     */
    private function canShowForGroupedOnListing(ProductInterface $product): bool
    {
        return ($this->isGroupedProduct($product) &&
            $this->hasPrice($product) &&
            (int)$product->getData('grouped_qty') > 0);
    }

    /**
     * Check show conditions for simple product
     *
     * @param ProductInterface $product
     *
     * @return bool
     */
    private function canShowForSimpleOnListing(ProductInterface $product): bool
    {
        return $this->isSimpleProduct($product) && $this->getStockQty($product) > 0 && $this->hasPrice($product);
    }
}
