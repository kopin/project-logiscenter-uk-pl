<?php
declare(strict_types=1);
/**
 * @author    Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Logiscenter\AvailabilityInfo\Model;

use Logiscenter\AvailabilityInfo\Api\AvailabilityInfoProviderInterface;
use Logiscenter\AvailabilityInfoConfig\Api\Config\AvailabilityInfoConfigInterface;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Model\Product\Type;
use Magento\Catalog\Pricing\Price\FinalPrice;
use Magento\GroupedProduct\Model\Product\Type\Grouped;
use Magento\OfflineShipping\Model\Carrier\Freeshipping;

/**
 * AvailabilityInfoAbstract
 */
abstract class AvailabilityInfoAbstract implements AvailabilityInfoProviderInterface
{
    /**
     * Availability message text to show
     */
    protected const AVAILABILITY_MESSAGE = '';

    /**
     * Availability tooltip text to show
     */
    protected const TOOLTIP_TEXT_CODE = '';

    /**
     * Flag fo availability model can be shown
     */
    protected const CAN_SHOW_CHECK_AVAILABILITY_MODAL = false;

    /**
     * Availability message type
     */
    protected const AVAILABILITY_MESSAGE_TYPE = '';

    /**
     * @var AvailabilityInfoConfigInterface
     */
    private $availabilityInfoConfig;

    /**
     * @var Freeshipping
     */
    private $freeShipping;

    /**
     * @param Freeshipping                    $freeShipping
     * @param AvailabilityInfoConfigInterface $availabilityInfoConfig
     */
    public function __construct(
        Freeshipping $freeShipping,
        AvailabilityInfoConfigInterface $availabilityInfoConfig
    ) {
        $this->availabilityInfoConfig = $availabilityInfoConfig;
        $this->freeShipping = $freeShipping;
    }

    /**
     * @param ProductInterface $product
     * @param bool             $isListing
     *
     * @return bool
     */
    abstract protected function shouldProcess(ProductInterface $product, bool $isListing = false): bool;

    /**
     * @return array
     */
    protected function getInfoData(): array
    {
        return [
            self::KEY_AVAILABILITY_MESSAGE              => __(static::AVAILABILITY_MESSAGE)->render(),
            self::KEY_TOOLTIP_TEXT                      => $this->getTooltipText(static::TOOLTIP_TEXT_CODE),
            self::KEY_CAN_SHOW_CHECK_AVAILABILITY_MODAL => static::CAN_SHOW_CHECK_AVAILABILITY_MODAL,
            self::KEY_MESSAGE_TYPE                      => static::AVAILABILITY_MESSAGE_TYPE,
        ];
    }

    /**
     *
     * @param ProductInterface $product
     * @param bool             $isListing
     *
     * @return array
     */
    public function getAvailabilityInfo(ProductInterface $product, bool $isListing = false): array
    {
        if ($this->shouldProcess($product, $isListing)) {
            return $this->getInfoData();
        }

        return [];
    }

    /**
     * Get the stock qty.
     *
     * @param ProductInterface $product
     *
     * @return int
     */
    protected function getStockQty(ProductInterface $product): int
    {
        /**
         * stock_qty will only defined for associated child of grouped
         * (See Logiscenter\GroupedModels\Plugin\Model\Product\Type\Grouped\AddExtraInfoPlugin)
         * and related products (See Logiscenter\GroupedRelated\Model\Product\Related)
         */
        if ($product->hasData('stock_qty')) {
            return (int)$product->getData('stock_qty');
        }

        $stockItem = $product->getExtensionAttributes()->getStockItem();
        if ($stockItem) {
            return (int)$stockItem->getQty();
        }

        return 0;
    }

    /**
     * @param ProductInterface $product
     *
     * @return bool
     */
    protected function isFreeShipping(ProductInterface $product): bool
    {
        $freeShippingPrice = (float)$this->freeShipping->getConfigData('free_shipping_subtotal');
        $finalPrice = $product->getPriceInfo()->getPrice(FinalPrice::PRICE_CODE)->getValue();
        return (float)$finalPrice >= $freeShippingPrice;
    }

    /**
     * @param string|null $code
     * @param null        $storeId
     *
     * @return string
     */
    protected function getTooltipText(?string $code = null, $storeId = null): string
    {
        return $this->availabilityInfoConfig->getTooltipText($code, $storeId);
    }

    /**
     * @param ProductInterface $product
     *
     * @return bool
     */
    protected function isSimpleProduct(ProductInterface $product): bool
    {
        return $product->getTypeId() === Type::TYPE_SIMPLE;
    }

    /**
     * @param ProductInterface $product
     *
     * @return bool
     */
    protected function isGroupedProduct(ProductInterface $product): bool
    {
        return $product->getTypeId() === Grouped::TYPE_CODE;
    }

    /**
     * @param $product
     *
     * @return bool
     */
    protected function hasPrice($product): bool
    {
        $price = $product->getPriceInfo()->getPrice(FinalPrice::PRICE_CODE)->getValue();
        return $price > 0;
    }
}
