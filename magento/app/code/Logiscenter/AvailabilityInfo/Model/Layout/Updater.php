<?php
declare(strict_types=1);

namespace Logiscenter\AvailabilityInfo\Model\Layout;

use Amasty\Mostviewed\Block\Widget\Wrapper;
use Amasty\Mostviewed\Model\Layout\Updater as AmastyUpdater;
use Amasty\Mostviewed\Model\OptionSource\BlockPosition;

/**
 * Override of original updater to replace template
 */
class Updater extends AmastyUpdater
{
    const CONTENT_TEMPLATE = 'Logiscenter_AvailabilityInfo::content/grid.phtml';

    /**
     * @inheritDoc
     */
    public function generateLayoutUpdateXml($position)
    {
        $nameInLayout = 'amrelated_' . $position;
        $xml = '<body><referenceContainer name="' . $this->getContainerByPosition($position) . '">';
        $xml .= '<block class="' . Wrapper::class .
            '" name="' . $nameInLayout .
            '" template="' . $this->getTemplateByPosition($position) . '" ';
        if ($positionAttribute = $this->getPositionAttribute($position)) {
            $xml .= $positionAttribute;
        }
        $xml .= '>';
        // @codingStandardsIgnoreStart
        $xml .= '<action method="setData">' .
            '<argument name="name" xsi:type="string">position</argument>' .
            '<argument name="value" xsi:type="string">' . $position . '</argument></action>';
        $xml .= '</block></referenceContainer>';
        // @codingStandardsIgnoreEnd
        if ($move = $this->getMoveElement($position, $nameInLayout)) {
            $xml .= $move;
        }
        $xml .= '</body>';

        return $xml;
    }

    /**
     * @param $position
     *
     * @return string
     */
    private function getTemplateByPosition($position)
    {
        switch ($position) {
            case BlockPosition::CATEGORY_SIDEBAR_BOTTOM:
            case BlockPosition::CATEGORY_SIDEBAR_TOP:
            case BlockPosition::PRODUCT_SIDEBAR_BOTTOM:
            case BlockPosition::PRODUCT_SIDEBAR_TOP:
                $template = self::SIDEBAR_TEMPLATE;
                break;
            default:
                $template = self::CONTENT_TEMPLATE;
        }

        return $template;
    }

    /**
     * @param string $position
     *
     * @return string
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    private function getPositionAttribute($position)
    {
        switch ($position) {
            case BlockPosition::PRODUCT_AFTER_UPSELL:
                $positionAttribute = 'after="' . self::UPSELL_NAME . '"';
                break;
            case BlockPosition::PRODUCT_AFTER_RELATED:
                $positionAttribute = 'after="' . self::RELATED_NAME . '"';
                break;
            case BlockPosition::CART_AFTER_CROSSSEL:
            case BlockPosition::CART_BEFORE_CROSSSEL:
                $positionAttribute = '';
                break;
            case BlockPosition::PRODUCT_BEFORE_RELATED:
                $positionAttribute = 'before="' . self::RELATED_NAME . '"';
                break;
            case BlockPosition::PRODUCT_BEFORE_TAB:
                $positionAttribute = 'before="' . self::TAB_NAME . '"';
                break;
            case BlockPosition::PRODUCT_CONTENT_TAB:
                $positionAttribute = 'group="detailed_info"';
                break;
            case BlockPosition::PRODUCT_BEFORE_UPSELL:
                $positionAttribute = 'before="' . self::UPSELL_NAME . '"';
                break;
            case BlockPosition::CATEGORY_SIDEBAR_BOTTOM:
            case BlockPosition::PRODUCT_SIDEBAR_BOTTOM:
            case BlockPosition::PRODUCT_CONTENT_BOTTOM:
            case BlockPosition::CATEGORY_CONTENT_BOTTOM:
            case BlockPosition::CART_CONTENT_BOTTOM:
                $positionAttribute = 'after="-"';
                break;
            case BlockPosition::CART_CONTENT_TOP:
            case BlockPosition::CATEGORY_CONTENT_TOP:
            case BlockPosition::CATEGORY_SIDEBAR_TOP:
            case BlockPosition::PRODUCT_SIDEBAR_TOP:
            case BlockPosition::PRODUCT_CONTENT_TOP:
                $positionAttribute = 'before="-"';
                break;
            default:
                $positionAttribute = '';
        }

        return $positionAttribute;
    }

    /**
     * @param string $position
     * @param string $name
     *
     * @return string
     */
    private function getMoveElement($position, $name)
    {
        switch ($position) {
            case BlockPosition::CART_AFTER_CROSSSEL:
                $move = ' <move element="' . $name
                    . '" destination="checkout.cart.container" after="' . self::CROSSEL_NAME . '"/>';
                break;
            case BlockPosition::CART_BEFORE_CROSSSEL:
                $move = ' <move element="' . $name
                    . '" destination="checkout.cart.container" before="' . self::CROSSEL_NAME . '"/>';
                break;
            default:
                $move = '';
        }

        return $move;
    }

    /**
     * @param $position
     *
     * @return string
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    private function getContainerByPosition($position)
    {
        switch ($position) {
            case BlockPosition::PRODUCT_CONTENT_TOP:
            case BlockPosition::CART_CONTENT_TOP:
            case BlockPosition::CATEGORY_CONTENT_TOP:
                $container = 'content.top';
                break;
            case BlockPosition::CART_CONTENT_BOTTOM:
            case BlockPosition::CATEGORY_CONTENT_BOTTOM:
            case BlockPosition::PRODUCT_CONTENT_BOTTOM:
                $container = 'content.bottom';
                break;
            case BlockPosition::CATEGORY_SIDEBAR_BOTTOM:
            case BlockPosition::PRODUCT_SIDEBAR_BOTTOM:
                $container = 'sidebar.additional';
                break;
            case BlockPosition::CATEGORY_SIDEBAR_TOP:
            case BlockPosition::PRODUCT_SIDEBAR_TOP:
                $container = 'sidebar.main';
                break;
            case BlockPosition::PRODUCT_AFTER_RELATED:
            case BlockPosition::PRODUCT_BEFORE_RELATED:
            case BlockPosition::PRODUCT_AFTER_UPSELL:
            case BlockPosition::PRODUCT_BEFORE_UPSELL:
                $container = 'content.aside';
                break;
            case BlockPosition::CART_AFTER_CROSSSEL:
            case BlockPosition::CART_BEFORE_CROSSSEL:
            case BlockPosition::PRODUCT_BEFORE_TAB:
                $container = 'content';
                break;
            case BlockPosition::PRODUCT_CONTENT_TAB:
                $container = self::TAB_NAME;
                break;
            default:
                $container = '';
        }

        return $container;
    }
}
