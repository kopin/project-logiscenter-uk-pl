<?php
declare(strict_types=1);
/**
 * @author    Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Logiscenter\AvailabilityInfo\Model;

use Logiscenter\AvailabilityInfo\Api\AvailabilityInfoProviderInterface;
use Logiscenter\AvailabilityInfoConfig\Config\AvailabilityInfoModuleStatus;
use Magento\Catalog\Api\Data\ProductInterface;

/**
 * Composite availability info checker class
 */
class AvailabilityInfoProvider implements AvailabilityInfoProviderInterface
{
    /**
     * @var AvailabilityInfoProviderInterface[]
     */
    private $availabilityInfoChecker;

    /**
     * @var AvailabilityInfoModuleStatus
     */
    private $moduleStatus;

    /**
     * AvailabilityInfoProvider constructor.
     *
     * @param AvailabilityInfoModuleStatus        $moduleStatus
     * @param AvailabilityInfoProviderInterface[] $availabilityInfoCheckers
     */
    public function __construct(
        AvailabilityInfoModuleStatus $moduleStatus,
        array $availabilityInfoCheckers = []
    ) {
        $this->availabilityInfoChecker = $availabilityInfoCheckers;
        $this->moduleStatus = $moduleStatus;
    }

    /**
     * @inheritdoc
     */
    public function getAvailabilityInfo(ProductInterface $product, bool $isListing = false): array
    {
        if (!$this->moduleStatus->isEnabled()) {
            return [];
        }
        foreach ($this->availabilityInfoChecker as $checker) {
            if (!$checker instanceof AvailabilityInfoProviderInterface) {
                throw new \LogicException('Invalid availability checker specified!');
            }
            $info = $checker->getAvailabilityInfo($product, $isListing);
            if ($info) {
                return $info;
            }
        }

        return [];
    }
}
