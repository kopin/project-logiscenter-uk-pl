<?php
declare(strict_types=1);
/**
 * @author    Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Logiscenter\AvailabilityInfo\Model\Parameters;

use Magento\Framework\Exception\LocalizedException;

/**
 * Checks if all required params are present
 */
class Checker
{
    /** @var array */
    protected $requiredParams = [
        'sku',
        'name',
        'email',
        'comment',
    ];

    /**
     * Checks that all required parameters are present
     *
     * @param array $params
     *
     * @throws LocalizedException
     */
    public function checkParams(array $params): void
    {
        $errorMsg = 'Required param %1 missed. Please fill all required fields and try again';

        foreach ($this->requiredParams as $requiredParam) {
            if (!isset($params[$requiredParam]) || empty(trim($params[$requiredParam]))) {
                throw new LocalizedException(__($errorMsg, $requiredParam));
            }
        }
    }
}
