<?php
declare(strict_types=1);
/**
 * @author    Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Logiscenter\AvailabilityInfo\Service;

use Logiscenter\AvailabilityInfo\Api\AvailabilityInfoProviderInterface;
use Logiscenter\AvailabilityInfo\Api\GetAvailabilityInfoDataInterface;
use Logiscenter\AvailabilityInfoConfig\Api\Config\AvailabilityInfoModuleStatusInterface;
use Magento\Catalog\Api\Data\ProductInterface;

/**
 * Get availability info data service class
 */
class GetAvailabilityInfoData implements GetAvailabilityInfoDataInterface
{
    /**
     * @var AvailabilityInfoProviderInterface
     */
    private $availabilityInfo;

    /**
     * @var AvailabilityInfoModuleStatusInterface
     */
    private $moduleStatus;

    /**
     * @var array
     */
    private $availabilityInfoData = [];

    /**
     * @param AvailabilityInfoProviderInterface     $availabilityInfo
     * @param AvailabilityInfoModuleStatusInterface $moduleStatus
     */
    public function __construct(
        AvailabilityInfoProviderInterface $availabilityInfo,
        AvailabilityInfoModuleStatusInterface $moduleStatus
    ) {
        $this->availabilityInfo = $availabilityInfo;
        $this->moduleStatus = $moduleStatus;
    }

    /**
     * @inheritdoc
     */
    public function execute(ProductInterface $product, bool $isListing = false): array
    {
        if (!$this->moduleStatus->isEnabled()) {
            return [];
        }
        $productId = (int)$product->getId();
        $pageKey = $isListing ? 'listing' : 'pdp';
        if (isset($this->availabilityInfoData[$pageKey][$productId])) {
            return $this->availabilityInfoData[$pageKey][$productId];
        }

        $this->availabilityInfoData[$pageKey][$productId] = $this->availabilityInfo->getAvailabilityInfo(
            $product,
            $isListing
        );

        return $this->availabilityInfoData[$pageKey][$productId] ?? [];
    }
}
