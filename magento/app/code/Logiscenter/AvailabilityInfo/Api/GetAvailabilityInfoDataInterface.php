<?php
declare(strict_types=1);
/**
 * @author    Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Logiscenter\AvailabilityInfo\Api;

use Magento\Catalog\Api\Data\ProductInterface;

/**
 * Retrieve product availability info service interface
 */
interface GetAvailabilityInfoDataInterface
{
    /**
     * Return array of availability info data
     *
     * @param ProductInterface $product
     * @param bool             $isListing
     *
     * @return array
     */
    public function execute(ProductInterface $product, bool $isListing = false): array;
}
