<?php
declare(strict_types=1);
/**
 * @author    Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Logiscenter\AvailabilityInfo\Api;

use Magento\Catalog\Api\Data\ProductInterface;

/**
 * Product availability info checker interface
 */
interface AvailabilityInfoProviderInterface
{
    public const KEY_AVAILABILITY_MESSAGE              = 'availability_message';
    public const KEY_TOOLTIP_TEXT                      = 'tooltip_text';
    public const KEY_CAN_SHOW_CHECK_AVAILABILITY_MODAL = 'allow_check_availability';
    public const KEY_MESSAGE_TYPE                      = 'message_type';

    /**
     * Return responding availability info text
     *
     * @param ProductInterface $product
     * @param bool             $isListing
     *
     * @return array
     */
    public function getAvailabilityInfo(ProductInterface $product, bool $isListing = false): array;
}
