<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Logiscenter\AvailabilityInfoConfig\Test\Unit\Plugin\Api\Status;

use Logiscenter\AvailabilityInfoConfig\Plugin\Api\Status\CheckAvailabilityInfoModuleStatusPlugin;
use Interactiv4\Framework\Test\Unit\Plugin\Api\Status\CheckBooleanStatusPluginTestTrait;
use PHPUnit\Framework\TestCase;

/**
 * Class CheckAvailabilityInfoModuleStatusPlugin.
 *
 * Test plugin that allows to intercept module status.
 *
 * @group config
 *
 * @internal
 */
class CheckAvailabilityInfoModuleStatusPluginTest extends TestCase
{
    use CheckBooleanStatusPluginTestTrait;

    /**
     * {@inheritdoc}
     */
    protected function getPluginClass(): string
    {
        return CheckAvailabilityInfoModuleStatusPlugin::class;
    }
}
