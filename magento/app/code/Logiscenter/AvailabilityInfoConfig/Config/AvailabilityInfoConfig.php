<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Logiscenter\AvailabilityInfoConfig\Config;

use Logiscenter\AvailabilityInfoConfig\Api\Config\AvailabilityInfoConfigInterface;
use Logiscenter\AvailabilityInfoConfig\Config\Db\DbAvailabilityInfoConfig;

/**
 * Class AvailabilityInfoConfig.
 *
 * This is not an api class, implementation may change at any time.
 *
 * @see AvailabilityInfoConfigInterface, use this to retrieve config.
 *
 * For example, it now relies on DbAvailabilityInfoConfig to read information from database,
 * but this implementation can be changed when needed, without prior notice.
 */
class AvailabilityInfoConfig extends DbAvailabilityInfoConfig implements AvailabilityInfoConfigInterface
{
    private const FREE_SHIPPING_PLACEHOLDER = '{freeShippingAmount}';

    /**
     * @inheritDoc
     */
    public function getTooltipText(?string $code = null, $storeId = null): string
    {
        $freeShippingAmount = $this->getMinimalFreeShippingOrderAmount($storeId);
        switch ($code) {
            case self::SHIPS_TODAY_FREE:
                $text = $this->getInStockTodayFree();
                break;
            case self::SHIPS_TODAY:
                $text = $this->getInStockToday();
                break;
            case self::ONE_WEEK:
                $text = $this->getOneWeek();
                break;
            case self::ONE_WEEK_FREE:
                $text = $this->getOneWeekFree();
                break;
            case self::CHECK_AVAILABILITY:
                $text = $this->getCheckAvailability();
                break;
            default:
                $text = '';
        }

        return str_replace(self::FREE_SHIPPING_PLACEHOLDER, $freeShippingAmount, $text);
    }
}
