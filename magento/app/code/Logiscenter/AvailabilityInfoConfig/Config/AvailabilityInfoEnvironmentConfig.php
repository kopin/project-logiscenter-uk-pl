<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Logiscenter\AvailabilityInfoConfig\Config;

use Logiscenter\AvailabilityInfoConfig\Api\Config\AvailabilityInfoEnvironmentConfigInterface;
use Logiscenter\AvailabilityInfoConfig\Config\Db\DbAvailabilityInfoEnvironmentConfig;

/**
 * Class AvailabilityInfoEnvironmentConfig.
 *
 * This is not an api class, implementation may change at any time.
 * @see AvailabilityInfoEnvironmentConfigInterface, use this to retrieve config.
 *
 * For example, it now relies on DbAvailabilityInfoEnvironmentConfig to read information from database,
 * but this implementation can be changed when needed, without prior notice.
 */
class AvailabilityInfoEnvironmentConfig extends DbAvailabilityInfoEnvironmentConfig implements
    AvailabilityInfoEnvironmentConfigInterface
{
}
