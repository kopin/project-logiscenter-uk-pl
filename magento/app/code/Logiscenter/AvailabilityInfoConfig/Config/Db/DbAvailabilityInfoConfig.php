<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Logiscenter\AvailabilityInfoConfig\Config\Db;

use Interactiv4\ConfigHelper\Api\Config\Db\DbConfigHelperInterface;
use Interactiv4\ConfigHelper\Api\Config\Db\Provider\DbConfigGroupPathProviderTrait;
use Interactiv4\ConfigHelper\Api\Config\Db\Provider\DbConfigSectionPathProviderTrait;
use Interactiv4\ConfigHelper\Model\Config\Db\DbConfigHelper;
use Logiscenter\AvailabilityInfoConfig\Api\Config\Db\Provider\AvailabilityInfoGroupPathProviderInterface;
use Logiscenter\AvailabilityInfoConfig\Api\Config\Db\Provider\AvailabilityInfoSectionPathProviderInterface;
use Magento\Store\Model\ScopeInterface;

/**
 * Class DbAvailabilityInfoConfig.
 *
 * This is not an api class, implementation may change at any time.
 *
 * @see \Logiscenter\AvailabilityInfoConfig\Api\Config\AvailabilityInfoConfigInterface,
 * use this to retrieve config.
 */
class DbAvailabilityInfoConfig extends DbConfigHelper implements
    AvailabilityInfoSectionPathProviderInterface,
    AvailabilityInfoGroupPathProviderInterface,
    DbConfigHelperInterface
{
    use DbConfigSectionPathProviderTrait;
    use DbConfigGroupPathProviderTrait;

    private const XML_FIELD_IN_STOCK_TODAY_FREE = 'in_stock_today_free';

    private const XML_FIELD_IN_STOCK_TODAY = 'in_stock_today';

    private const XML_FIELD_ONE_WEEK = 'one_week';

    private const XML_FIELD_ONE_WEEK_FREE = 'one_week_free';

    private const XML_FIELD_CHECK_AVAILABILITY = 'check_availability';

    /**
     * @param int|string|null $storeId
     *
     * @return string
     */
    public function getInStockTodayFree($storeId = null): string
    {
        return (string)$this->getConfig(self::XML_FIELD_IN_STOCK_TODAY_FREE, $storeId);
    }

    /**
     * @param int|string|null $storeId
     *
     * @return string
     */
    public function getInStockToday($storeId = null): string
    {
        return (string)$this->getConfig(self::XML_FIELD_IN_STOCK_TODAY, $storeId);
    }

    /**
     * @param int|string|null $storeId
     *
     * @return string
     */
    public function getOneWeek($storeId = null): string
    {
        return (string)$this->getConfig(self::XML_FIELD_ONE_WEEK, $storeId);
    }

    /**
     * @param int|string|null $storeId
     *
     * @return string
     */
    public function getOneWeekFree($storeId = null): string
    {
        return (string)$this->getConfig(self::XML_FIELD_ONE_WEEK_FREE, $storeId);
    }

    /**
     * @param int|string|null $storeId
     *
     * @return string
     */
    public function getCheckAvailability($storeId = null): string
    {
        return (string)$this->getConfig(self::XML_FIELD_CHECK_AVAILABILITY, $storeId);
    }

    /**
     * @param int|string|null $storeId
     *
     * @return string
     */
    public function getMinimalFreeShippingOrderAmount($storeId = null): string
    {
        return (string)$this->scopeConfig->getValue(
            'carriers/freeshipping/free_shipping_subtotal',
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }
}
