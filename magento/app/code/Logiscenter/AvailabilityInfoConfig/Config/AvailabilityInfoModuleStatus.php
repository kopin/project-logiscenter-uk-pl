<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Logiscenter\AvailabilityInfoConfig\Config;

use Logiscenter\AvailabilityInfoConfig\Api\Config\AvailabilityInfoModuleStatusInterface;
use Logiscenter\AvailabilityInfoConfig\Api\Config\AvailabilityInfoStatusConfigInterface;

/**
 * Class AvailabilityInfoModuleStatus.
 *
 * This is not an api class, implementation may change at any time.
 * @see AvailabilityInfoModuleStatusInterface, use this to retrieve config.
 *
 * For example, it now relies on AvailabilityInfoStatusConfigInterface to read config,
 * but this implementation can be changed when needed, without prior notice.
 */
class AvailabilityInfoModuleStatus implements AvailabilityInfoModuleStatusInterface
{
    /**
     * @var AvailabilityInfoStatusConfigInterface
     */
    private $statusConfig;

    /**
     * AvailabilityInfoModuleStatus constructor.
     * @param AvailabilityInfoStatusConfigInterface $statusConfig
     */
    public function __construct(
        AvailabilityInfoStatusConfigInterface $statusConfig
    ) {
        $this->statusConfig = $statusConfig;
    }

    /**
     * Compare boolean status, using optionally supplied data
     *
     * @param bool $expectedStatusValue
     * @param int|string|null $storeId
     * @return bool
     */
    public function isBooleanStatus(
        bool $expectedStatusValue,
        $storeId = null
    ): bool {
        return $this->statusConfig->isBooleanStatus($expectedStatusValue, $storeId);
    }

    /**
     * Read boolean status, using optionally supplied data
     *
     * @param int|string|null $storeId
     * @return bool
     */
    public function isEnabled($storeId = null): bool
    {
        return $this->statusConfig->isEnabled($storeId);
    }
}
