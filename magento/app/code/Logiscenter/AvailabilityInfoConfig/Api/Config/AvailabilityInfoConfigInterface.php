<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Logiscenter\AvailabilityInfoConfig\Api\Config;

/**
 * Interface AvailabilityInfoConfigInterface.
 *
 * Config provider interface
 *
 * @api
 */
interface AvailabilityInfoConfigInterface
{
    public const SHIPS_TODAY_FREE   = 'ships_today_free';
    public const SHIPS_TODAY        = 'ships_today';
    public const ONE_WEEK_FREE      = 'one_week_free';
    public const ONE_WEEK           = 'one_week';
    public const CHECK_AVAILABILITY = 'check_availability';

    /**
     * @param string|null     $code
     * @param int|string|null $storeId
     *
     * @return string
     */
    public function getTooltipText(?string $code = null, $storeId = null): string;
}
