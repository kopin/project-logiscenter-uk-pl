<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Logiscenter\AvailabilityInfoConfig\Api\Config;

use Interactiv4\Environment\Api\AllowedEnvironmentsAwareInterface;

/**
 * Interface AvailabilityInfoEnvironmentConfigInterface.
 *
 * Environment config provider interface.
 *
 * @api
 */
interface AvailabilityInfoEnvironmentConfigInterface extends AllowedEnvironmentsAwareInterface
{
}
