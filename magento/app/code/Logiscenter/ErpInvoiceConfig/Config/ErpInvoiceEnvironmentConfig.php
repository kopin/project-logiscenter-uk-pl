<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ErpInvoiceConfig\Config;

use Logiscenter\ErpInvoiceConfig\Api\Config\ErpInvoiceEnvironmentConfigInterface;
use Logiscenter\ErpInvoiceConfig\Config\Db\DbErpInvoiceEnvironmentConfig;

/**
 * Class ErpInvoiceEnvironmentConfig.
 *
 * This is not an api class, implementation may change at any time.
 * @see ErpInvoiceEnvironmentConfigInterface, use this to retrieve config.
 *
 * For example, it now relies on DbErpInvoiceEnvironmentConfig to read information from database,
 * but this implementation can be changed when needed, without prior notice.
 */
class ErpInvoiceEnvironmentConfig extends DbErpInvoiceEnvironmentConfig implements
    ErpInvoiceEnvironmentConfigInterface
{
}
