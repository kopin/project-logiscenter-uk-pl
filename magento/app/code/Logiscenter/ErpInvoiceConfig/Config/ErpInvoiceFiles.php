<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ErpInvoiceConfig\Config;

use Logiscenter\ErpInvoiceConfig\Api\Config\ErpInvoiceFilesInterface;
use Logiscenter\ErpInvoiceConfig\Config\Db\DbErpInvoiceFiles;

/**
 * Class ErpInvoiceConfig.
 *
 * This is not an api class, implementation may change at any time.
 *
 * @see ErpInvoiceFilesInterface, use this to retrieve config.
 *
 * For example, it now relies on DbErpInvoiceConfig to read information from database,
 * but this implementation can be changed when needed, without prior notice.
 */
class ErpInvoiceFiles extends DbErpInvoiceFiles implements ErpInvoiceFilesInterface
{
}
