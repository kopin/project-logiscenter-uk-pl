<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ErpInvoiceConfig\Config\Db;

use Interactiv4\ConfigHelper\Api\Config\Db\DbConfigHelperInterface;
use Interactiv4\ConfigHelper\Api\Config\Db\Provider\DbConfigGroupPathProviderTrait;
use Interactiv4\ConfigHelper\Api\Config\Db\Provider\DbConfigSectionPathProviderTrait;
use Interactiv4\ConfigHelper\Model\Config\Db\DbConfigHelper;
use Logiscenter\ErpInvoiceConfig\Api\Config\Db\Provider\ErpInvoiceNotificationGroupPathProviderInterface;
use Logiscenter\ErpInvoiceConfig\Api\Config\Db\Provider\ErpInvoiceSectionPathProviderInterface;

/**
 * Class DbErpInvoiceNotification.
 *
 * This is not an api class, implementation may change at any time.
 * @see \Logiscenter\ErpInvoiceConfig\Api\Config\ErpInvoiceFilesInterface,
 * use this to retrieve config.
 */
class DbErpInvoiceNotification extends DbConfigHelper implements
    ErpInvoiceSectionPathProviderInterface,
    ErpInvoiceNotificationGroupPathProviderInterface,
    DbConfigHelperInterface
{
    use DbConfigSectionPathProviderTrait;
    use DbConfigGroupPathProviderTrait;

    private const EMAIL_SENDER = 'email_sender';
    private const EMAIL_TEMPLATE = 'email_template';

    /**
     * @param int|string|null $storeId
     *
     * @return string
     */
    public function getEmailSender($storeId = null): string
    {
        return (string)$this->getConfig(self::EMAIL_SENDER, $storeId);
    }

    /**
     * @param int|string|null $storeId
     *
     * @return string
     */
    public function getEmailTemplate($storeId = null): string
    {
        return (string)$this->getConfig(self::EMAIL_TEMPLATE, $storeId);
    }
}
