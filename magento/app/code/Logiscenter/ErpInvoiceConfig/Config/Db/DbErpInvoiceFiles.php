<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ErpInvoiceConfig\Config\Db;

use Interactiv4\ConfigHelper\Api\Config\Db\DbConfigHelperInterface;
use Interactiv4\ConfigHelper\Api\Config\Db\Provider\DbConfigGroupPathProviderTrait;
use Interactiv4\ConfigHelper\Api\Config\Db\Provider\DbConfigSectionPathProviderTrait;
use Interactiv4\ConfigHelper\Model\Config\Db\DbConfigHelper;
use Logiscenter\ErpInvoiceConfig\Api\Config\Db\Provider\ErpInvoiceFilesGroupPathProviderInterface;
use Logiscenter\ErpInvoiceConfig\Api\Config\Db\Provider\ErpInvoiceSectionPathProviderInterface;

/**
 * Class DbErpInvoiceConfig.
 *
 * This is not an api class, implementation may change at any time.
 * @see \Logiscenter\ErpInvoiceConfig\Api\Config\ErpInvoiceFilesInterface,
 * use this to retrieve config.
 */
class DbErpInvoiceFiles extends DbConfigHelper implements
    ErpInvoiceSectionPathProviderInterface,
    ErpInvoiceFilesGroupPathProviderInterface,
    DbConfigHelperInterface
{
    use DbConfigSectionPathProviderTrait;
    use DbConfigGroupPathProviderTrait;

    private const UPLOAD_PATH = 'upload_path';

    /**
     * @param int|string|null $storeId
     *
     * @return string
     */
    public function getUploadPath($storeId = null): string
    {
        return (string)$this->getConfig(self::UPLOAD_PATH, $storeId);
    }
}
