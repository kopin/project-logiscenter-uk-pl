<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ErpInvoiceConfig\Config;

use Logiscenter\ErpInvoiceConfig\Api\Config\ErpInvoiceModuleStatusInterface;
use Logiscenter\ErpInvoiceConfig\Api\Config\ErpInvoiceStatusConfigInterface;

/**
 * Class ErpInvoiceModuleStatus.
 *
 * This is not an api class, implementation may change at any time.
 * @see ErpInvoiceModuleStatusInterface, use this to retrieve config.
 *
 * For example, it now relies on ErpInvoiceStatusConfigInterface to read config,
 * but this implementation can be changed when needed, without prior notice.
 */
class ErpInvoiceModuleStatus implements ErpInvoiceModuleStatusInterface
{
    /**
     * @var ErpInvoiceStatusConfigInterface
     */
    private $statusConfig;

    /**
     * ErpInvoiceModuleStatus constructor.
     * @param ErpInvoiceStatusConfigInterface $statusConfig
     */
    public function __construct(
        ErpInvoiceStatusConfigInterface $statusConfig
    ) {
        $this->statusConfig = $statusConfig;
    }

    /**
     * Compare boolean status, using optionally supplied data
     *
     * @param bool $expectedStatusValue
     * @param int|string|null $storeId
     * @return bool
     */
    public function isBooleanStatus(
        bool $expectedStatusValue,
        $storeId = null
    ): bool {
        return $this->statusConfig->isBooleanStatus($expectedStatusValue, $storeId);
    }

    /**
     * Read boolean status, using optionally supplied data
     *
     * @param int|string|null $storeId
     * @return bool
     */
    public function isEnabled($storeId = null): bool
    {
        return $this->statusConfig->isEnabled($storeId);
    }
}
