<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ErpInvoiceConfig\Config;

use Logiscenter\ErpInvoiceConfig\Api\Config\ErpInvoiceNotificationInterface;
use Logiscenter\ErpInvoiceConfig\Config\Db\DbErpInvoiceNotification;

/**
 * Class ErpInvoiceConfig.
 *
 * This is not an api class, implementation may change at any time.
 *
 * @see ErpInvoiceNotificationInterface, use this to retrieve config.
 *
 * For example, it now relies on DbErpInvoiceConfig to read information from database,
 * but this implementation can be changed when needed, without prior notice.
 */
class ErpInvoiceNotification extends DbErpInvoiceNotification implements ErpInvoiceNotificationInterface
{
}
