<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ErpInvoiceConfig\Config;

use Logiscenter\ErpInvoiceConfig\Api\Config\ErpInvoiceStatusConfigInterface;
use Logiscenter\ErpInvoiceConfig\Config\Db\DbErpInvoiceStatusConfig;

/**
 * Class ErpInvoiceStatusConfig.
 *
 * This is not an api class, implementation may change at any time.
 * @see ErpInvoiceStatusConfigInterface, use this to retrieve config.
 *
 * For example, it now relies on DbErpInvoiceStatusConfig to read information from database,
 * but this implementation can be changed when needed, without prior notice.
 */
class ErpInvoiceStatusConfig implements ErpInvoiceStatusConfigInterface
{
    /**
     * @var DbErpInvoiceStatusConfig
     */
    private $dbStatusConfig;

    /**
     * ErpInvoiceStatusConfig constructor.
     *
     * @param DbErpInvoiceStatusConfig $dbStatusConfig
     */
    public function __construct(
        DbErpInvoiceStatusConfig $dbStatusConfig
    ) {
        $this->dbStatusConfig = $dbStatusConfig;
    }

    /**
     * Compare boolean status, using optionally supplied data
     *
     * @param bool $expectedStatusValue
     * @param int|string|null $storeId
     * @return bool
     */
    public function isBooleanStatus(
        bool $expectedStatusValue,
        $storeId = null
    ): bool {
        return $this->dbStatusConfig->isBooleanStatus($expectedStatusValue, $storeId);
    }

    /**
     * Read boolean status, using optionally supplied data
     *
     * @param int|string|null $storeId
     * @return bool
     */
    public function isEnabled($storeId = null): bool
    {
        return $this->dbStatusConfig->isEnabled($storeId);
    }

    /**
     * Enable status, using optionally supplied data
     *
     * @param int|string|null $storeId
     * @return void
     */
    public function enable($storeId = null): void
    {
        $this->dbStatusConfig->enable($storeId);
    }

    /**
     * Disable status, using optionally supplied data
     *
     * @param int|string|null $storeId
     * @return void
     */
    public function disable($storeId = null): void
    {
        $this->dbStatusConfig->disable($storeId);
    }
}
