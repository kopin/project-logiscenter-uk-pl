<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ErpInvoiceConfig\Test\Unit\Plugin\Api\Status;

use Logiscenter\ErpInvoiceConfig\Plugin\Api\Status\CheckErpInvoiceModuleStatusPlugin;
use Interactiv4\Framework\Test\Unit\Plugin\Api\Status\CheckBooleanStatusPluginTestTrait;
use PHPUnit\Framework\TestCase;

/**
 * Class CheckErpInvoiceModuleStatusPlugin.
 *
 * Test plugin that allows to intercept module status.
 *
 * @group config
 *
 * @internal
 */
class CheckErpInvoiceModuleStatusPluginTest extends TestCase
{
    use CheckBooleanStatusPluginTestTrait;

    /**
     * {@inheritdoc}
     */
    protected function getPluginClass(): string
    {
        return CheckErpInvoiceModuleStatusPlugin::class;
    }
}
