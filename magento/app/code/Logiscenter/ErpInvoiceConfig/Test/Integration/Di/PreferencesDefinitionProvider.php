<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ErpInvoiceConfig\Test\Integration\Di;

use Interactiv4\Framework\Api\Di\Definition\DefinitionPoolInterface;
use Interactiv4\Framework\Api\Di\Definition\DefinitionPoolProviderInterface;
use Interactiv4\Framework\Di\Definition\Definition;
use Interactiv4\Framework\Di\Definition\DefinitionPool;
use Interactiv4\Framework\Di\Definition\DefinitionName;
use Interactiv4\Framework\Di\Definition\DefinitionValue;

/**
 * Class PreferencesDefinitionProvider.
 *
 * Preferences definition as they appear in di configuration files.
 */
class PreferencesDefinitionProvider implements DefinitionPoolProviderInterface
{
    /**
     * @var DefinitionPoolInterface
     */
    private $definitionPool;

    /**
     * {@inheritdoc}
     */
    public function getDefinitionPool(): DefinitionPoolInterface
    {
        if (!isset($this->definitionPool)) {
            $this->definitionPool = new DefinitionPool(
                new Definition(
                    new DefinitionName(
                        'Logiscenter\\ErpInvoiceConfig\\Api\\Config\\ErpInvoiceModuleStatusInterface'
                    ),
                    new DefinitionValue(
                        \Logiscenter\ErpInvoiceConfig\Api\Config\ErpInvoiceModuleStatusInterface::class,
                        \Logiscenter\ErpInvoiceConfig\Config\ErpInvoiceModuleStatus::class,
                        (string) \Magento\Framework\App\Area::AREA_GLOBAL,
                        false,
                        null,
                        null
                    )
                ),
                new Definition(
                    new DefinitionName(
                        'Logiscenter\\ErpInvoiceConfig\\Api\\Config\\ErpInvoiceStatusConfigInterface'
                    ),
                    new DefinitionValue(
                        \Logiscenter\ErpInvoiceConfig\Api\Config\ErpInvoiceStatusConfigInterface::class,
                        \Logiscenter\ErpInvoiceConfig\Config\ErpInvoiceStatusConfig::class,
                        (string) \Magento\Framework\App\Area::AREA_GLOBAL,
                        false,
                        null,
                        null
                    )
                ),
                new Definition(
                    new DefinitionName(
                        'Logiscenter\\ErpInvoiceConfig\\Api\\Config\\ErpInvoiceEnvironmentConfigInterface'
                    ),
                    new DefinitionValue(
                        \Logiscenter\ErpInvoiceConfig\Api\Config\ErpInvoiceEnvironmentConfigInterface::class,
                        \Logiscenter\ErpInvoiceConfig\Config\ErpInvoiceEnvironmentConfig::class,
                        (string) \Magento\Framework\App\Area::AREA_GLOBAL,
                        false,
                        null,
                        null
                    )
                ),
                new Definition(
                    new DefinitionName(
                        'Logiscenter\\ErpInvoiceConfig\\Api\\Config\\ErpInvoiceFilesInterface'
                    ),
                    new DefinitionValue(
                        \Logiscenter\ErpInvoiceConfig\Api\Config\ErpInvoiceFilesInterface::class,
                        \Logiscenter\ErpInvoiceConfig\Config\ErpInvoiceFiles::class,
                        (string) \Magento\Framework\App\Area::AREA_GLOBAL,
                        false,
                        null,
                        null
                    )
                )
            );
        }

        return $this->definitionPool;
    }
}
