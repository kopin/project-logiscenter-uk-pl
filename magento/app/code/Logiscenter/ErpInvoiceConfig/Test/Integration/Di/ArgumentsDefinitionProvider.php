<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ErpInvoiceConfig\Test\Integration\Di;

use Interactiv4\Framework\Api\Di\Definition\DefinitionPoolInterface;
use Interactiv4\Framework\Api\Di\Definition\DefinitionPoolProviderInterface;
use Interactiv4\Framework\Di\Definition\Definition;
use Interactiv4\Framework\Di\Definition\DefinitionPool;
use Interactiv4\Framework\Di\Definition\DefinitionName;
use Interactiv4\Framework\Di\Definition\DefinitionValue;

/**
 * Class ArgumentsDefinitionProvider.
 *
 * Arguments definition as they appear in di configuration files.
 */
class ArgumentsDefinitionProvider implements DefinitionPoolProviderInterface
{
    /**
     * @var DefinitionPoolInterface
     */
    private $definitionPool;

    /**
     * {@inheritdoc}
     */
    public function getDefinitionPool(): DefinitionPoolInterface
    {
        if (!isset($this->definitionPool)) {
            $this->definitionPool = new DefinitionPool(
                new Definition(
                    new DefinitionName(
                        'Logiscenter\\ErpInvoiceConfig\\Plugin\\Api\\Status\\CheckErpInvoiceModuleStatusPlugin',
                        'booleanStatusRead'
                    ),
                    new DefinitionValue(
                        \Logiscenter\ErpInvoiceConfig\Plugin\Api\Status\CheckErpInvoiceModuleStatusPlugin::class,
                        \Logiscenter\ErpInvoiceConfig\Api\Config\ErpInvoiceModuleStatusInterface::class,
                        (string) \Magento\Framework\App\Area::AREA_GLOBAL,
                        false,
                        null,
                        null
                    )
                ),
                new Definition(
                    new DefinitionName(
                        'Logiscenter\\ErpInvoiceConfig\\Plugin\\Api\\Status\\CheckErpInvoiceEnvironmentPlugin',
                        'allowedEnvironmentsAware'
                    ),
                    new DefinitionValue(
                        \Logiscenter\ErpInvoiceConfig\Plugin\Api\Status\CheckErpInvoiceEnvironmentPlugin::class,
                        \Logiscenter\ErpInvoiceConfig\Api\Config\ErpInvoiceEnvironmentConfigInterface::class,
                        (string) \Magento\Framework\App\Area::AREA_GLOBAL,
                        false,
                        null,
                        null
                    )
                ),
                new Definition(
                    new DefinitionName(
                        'Interactiv4\\BaseConfig\\Pool\\ModuleStatusPool',
                        'objects',
                        'Logiscenter_ErpInvoiceConfig'
                    ),
                    new DefinitionValue(
                        'Interactiv4\\BaseConfig\\Pool\\ModuleStatusPool',
                        \Logiscenter\ErpInvoiceConfig\Api\Config\ErpInvoiceModuleStatusInterface::class,
                        (string) \Magento\Framework\App\Area::AREA_GLOBAL,
                        false,
                        null,
                        null
                    )
                ),
                new Definition(
                    new DefinitionName(
                        'Logiscenter\\ErpInvoiceConfig\\Observer\\RefreshDataUponConfigChangeObserver',
                        'refreshCacheStrategy'
                    ),
                    new DefinitionValue(
                        \Logiscenter\ErpInvoiceConfig\Observer\RefreshDataUponConfigChangeObserver::class,
                        \Interactiv4\Framework\Cache\RefreshCacheAppStateDependantStrategy::class,
                        (string) \Magento\Framework\App\Area::AREA_ADMINHTML,
                        false,
                        null,
                        null
                    )
                ),
                new Definition(
                    new DefinitionName(
                        'Logiscenter\\ErpInvoiceConfig\\Observer\\RefreshDataUponConfigChangeObserver',
                        'cacheTypes',
                        'config'
                    ),
                    new DefinitionValue(
                        \Logiscenter\ErpInvoiceConfig\Observer\RefreshDataUponConfigChangeObserver::class,
                        (string) \Magento\Framework\App\Cache\Type\Config::TYPE_IDENTIFIER,
                        (string) \Magento\Framework\App\Area::AREA_ADMINHTML,
                        false,
                        null,
                        null
                    )
                ),
                new Definition(
                    new DefinitionName(
                        'Logiscenter\\ErpInvoiceConfig\\Observer\\RefreshDataUponConfigChangeObserver',
                        'cacheTypes',
                        'block_html'
                    ),
                    new DefinitionValue(
                        \Logiscenter\ErpInvoiceConfig\Observer\RefreshDataUponConfigChangeObserver::class,
                        (string) \Magento\Framework\App\Cache\Type\Block::TYPE_IDENTIFIER,
                        (string) \Magento\Framework\App\Area::AREA_ADMINHTML,
                        false,
                        null,
                        null
                    )
                ),
                new Definition(
                    new DefinitionName(
                        'Logiscenter\\ErpInvoiceConfig\\Observer\\RefreshDataUponConfigChangeObserver',
                        'cacheTypes',
                        'full_page'
                    ),
                    new DefinitionValue(
                        \Logiscenter\ErpInvoiceConfig\Observer\RefreshDataUponConfigChangeObserver::class,
                        (string) \Magento\PageCache\Model\Cache\Type::TYPE_IDENTIFIER,
                        (string) \Magento\Framework\App\Area::AREA_ADMINHTML,
                        false,
                        null,
                        null
                    )
                )
            );
        }

        return $this->definitionPool;
    }
}
