<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ErpInvoiceConfig\Api\Config;

/**
 * Interface ErpInvoiceNotificationInterface.
 *
 * Config provider interface
 *
 * @api
 */
interface ErpInvoiceNotificationInterface
{
    /**
     * @param int|string|null $storeId
     *
     * @return string
     */
    public function getEmailSender($storeId = null): string;

    /**
     * @param int|string|null $storeId
     *
     * @return string
     */
    public function getEmailTemplate($storeId = null): string;
}
