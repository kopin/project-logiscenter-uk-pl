<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ErpInvoiceConfig\Api\Config;

/**
 * Interface ErpInvoiceFilesInterface.
 *
 * Config provider interface
 *
 * @api
 */
interface ErpInvoiceFilesInterface
{
    /**
     * @param int|string|null $storeId
     *
     * @return string
     */
    public function getUploadPath($storeId = null): string;
}
