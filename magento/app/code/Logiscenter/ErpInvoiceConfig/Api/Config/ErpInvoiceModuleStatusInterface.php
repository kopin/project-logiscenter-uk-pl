<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ErpInvoiceConfig\Api\Config;

use Interactiv4\ConfigHelper\Api\Config\Status\ModuleStatusInterface;

/**
 * Interface ErpInvoiceModuleStatusInterface.
 *
 * Module status provider interface.
 *
 * @api
 */
interface ErpInvoiceModuleStatusInterface extends ModuleStatusInterface
{
}
