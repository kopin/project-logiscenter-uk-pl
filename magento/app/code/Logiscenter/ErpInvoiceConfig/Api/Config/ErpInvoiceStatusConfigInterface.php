<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ErpInvoiceConfig\Api\Config;

use Interactiv4\Contracts\SPL\BooleanStatus\Api\BooleanStatusCompareInterface;
use Interactiv4\Contracts\SPL\BooleanStatus\Api\BooleanStatusPutInterface;
use Interactiv4\Contracts\SPL\BooleanStatus\Api\BooleanStatusReadInterface;

/**
 * Interface ErpInvoiceStatusConfigInterface.
 *
 * Status provider interface.
 *
 * @api
 */
interface ErpInvoiceStatusConfigInterface extends
    BooleanStatusReadInterface,
    BooleanStatusCompareInterface,
    BooleanStatusPutInterface
{
}
