<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ProductTabsConfig\Config;

use Logiscenter\ProductTabsConfig\Api\Config\ProductTabsConfigInterface;
use Logiscenter\ProductTabsConfig\Config\Db\DbProductTabsConfig;

/**
 * Class ProductTabsConfig.
 *
 * This is not an api class, implementation may change at any time.
 * @see ProductTabsConfigInterface, use this to retrieve config.
 *
 * For example, it now relies on DbProductTabsConfig to read information from database,
 * but this implementation can be changed when needed, without prior notice.
 */
class ProductTabsConfig extends DbProductTabsConfig implements
    ProductTabsConfigInterface
{
}
