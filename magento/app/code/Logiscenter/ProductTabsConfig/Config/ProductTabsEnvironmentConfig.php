<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ProductTabsConfig\Config;

use Logiscenter\ProductTabsConfig\Api\Config\ProductTabsEnvironmentConfigInterface;
use Logiscenter\ProductTabsConfig\Config\Db\DbProductTabsEnvironmentConfig;

/**
 * Class ProductTabsEnvironmentConfig.
 *
 * This is not an api class, implementation may change at any time.
 * @see ProductTabsEnvironmentConfigInterface, use this to retrieve config.
 *
 * For example, it now relies on DbProductTabsEnvironmentConfig to read information from database,
 * but this implementation can be changed when needed, without prior notice.
 */
class ProductTabsEnvironmentConfig extends DbProductTabsEnvironmentConfig implements
    ProductTabsEnvironmentConfigInterface
{
}
