<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ProductTabsConfig\Config;

use Logiscenter\ProductTabsConfig\Api\Config\ProductTabsModuleStatusInterface;
use Logiscenter\ProductTabsConfig\Api\Config\ProductTabsStatusConfigInterface;

/**
 * Class ProductTabsModuleStatus.
 *
 * This is not an api class, implementation may change at any time.
 * @see ProductTabsModuleStatusInterface, use this to retrieve config.
 *
 * For example, it now relies on ProductTabsStatusConfigInterface to read config,
 * but this implementation can be changed when needed, without prior notice.
 */
class ProductTabsModuleStatus implements ProductTabsModuleStatusInterface
{
    /**
     * @var ProductTabsStatusConfigInterface
     */
    private $statusConfig;

    /**
     * ProductTabsModuleStatus constructor.
     * @param ProductTabsStatusConfigInterface $statusConfig
     */
    public function __construct(
        ProductTabsStatusConfigInterface $statusConfig
    ) {
        $this->statusConfig = $statusConfig;
    }

    /**
     * Compare boolean status, using optionally supplied data
     *
     * @param bool $expectedStatusValue
     * @param int|string|null $storeId
     * @return bool
     */
    public function isBooleanStatus(
        bool $expectedStatusValue,
        $storeId = null
    ): bool {
        return $this->statusConfig->isBooleanStatus($expectedStatusValue, $storeId);
    }

    /**
     * Read boolean status, using optionally supplied data
     *
     * @param int|string|null $storeId
     * @return bool
     */
    public function isEnabled($storeId = null): bool
    {
        return $this->statusConfig->isEnabled($storeId);
    }
}
