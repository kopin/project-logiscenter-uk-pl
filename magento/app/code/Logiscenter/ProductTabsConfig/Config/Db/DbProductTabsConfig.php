<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ProductTabsConfig\Config\Db;

use Interactiv4\ConfigHelper\Api\Config\Db\DbConfigHelperInterface;
use Interactiv4\ConfigHelper\Api\Config\Db\Provider\DbConfigGroupPathProviderTrait;
use Interactiv4\ConfigHelper\Api\Config\Db\Provider\DbConfigSectionPathProviderTrait;
use Interactiv4\ConfigHelper\Model\Config\Db\DbConfigHelper;
use Logiscenter\ProductTabsConfig\Api\Config\Db\Provider\ProductTabsGroupPathProviderInterface;
use Logiscenter\ProductTabsConfig\Api\Config\Db\Provider\ProductTabsSectionPathProviderInterface;

/**
 * Class DbProductTabsConfig.
 *
 * This is not an api class, implementation may change at any time.
 * @see \Logiscenter\ProductTabsConfig\Api\Config\ProductTabsConfigInterface,
 * use this to retrieve config.
 */
class DbProductTabsConfig extends DbConfigHelper implements
    ProductTabsSectionPathProviderInterface,
    ProductTabsGroupPathProviderInterface,
    DbConfigHelperInterface
{
    use DbConfigSectionPathProviderTrait;
    use DbConfigGroupPathProviderTrait;

    private const XML_FILED_PAGE_SIZE = 'products_per_page';

    /**
     * @param $storeId
     *
     * @return string
     */
    public function getPageSize($storeId = null): string
    {
        return (string)$this->getConfig(self::XML_FILED_PAGE_SIZE, $storeId);
    }
}
