<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ProductTabsConfig\Test\Integration\Di;

use Interactiv4\Framework\Api\Di\Definition\DefinitionPoolInterface;
use Interactiv4\Framework\Api\Di\Definition\DefinitionPoolProviderInterface;
use Interactiv4\Framework\Di\Definition\Definition;
use Interactiv4\Framework\Di\Definition\DefinitionPool;
use Interactiv4\Framework\Di\Definition\DefinitionName;
use Interactiv4\Framework\Di\Definition\DefinitionValue;

/**
 * Class PreferencesDefinitionProvider.
 *
 * Preferences definition as they appear in di configuration files.
 */
class PreferencesDefinitionProvider implements DefinitionPoolProviderInterface
{
    /**
     * @var DefinitionPoolInterface
     */
    private $definitionPool;

    /**
     * {@inheritdoc}
     */
    public function getDefinitionPool(): DefinitionPoolInterface
    {
        if (!isset($this->definitionPool)) {
            $this->definitionPool = new DefinitionPool(
                new Definition(
                    new DefinitionName(
                        'Logiscenter\\ProductTabsConfig\\Api\\Config\\ProductTabsModuleStatusInterface'
                    ),
                    new DefinitionValue(
                        \Logiscenter\ProductTabsConfig\Api\Config\ProductTabsModuleStatusInterface::class,
                        \Logiscenter\ProductTabsConfig\Config\ProductTabsModuleStatus::class,
                        (string) \Magento\Framework\App\Area::AREA_GLOBAL,
                        false,
                        null,
                        null
                    )
                ),
                new Definition(
                    new DefinitionName(
                        'Logiscenter\\ProductTabsConfig\\Api\\Config\\ProductTabsStatusConfigInterface'
                    ),
                    new DefinitionValue(
                        \Logiscenter\ProductTabsConfig\Api\Config\ProductTabsStatusConfigInterface::class,
                        \Logiscenter\ProductTabsConfig\Config\ProductTabsStatusConfig::class,
                        (string) \Magento\Framework\App\Area::AREA_GLOBAL,
                        false,
                        null,
                        null
                    )
                ),
                new Definition(
                    new DefinitionName(
                        'Logiscenter\\ProductTabsConfig\\Api\\Config\\ProductTabsEnvironmentConfigInterface'
                    ),
                    new DefinitionValue(
                        \Logiscenter\ProductTabsConfig\Api\Config\ProductTabsEnvironmentConfigInterface::class,
                        \Logiscenter\ProductTabsConfig\Config\ProductTabsEnvironmentConfig::class,
                        (string) \Magento\Framework\App\Area::AREA_GLOBAL,
                        false,
                        null,
                        null
                    )
                ),
                new Definition(
                    new DefinitionName(
                        'Logiscenter\\ProductTabsConfig\\Api\\Config\\ProductTabsConfigInterface'
                    ),
                    new DefinitionValue(
                        \Logiscenter\ProductTabsConfig\Api\Config\ProductTabsConfigInterface::class,
                        \Logiscenter\ProductTabsConfig\Config\ProductTabsConfig::class,
                        (string) \Magento\Framework\App\Area::AREA_GLOBAL,
                        false,
                        null,
                        null
                    )
                )
            );
        }

        return $this->definitionPool;
    }
}
