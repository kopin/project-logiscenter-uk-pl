<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ProductTabsConfig\Api\Config;

use Interactiv4\Environment\Api\AllowedEnvironmentsAwareInterface;

/**
 * Interface ProductTabsEnvironmentConfigInterface.
 *
 * Environment config provider interface.
 *
 * @api
 */
interface ProductTabsEnvironmentConfigInterface extends AllowedEnvironmentsAwareInterface
{
}
