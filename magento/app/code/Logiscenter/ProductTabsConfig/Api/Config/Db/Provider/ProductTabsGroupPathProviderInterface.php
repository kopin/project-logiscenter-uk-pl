<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ProductTabsConfig\Api\Config\Db\Provider;

use Interactiv4\ConfigHelper\Api\Config\Db\Provider\DbConfigGroupPathProviderInterface;

/**
 * Interface ProductTabsGroupPathProviderInterface.
 *
 * Group path provider for producttabs group.
 *
 * @api
 */
interface ProductTabsGroupPathProviderInterface extends DbConfigGroupPathProviderInterface
{
    public const GROUP_PATH = 'producttabs';
}
