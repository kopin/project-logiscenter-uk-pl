<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ProductTabsConfig\Api\Config;

/**
 * Interface ProductTabsConfigInterface.
 *
 * Config provider interface.
 *
 * @api
 */
interface ProductTabsConfigInterface
{
    /**
     * @param int|string|null $storeId
     *
     * @return string
     */
    public function getPageSize($storeId = null): string;
}
