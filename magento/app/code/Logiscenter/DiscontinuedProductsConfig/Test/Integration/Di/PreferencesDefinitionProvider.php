<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Logiscenter\DiscontinuedProductsConfig\Test\Integration\Di;

use Interactiv4\Framework\Api\Di\Definition\DefinitionPoolInterface;
use Interactiv4\Framework\Api\Di\Definition\DefinitionPoolProviderInterface;
use Interactiv4\Framework\Di\Definition\Definition;
use Interactiv4\Framework\Di\Definition\DefinitionPool;
use Interactiv4\Framework\Di\Definition\DefinitionName;
use Interactiv4\Framework\Di\Definition\DefinitionValue;

/**
 * Class PreferencesDefinitionProvider.
 *
 * Preferences definition as they appear in di configuration files.
 */
class PreferencesDefinitionProvider implements DefinitionPoolProviderInterface
{
    /**
     * @var DefinitionPoolInterface
     */
    private $definitionPool;

    /**
     * {@inheritdoc}
     */
    public function getDefinitionPool(): DefinitionPoolInterface
    {
        if (!isset($this->definitionPool)) {
            $this->definitionPool = new DefinitionPool(
	new Definition(
	new DefinitionName(
	'Logiscenter\\DiscontinuedProductsConfig\\Api\\Config\\DiscontinuedProductsModuleStatusInterface'
	),
		new DefinitionValue(
	\Logiscenter\DiscontinuedProductsConfig\Api\Config\DiscontinuedProductsModuleStatusInterface::class,
		\Logiscenter\DiscontinuedProductsConfig\Config\DiscontinuedProductsModuleStatus::class,
		(string) \Magento\Framework\App\Area::AREA_GLOBAL,
		false,
		null,
		null
	)
	),
		new Definition(
	new DefinitionName(
	'Logiscenter\\DiscontinuedProductsConfig\\Api\\Config\\DiscontinuedProductsStatusConfigInterface'
	),
		new DefinitionValue(
	\Logiscenter\DiscontinuedProductsConfig\Api\Config\DiscontinuedProductsStatusConfigInterface::class,
		\Logiscenter\DiscontinuedProductsConfig\Config\DiscontinuedProductsStatusConfig::class,
		(string) \Magento\Framework\App\Area::AREA_GLOBAL,
		false,
		null,
		null
	)
	),
		new Definition(
	new DefinitionName(
	'Logiscenter\\DiscontinuedProductsConfig\\Api\\Config\\DiscontinuedProductsEnvironmentConfigInterface'
	),
		new DefinitionValue(
	\Logiscenter\DiscontinuedProductsConfig\Api\Config\DiscontinuedProductsEnvironmentConfigInterface::class,
		\Logiscenter\DiscontinuedProductsConfig\Config\DiscontinuedProductsEnvironmentConfig::class,
		(string) \Magento\Framework\App\Area::AREA_GLOBAL,
		false,
		null,
		null
	)
	),
		new Definition(
	new DefinitionName(
	'Logiscenter\\DiscontinuedProductsConfig\\Api\\Config\\DiscontinuedProductsConfigInterface'
	),
		new DefinitionValue(
	\Logiscenter\DiscontinuedProductsConfig\Api\Config\DiscontinuedProductsConfigInterface::class,
		\Logiscenter\DiscontinuedProductsConfig\Config\DiscontinuedProductsConfig::class,
		(string) \Magento\Framework\App\Area::AREA_GLOBAL,
		false,
		null,
		null
	)
	)
	);
        }

        return $this->definitionPool;
    }
}
