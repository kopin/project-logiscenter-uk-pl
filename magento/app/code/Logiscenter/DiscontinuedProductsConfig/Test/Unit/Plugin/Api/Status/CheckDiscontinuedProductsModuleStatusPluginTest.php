<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Logiscenter\DiscontinuedProductsConfig\Test\Unit\Plugin\Api\Status;

use Logiscenter\DiscontinuedProductsConfig\Plugin\Api\Status\CheckDiscontinuedProductsModuleStatusPlugin;
use Interactiv4\Framework\Test\Unit\Plugin\Api\Status\CheckBooleanStatusPluginTestTrait;
use PHPUnit\Framework\TestCase;

/**
 * Class CheckDiscontinuedProductsModuleStatusPlugin.
 *
 * Test plugin that allows to intercept module status.
 *
 * @group config
 *
 * @internal
 */
class CheckDiscontinuedProductsModuleStatusPluginTest extends TestCase
{
    use CheckBooleanStatusPluginTestTrait;

    /**
     * {@inheritdoc}
     */
    protected function getPluginClass(): string
    {
        return CheckDiscontinuedProductsModuleStatusPlugin::class;
    }
}
