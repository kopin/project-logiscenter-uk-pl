<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Logiscenter\DiscontinuedProductsConfig\Api\Config;

/**
 * Interface DiscontinuedProductsConfigInterface.
 *
 * Config provider interface.
 *
 * @api
 */
interface DiscontinuedProductsConfigInterface
{
}
