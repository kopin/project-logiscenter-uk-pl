<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Logiscenter\DiscontinuedProductsConfig\Api\Config\Db\Provider;

use Interactiv4\ConfigHelper\Api\Config\Db\Provider\DbConfigGroupPathProviderInterface;

/**
 * Interface DiscontinuedProductsGroupPathProviderInterface.
 *
 * Group path provider for discontinuedproducts group.
 *
 * @api
 */
interface DiscontinuedProductsGroupPathProviderInterface extends DbConfigGroupPathProviderInterface
{
    public const GROUP_PATH = 'discontinuedproducts';
}
