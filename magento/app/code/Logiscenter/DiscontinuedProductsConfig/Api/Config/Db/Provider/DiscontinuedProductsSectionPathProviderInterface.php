<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Logiscenter\DiscontinuedProductsConfig\Api\Config\Db\Provider;

use Interactiv4\ConfigHelper\Api\Config\Db\Provider\DbConfigSectionPathProviderInterface;

/**
 * Interface DiscontinuedProductsSectionPathProviderInterface.
 *
 * Section path provider for logiscenter_discontinuedproductsconfig section.
 *
 * @api
 */
interface DiscontinuedProductsSectionPathProviderInterface extends DbConfigSectionPathProviderInterface
{
    public const SECTION_PATH = 'logiscenter_discontinuedproductsconfig';
}
