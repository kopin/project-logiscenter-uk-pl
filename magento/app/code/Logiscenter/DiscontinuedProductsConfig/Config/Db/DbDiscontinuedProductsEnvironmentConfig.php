<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Logiscenter\DiscontinuedProductsConfig\Config\Db;

use Interactiv4\ConfigHelper\Api\Config\Db\DbConfigHelperInterface;
use Interactiv4\ConfigHelper\Api\Config\Db\Provider\DbConfigGroupPathProviderTrait;
use Interactiv4\ConfigHelper\Api\Config\Db\Provider\DbConfigSectionPathProviderTrait;
use Interactiv4\ConfigHelper\Model\Config\Db\DbConfigHelper;
use Interactiv4\Environment\Api\Config\Db\AllowedEnvironmentsInterface;
use Interactiv4\Environment\Api\Config\Db\AllowedEnvironmentsTrait;
use Interactiv4\EnvironmentConfig\Api\Config\Db\Provider\EnvironmentGroupPathProviderInterface;
use Logiscenter\DiscontinuedProductsConfig\Api\Config\Db\Provider\DiscontinuedProductsSectionPathProviderInterface;

/**
 * Class DbDiscontinuedProductsEnvironmentConfig.
 *
 * This is not an api class, implementation may change at any time.
 * @see \Logiscenter\DiscontinuedProductsConfig\Api\Config\DiscontinuedProductsEnvironmentConfigInterface,
 * use this to retrieve config.
 */
class DbDiscontinuedProductsEnvironmentConfig extends DbConfigHelper implements
    DiscontinuedProductsSectionPathProviderInterface,
    EnvironmentGroupPathProviderInterface,
    AllowedEnvironmentsInterface,
    DbConfigHelperInterface
{
    use DbConfigSectionPathProviderTrait;
    use DbConfigGroupPathProviderTrait;
    use AllowedEnvironmentsTrait;
}
