<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Logiscenter\DiscontinuedProductsConfig\Config;

use Logiscenter\DiscontinuedProductsConfig\Api\Config\DiscontinuedProductsModuleStatusInterface;
use Logiscenter\DiscontinuedProductsConfig\Api\Config\DiscontinuedProductsStatusConfigInterface;

/**
 * Class DiscontinuedProductsModuleStatus.
 *
 * This is not an api class, implementation may change at any time.
 * @see DiscontinuedProductsModuleStatusInterface, use this to retrieve config.
 *
 * For example, it now relies on DiscontinuedProductsStatusConfigInterface to read config,
 * but this implementation can be changed when needed, without prior notice.
 */
class DiscontinuedProductsModuleStatus implements DiscontinuedProductsModuleStatusInterface
{
    /**
     * @var DiscontinuedProductsStatusConfigInterface
     */
    private $statusConfig;

    /**
     * DiscontinuedProductsModuleStatus constructor.
     * @param DiscontinuedProductsStatusConfigInterface $statusConfig
     */
    public function __construct(
        DiscontinuedProductsStatusConfigInterface $statusConfig
    ) {
        $this->statusConfig = $statusConfig;
    }

    /**
     * Compare boolean status, using optionally supplied data
     *
     * @param bool $expectedStatusValue
     * @param int|string|null $storeId
     * @return bool
     */
    public function isBooleanStatus(
        bool $expectedStatusValue,
        $storeId = null
    ): bool {
        return $this->statusConfig->isBooleanStatus($expectedStatusValue, $storeId);
    }

    /**
     * Read boolean status, using optionally supplied data
     *
     * @param int|string|null $storeId
     * @return bool
     */
    public function isEnabled($storeId = null): bool
    {
        return $this->statusConfig->isEnabled($storeId);
    }
}
