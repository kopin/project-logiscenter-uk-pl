<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Logiscenter\DiscontinuedProductsConfig\Config;

use Logiscenter\DiscontinuedProductsConfig\Api\Config\DiscontinuedProductsConfigInterface;
use Logiscenter\DiscontinuedProductsConfig\Config\Db\DbDiscontinuedProductsConfig;

/**
 * Class DiscontinuedProductsConfig.
 *
 * This is not an api class, implementation may change at any time.
 * @see DiscontinuedProductsConfigInterface, use this to retrieve config.
 *
 * For example, it now relies on DbDiscontinuedProductsConfig to read information from database,
 * but this implementation can be changed when needed, without prior notice.
 */
class DiscontinuedProductsConfig extends DbDiscontinuedProductsConfig implements
    DiscontinuedProductsConfigInterface
{
}
