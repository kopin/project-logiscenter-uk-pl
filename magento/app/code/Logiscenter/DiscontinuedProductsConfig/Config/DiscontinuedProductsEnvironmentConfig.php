<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Logiscenter\DiscontinuedProductsConfig\Config;

use Logiscenter\DiscontinuedProductsConfig\Api\Config\DiscontinuedProductsEnvironmentConfigInterface;
use Logiscenter\DiscontinuedProductsConfig\Config\Db\DbDiscontinuedProductsEnvironmentConfig;

/**
 * Class DiscontinuedProductsEnvironmentConfig.
 *
 * This is not an api class, implementation may change at any time.
 * @see DiscontinuedProductsEnvironmentConfigInterface, use this to retrieve config.
 *
 * For example, it now relies on DbDiscontinuedProductsEnvironmentConfig to read information from database,
 * but this implementation can be changed when needed, without prior notice.
 */
class DiscontinuedProductsEnvironmentConfig extends DbDiscontinuedProductsEnvironmentConfig implements
    DiscontinuedProductsEnvironmentConfigInterface
{
}
