<?php
declare(strict_types=1);
/**
 * @author    Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Logiscenter\CompatibleProducts\Model\ResourceModel\Product;

use Logiscenter\CompatibleProductsConfig\Api\Config\CompatibleProductsModuleStatusInterface;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Model\Product\Link;
use Magento\Catalog\Model\Product\LinkFactory as LinkModelFactory;
use Magento\Catalog\Model\Product\Visibility;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Magento\Catalog\Model\ResourceModel\Product\Link\CollectionFactory as LinkCollectionFactory;
use Magento\Framework\EntityManager\MetadataPool;

/**
 * Class to retrieve compatible product
 */
class Compatible
{
    private const ATTRIBUTE_TO_SELECT = [
        'name',
        'thumbnail',
        'url_key',
        'short_description',
        'grouped_qty',
        'visibility',
    ];

    /** @var ProductCollectionFactory */
    private $productCollectionFactory;

    /** @var LinkCollectionFactory */
    private $linkCollectionFactory;

    /** @var LinkModelFactory */
    private $linkTypeModelFactory;

    /** @var Visibility */
    private $catalogProductVisibility;

    /** @var MetadataPool */
    private $metadataPool;

    /** @var CompatibleProductsModuleStatusInterface */
    private $status;

    /**
     * @param ProductCollectionFactory                $productCollectionFactory
     * @param LinkCollectionFactory                   $collectionFactory
     * @param LinkModelFactory                        $linkTypeModelFactory
     * @param Visibility                              $catalogProductVisibility
     * @param MetadataPool                            $metadataPool
     * @param CompatibleProductsModuleStatusInterface $status
     */
    public function __construct(
        ProductCollectionFactory $productCollectionFactory,
        LinkCollectionFactory $collectionFactory,
        LinkModelFactory $linkTypeModelFactory,
        Visibility $catalogProductVisibility,
        MetadataPool $metadataPool,
        CompatibleProductsModuleStatusInterface $status
    ) {
        $this->linkCollectionFactory = $collectionFactory;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->linkTypeModelFactory = $linkTypeModelFactory;
        $this->catalogProductVisibility = $catalogProductVisibility;
        $this->metadataPool = $metadataPool;
        $this->status = $status;
    }

    /**
     * Get compatible products
     *
     * @param ProductInterface $product
     * @param null|int         $limit
     * @return array
     */
    public function getProducts(ProductInterface $product, ?int $limit = null): array
    {
        if (!$this->status->isEnabled()) {
            return [];
        }

        $invertedProductIds = $this->getInvertRelatedProductsIds($product);

        if (empty($invertedProductIds)) {
            return [];
        }

        $productCollection = $this->productCollectionFactory->create();
        $productCollection->setVisibility($this->catalogProductVisibility->getVisibleInCatalogIds());
        $productCollection->addAttributeToSelect(self::ATTRIBUTE_TO_SELECT);
        $productCollection->addAttributeToFilter($this->getProductEntityIdField(), ['in' => $invertedProductIds]);
        $productCollection->setOrder('grouped_qty');

        if ($limit) {
            $productCollection->getSelect()->limit($limit);
        }

        return $productCollection->getItems();
    }

    /**
     * Get invert-related product ids
     *
     * @param ProductInterface $product
     * @return array
     */
    private function getInvertRelatedProductsIds(ProductInterface $product): array
    {
        $relatedLinkModel = $this->linkTypeModelFactory->create();
        $relatedLinkModel->setLinkTypeId(Link::LINK_TYPE_RELATED);
        $linkCollection = $this->linkCollectionFactory->create();
        $linkCollection->setLinkModel($relatedLinkModel);
        $linkCollection->addLinkTypeIdFilter();
        $linkCollection->addFieldToFilter('linked_product_id', ['eq' => $product->getId()]);

        return $linkCollection->getColumnValues('product_id');
    }

    /**
     * Return product id field name - entity_id|row_id
     *
     * @return string
     */
    private function getProductEntityIdField(): string
    {
        return $this->metadataPool->getMetadata(ProductInterface::class)->getLinkField();
    }
}
