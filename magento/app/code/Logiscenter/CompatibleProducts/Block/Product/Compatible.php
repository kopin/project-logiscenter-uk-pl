<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Logiscenter\CompatibleProducts\Block\Product;

use Logiscenter\CompatibleProducts\Model\ResourceModel\Product\Compatible as CompatibleProducts;
use Logiscenter\CompatibleProductsConfig\Api\Config\CompatibleProductsModuleStatusInterface;
use Magento\Catalog\Helper\Data;
use Magento\Framework\Stdlib\ArrayUtils;
use Magento\Catalog\Block\Product\View\AbstractView;
use Magento\Catalog\Block\Product\Context;

/**
 * Block to provide compatible products.
 */
class Compatible extends AbstractView
{
    /** @var CompatibleProducts */
    private $compatibleProductsProvider;

    /** @var Data */
    private $productHelper;

    /** @var CompatibleProductsModuleStatusInterface */
    private $status;

    /**
     * @param ArrayUtils                              $arrayUtils
     * @param Context                                 $context
     * @param CompatibleProducts                      $compatibleProductsProvider
     * @param Data                                    $productHelper
     * @param CompatibleProductsModuleStatusInterface $status
     * @param array                                   $data
     */
    public function __construct(
        ArrayUtils $arrayUtils,
        Context $context,
        CompatibleProducts $compatibleProductsProvider,
        Data $productHelper,
        CompatibleProductsModuleStatusInterface $status,
        array $data = []
    ) {
        $this->compatibleProductsProvider = $compatibleProductsProvider;
        $this->productHelper = $productHelper;
        $this->status = $status;

        parent::__construct(
            $context,
            $arrayUtils,
            $data
        );
    }



    /**
     * Get compatible products for current product
     *
     * @return array
     */
    public function getCompatibleProducts(): array
    {
        if (!$this->status->isEnabled()) {
            return [];
        }

        $product = $this->productHelper->getProduct();

        return $this->compatibleProductsProvider->getProducts($product, 20);
    }
}
