<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\DeletedUrlConfig\Config;

use Logiscenter\DeletedUrlConfig\Api\Config\DeletedUrlEnvironmentConfigInterface;
use Logiscenter\DeletedUrlConfig\Config\Db\DbDeletedUrlEnvironmentConfig;

/**
 * Class DeletedUrlEnvironmentConfig.
 *
 * This is not an api class, implementation may change at any time.
 * @see DeletedUrlEnvironmentConfigInterface, use this to retrieve config.
 *
 * For example, it now relies on DbDeletedUrlEnvironmentConfig to read information from database,
 * but this implementation can be changed when needed, without prior notice.
 */
class DeletedUrlEnvironmentConfig extends DbDeletedUrlEnvironmentConfig implements
    DeletedUrlEnvironmentConfigInterface
{
}
