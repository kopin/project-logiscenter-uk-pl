<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\DeletedUrlConfig\Config;

use Logiscenter\DeletedUrlConfig\Api\Config\DeletedUrlModuleStatusInterface;
use Logiscenter\DeletedUrlConfig\Api\Config\DeletedUrlStatusConfigInterface;

/**
 * Class DeletedUrlModuleStatus.
 *
 * This is not an api class, implementation may change at any time.
 * @see DeletedUrlModuleStatusInterface, use this to retrieve config.
 *
 * For example, it now relies on DeletedUrlStatusConfigInterface to read config,
 * but this implementation can be changed when needed, without prior notice.
 */
class DeletedUrlModuleStatus implements DeletedUrlModuleStatusInterface
{
    /**
     * @var DeletedUrlStatusConfigInterface
     */
    private $statusConfig;

    /**
     * DeletedUrlModuleStatus constructor.
     * @param DeletedUrlStatusConfigInterface $statusConfig
     */
    public function __construct(
        DeletedUrlStatusConfigInterface $statusConfig
    ) {
        $this->statusConfig = $statusConfig;
    }

    /**
     * Compare boolean status, using optionally supplied data
     *
     * @param bool $expectedStatusValue
     * @param int|string|null $storeId
     * @return bool
     */
    public function isBooleanStatus(
        bool $expectedStatusValue,
        $storeId = null
    ): bool {
        return $this->statusConfig->isBooleanStatus($expectedStatusValue, $storeId);
    }

    /**
     * Read boolean status, using optionally supplied data
     *
     * @param int|string|null $storeId
     * @return bool
     */
    public function isEnabled($storeId = null): bool
    {
        return $this->statusConfig->isEnabled($storeId);
    }
}
