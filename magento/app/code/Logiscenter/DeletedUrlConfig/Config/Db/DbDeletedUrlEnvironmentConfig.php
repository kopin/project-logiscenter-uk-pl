<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\DeletedUrlConfig\Config\Db;

use Interactiv4\ConfigHelper\Api\Config\Db\DbConfigHelperInterface;
use Interactiv4\ConfigHelper\Api\Config\Db\Provider\DbConfigGroupPathProviderTrait;
use Interactiv4\ConfigHelper\Api\Config\Db\Provider\DbConfigSectionPathProviderTrait;
use Interactiv4\ConfigHelper\Model\Config\Db\DbConfigHelper;
use Interactiv4\Environment\Api\Config\Db\AllowedEnvironmentsInterface;
use Interactiv4\Environment\Api\Config\Db\AllowedEnvironmentsTrait;
use Interactiv4\EnvironmentConfig\Api\Config\Db\Provider\EnvironmentGroupPathProviderInterface;
use Logiscenter\DeletedUrlConfig\Api\Config\Db\Provider\DeletedUrlSectionPathProviderInterface;

/**
 * Class DbDeletedUrlEnvironmentConfig.
 *
 * This is not an api class, implementation may change at any time.
 * @see \Logiscenter\DeletedUrlConfig\Api\Config\DeletedUrlEnvironmentConfigInterface,
 * use this to retrieve config.
 */
class DbDeletedUrlEnvironmentConfig extends DbConfigHelper implements
    DeletedUrlSectionPathProviderInterface,
    EnvironmentGroupPathProviderInterface,
    AllowedEnvironmentsInterface,
    DbConfigHelperInterface
{
    use DbConfigSectionPathProviderTrait;
    use DbConfigGroupPathProviderTrait;
    use AllowedEnvironmentsTrait;
}
