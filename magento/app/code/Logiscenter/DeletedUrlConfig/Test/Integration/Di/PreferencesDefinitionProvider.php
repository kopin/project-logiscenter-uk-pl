<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\DeletedUrlConfig\Test\Integration\Di;

use Interactiv4\Framework\Api\Di\Definition\DefinitionPoolInterface;
use Interactiv4\Framework\Api\Di\Definition\DefinitionPoolProviderInterface;
use Interactiv4\Framework\Di\Definition\Definition;
use Interactiv4\Framework\Di\Definition\DefinitionPool;
use Interactiv4\Framework\Di\Definition\DefinitionName;
use Interactiv4\Framework\Di\Definition\DefinitionValue;

/**
 * Class PreferencesDefinitionProvider.
 *
 * Preferences definition as they appear in di configuration files.
 */
class PreferencesDefinitionProvider implements DefinitionPoolProviderInterface
{
    /**
     * @var DefinitionPoolInterface
     */
    private $definitionPool;

    /**
     * {@inheritdoc}
     */
    public function getDefinitionPool(): DefinitionPoolInterface
    {
        if (!isset($this->definitionPool)) {
            $this->definitionPool = new DefinitionPool(
                new Definition(
                    new DefinitionName(
                        'Logiscenter\\DeletedUrlConfig\\Api\\Config\\DeletedUrlModuleStatusInterface'
                    ),
                    new DefinitionValue(
                        \Logiscenter\DeletedUrlConfig\Api\Config\DeletedUrlModuleStatusInterface::class,
                        \Logiscenter\DeletedUrlConfig\Config\DeletedUrlModuleStatus::class,
                        (string) \Magento\Framework\App\Area::AREA_GLOBAL,
                        false,
                        null,
                        null
                    )
                ),
                new Definition(
                    new DefinitionName(
                        'Logiscenter\\DeletedUrlConfig\\Api\\Config\\DeletedUrlStatusConfigInterface'
                    ),
                    new DefinitionValue(
                        \Logiscenter\DeletedUrlConfig\Api\Config\DeletedUrlStatusConfigInterface::class,
                        \Logiscenter\DeletedUrlConfig\Config\DeletedUrlStatusConfig::class,
                        (string) \Magento\Framework\App\Area::AREA_GLOBAL,
                        false,
                        null,
                        null
                    )
                ),
                new Definition(
                    new DefinitionName(
                        'Logiscenter\\DeletedUrlConfig\\Api\\Config\\DeletedUrlEnvironmentConfigInterface'
                    ),
                    new DefinitionValue(
                        \Logiscenter\DeletedUrlConfig\Api\Config\DeletedUrlEnvironmentConfigInterface::class,
                        \Logiscenter\DeletedUrlConfig\Config\DeletedUrlEnvironmentConfig::class,
                        (string) \Magento\Framework\App\Area::AREA_GLOBAL,
                        false,
                        null,
                        null
                    )
                ),
                new Definition(
                    new DefinitionName(
                        'Logiscenter\\DeletedUrlConfig\\Api\\Config\\DeletedUrlConfigInterface'
                    ),
                    new DefinitionValue(
                        \Logiscenter\DeletedUrlConfig\Api\Config\DeletedUrlConfigInterface::class,
                        \Logiscenter\DeletedUrlConfig\Config\DeletedUrlConfig::class,
                        (string) \Magento\Framework\App\Area::AREA_GLOBAL,
                        false,
                        null,
                        null
                    )
                )
            );
        }

        return $this->definitionPool;
    }
}
