<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\GroupedPriceConfig\Api\Config\Db\Provider;

use Interactiv4\ConfigHelper\Api\Config\Db\Provider\DbConfigSectionPathProviderInterface;

/**
 * Interface GroupedPriceSectionPathProviderInterface.
 *
 * Section path provider for logiscenter_groupedpriceconfig section.
 *
 * @api
 */
interface GroupedPriceSectionPathProviderInterface extends DbConfigSectionPathProviderInterface
{
    public const SECTION_PATH = 'logiscenter_groupedpriceconfig';
}
