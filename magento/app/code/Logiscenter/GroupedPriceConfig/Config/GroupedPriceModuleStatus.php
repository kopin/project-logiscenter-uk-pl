<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\GroupedPriceConfig\Config;

use Logiscenter\GroupedPriceConfig\Api\Config\GroupedPriceModuleStatusInterface;
use Logiscenter\GroupedPriceConfig\Api\Config\GroupedPriceStatusConfigInterface;

/**
 * Class GroupedPriceModuleStatus.
 *
 * This is not an api class, implementation may change at any time.
 * @see GroupedPriceModuleStatusInterface, use this to retrieve config.
 *
 * For example, it now relies on GroupedPriceStatusConfigInterface to read config,
 * but this implementation can be changed when needed, without prior notice.
 */
class GroupedPriceModuleStatus implements GroupedPriceModuleStatusInterface
{
    /**
     * @var GroupedPriceStatusConfigInterface
     */
    private $statusConfig;

    /**
     * GroupedPriceModuleStatus constructor.
     * @param GroupedPriceStatusConfigInterface $statusConfig
     */
    public function __construct(
        GroupedPriceStatusConfigInterface $statusConfig
    ) {
        $this->statusConfig = $statusConfig;
    }

    /**
     * Compare boolean status, using optionally supplied data
     *
     * @param bool $expectedStatusValue
     * @param int|string|null $storeId
     * @return bool
     */
    public function isBooleanStatus(
        bool $expectedStatusValue,
        $storeId = null
    ): bool {
        return $this->statusConfig->isBooleanStatus($expectedStatusValue, $storeId);
    }

    /**
     * Read boolean status, using optionally supplied data
     *
     * @param int|string|null $storeId
     * @return bool
     */
    public function isEnabled($storeId = null): bool
    {
        return $this->statusConfig->isEnabled($storeId);
    }
}
