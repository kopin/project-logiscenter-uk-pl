<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\GroupedPriceConfig\Config;

use Logiscenter\GroupedPriceConfig\Api\Config\GroupedPriceEnvironmentConfigInterface;
use Logiscenter\GroupedPriceConfig\Config\Db\DbGroupedPriceEnvironmentConfig;

/**
 * Class GroupedPriceEnvironmentConfig.
 *
 * This is not an api class, implementation may change at any time.
 * @see GroupedPriceEnvironmentConfigInterface, use this to retrieve config.
 *
 * For example, it now relies on DbGroupedPriceEnvironmentConfig to read information from database,
 * but this implementation can be changed when needed, without prior notice.
 */
class GroupedPriceEnvironmentConfig extends DbGroupedPriceEnvironmentConfig implements
    GroupedPriceEnvironmentConfigInterface
{
}
