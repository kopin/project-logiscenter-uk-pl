<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\CustomerInvoice\Block\MyAccount;

use Logiscenter\CustomerInvoiceConfig\Api\Config\CustomerInvoiceModuleStatusInterface;
use Logiscenter\ErpInvoice\Model\ResourceModel\Invoice\Collection;
use Logiscenter\ErpInvoice\Model\ResourceModel\Invoice\CollectionFactory;
use Magento\Customer\Model\Session;
use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

/**
 * Customer invoice list block class
 */
class InvoiceList extends Template
{
    const FILE_DOWNLOAD_PATH = 'customer_invoice/customer/filedownload';

    /**
     * @var array|Session
     */
    private $customerSession;

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var UrlInterface
     */
    private $urlBuilder;

    /**
     * @var Collection
     */
    private $collection;

    /**
     * @var CustomerInvoiceModuleStatusInterface
     */
    private $moduleStatus;

    /**
     * @param Context                              $context
     * @param Session                              $customerSession
     * @param CollectionFactory                    $collectionFactory
     * @param UrlInterface                         $urlBuilder
     * @param CustomerInvoiceModuleStatusInterface $moduleStatus
     * @param array                                $data
     */
    public function __construct(
        Context $context,
        Session $customerSession,
        CollectionFactory $collectionFactory,
        UrlInterface $urlBuilder,
        CustomerInvoiceModuleStatusInterface $moduleStatus,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->customerSession = $customerSession;
        $this->urlBuilder = $urlBuilder;
        $this->collectionFactory = $collectionFactory;
        $this->moduleStatus = $moduleStatus;
    }

    /**
     * Return customer invoice items collection
     *
     * @return Collection
     */
    public function getCollection(): Collection
    {
        if ($this->collection !== null) {
            return $this->collection;
        }
        $customerId = $this->moduleStatus->isEnabled() ? (int)$this->customerSession->getCustomerId() : 0;
        //get values of current page
        $page = $this->getRequest()->getParam('p', 1);
        //get values of current limit
        $pageSize = $this->getRequest()->getParam('limit', 10);

        $collection = $this->collectionFactory->create()->addFilter(
            'customer_id',
            $customerId
        );

        $collection->setPageSize($pageSize);
        $collection->setCurPage($page);
        $this->collection = $collection;

        return $this->collection;
    }

    /**
     * Return file download url
     *
     * @param int $entityId
     *
     * @return string
     */
    public function getFileDownloadUrl(int $entityId): string
    {
        return $this->urlBuilder->getUrl(
            self::FILE_DOWNLOAD_PATH,
            ['entity_id' => $entityId]
        );
    }

    /**
     * @inheritDoc
     * phpcs:disable
     */
    //@codingStandardsIgnoreStart
    protected function _prepareLayout()
    {
        //phpcs:enable
        //@codingStandardsIgnoreEnd
        parent::_prepareLayout();
        if ($this->getCollection()) {
            $pager = $this->getLayout()->createBlock(
                \Magento\Theme\Block\Html\Pager::class,
                'invoices.history.pager'
            )->setCollection(
                $this->getCollection()
            );
            $this->setChild('pager', $pager);
            $this->getCollection()->load();
        }
        return $this;
    }

    /**
     * Get Pager child block output
     *
     * @return string
     */
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }
}
