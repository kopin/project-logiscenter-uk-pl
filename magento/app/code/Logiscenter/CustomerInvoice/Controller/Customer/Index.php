<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\CustomerInvoice\Controller\Customer;

use Logiscenter\CustomerInvoiceConfig\Api\Config\CustomerInvoiceModuleStatusInterface;
use Magento\Customer\Controller\AbstractAccount;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\HttpGetActionInterface;

/**
 * Customer invoice tab action class
 */
class Index extends AbstractAccount implements HttpGetActionInterface
{
    /**
     * @var CustomerInvoiceModuleStatusInterface
     */
    private $moduleStatus;

    /**
     * @param Context                              $context
     * @param CustomerInvoiceModuleStatusInterface $moduleStatus
     */
    public function __construct(Context $context, CustomerInvoiceModuleStatusInterface $moduleStatus)
    {
        parent::__construct($context);
        $this->moduleStatus = $moduleStatus;
    }

    /**
     * @inheritdoc
     */
    public function execute()
    {
        if (!$this->moduleStatus->isEnabled()) {
            $this->_redirect('customer/account/');
        }
        $this->_view->loadLayout();
        $this->_view->renderLayout();
    }
}
