<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\CustomerInvoice\Controller\Customer;

use Logiscenter\CustomerInvoiceConfig\Api\Config\CustomerInvoiceModuleStatusInterface;
use Logiscenter\ErpInvoice\Api\FileContentInterface;
use Logiscenter\ErpInvoice\Api\InvoiceRepositoryInterface;
use Magento\Customer\Controller\AbstractAccount;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\ObjectManager\ContextInterface;

/**
 * Customer invoice file download action class
 */
class FileDownload extends AbstractAccount implements HttpGetActionInterface
{
    /**
     * @var FileFactory
     */
    private $fileFactory;

    /**
     * @var FileContentInterface
     */
    private $fileContent;

    /**
     * @var Session
     */
    private $customerSession;

    /**
     * @var ContextInterface
     */
    private $context;

    /**
     * @var InvoiceRepositoryInterface
     */
    private $invoiceRepository;

    /**
     * @var CustomerInvoiceModuleStatusInterface
     */
    private $moduleStatus;

    /**
     * @param Context                              $context
     * @param Session                              $customerSession
     * @param FileFactory                          $fileFactory
     * @param FileContentInterface                 $fileContent
     * @param InvoiceRepositoryInterface           $invoiceRepository
     * @param CustomerInvoiceModuleStatusInterface $moduleStatus
     */
    public function __construct(
        Context $context,
        Session $customerSession,
        FileFactory $fileFactory,
        FileContentInterface $fileContent,
        InvoiceRepositoryInterface $invoiceRepository,
        CustomerInvoiceModuleStatusInterface $moduleStatus
    ) {
        parent::__construct($context);
        $this->context = $context;
        $this->fileFactory = $fileFactory;
        $this->fileContent = $fileContent;
        $this->customerSession = $customerSession;
        $this->invoiceRepository = $invoiceRepository;
        $this->moduleStatus = $moduleStatus;
    }

    /**
     * @inheritDoc
     */
    public function execute()
    {
        if (!$this->moduleStatus->isEnabled()) {
            return $this->context->getResultRedirectFactory()->create()->setPath('customer/account/');
        }
        $entityId = (int)$this->_request->getParam('entity_id');
        if (!$entityId) {
            return $this->context->getResultRedirectFactory()->create()->setPath('*/*');
        }
        try {
            $invoice = $this->invoiceRepository->get($entityId);
        } catch (NoSuchEntityException $exception) {
            return $this->context->getResultRedirectFactory()->create()->setPath('*/*');
        }
        $fileContent = $this->fileContent->getByFileNameAndCustomerId(
            $invoice->getFile(),
            (int)$this->customerSession->getCustomerId()
        );

        if (!$fileContent) {
            return $this->context->getResultRedirectFactory()->create()->setPath('*/*');
        }
        $fileContent = ['type' => 'string', 'value' => $fileContent, 'rm' => true];

        return $this->fileFactory->create(
            $invoice->getFile(),
            $fileContent,
            DirectoryList::VAR_DIR,
            'application/pdf'
        );
    }
}
