<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ProductDownloadsConfig\Model\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;
use MageWorx\Downloads\Model\Section;

/**
 * Source model for sections config
 */
class Sections implements OptionSourceInterface
{
    /**
     * @var Section
     */
    private $sectionsList;

    /**
     * @param Section $sectionsList
     */
    public function __construct(Section $sectionsList)
    {
        $this->sectionsList = $sectionsList;
    }

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return $this->sectionsList->getSectionList();
    }
}
