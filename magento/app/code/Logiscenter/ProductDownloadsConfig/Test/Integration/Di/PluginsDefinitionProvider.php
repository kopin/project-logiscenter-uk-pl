<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ProductDownloadsConfig\Test\Integration\Di;

use Interactiv4\Framework\Api\Di\Definition\DefinitionPoolInterface;
use Interactiv4\Framework\Api\Di\Definition\DefinitionPoolProviderInterface;
use Interactiv4\Framework\Di\Definition\Definition;
use Interactiv4\Framework\Di\Definition\DefinitionPool;
use Interactiv4\Framework\Di\Definition\DefinitionName;
use Interactiv4\Framework\Di\Definition\DefinitionValue;

/**
 * Class PluginsDefinitionProvider.
 *
 * Plugins definition as they appear in di configuration files.
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class PluginsDefinitionProvider implements DefinitionPoolProviderInterface
{
    /**
     * @var DefinitionPoolInterface
     */
    private $definitionPool;

    /**
     * {@inheritdoc}
     *
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function getDefinitionPool(): DefinitionPoolInterface
    {
        if (!isset($this->definitionPool)) {
            $this->definitionPool = new DefinitionPool(
	new Definition(
	new DefinitionName(
	'Logiscenter\\ProductDownloadsConfig\\Plugin\\Api\\Status\\CheckProductDownloadsEnvironmentPlugin',
		'afterIsEnabled'
	),
		new DefinitionValue(
	\Logiscenter\ProductDownloadsConfig\Api\Config\ProductDownloadsModuleStatusInterface::class,
		\Logiscenter\ProductDownloadsConfig\Plugin\Api\Status\CheckProductDownloadsEnvironmentPlugin::class,
		(string) \Magento\Framework\App\Area::AREA_GLOBAL,
		false,
		10,
		null
	)
	)
	);
        }

        return $this->definitionPool;
    }
}
