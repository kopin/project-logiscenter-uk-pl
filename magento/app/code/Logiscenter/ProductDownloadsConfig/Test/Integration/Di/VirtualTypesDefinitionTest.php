<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ProductDownloadsConfig\Test\Integration\Di;

use Interactiv4\Framework\Api\Di\Definition\DefinitionPoolInterface;
use Interactiv4\Framework\Api\Di\Definition\DefinitionPoolProviderInterface;
use Interactiv4\Framework\Api\Di\DiDefinitionTestInterface;
use Interactiv4\Framework\Api\Di\VirtualTypesDefinitionTestTrait;
use PHPUnit\Framework\TestCase;

/**
 * Class VirtualTypesDefinitionTest.
 *
 * Test virtual types are defined in a specific way.
 *
 * @internal
 */
class VirtualTypesDefinitionTest extends TestCase implements DiDefinitionTestInterface, DefinitionPoolProviderInterface
{
    use VirtualTypesDefinitionTestTrait;

    /**
     * @var DefinitionPoolProviderInterface
     */
    private static $definitionProvider;

    /**
     * {@inheritdoc}
     */
    public static function tearDownAfterClass()
    {
        self::$definitionProvider = null;
        parent::tearDownAfterClass();
    }

    /**
     * {@inheritdoc}
     */
    public function getDefinitionPool(): DefinitionPoolInterface
    {
        self::$definitionProvider = self::$definitionProvider ?? new VirtualTypesDefinitionProvider();

        return self::$definitionProvider->getDefinitionPool();
    }
}
