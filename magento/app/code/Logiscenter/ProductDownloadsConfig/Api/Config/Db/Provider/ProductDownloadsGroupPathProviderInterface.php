<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ProductDownloadsConfig\Api\Config\Db\Provider;

use Interactiv4\ConfigHelper\Api\Config\Db\Provider\DbConfigGroupPathProviderInterface;

/**
 * Interface ProductDownloadsGroupPathProviderInterface.
 *
 * Group path provider for productdownloads group.
 *
 * @api
 */
interface ProductDownloadsGroupPathProviderInterface extends DbConfigGroupPathProviderInterface
{
    public const GROUP_PATH = 'productdownloads';
}
