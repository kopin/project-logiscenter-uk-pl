<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ProductDownloadsConfig\Api\Config;

/**
 * Interface ProductDownloadsConfigInterface.
 *
 * Config provider interface
 *
 * @api
 */
interface ProductDownloadsConfigInterface
{
    /**
     * @param int|string|null $storeId
     *
     * @return int
     */
    public function getSection($storeId = null): int;

    /**
     * @param int|string|null $storeId
     *
     * @return string
     */
    public function getDatasheetPrefix($storeId = null): string;

    /**
     * @param int|string|null $storeId
     *
     * @return string
     */
    public function getDocumentFolder($storeId = null): string;
}
