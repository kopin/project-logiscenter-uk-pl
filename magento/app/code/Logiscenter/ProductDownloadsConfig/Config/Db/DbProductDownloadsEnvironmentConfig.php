<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ProductDownloadsConfig\Config\Db;

use Interactiv4\ConfigHelper\Api\Config\Db\DbConfigHelperInterface;
use Interactiv4\ConfigHelper\Api\Config\Db\Provider\DbConfigGroupPathProviderTrait;
use Interactiv4\ConfigHelper\Api\Config\Db\Provider\DbConfigSectionPathProviderTrait;
use Interactiv4\ConfigHelper\Model\Config\Db\DbConfigHelper;
use Interactiv4\Environment\Api\Config\Db\AllowedEnvironmentsInterface;
use Interactiv4\Environment\Api\Config\Db\AllowedEnvironmentsTrait;
use Interactiv4\EnvironmentConfig\Api\Config\Db\Provider\EnvironmentGroupPathProviderInterface;
use Logiscenter\ProductDownloadsConfig\Api\Config\Db\Provider\ProductDownloadsSectionPathProviderInterface;

/**
 * Class DbProductDownloadsEnvironmentConfig.
 *
 * This is not an api class, implementation may change at any time.
 * @see \Logiscenter\ProductDownloadsConfig\Api\Config\ProductDownloadsEnvironmentConfigInterface,
 * use this to retrieve config.
 */
class DbProductDownloadsEnvironmentConfig extends DbConfigHelper implements
    ProductDownloadsSectionPathProviderInterface,
    EnvironmentGroupPathProviderInterface,
    AllowedEnvironmentsInterface,
    DbConfigHelperInterface
{
    use DbConfigSectionPathProviderTrait;
    use DbConfigGroupPathProviderTrait;
    use AllowedEnvironmentsTrait;
}
