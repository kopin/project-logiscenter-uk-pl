<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ProductDownloadsConfig\Config\Db;

use Interactiv4\ConfigHelper\Api\Config\Db\DbConfigHelperInterface;
use Interactiv4\ConfigHelper\Api\Config\Db\Provider\DbConfigGroupPathProviderTrait;
use Interactiv4\ConfigHelper\Api\Config\Db\Provider\DbConfigSectionPathProviderTrait;
use Interactiv4\ConfigHelper\Model\Config\Db\DbConfigHelper;
use Logiscenter\ProductDownloadsConfig\Api\Config\Db\Provider\ProductDownloadsGroupPathProviderInterface;
use Logiscenter\ProductDownloadsConfig\Api\Config\Db\Provider\ProductDownloadsSectionPathProviderInterface;

/**
 * Class DbProductDownloadsConfig.
 *
 * This is not an api class, implementation may change at any time.
 * @see \Logiscenter\ProductDownloadsConfig\Api\Config\ProductDownloadsConfigInterface,
 * use this to retrieve config.
 */
class DbProductDownloadsConfig extends DbConfigHelper implements
    ProductDownloadsSectionPathProviderInterface,
    ProductDownloadsGroupPathProviderInterface,
    DbConfigHelperInterface
{
    use DbConfigSectionPathProviderTrait;
    use DbConfigGroupPathProviderTrait;

    private const XML_FIELD_SECTION = 'section';

    private const XML_FIELD_DATASHEET_PREFIX = 'data_sheet_name_prefix';

    private const XML_FIELD_DOCUMENTS_SOURCE_FOLDER = 'documents_folder';

    /**
     * @param int|string|null $storeId
     *
     * @return bool
     */
    public function getSection($storeId = null): int
    {
        return (int)$this->getConfig(self::XML_FIELD_SECTION, $storeId);
    }

    /**
     * @param int|string|null $storeId
     *
     * @return string
     */
    public function getDocumentFolder($storeId = null): string
    {
        return (string)$this->getConfig(self::XML_FIELD_DOCUMENTS_SOURCE_FOLDER, $storeId);
    }

    /**
     * @param int|string|null $storeId
     *
     * @return string
     */
    public function getDatasheetPrefix($storeId = null): string
    {
        return (string)$this->getConfig(self::XML_FIELD_DATASHEET_PREFIX, $storeId);
    }
}
