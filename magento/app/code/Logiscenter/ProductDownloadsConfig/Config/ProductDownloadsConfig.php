<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ProductDownloadsConfig\Config;

use Logiscenter\ProductDownloadsConfig\Api\Config\ProductDownloadsConfigInterface;
use Logiscenter\ProductDownloadsConfig\Config\Db\DbProductDownloadsConfig;

/**
 * Class ProductDownloadsConfig.
 *
 * This is not an api class, implementation may change at any time.
 * @see ProductDownloadsConfigInterface, use this to retrieve config.
 *
 * For example, it now relies on DbProductDownloadsConfig to read information from database,
 * but this implementation can be changed when needed, without prior notice.
 */
class ProductDownloadsConfig extends DbProductDownloadsConfig implements
    ProductDownloadsConfigInterface
{
}
