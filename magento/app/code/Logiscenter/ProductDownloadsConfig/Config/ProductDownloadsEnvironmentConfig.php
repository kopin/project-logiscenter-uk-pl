<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ProductDownloadsConfig\Config;

use Logiscenter\ProductDownloadsConfig\Api\Config\ProductDownloadsEnvironmentConfigInterface;
use Logiscenter\ProductDownloadsConfig\Config\Db\DbProductDownloadsEnvironmentConfig;

/**
 * Class ProductDownloadsEnvironmentConfig.
 *
 * This is not an api class, implementation may change at any time.
 * @see ProductDownloadsEnvironmentConfigInterface, use this to retrieve config.
 *
 * For example, it now relies on DbProductDownloadsEnvironmentConfig to read information from database,
 * but this implementation can be changed when needed, without prior notice.
 */
class ProductDownloadsEnvironmentConfig extends DbProductDownloadsEnvironmentConfig implements
    ProductDownloadsEnvironmentConfigInterface
{
}
