<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Logiscenter\ProductDownloadsConfig\Config;

use Logiscenter\ProductDownloadsConfig\Api\Config\ProductDownloadsModuleStatusInterface;
use Logiscenter\ProductDownloadsConfig\Api\Config\ProductDownloadsStatusConfigInterface;

/**
 * Class ProductDownloadsModuleStatus.
 *
 * This is not an api class, implementation may change at any time.
 * @see ProductDownloadsModuleStatusInterface, use this to retrieve config.
 *
 * For example, it now relies on ProductDownloadsStatusConfigInterface to read config,
 * but this implementation can be changed when needed, without prior notice.
 */
class ProductDownloadsModuleStatus implements ProductDownloadsModuleStatusInterface
{
    /**
     * @var ProductDownloadsStatusConfigInterface
     */
    private $statusConfig;

    /**
     * ProductDownloadsModuleStatus constructor.
     * @param ProductDownloadsStatusConfigInterface $statusConfig
     */
    public function __construct(
        ProductDownloadsStatusConfigInterface $statusConfig
    ) {
        $this->statusConfig = $statusConfig;
    }

    /**
     * Compare boolean status, using optionally supplied data
     *
     * @param bool $expectedStatusValue
     * @param int|string|null $storeId
     * @return bool
     */
    public function isBooleanStatus(
        bool $expectedStatusValue,
        $storeId = null
    ): bool {
        return $this->statusConfig->isBooleanStatus($expectedStatusValue, $storeId);
    }

    /**
     * Read boolean status, using optionally supplied data
     *
     * @param int|string|null $storeId
     * @return bool
     */
    public function isEnabled($storeId = null): bool
    {
        return $this->statusConfig->isEnabled($storeId);
    }
}
