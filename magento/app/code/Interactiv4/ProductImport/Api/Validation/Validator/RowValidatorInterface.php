<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImport\Api\Validation\Validator;

interface RowValidatorInterface
{
    /**
     * @param array $row
     *
     * @return bool
     */
    public function isValid(array $row): bool;

    /**
     * @return array
     */
    public function getErrors(): array;

    /**
     * @param $importMode
     *
     * @return bool
     */
    public function shouldValidate($importMode): bool;
}
