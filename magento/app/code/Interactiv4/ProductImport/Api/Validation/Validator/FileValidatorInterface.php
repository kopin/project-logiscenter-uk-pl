<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImport\Api\Validation\Validator;

interface FileValidatorInterface
{
    /**
     * @param array $headers
     * @param array $rows
     *
     * @return bool
     */
    public function isValid(array &$headers, array $rows): bool;

    /**
     * @return array
     */
    public function getErrors(): array;

    /**
     * @param $importMode
     *
     * @return bool
     */
    public function shouldValidate($importMode): bool;
}
