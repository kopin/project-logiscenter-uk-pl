<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImport\Api\Validation;

interface ValidationResultInterface
{
    /**
     * @return bool
     */
    public function isValid(): bool;

    /**
     * @return bool
     */
    public function isValidFile(): bool;

    /**
     * @param bool $validFile
     */
    public function setValidFile(bool $validFile): void;

    /**
     * @return array
     */
    public function getValidatedRows(): array;

    /**
     * @param int   $rowNum
     * @param array $row
     */
    public function addValidatedRow(int $rowNum, array $row): void;

    /**
     * @return array
     */
    public function getInvalidatedRows(): array;

    /**
     * @param int   $rowNum
     * @param array $row
     */
    public function addInvalidatedRow(int $rowNum, array $row): void;

    /**
     * @return array
     */
    public function getRowErrorsMsg(): array;

    /**
     * @param string $msg
     */
    public function addRowErrorMsg(string $msg): void;

    /**
     * @return array
     */
    public function getFileErrorsMsg(): array;

    /**
     * @param string $msg
     */
    public function addFileErrorMsg(string $msg): void;

    /**
     * @return int
     */
    public function getTotalRows(): int;

    /**
     * @param $totalRows
     *
     * @return int
     */
    public function setTotalRows($totalRows): int;

    /**
     * @return int
     */
    public function getTotalValidRows(): int;

    /**
     * @return int
     */
    public function getTotalErrorRows(): int;

    /**
     * @return int
     */
    public function getTotalNewRows(): int;

    /**
     * @return int
     */
    public function getTotalUpdatedRows(): int;
}
