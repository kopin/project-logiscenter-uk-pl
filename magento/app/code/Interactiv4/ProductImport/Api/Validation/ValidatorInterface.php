<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImport\Api\Validation;

interface ValidatorInterface
{
    /**
     * @return string
     */
    public function getImportMode(): string;

    /**
     * @param string $importMode
     *
     * @return ValidatorInterface
     */
    public function setImportMode(string $importMode): ValidatorInterface;

    /**
     * @param string $filename
     *
     * @return ValidatorInterface
     */
    public function validateFile(string $filename): ValidatorInterface;

    /**
     * @param array $headers
     * @param array $rows
     *
     * @return ValidatorInterface
     */
    public function validate(array $headers, array $rows): ValidatorInterface;

    /**
     * @return array
     */
    public function getHeadersInformationValues(): array;

    /**
     * @param $headers
     */
    public function validateHeaders($headers): void;

    /**
     * @return ValidationResultInterface
     */
    public function getResult(): ValidationResultInterface;
}
