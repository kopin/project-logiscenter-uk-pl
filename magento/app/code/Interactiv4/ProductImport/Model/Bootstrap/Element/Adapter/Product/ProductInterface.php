<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImport\Model\Bootstrap\Element\Adapter\Product;

use Interactiv4\ImportExport\Model\Bootstrap\Element\Adapter\Product\ProductInterface as ImportExportProductInterface;

/**
 * Interface ProductInterface.
 */
interface ProductInterface extends ImportExportProductInterface
{
    public const INTERNAL_PRODUCT_TYPE = 'type';

    public const PRICE_INFO = 'priceInfo';

    public const COLUMN_STORE_CODE = 'store_code';

    // Stock const
    public const COLUMN_MIN_SALE_QTY = 'min_sale_qty';

    public const COLUMN_USE_CONFIG_MIN_SALE_QTY = 'use_config_min_sale_qty';

    public const COLUMN_ENABLE_QTY_INCREMENTS = 'enable_qty_increments';

    public const COLUMN_USE_CONFIG_QTY_INCREMENTS = 'use_config_qty_increments';

    public const COLUMN_USE_CONFIG_ENABLE_QTY_INC = 'use_config_enable_qty_inc';

    // Price const
    public const COLUMN_SPECIAL_FROM = 'special_from_date';

    public const COLUMN_SPECIAL_TO = 'special_to_date';

    public const COLUMN_SPECIAL_PRICE = 'special_price';

    public const COLUMN_COST = 'cost';

    public const COLUMN_MSRP = 'msrp';

    public const COLUMN_MSRP_ENABLED = 'msrp_enabled';
    /**
     * 0 = Use config, 1 = On Gesture, 2 = In Cart, 3 = Before Order Confirmation.
     */
    public const COLUMN_MSRP_DISPLAY_ACTUAL_PRICE_TYPE = 'msrp_display_actual_price_type';

    // Grouped Associated Child
    public const COLUMN_ASSOCIATED_SKUS = 'grouped_associated_child';

    // Relations: related, cross sell and up sell
    public const COLUMN_PRODUCT_LINK = 'product_link';

    //Media const
    public const COLUMN_ADDITIONAL_IMAGES = 'additional_images';

    // Url key
    public const COLUMN_URL_KEY_CREATE_REDIRECT = 'url_key_create_redirect';
}
