<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImport\Model\Bootstrap\Element\Connection\Method;

use Interactiv4\ImportExport\Api\Bootstrap\Element\Connection\MethodInterface;

class GetFile implements MethodInterface
{
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return '';
    }

    /**
     * {@inheritdoc}
     */
    public function getParameters($data)
    {
        return [
            'outFile' => $data['filePath'],
        ];
    }
}
