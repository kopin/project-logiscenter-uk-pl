<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImport\Model\Bootstrap\Element\Transformation;

use Interactiv4\ImportExport\Api\Bootstrap\Element\Transformation\TransformationInterface;
use Interactiv4\ImportExport\Model\Bootstrap\Config;
use Interactiv4\ProductImport\Api\Validation\ValidatorInterfaceFactory;
use Magento\Framework\Validation\ValidationException;
use Magento\ImportExport\Model\Import;

class Validation implements TransformationInterface
{
    private const ALLOWED_MODES = [
        Import::BEHAVIOR_APPEND,
        Import::BEHAVIOR_DELETE,
    ];

    /**
     * @var ValidatorInterfaceFactory
     */
    private $validatorFactory;

    /**
     * Validation constructor.
     *
     * @param ValidatorInterfaceFactory $validatorFactory
     */
    public function __construct(
        ValidatorInterfaceFactory $validatorFactory
    ) {
        $this->validatorFactory = $validatorFactory;
    }

    /**
     * @param array $data
     *
     * @throws ValidationException
     *
     * @return array
     */
    public function process(array $data): array
    {
        $config = $data['config'];
        $importMode = $config->getInData('importMode') ?? '';
        if (!isset($data['mainSource']['rows']) || !\in_array($importMode, self::ALLOWED_MODES)) {
            return $data;
        }
        $rows = $data['mainSource']['rows'] ?? [];
        $headers = $data['mainSource']['headers'] ?? [];

        $validator = $this->validatorFactory->create();
        $result = $validator->setImportMode($importMode)->validate($headers, $rows)->getResult();
        if (!$result->isValid()) {
            $this->logInvalidResult($data['config'], $result);
            $msg = __('Import file is not valid and will not be processed, see log for further detail');
            throw new ValidationException($msg);
        }
        $this->logValidResult($config, $result);
        // After validation, we replace rows on mainSource in order to skip invalid rows and process only valid rows
        $data['mainSource']['rows'] = $result->getValidatedRows();
        $data['mainSource']['headersInfo'] = $validator->getHeadersInformationValues();

        return $data;
    }

    private function logInvalidResult(Config $config, $result): void
    {
        $this->log(
            $config,
            (string) __('ERROR: Import file is not valid and will not be processed, see details for more info:')
        );
        if ($result->getFileErrorsMsg()) {
            $this->log($config, (string) __('%1 File error(s):', \count($result->getFileErrorsMsg())));
        }
        foreach ($result->getFileErrorsMsg() as $error) {
            $this->log($config, (string) __($error));
        }
        if ($result->getTotalErrorRows()) {
            $this->log($config, (string) __('%1 Row(s) with error:', $result->getTotalErrorRows()));
        }
        foreach ($result->getRowErrorsMsg() as $error) {
            $this->log($config, (string) __($error));
        }
    }

    /**
     * @param Config $config
     * @param        $result
     */
    private function logValidResult(Config $config, $result): void
    {
        $this->log(
            $config,
            (string) __('Import file is valid and will be processed and rows with error will be skipped:')
        );
        if ($result->getTotalRows()) {
            $this->log(
                $config,
                (string) __('%1 row(s) found in import file.', $result->getTotalRows())
            );
        }
        $this->log($config, (string) __('%1 Valid Row(s)', $result->getTotalValidRows()));
        $this->log($config, (string) __('%1 rows for new products.', $result->getTotalNewRows()));
        $this->log($config, (string) __('%1 rows for existing products.', $result->getTotalUpdatedRows()));
        if ($result->getTotalErrorRows()) {
            $this->log($config, (string) __('%1 Invalid Row(s):', $result->getTotalErrorRows()));
        }
        foreach ($result->getRowErrorsMsg() as $error) {
            $this->log($config, (string) __($error));
        }
    }

    /**
     * @param Config $config
     * @param string $msg
     */
    private function log(Config $config, string $msg): void
    {
        $logFile = $config->getInData('logFile') ?? '';
        $config->log($logFile, 'VALIDATION: ' . $msg . PHP_EOL);
    }
}
