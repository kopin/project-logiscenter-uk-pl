<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImport\Model\Bootstrap\Element\Transformation;

use Interactiv4\ImportExport\Api\Bootstrap\Element\Transformation\TransformationInterface;
use Interactiv4\ImportExport\Model\Bootstrap\Config;
use Interactiv4\ProductImport\Model\Bootstrap\Element\Adapter\Price as PriceAdapter;
use Interactiv4\ProductImport\Model\Bootstrap\Element\Adapter\Product\ProductInterface as ProductAdapter;
use Interactiv4\ProductImport\Model\Bootstrap\Element\Transformation\Product\ProductInterface as ProductTransformer;
use Magento\Catalog\Model\Product\Type as ProductType;
use Magento\Downloadable\Model\Product\Type as DownloadableType;
use Magento\ImportExport\Model\Import;
use Magento\Store\Api\Data\StoreInterface;
use Magento\Store\Model\StoreManagerInterface;

class Price implements TransformationInterface
{
    /**
     * Allowed product type_id for import the price.
     */
    public const ALLOWED_PRODUCT_TYPES = [
        ProductType::TYPE_SIMPLE,
        ProductType::TYPE_VIRTUAL,
        DownloadableType::TYPE_DOWNLOADABLE,
    ];

    private const ALLOWED_MODES = [
        Import::BEHAVIOR_APPEND,
    ];

    /**
     * Columns mapping to be applied in the format
     * ['source_column' => 'destination_column'].
     */
    public const COLUMNS_MAPPING = [
        ProductTransformer::COLUMN_PRICE => ProductAdapter::COLUMN_PRICE,
        ProductTransformer::COLUMN_SPECIAL_PRICE => ProductAdapter::COLUMN_SPECIAL_PRICE,
        ProductTransformer::COLUMN_SPECIAL_FROM => ProductAdapter::COLUMN_SPECIAL_FROM,
        ProductTransformer::COLUMN_SPECIAL_TO => ProductAdapter::COLUMN_SPECIAL_TO,
        ProductTransformer::COLUMN_COST => ProductAdapter::COLUMN_COST,
        ProductTransformer::COLUMN_MSRP => ProductAdapter::COLUMN_MSRP,
        ProductTransformer::COLUMN_MSRP_ENABLED => ProductAdapter::COLUMN_MSRP_ENABLED,
        ProductTransformer::COLUMN_MSRP_DISPLAY_ACTUAL_PRICE => ProductAdapter::COLUMN_MSRP_DISPLAY_ACTUAL_PRICE_TYPE,
    ];

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var StoreInterface[]
     */
    private $stores;

    /**
     * @var array
     */
    private $storesIdsByCode;

    /**
     * Price constructor.
     *
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        StoreManagerInterface $storeManager
    ) {
        $this->storeManager = $storeManager;
    }

    /**
     * Process price transformation.
     *
     * @param array $data
     *
     * @return array
     */
    public function process(array $data): array
    {
        $config = $data['config'];
        $importMode = $config->getInData('importMode') ?? '';
        if (!isset($data['mainSource']['rows']) || !\in_array($importMode, self::ALLOWED_MODES)) {
            return $data;
        }
        $result = [];
        $rows = $data['mainSource']['rows'];
        $chunks = \array_chunk($rows, PriceAdapter::BATCH_SIZE);
        foreach ($chunks as $chunk) {
            foreach ($chunk as $row) {
                try {
                    $productType = $row['current_product_type'] ?? $row[ProductTransformer::COLUMN_TYPE] ?? '';
                    if (!empty($productType) && !\in_array($productType, self::ALLOWED_PRODUCT_TYPES)) {
                        continue;
                    }
                    $sku = $row[ProductTransformer::COLUMN_SKU];
                    $row = $this->prepareData($row);
                    $row[ProductAdapter::STORE_INFO] = $this->preparePriceInfoByStores($row);
                    $priceInfo = $this->applyColumnNameTransformation($row);

                    if (!empty($priceInfo)) {
                        $result[] = [
                            ProductAdapter::COLUMN_SKU => $sku,
                            ProductAdapter::PRICE_INFO => $priceInfo,
                            ProductAdapter::STORE_INFO => $row[ProductAdapter::STORE_INFO],
                        ];
                    }
                } catch (\Exception $e) {
                    $this->log(
                        $config,
                        (string) __(
                            'Unexpected error processing price item %1: %2',
                            $sku,
                            $e->getMessage()
                        )
                    );
                }
            }
        }

        $data[PriceAdapter::TO_ADAPTER][PriceAdapter::getIdentified()] = $result;

        return $data;
    }

    /**
     * @param array $row
     *
     * @return array
     */
    private function prepareData(array $row): array
    {
        $isNew = $row[ProductTransformer::COLUMN_IS_NEW] ?? false;

        /* new products */
        if (true === $isNew) {
            $row[ProductAdapter::STORE_INFO] = [0];

            if ($this->noExistsOrEmpty($row, ProductTransformer::COLUMN_PRICE)) {
                $row[ProductTransformer::COLUMN_PRICE] = 0;
            }
        }

        /* existing products */
        if (false === $isNew) {
            //If msrp_enabled = 0, then msrp has to be removed
            if (
                isset($row[ProductTransformer::COLUMN_MSRP_ENABLED])
                && 0 == $row[ProductTransformer::COLUMN_MSRP_ENABLED]
            ) {
                $row[ProductTransformer::COLUMN_MSRP] = null;
            }
            // If special_price is empty, related special_from and special_to values has to be removed
            if ($this->existsAndNotValue($row, ProductTransformer::COLUMN_SPECIAL_PRICE)) {
                $row[ProductTransformer::COLUMN_SPECIAL_PRICE] = null;
                $row[ProductTransformer::COLUMN_SPECIAL_FROM] = null;
                $row[ProductTransformer::COLUMN_SPECIAL_TO] = null;
            }
        }

        return $row;
    }

    /**
     * @param $row
     *
     * @return array
     */
    private function preparePriceInfoByStores($row): array
    {
        $storeInfo = [];
        if ($this->existsAndHasValue($row, ProductTransformer::COLUMN_STORE_CODE)) {
            $storeId = $this->getStoreId($row[ProductTransformer::COLUMN_STORE_CODE]);
            if (!$storeId) {
                // Invalid store code will act as empty store code column or not existing column
                return [0];
            }
            $storeInfo = [$storeId];
        }

        if ($this->noExistsOrEmpty($row, ProductTransformer::COLUMN_STORE_CODE)) {
            $storeInfo = [0];
        }

        return $storeInfo;
    }

    private function applyColumnNameTransformation(array $row): array
    {
        $transformedRow = [];
        foreach (self::COLUMNS_MAPPING as $sourceColumn => $destinationColumn) {
            if ($this->existsAndHasValue($row, $sourceColumn)) {
                $transformedRow[$destinationColumn] = $row[$sourceColumn];
            }
        }

        return $transformedRow;
    }

    /**
     * @return StoreInterface[]
     */
    private function getStores(): array
    {
        if (!isset($this->stores)) {
            $this->stores = $this->storeManager->getStores();
        }

        return $this->stores;
    }

    /**
     * @param string $storeCode
     *
     * @return int
     */
    private function getStoreId(string $storeCode): int
    {
        if (!isset($this->storesIdsByCode[$storeCode])) {
            $stores = $this->getStores();
            foreach ($stores as $store) {
                if ($store->getCode() === $storeCode) {
                    $this->storesIdsByCode[$storeCode] = (int) $store->getId();
                    break;
                }
            }
        }

        return $this->storesIdsByCode[$storeCode] ?? 0;
    }

    /**
     * @param array  $row
     * @param string $column
     *
     * @return bool
     */
    private function existsAndHasValue(array $row, string $column): bool
    {
        return \array_key_exists($column, $row) && '' !== $row[$column];
    }

    /**
     * @param array  $row
     * @param string $column
     *
     * @return bool
     */
    private function existsAndNotValue(array $row, string $column): bool
    {
        return \array_key_exists($column, $row) && '' === $row[$column];
    }

    /**
     * @param array  $row
     * @param string $column
     *
     * @return bool
     */
    private function noExistsOrEmpty(array $row, string $column): bool
    {
        return !\array_key_exists($column, $row) || '' === $row[$column];
    }

    /**
     * @param Config $config
     * @param string $msg
     */
    private function log(Config $config, string $msg): void
    {
        $logFile = $config->getInData('logFile') ?? '';
        $config->log($logFile, 'PRICE: ' . $msg . PHP_EOL);
    }
}
