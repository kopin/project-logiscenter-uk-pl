<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImport\Model\Bootstrap\Element\Adapter;

use Interactiv4\ImportExport\Api\Bootstrap\Element\Adapter\AdapterInterface;
use Interactiv4\ImportExport\Model\Bootstrap\Config;
use Interactiv4\ImportExport\Model\TraitModel\Config as ConfigTrait;
use Interactiv4\ProductImport\Model\Bootstrap\Element\Adapter\Product\ProductInterface as ProductAdapter;
use Interactiv4\ProductImport\Model\Bootstrap\Element\Adapter\Stock\Data as DataStock;
use Magento\Framework\App\ResourceConnection;

class Stock implements AdapterInterface
{
    use ConfigTrait;

    private const IDENTIFIED = 'StockProduct';
    public const  BATCH_SIZE = 1000;

    /**
     * @var DataStock
     */
    private $dataStock;

    /**
     * @var ResourceConnection
     */
    private $resource;

    /**
     * @var \Magento\Framework\DB\Adapter\AdapterInterface
     */
    private $connection;

    /**
     * Stock constructor.
     *
     * @param DataStock          $dataStock
     * @param ResourceConnection $resource
     */
    public function __construct(
        DataStock          $dataStock,
        ResourceConnection $resource
    ) {
        $this->dataStock = $dataStock;
        $this->resource = $resource;
    }

    /**
     * {@inheritdoc}
     */
    // phpcs:disable
    public static function getIdentified()
    {
        // phpcs:enable
        return self::IDENTIFIED;
    }

    /**
     * {@inheritdoc}
     */
    public function process(array $data)
    {
        if (!isset($data[self::TO_ADAPTER][self::getIdentified()])) {
            return $data;
        }

        $config = $data['config'];
        $this->connection = $this->resource->getConnection();
        $items = $data[self::TO_ADAPTER][self::getIdentified()];
        $chunks = \array_chunk($items, self::BATCH_SIZE);
        $processedProductIds = $config->getOutData('processed_products') ?? [];
        foreach ($chunks as $chunk) {
            $skuToId = $this->getProductIdsBySkuList(\array_column($chunk, ProductAdapter::COLUMN_SKU));
            try {
                $newProcessedProductIds = [];
                foreach ($chunk as $key => &$item) {
                    $sku = $item[ProductAdapter::COLUMN_SKU];
                    if (!isset($skuToId[$sku])) {
                        unset($chunk[$key]);
                        $this->log(
                            $config,
                            (string)__('Sku %1 was not found for stock import', $sku)
                        );

                        continue;
                    }
                    $item[ProductAdapter::COLUMN_ENTITY_ID] = (int)$skuToId[$sku];

                    $newProcessedProductIds[] = $skuToId[$sku];
                }

                $this->dataStock->processItems($chunk);
                $processedProductIds += $newProcessedProductIds;
                $this->log(
                    $config,
                    (string)__(
                        'Success processed stock item for batch %1',
                        trim(implode(', ', array_keys($skuToId)), ',')
                    )
                );
            } catch (\Exception $e) {
                $this->log(
                    $config,
                    (string)__(
                        'Unexpected error processing stock batch %1: %2',
                        trim(implode(', ', array_keys($skuToId)), ','),
                        $e->getMessage()
                    )
                );
            }
        }
        $config->setOutData(\array_unique($processedProductIds), 'processed_products');

        return $data;
    }

    /**
     * @param array $skuList
     *
     * @return array
     */
    private function getProductIdsBySkuList(array $skuList): array
    {
        $select = $this->connection->select()->from('catalog_product_entity', ['sku', 'entity_id'])->where(
            'sku IN(?)',
            $skuList
        );

        return $this->connection->fetchPairs($select);
    }

    /**
     * @param Config $config
     * @param string $msg
     */
    private function log(Config $config, string $msg): void
    {
        $logFile = $config->getInData('logFile') ?? '';
        $config->log($logFile, 'STOCK SAVING: ' . $msg . PHP_EOL);
    }
}
