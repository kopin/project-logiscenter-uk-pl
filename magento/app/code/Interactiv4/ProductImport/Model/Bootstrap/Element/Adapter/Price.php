<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImport\Model\Bootstrap\Element\Adapter;

use Interactiv4\ImportExport\Api\Bootstrap\Element\Adapter\AdapterInterface;
use Interactiv4\ImportExport\Model\Bootstrap\Config;
use Interactiv4\ImportExport\Model\TraitModel\Config as ConfigTrait;
use Interactiv4\ProductImport\Model\Bootstrap\Element\Adapter\Price\Data as DataPrice;
use Interactiv4\ProductImport\Model\Bootstrap\Element\Adapter\Product\ProductInterface as ProductAdapter;
use Magento\Framework\App\ResourceConnection;

class Price implements AdapterInterface
{
    use ConfigTrait;

    private const IDENTIFIED = 'PriceProduct';
    public const  BATCH_SIZE = 1000;

    /**
     * @var DataPrice
     */
    private $dataPrice;

    /**
     * @var ResourceConnection
     */
    private $resource;

    /**
     * @var \Magento\Framework\DB\Adapter\AdapterInterface
     */
    private $connection;

    /**
     * Price constructor.
     *
     * @param DataPrice          $dataPrice
     * @param ResourceConnection $resource
     */
    public function __construct(
        DataPrice $dataPrice,
        ResourceConnection $resource
    ) {
        $this->dataPrice = $dataPrice;
        $this->resource = $resource;
    }

    /**
     * {@inheritdoc}
     */
    // phpcs:disable
    public static function getIdentified()
    {
        // phpcs:enable
        return self::IDENTIFIED;
    }

    /**
     * {@inheritdoc}
     */
    public function process(array $data)
    {
        if (!isset($data[self::TO_ADAPTER][self::getIdentified()])) {
            return $data;
        }

        $config = $data['config'];
        $this->connection = $this->resource->getConnection();
        $items = $data[self::TO_ADAPTER][self::getIdentified()];
        $chunks = \array_chunk($items, self::BATCH_SIZE);
        $processedProductIds = $config->getOutData('processed_products') ?? [];
        foreach ($chunks as $chunk) {
            $skuToId = $this->getProductIdsBySkuList(\array_column($chunk, ProductAdapter::COLUMN_SKU));
            foreach ($chunk as $item) {
                try {
                    $sku = $item[ProductAdapter::COLUMN_SKU];
                    if (!isset($skuToId[$sku])) { //Check if product exists
                        $this->log(
                            $config,
                            (string) __('Sku %1 was not found for price import', $sku)
                        );
                        continue;
                    }
                    $item[ProductAdapter::COLUMN_ENTITY_ID] = (int) $skuToId[$sku];
                    $id = $this->dataPrice->processItem($item);
                    if ($id) {
                        $this->log(
                            $config,
                            (string) __(
                                'Success processed price item %1',
                                $item[ProductAdapter::COLUMN_SKU]
                            )
                        );
                        $processedProductIds[] = $id;
                    }
                } catch (\Exception $e) {
                    $this->log(
                        $config,
                        (string) __(
                            'Unexpected error processing price item %1: %2',
                            $item[ProductAdapter::COLUMN_SKU],
                            $e->getMessage()
                        )
                    );
                }
            }
        }
        $config->setOutData(\array_unique($processedProductIds), 'processed_products');

        return $data;
    }

    /**
     * @param array $skuList
     *
     * @return array
     */
    private function getProductIdsBySkuList(array $skuList): array
    {
        $select = $this->connection->select()->from(
            'catalog_product_entity',
            ['sku', 'entity_id']
        )->where(
            'sku IN(?)',
            $skuList
        );

        return $this->connection->fetchPairs($select);
    }

    /**
     * @param Config $config
     * @param string $msg
     */
    private function log(Config $config, string $msg): void
    {
        $logFile = $config->getInData('logFile') ?? '';
        $config->log($logFile, 'PRICE SAVING: ' . $msg . PHP_EOL);
    }
}
