<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImport\Model\Bootstrap\Element\Adapter\Product\Data;

use Exception;
use Interactiv4\ProductImport\Model\Bootstrap\Element\Adapter\Product\ProductInterface as ProductAdapter;
use Magento\Catalog\Api\Data\ProductInterfaceFactory;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\ResourceModel\Product\Action;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\StateException;
use Magento\Store\Model\StoreManagerInterface;

class Product
{
    /**
     * @var Action
     */
    private $productAction;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var ProductInterfaceFactory
     */
    private $productFactory;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * Product constructor.
     *
     * @param ProductRepositoryInterface $productRepository
     * @param ProductInterfaceFactory    $productFactory
     * @param Action                     $productAction
     * @param StoreManagerInterface      $storeManager
     */
    public function __construct(
        ProductRepositoryInterface $productRepository,
        ProductInterfaceFactory $productFactory,
        Action $productAction,
        StoreManagerInterface $storeManager
    ) {
        $this->productRepository = $productRepository;
        $this->productFactory = $productFactory;
        $this->productAction = $productAction;
        $this->storeManager = $storeManager;
    }

    /**
     * @param array $item
     *
     * @throws Exception
     *
     * @return int
     */
    public function processItem(array $item): int
    {
        if (!isset($item[ProductAdapter::COLUMN_ENTITY_ID])) {
            return $this->create($item);
        }

        return $this->update($item);
    }

    /**
     * @param array $item
     *
     * @throws CouldNotSaveException
     * @throws InputException
     * @throws StateException
     *
     * @return int
     */
    private function create(array $item): int
    {
        $storeId = isset($item[ProductAdapter::STORE_INFO]) ? \reset($item[ProductAdapter::STORE_INFO]) : 0;
        unset($item[ProductAdapter::STORE_INFO]);
        $product = $this->productFactory->create();
        $product->setTypeId($item[ProductAdapter::INTERNAL_PRODUCT_TYPE]);
        $product->setAttributeSetId($item[ProductAdapter::COLUMN_ATTRIBUTE_SET]);
        $product->addData($item);
        if (isset($item[ProductAdapter::COLUMN_URL_KEY]) && !empty($item[ProductAdapter::COLUMN_URL_KEY])) {
            $product->setUrlKey($product->formatUrlKey($item[ProductAdapter::COLUMN_URL_KEY]));
        }
        $currentStore = $this->storeManager->getStore()->getId();
        try {
            $this->storeManager->setCurrentStore($storeId);
            $newProduct = $this->productRepository->save($product);
        } finally {
            $this->storeManager->setCurrentStore($currentStore);
        }

        return (int) $newProduct->getId();
    }

    /**
     * @param array $data
     *
     * @throws Exception
     *
     * @return int
     */
    private function update(array $data): int
    {
        $productId = (int) $data[ProductAdapter::COLUMN_ENTITY_ID];
        $storeInfo = $data[ProductAdapter::STORE_INFO];
        $urlKey = $data[ProductAdapter::COLUMN_URL_KEY] ?? null;
        $createRedirect = $data[ProductAdapter::COLUMN_URL_KEY_CREATE_REDIRECT] ?? null;

        if ($urlKey) {
            $product = $this->productRepository->get($data[ProductAdapter::COLUMN_SKU]);
            $this->processUrl($product, $product->formatUrlKey($urlKey), $createRedirect, $storeInfo);
        }

        unset(
            $data[ProductAdapter::COLUMN_ENTITY_ID],
            $data[ProductAdapter::COLUMN_SKU],
            $data[ProductAdapter::STORE_INFO],
            $data[ProductAdapter::INTERNAL_PRODUCT_TYPE],
            $data[ProductAdapter::COLUMN_ATTRIBUTE_SET],
            $data[ProductAdapter::COLUMN_URL_KEY],
            $data[ProductAdapter::COLUMN_URL_KEY_CREATE_REDIRECT]
        );
        if (empty($data)) {
            return 0;
        }

        foreach ($storeInfo as $store) {
            $this->productAction->updateAttributes(
                [$productId],
                $data,
                $store
            );
        }

        return $productId;
    }

    /**
     * @param $product
     * @param $urlKey
     * @param $createRedirect
     * @param $storeInfo
     */
    private function processUrl($product, $urlKey, $createRedirect, $storeInfo): void
    {
        foreach ($storeInfo as $storeId) {
            $product->setStoreId($storeId)
                ->setUrlKey($urlKey)
                ->setData('save_rewrites_history', (int) $createRedirect)
                ->save()
            ;
        }
    }
}
