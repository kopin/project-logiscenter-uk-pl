<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImport\Model\Bootstrap\Element\Adapter\Product;

use Interactiv4\ImportExport\Api\Bootstrap\Element\Adapter\AdapterInterface;
use Interactiv4\ImportExport\Model\Bootstrap\Config;
use Interactiv4\ImportExport\Model\TraitModel\Config as ConfigTrait;
use Interactiv4\ProductImport\Model\Bootstrap\Element\Adapter\Product\Data\Product;
use Interactiv4\ProductImport\Model\Bootstrap\Element\Adapter\Product\ProductInterface as ProductAdapter;
use Magento\Framework\App\ResourceConnection;

class Main implements AdapterInterface
{
    use ConfigTrait;

    private const IDENTIFIED = 'MainProduct';
    public const  BATCH_SIZE = 1000;

    /**
     * @var Product
     */
    private $productResource;

    /**
     * @var ResourceConnection
     */
    private $resource;

    /**
     * @var \Magento\Framework\DB\Adapter\AdapterInterface
     */
    private $connection;

    /**
     * Main constructor.
     *
     * @param Product            $productResource
     * @param ResourceConnection $resource
     */
    public function __construct(
        Product $productResource,
        ResourceConnection $resource
    ) {
        $this->productResource = $productResource;
        $this->resource = $resource;
    }

    /**
     * {@inheritdoc}
     */
    // phpcs:disable
    public static function getIdentified()
    {
        // phpcs:enable
        return self::IDENTIFIED;
    }

    /**
     * {@inheritdoc}
     */
    public function process(array $data)
    {
        if (!isset($data[self::TO_ADAPTER][self::getIdentified()])) {
            return $data;
        }

        $config = $data['config'];
        $this->connection = $this->resource->getConnection();
        $items = $data[self::TO_ADAPTER][self::getIdentified()];
        $chunks = \array_chunk($items, self::BATCH_SIZE);
        $processedProductIds = $config->getOutData('processed_products') ?? [];
        $processedProductIdsWithDocuments = [];
        foreach ($chunks as $chunk) {
            $skuToId = $this->getProductIdsBySkuList(\array_column($chunk, ProductAdapter::COLUMN_SKU));
            foreach ($chunk as $item) {
                try {
                    $sku = $item[ProductAdapter::COLUMN_SKU];
                    if (isset($skuToId[$sku])) {
                        $item[ProductAdapter::COLUMN_ENTITY_ID] = (int) $skuToId[$sku];
                    }
                    $id = $this->productResource->processItem($item);
                    if ($id) {
                        $this->log(
                            $config,
                            (string) __(
                                'Success processed product item %1',
                                $item[ProductAdapter::COLUMN_SKU]
                            )
                        );
                        if (isset($item['additional_links']) || isset($item['datasheet'])) {
                            $processedProductIdsWithDocuments[] = $id;
                        }
                        $processedProductIds[] = $id;
                    }
                } catch (\Exception $e) {
                    $this->log(
                        $config,
                        (string) __(
                            'Unexpected error saving product item %1: %2',
                            $item[ProductAdapter::COLUMN_SKU],
                            $e->getMessage()
                        )
                    );
                }
            }
        }
        $config->setOutData(\array_unique($processedProductIds), 'processed_products');
        $config->setOutData(\array_unique($processedProductIdsWithDocuments), 'processed_product_documents');

        return $data;
    }

    /**
     * @param array $skuList
     *
     * @return array
     */
    private function getProductIdsBySkuList(array $skuList): array
    {
        $select = $this->connection->select()->from(
            'catalog_product_entity',
            ['sku', 'entity_id']
        )->where(
            'sku IN(?)',
            $skuList
        );

        return $this->connection->fetchPairs($select);
    }

    /**
     * @param Config $config
     * @param string $msg
     */
    private function log(Config $config, string $msg): void
    {
        $logFile = $config->getInData('logFile') ?? '';
        $config->log($logFile, 'PRODUCT SAVING: ' . $msg . PHP_EOL);
    }
}
