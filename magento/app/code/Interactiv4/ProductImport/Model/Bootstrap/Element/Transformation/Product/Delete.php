<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImport\Model\Bootstrap\Element\Transformation\Product;

use Interactiv4\ImportExport\Api\Bootstrap\Element\Transformation\TransformationInterface;
use Interactiv4\ImportExport\Model\Bootstrap\Config;
use Interactiv4\ProductImport\Model\Bootstrap\Element\Adapter\Product\Delete as DeleteAdapter;
use Interactiv4\ProductImport\Model\Bootstrap\Element\Adapter\Product\ProductInterface as ProductAdapter;
use Magento\Catalog\Model\Product\Type as ProductType;
use Magento\Downloadable\Model\Product\Type as DownloadableType;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\GroupedProduct\Model\Product\Type\Grouped as GroupedType;
use Magento\ImportExport\Model\Import;

class Delete implements TransformationInterface
{
    private const ALLOWED_MODES = [
        Import::BEHAVIOR_DELETE,
    ];

    /**
     * Allowed product type_id for delete product.
     */
    public const ALLOWED_PRODUCT_TYPES = [
        ProductType::TYPE_SIMPLE,
        ProductType::TYPE_VIRTUAL,
        DownloadableType::TYPE_DOWNLOADABLE,
        GroupedType::TYPE_CODE,
    ];

    /**
     * @var ResourceConnection
     */
    private $resource;

    /**
     * @var AdapterInterface
     */
    private $connection;

    /**
     * Delete constructor.
     *
     * @param ResourceConnection $resource
     */
    public function __construct(
        ResourceConnection $resource
    ) {
        $this->resource = $resource;
    }

    /**
     * Process stock transformation.
     *
     * @param array $data
     *
     * @return array
     */
    public function process(array $data): array
    {
        $config = $data['config'];
        $importMode = $config->getInData('importMode') ?? '';
        if (!isset($data['mainSource']['rows']) || !\in_array($importMode, self::ALLOWED_MODES)) {
            return $data;
        }
        $result = [];
        $rows = $data['mainSource']['rows'];
        $this->connection = $this->resource->getConnection();
        $chunks = \array_chunk($rows, DeleteAdapter::BATCH_SIZE);
        $processedSkus = [];
        foreach ($chunks as $chunk) {
            $skuToId = $this->getProductIdsBySkuList(\array_column($chunk, ProductAdapter::COLUMN_SKU));
            foreach ($chunk as $row) {
                try {
                    $sku = $row[ProductAdapter::COLUMN_SKU];
                    if (\in_array($sku, $processedSkus)) {
                        $this->log(
                            $config,
                            (string) __(
                                'Sku %1 appeared in an earlier row of the file. Ignoring next occurrences for deletion',
                                $sku
                            )
                        );
                        continue;
                    }
                    if (!isset($skuToId[$sku])) { //Check if product exists
                        $this->log(
                            $config,
                            (string) __('Sku %1 was not found or it has an invalid product type to delete', $sku)
                        );
                        continue;
                    }
                    $result[$row['current_product_type']][] = [
                        ProductAdapter::COLUMN_SKU => $sku,
                        ProductAdapter::COLUMN_ENTITY_ID => (int) $skuToId[$sku],
                        ProductAdapter::INTERNAL_PRODUCT_TYPE => $row['current_product_type'] ?? '',
                    ];
                    $processedSkus[] = $sku;
                } catch (\Exception $e) {
                    $this->log(
                        $config,
                        (string) __(
                            'Unexpected error processing item %1: %2',
                            $sku,
                            $e->getMessage()
                        )
                    );
                }
            }
        }

        $result = $this->orderByProductType($result);

        $data[DeleteAdapter::TO_ADAPTER] = [
            DeleteAdapter::getIdentified() => $result,
        ];

        return $data;
    }

    /**
     * @param array $skuList
     *
     * @return array
     */
    private function getProductIdsBySkuList(array $skuList): array
    {
        $select = $this->connection->select()->from(
            'catalog_product_entity',
            ['sku', 'entity_id']
        )->where(
            'sku IN(?)',
            $skuList
        )->where(
            'type_id IN(?)',
            self::ALLOWED_PRODUCT_TYPES
        );

        return $this->connection->fetchPairs($select);
    }

    private function orderByProductType(array $result): array
    {
        $resultOrdered = [];
        if (isset($result[GroupedType::TYPE_CODE])) {
            $resultOrdered = \array_merge($resultOrdered, $result[GroupedType::TYPE_CODE]);
        }
        if (isset($result[ProductType::TYPE_SIMPLE])) {
            $resultOrdered = \array_merge($resultOrdered, $result[ProductType::TYPE_SIMPLE]);
        }
        if (isset($result[ProductType::TYPE_VIRTUAL])) {
            $resultOrdered = \array_merge($resultOrdered, $result[ProductType::TYPE_VIRTUAL]);
        }
        if (isset($result[DownloadableType::TYPE_DOWNLOADABLE])) {
            $resultOrdered = \array_merge($resultOrdered, $result[DownloadableType::TYPE_DOWNLOADABLE]);
        }

        return $resultOrdered;
    }

    /**
     * @param Config $config
     * @param string $msg
     */
    private function log(Config $config, string $msg): void
    {
        $logFile = $config->getInData('logFile') ?? '';
        $config->log($logFile, 'DELETE PRODUCT: ' . $msg . PHP_EOL);
    }
}
