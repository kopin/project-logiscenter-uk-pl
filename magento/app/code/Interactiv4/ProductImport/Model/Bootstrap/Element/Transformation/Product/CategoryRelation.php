<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImport\Model\Bootstrap\Element\Transformation\Product;

use Interactiv4\ImportExport\Api\Bootstrap\Element\Transformation\TransformationInterface;
use Interactiv4\ImportExport\Model\Bootstrap\Config;
use Interactiv4\ProductImport\Model\Bootstrap\Element\Adapter\Product\CategoryRelation as CategoryRelationAdapter;
use Interactiv4\ProductImport\Model\Bootstrap\Element\Adapter\Product\ProductInterface;
use Interactiv4\ProductImport\Model\Bootstrap\Element\Transformation\Data\Product as TransformationResource;
use Magento\ImportExport\Model\Import;

class CategoryRelation implements TransformationInterface
{
    private const ALLOWED_MODES = [
        Import::BEHAVIOR_APPEND,
    ];

    /**
     * @var TransformationResource
     */
    private $transformationResource;

    /**
     * Simple constructor.
     *
     * @param TransformationResource $transformationResource
     */
    public function __construct(
        TransformationResource $transformationResource
    ) {
        $this->transformationResource = $transformationResource;
    }

    /**
     * @param array $data
     *
     * @return array
     */
    public function process(array $data): array
    {
        $config = $data['config'];
        $importMode = $config->getInData('importMode') ?? '';
        if (!isset($data['mainSource']['rows']) || !\in_array($importMode, self::ALLOWED_MODES)) {
            return $data;
        }

        foreach ($data['mainSource']['rows'] as $index => $row) {
            try {
                $row = $this->transformationResource->prepareCategoryRelation($row);
                if (null !== $row[ProductInterface::COLUMN_CATEGORIES_ID]) {
                    $data[CategoryRelationAdapter::TO_ADAPTER][CategoryRelationAdapter::getIdentified()][] = $row;
                }
            } catch (\Exception $e) {
                $this->log(
                    $config,
                    (string) __(
                        "Unexpected error processing for row num %1. Category Relation can't be processed: %2",
                        $index,
                        $e->getMessage()
                    )
                );
                continue;
            }
        }

        return $data;
    }

    /**
     * @param Config $config
     * @param string $msg
     */
    private function log(Config $config, string $msg): void
    {
        $logFile = $config->getInData('logFile') ?? '';
        $config->log($logFile, 'Category Relation Transformation Data Error: ' . $msg . PHP_EOL);
    }
}
