<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImport\Model\Bootstrap\Element\Adapter\Price;

use Interactiv4\ProductImport\Model\Bootstrap\Element\Adapter\Product\ProductInterface as ProductAdapter;
use Magento\Catalog\Model\ResourceModel\Product\Action;

class Data
{
    /**
     * @var Action
     */
    private $productAction;

    /**
     * Data constructor.
     *
     * @param Action $productAction
     */
    public function __construct(
        Action $productAction
    ) {
        $this->productAction = $productAction;
    }

    /**
     * @param array $item
     *
     * @throws \Exception
     *
     * @return int
     */
    public function processItem(array $item): int
    {
        $result = 0;
        if (isset($item[ProductAdapter::PRICE_INFO])) {
            $this->update(
                $item[ProductAdapter::COLUMN_ENTITY_ID],
                $item[ProductAdapter::PRICE_INFO],
                $item[ProductAdapter::STORE_INFO]
            );

            return (int) $item[ProductAdapter::COLUMN_ENTITY_ID];
        }

        return $result;
    }

    /**
     * @param int   $productId
     * @param array $productPrices
     * @param array $stores
     *
     * @throws \Exception
     */
    private function update(int $productId, array $productPrices, array $stores): void
    {
        foreach ($stores as $store) {
            $this->productAction->updateAttributes(
                [$productId],
                $productPrices,
                $store
            );
        }
    }
}
