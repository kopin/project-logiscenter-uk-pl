<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImport\Model\Bootstrap\Element\Reader;

use Interactiv4\ImportExport\Api\Bootstrap\Element\Reader\FileReaderInterface;
use Interactiv4\ProductImportConfig\Api\Config\ProductImportConfigInterface;
use Magento\Framework\Exception\LocalizedException;

class CsvReader implements FileReaderInterface
{
    public const DEFAULT_DELIMITER = ';';

    public const DEFAULT_ENCLOSURE = '"';

    public const DEFAULT_ENCODING = 'UTF-8';

    /**
     * @var ProductImportConfigInterface
     */
    private $productImportConfig;

    /**
     * @var string
     */
    private $fieldEnclosure;

    /**
     * @var string
     */
    private $fieldSeparator;

    /**
     * @var string
     */
    private $fileName;

    /**
     * @var \SplFileObject
     */
    private $csvFile;

    /**
     * @var array
     */
    private $columns;

    /**
     * Reader constructor.
     *
     * @param ProductImportConfigInterface $productImportConfig
     */
    public function __construct(
        ProductImportConfigInterface $productImportConfig
    ) {
        $this->productImportConfig = $productImportConfig;
    }

    /**
     * {@inheritdoc}
     */
    public function open($fileName)
    {
        $this->fileName = $fileName;
        $this->csvFile = new \SplFileObject($this->fileName);

        return $this->csvFile;
    }

    /**
     * {@inheritdoc}
     */
    public function getLines(
        $init,
        &$end
    ) {
        $data = [];
        $rows = [];
        $file = $this->getFile();
        $file->seek($init);
        $separator = $this->getFieldSeparator();
        $enclosure = $this->getFieldEnclosure();
        if ($this->columns === null) {
            $this->columns = $file->fgetcsv($separator, $enclosure);
        }
        $data['headers'] = $this->columns;
        $batchValidKeys = \range($init, $end);
        $rowNum = $file->key();
        while (\in_array($file->key(), $batchValidKeys) && !$file->eof()) {
            if (\in_array($file->key(), $batchValidKeys)) {
                ++$rowNum;
                $row = $file->fgetcsv($separator, $enclosure);
                if (self::DEFAULT_ENCODING !== $this->productImportConfig->getEncoding()) {
                    $row = \utf8_encode($row);
                }
                if (\count($row) !== \count($this->columns)) {
                    continue;
                }

                $rows[$rowNum] = \array_combine($this->columns, $row);
            }
        }

        $data['rows'] = $rows;

        return !empty($data['rows']) ? $data : [];
    }

    /**
     * {@inheritdoc}
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;
        $this->csvFile = null;
    }

    /**
     * @return string
     */
    public function getFieldSeparator(): string
    {
        return $this->fieldSeparator
            ?: $this->productImportConfig->getSeparator()
                ?: self::DEFAULT_DELIMITER;
    }

    /**
     * @param string $fieldSeparator
     */
    public function setFieldSeparator(string $fieldSeparator): void
    {
        $this->fieldSeparator = $fieldSeparator;
    }

    /**
     * @return string
     */
    public function getFieldEnclosure(): string
    {
        return $this->fieldEnclosure
            ?: $this->productImportConfig->getEnclosure()
                ?: self::DEFAULT_ENCLOSURE;
    }

    /**
     * @param string $fieldEnclosure
     */
    public function setFieldEnclosure(string $fieldEnclosure): void
    {
        $this->fieldEnclosure = $fieldEnclosure;
    }

    /**
     * @param $filename
     *
     * @return array
     */
    public function getFileData($filename): array
    {
        $end = 1000000;
        $this->setFileName($filename);

        return $this->getLines(0, $end);
    }

    /**
     * @throws \Exception
     *
     * @return \SplFileObject
     */
    private function getFile()
    {
        if (null === $this->csvFile) {
            if (!\file_exists($this->fileName)) {
                throw new LocalizedException(__('File "' . $this->fileName . '" does not exist'));
            }
            $this->open($this->fileName);
        }

        return $this->csvFile;
    }
}
