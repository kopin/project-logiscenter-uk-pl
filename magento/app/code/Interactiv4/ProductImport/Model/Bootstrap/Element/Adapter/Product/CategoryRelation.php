<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImport\Model\Bootstrap\Element\Adapter\Product;

use Interactiv4\ImportExport\Api\Bootstrap\Element\Adapter\AdapterInterface;
use Interactiv4\ImportExport\Model\Bootstrap\Config;
use Interactiv4\ImportExport\Model\Bootstrap\Element\Adapter\Product\ProductInterface;
use Interactiv4\ImportExport\Model\TraitModel\Config as ConfigTrait;
use Magento\Catalog\Api\CategoryLinkManagementInterface;
use Magento\Catalog\Model\ResourceModel\Category as CategoryResource;
use Magento\Catalog\Model\ResourceModel\Product as ProductResource;

/**
 * Adapter to save category-product relations.
 */
class CategoryRelation implements AdapterInterface
{
    use ConfigTrait;

    private const IDENTIFIED = 'CategoryRelation';

    /**
     * @var CategoryLinkManagementInterface
     */
    private $categoryLinkManagement;

    /**
     * @var CategoryResource
     */
    private $categoryResource;

    /**
     * @var ProductResource
     */
    private $productResource;

    /**
     * @param CategoryLinkManagementInterface $categoryLinkManagement
     * @param CategoryResource                $categoryResource
     * @param ProductResource                 $productResource
     */
    public function __construct(
        CategoryLinkManagementInterface $categoryLinkManagement,
        CategoryResource $categoryResource,
        ProductResource $productResource
    ) {
        $this->categoryLinkManagement = $categoryLinkManagement;
        $this->categoryResource = $categoryResource;
        $this->productResource = $productResource;
    }

    /**
     * {@inheritdoc}
     */
    // phpcs:disable
    public static function getIdentified()
    {
        // phpcs:enable
        return self::IDENTIFIED;
    }

    /**
     * {@inheritdoc}
     */
    public function process(array $data)
    {
        $categoryLinkData = $data[self::TO_ADAPTER][self::getIdentified()] ?? null;

        if (null === $categoryLinkData) {
            return $data;
        }

        $config = $data['config'];
        $processedProductIds = $config->getOutData('processed_products') ?? [];
        $processedCategoryIds = $config->getOutData('processed_category_relation') ?? [];
        $newProcessedCategoryIds = [];
        foreach ($categoryLinkData as $item) {
            $productSku = $item[ProductInterface::COLUMN_SKU];
            $id = $this->productResource->getIdBySku($productSku);
            if ($id) {
                $categoryIds = $this->filterNotExistingCategories($item[ProductInterface::COLUMN_CATEGORIES_ID]);
                try {
                    $this->categoryLinkManagement->assignProductToCategories($productSku, $categoryIds);
                    $processedProductIds[] = $id;
                    foreach ($categoryIds as $categoryId) {
                        $newProcessedCategoryIds[] = $categoryId;
                    }
                } catch (\Exception $e) {
                    $categoryIdsStr = \implode(', ', $categoryIds);
                    $msg = (string) __(
                        'Unexpected error while saving category product relation for product %1, category ids %2: %3',
                        $productSku,
                        $categoryIdsStr,
                        $e->getMessage()
                    );
                    $this->log($config, $msg);
                }
            }
        }
        $processedCategoryIds = \array_merge($processedCategoryIds, $newProcessedCategoryIds);
        $config->setOutData(\array_unique($processedProductIds), 'processed_products');
        $config->setOutData(\array_unique($processedCategoryIds), 'processed_category_relation');

        return $data;
    }

    /**
     * Filter not existing categories from the array.
     *
     * @param array $categoryIds
     *
     * @return array
     */
    private function filterNotExistingCategories(array $categoryIds): array
    {
        foreach ($categoryIds as $categoryId) {
            if ($this->categoryResource->checkId($categoryId)) {
                $result[] = $categoryId;
            }
        }

        return $result ?? [];
    }

    /**
     * Log message into log file.
     *
     * @param Config $config
     * @param string $msg
     *
     * @return void
     */
    private function log(Config $config, string $msg): void
    {
        $logFile = $config->getInData('logFile') ?? '';
        $config->log($logFile, 'CATEGORY RELATION SAVING: ' . $msg . PHP_EOL);
    }
}
