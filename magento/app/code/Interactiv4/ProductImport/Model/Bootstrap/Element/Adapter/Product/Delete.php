<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImport\Model\Bootstrap\Element\Adapter\Product;

use Interactiv4\ImportExport\Api\Bootstrap\Element\Adapter\AdapterInterface;
use Interactiv4\ImportExport\Model\Bootstrap\Config;
use Interactiv4\ImportExport\Model\Bootstrap\Element\Adapter\Product\ProductInterface as ProductAdapterInterface;
use Interactiv4\ImportExport\Model\TraitModel\Config as ConfigTrait;
use Interactiv4\ProductImport\Model\Bootstrap\Element\Adapter\Product\ProductInterface as ProductAdapter;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\Registry;

/**
 * Process delete.
 */
class Delete implements AdapterInterface
{
    use ConfigTrait;

    private const IDENTIFIED = 'DeleteProduct';
    public const BATCH_SIZE = 1000;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var ManagerInterface
     */
    private $eventManager;

    /**
     * @var Registry
     */
    private $registry;

    /**
     * Delete constructor.
     *
     * @param ProductRepositoryInterface $productRepository
     * @param ManagerInterface           $eventManager
     * @param Registry                   $registry
     */
    public function __construct(
        ProductRepositoryInterface $productRepository,
        ManagerInterface $eventManager,
        Registry $registry
    ) {
        $this->productRepository = $productRepository;
        $this->eventManager = $eventManager;
        $this->registry = $registry;
    }

    /**
     * {@inheritdoc}
     */
    // phpcs:disable
    public static function getIdentified()
    {
        // phpcs:enable
        return self::IDENTIFIED;
    }

    /**
     * {@inheritdoc}
     */
    public function process(array $data)
    {
        if (!isset($data[self::TO_ADAPTER][self::getIdentified()])) {
            return $data;
        }

        $config = $data['config'];
        $processedProductIds = $config->getOutData('processed_products') ?? [];
        foreach ($data[self::TO_ADAPTER][self::getIdentified()] as $item) {
            try {
                $this->registry->register('isSecureArea', true);
                $id = $this->processItem($item);
                $this->registry->unregister('isSecureArea');
                $this->log($config, (string) __('Success deleting item %1', $item[ProductAdapter::COLUMN_SKU]));
                $processedProductIds[] = $id;
            } catch (\Exception $e) {
                $this->log(
                    $config,
                    (string) __(
                        'Error deleting item %1: %2',
                        $item[ProductAdapter::COLUMN_SKU],
                        $e->getMessage()
                    )
                );
            }
        }
        $config->setOutData(\array_unique($processedProductIds), 'processed_products');

        return $data;
    }

    /**
     * Process delete for each item.
     *
     * @param array $item
     *
     * @return void
     */
    private function processItem(array $item): int
    {
        $product = $this->productRepository->get($item[ProductAdapterInterface::COLUMN_SKU]);
        $this->eventManager->dispatch('model_delete_before', ['object' => $product]);
        $this->eventManager->dispatch(
            $product->getEventPrefix() . '_delete_before',
            [
                'data_object' => $product,
                'product' => $product,
            ]
        );
        $this->productRepository->delete($product);

        return (int) $product->getId();
    }

    /**
     * Log message into log file.
     *
     * @param Config $config
     * @param string $msg
     *
     * @return void
     */
    private function log(Config $config, string $msg): void
    {
        $logFile = $config->getInData('logFile') ?? '';
        $config->log($logFile, 'DELETE PRODUCT: ' . $msg . PHP_EOL);
    }
}
