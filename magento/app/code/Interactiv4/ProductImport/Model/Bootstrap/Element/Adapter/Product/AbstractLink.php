<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImport\Model\Bootstrap\Element\Adapter\Product;

use Interactiv4\ImportExport\Model\Bootstrap\Config;
use Magento\Catalog\Api\Data\ProductLinkInterfaceFactory;
use Magento\Catalog\Api\ProductLinkManagementInterface;
use Magento\Catalog\Api\ProductLinkRepositoryInterface;
use Magento\Catalog\Api\ProductLinkTypeListInterface;
use Magento\Catalog\Model\ResourceModel\Product as ProductResource;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;

/**
 * Class consist of base logic for saving product links.
 */
abstract class AbstractLink
{
    /**
     * @var ProductResource
     */
    protected $productResource;

    /**
     * @var ProductLinkInterfaceFactory
     */
    private $productLinkFactory;

    /**
     * @var ProductLinkRepositoryInterface
     */
    private $linkRepository;

    /**
     * @var ProductLinkManagementInterface
     */
    private $linkManagement;

    /**
     * @var CollectionFactory
     */
    private $productCollectionFactory;

    /**
     * @var ProductLinkTypeListInterface
     */
    private $linkTypeProvider;

    /**
     * @param ProductResource                $productResource
     * @param ProductLinkInterfaceFactory    $productLinkFactory
     * @param ProductLinkRepositoryInterface $linkRepository
     * @param ProductLinkManagementInterface $linkManagement
     * @param CollectionFactory              $productCollectionFactory
     */
    public function __construct(
        ProductResource $productResource,
        ProductLinkInterfaceFactory $productLinkFactory,
        ProductLinkRepositoryInterface $linkRepository,
        ProductLinkManagementInterface $linkManagement,
        CollectionFactory $productCollectionFactory,
        ProductLinkTypeListInterface $linkTypeProvider
    ) {
        $this->productResource = $productResource;
        $this->productLinkFactory = $productLinkFactory;
        $this->linkRepository = $linkRepository;
        $this->linkManagement = $linkManagement;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->linkTypeProvider = $linkTypeProvider;
    }

    /**
     * Remove not existing products from saving.
     *
     * @param array $productSkus
     *
     * @return array
     */
    protected function getNewChildren(array $productSkus): array
    {
        $collection = $this->productCollectionFactory->create();
        $collection->addAttributeToFilter(ProductInterface::COLUMN_SKU, ['in' => $productSkus]);

        return $collection->getItems();
    }

    /**
     * Save product links.
     *
     * @param string $parentSku
     * @param array  $childProducts
     * @param int    $linkTypeId
     *
     * @return void
     */
    protected function saveProductLinks(string $parentSku, array $childProducts, int $linkTypeId): void
    {
        $linkType = $this->getLinkTypeById($linkTypeId);

        if (null !== $linkTypeId) {
            foreach ($childProducts as $childProduct) {
                $productLink = $this->productLinkFactory->create();
                $productLink->setSku($parentSku)
                    ->setLinkType($linkType)
                    ->setLinkedProductSku($childProduct->getSku())
                    ->setLinkedProductType($childProduct->getTypeId())
                ;

                $this->linkRepository->save($productLink);
            }
        }
    }

    /**
     * Delete existing links.
     *
     * @param string $productSku
     *
     * @return void
     */
    protected function deleteExistingLinks(string $productSku, int $linkTypeId): void
    {
        $linkType = $this->getLinkTypeById($linkTypeId);

        if (null !== $linkType) {
            $linkItems = $this->linkManagement->getLinkedItemsByType($productSku, $linkType);

            foreach ($linkItems as $link) {
                $this->linkRepository->delete($link);
            }
        }
    }

    /**
     * Log message into log file.
     *
     * @param Config $config
     * @param string $msg
     *
     * @return void
     */
    protected function log(Config $config, string $msg): void
    {
        $logFile = $config->getInData('logFile') ?? '';
        $config->log($logFile, 'PRODUCT LINKS SAVING: ' . $msg . PHP_EOL);
    }

    /**
     * Get link type by id.
     *
     * @param int $linkTypeId
     *
     * @return string|null
     */
    private function getLinkTypeById(int $linkTypeId): ?string
    {
        $linkTypeArr = \array_keys($this->linkTypeProvider->getLinkTypes(), $linkTypeId);

        return !empty($linkTypeArr)
            ? \reset($linkTypeArr)
            : null;
    }
}
