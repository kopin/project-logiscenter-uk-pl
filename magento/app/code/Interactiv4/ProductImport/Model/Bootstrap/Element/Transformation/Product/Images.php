<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImport\Model\Bootstrap\Element\Transformation\Product;

use Interactiv4\ImportExport\Api\Bootstrap\Element\Transformation\TransformationInterface;
use Interactiv4\ImportExport\Model\Bootstrap\Config;
use Interactiv4\ProductImport\Model\Bootstrap\Element\Adapter\Product\Images as ImagesAdapter;
use Interactiv4\ProductImport\Model\Bootstrap\Element\Transformation\Product\ProductInterface as ProductTransformer;
use Interactiv4\ProductImport\Model\Bootstrap\Element\Transformation\Data\Product as TransformationResource;
use Magento\Catalog\Model\Product\Type as ProductType;
use Magento\Downloadable\Model\Product\Type as DownloadableType;
use Magento\GroupedProduct\Model\Product\Type\Grouped as GroupedType;
use Magento\ImportExport\Model\Import;

class Images implements TransformationInterface
{
    private const ALLOWED_MODES = [
        Import::BEHAVIOR_APPEND,
    ];

    /**
     * Allowed product type_id for images product.
     */
    public const ALLOWED_PRODUCT_TYPES = [
        ProductType::TYPE_SIMPLE,
        ProductType::TYPE_VIRTUAL,
        DownloadableType::TYPE_DOWNLOADABLE,
        GroupedType::TYPE_CODE,
    ];

    /**
     * @var TransformationResource
     */
    private $transformationResource;

    /**
     * Simple constructor.
     *
     * @param TransformationResource $transformationResource
     */
    public function __construct(
        TransformationResource $transformationResource
    ) {
        $this->transformationResource = $transformationResource;
    }

    /**
     * @param array $data
     *
     * @return array
     */
    public function process(array $data): array
    {
        $config = $data['config'];
        $importMode = $config->getInData('importMode') ?? '';
        if (
            !isset($data['mainSource']['rows'])
            || !\in_array($importMode, self::ALLOWED_MODES)
        ) {
            return $data;
        }

        foreach ($data['mainSource']['rows'] as $index => $row) {
            try {
                $productType = $row['current_product_type'] ?? $row[ProductTransformer::COLUMN_TYPE] ?? '';
                if (!\in_array($productType, self::ALLOWED_PRODUCT_TYPES)) {
                    continue;
                }
                $imagesData = $this->transformationResource->prepareImages($row);
                if (!empty($imagesData)) {
                    $data[ImagesAdapter::TO_ADAPTER][ImagesAdapter::getIdentified()][] = $imagesData;
                }
            } catch (\Exception $e) {
                $this->log(
                    $config,
                    (string) __(
                        "Unexpected error processing row num %1. Product Image file can't be processed: %2",
                        $index,
                        $e->getMessage()
                    )
                );
                continue;
            }
        }

        return $data;
    }

    /**
     * @param Config $config
     * @param string $msg
     */
    private function log(Config $config, string $msg): void
    {
        $logFile = $config->getInData('logFile') ?? '';
        $config->log($logFile, 'Image Transformation Data Error: ' . $msg . PHP_EOL);
    }
}
