<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImport\Model\Bootstrap\Element\Transformation;

use Interactiv4\ImportExport\Api\Bootstrap\Element\Transformation\TransformationInterface;
use Interactiv4\ImportExport\Model\Bootstrap\Config;
use Interactiv4\ProductImport\Model\Bootstrap\Element\Adapter\Product\ProductInterface as ProductAdapter;
use Interactiv4\ProductImport\Model\Bootstrap\Element\Adapter\Stock as StockAdapter;
use Interactiv4\ProductImport\Model\Bootstrap\Element\Transformation\Product\ProductInterface as ProductTransformer;
use Magento\ImportExport\Model\Import;

class Stock implements TransformationInterface
{
    private const ALLOWED_MODES = [
        Import::BEHAVIOR_APPEND,
    ];

    /**
     * Columns mapping to be applied in the format
     * ['source_column' => 'destination_column'].
     */
    public const COLUMNS_MAPPING = [
        ProductTransformer::COLUMN_STOCK_QTY => ProductAdapter::COLUMN_STOCK_QTY,
        ProductTransformer::COLUMN_IN_STOCK => ProductAdapter::COLUMN_IN_STOCK,
        ProductTransformer::COLUMN_STOCK_QTY_INCREMENTS => ProductAdapter::COLUMN_STOCK_QTY_INCREMENTS,
        ProductTransformer::COLUMN_MIN_SALE_QTY => ProductAdapter::COLUMN_MIN_SALE_QTY,
        ProductTransformer::COLUMN_USE_CONFIG_MIN_SALE_QTY => ProductAdapter::COLUMN_USE_CONFIG_MIN_SALE_QTY,
        ProductTransformer::COLUMN_ENABLE_QTY_INCREMENTS => ProductAdapter::COLUMN_ENABLE_QTY_INCREMENTS,
        ProductTransformer::COLUMN_USE_CONFIG_QTY_INCREMENTS => ProductAdapter::COLUMN_USE_CONFIG_QTY_INCREMENTS,
        ProductTransformer::COLUMN_USE_CONFIG_ENABLE_QTY_INC => ProductAdapter::COLUMN_USE_CONFIG_ENABLE_QTY_INC,
    ];

    /**
     * Process stock transformation.
     *
     * @param array $data
     *
     * @return array
     */
    public function process(array $data): array
    {
        $config = $data['config'];
        $importMode = $config->getInData('importMode') ?? '';
        if (
            !isset($data['mainSource']['rows'])
            || !\in_array($importMode, self::ALLOWED_MODES)
        ) {
            return $data;
        }

        $result = [];
        $rows = $data['mainSource']['rows'];
        $chunks = \array_chunk($rows, StockAdapter::BATCH_SIZE);
        foreach ($chunks as $chunk) {
            foreach ($chunk as $row) {
                try {
                    $sku = $row[ProductTransformer::COLUMN_SKU];
                    $row = $this->applyColumnMapping($row);
                    $stockInfo = $this->prepareStockData($row);

                    if (!empty($stockInfo)) {
                        $result[] = [
                            ProductAdapter::COLUMN_SKU => $sku,
                            ProductAdapter::STOCK_INFO => $stockInfo,
                        ];
                    }
                } catch (\Exception $e) {
                    $this->log(
                        $config,
                        (string) __(
                            'Unexpected error processing stock item %1: %2',
                            $sku,
                            $e->getMessage()
                        )
                    );
                }
            }
        }

        $data[StockAdapter::TO_ADAPTER][StockAdapter::getIdentified()] = $result;

        return $data;
    }

    /**
     * @param array $row
     *
     * @return array
     */
    private function prepareStockData(array $row): array
    {
        // If is_in_stock is not defined we discern the value from qty
        if (!isset($row[ProductAdapter::COLUMN_IN_STOCK]) && isset($row[ProductAdapter::COLUMN_STOCK_QTY])) {
            $row[ProductAdapter::COLUMN_IN_STOCK] =
                $row[ProductAdapter::COLUMN_STOCK_QTY] > 0
                    ? '1'
                    : '0';
        }

        return $row;
    }

    private function applyColumnMapping(array $row): array
    {
        $transformedRow = [];
        foreach (self::COLUMNS_MAPPING as $sourceColumn => $destinationColumn) {
            if (isset($row[$sourceColumn]) && '' !== $row[$sourceColumn]) {
                $transformedRow[$destinationColumn] = $row[$sourceColumn];
            }
        }

        return $transformedRow;
    }

    /**
     * @param Config $config
     * @param string $msg
     */
    private function log(Config $config, string $msg): void
    {
        $logFile = $config->getInData('logFile') ?? '';
        $config->log($logFile, 'STOCK: ' . $msg . PHP_EOL);
    }
}
