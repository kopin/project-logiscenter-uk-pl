<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImport\Model\Bootstrap\Element\Adapter\Stock;

use Interactiv4\ProductImport\Service\GetSourceCodeForStock;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\DB\Adapter\AdapterInterface as DBConnection;
use Magento\Framework\Exception\LocalizedException;
use Magento\Inventory\Model\ResourceModel\SourceItem;

class Data
{
    public const TABLE_ENTITY_NAME = 'cataloginventory_stock_item';

    /**
     * @var ResourceConnection
     */
    private $resource;

    /**
     * @var DBConnection
     */
    private $connection;

    /**
     * @var GetSourceCodeForStock
     */
    private $getSourceCodeForStock;

    /**
     * StockData constructor.
     *
     * @param ResourceConnection $resource
     */
    public function __construct(
        ResourceConnection $resource,
        GetSourceCodeForStock $getSourceCodeForStock
    ) {
        $this->resource = $resource;
        $this->connection = $resource->getConnection();
        $this->getSourceCodeForStock = $getSourceCodeForStock;
    }

    /**
     * @param array $items
     *
     * @throws LocalizedException
     *
     * @return void
     */
    public function processItems(array $items): void
    {
        try {
            $this->connection->beginTransaction();
            $this->setStockData(
                $items
            );
            $this->connection->commit();
        } catch (\Exception $e) {
            $this->connection->rollBack();
            throw new LocalizedException(__('Error saving stock item %1', $e->getMessage()), $e, $e->getCode());
        }
    }

    /**
     * Save stock data by butches.
     *
     * @param array $items
     * @param int   $websiteId
     * @param int   $stockId
     */
    private function setStockData(
        array $items,
        $websiteId = 0,
        $stockId = 1
    ) {
        $inventoryTableName = $this->resource->getTableName(self::TABLE_ENTITY_NAME);
        $sourceItemTableName = $this->resource->getTableName(SourceItem::TABLE_NAME_SOURCE_ITEM);
        $inventoryData = [];
        $sourceItemData = [];

        foreach ($items as $key => $item) {
            $inventoryData[$key] = $item['stockInfo'];
            $inventoryData[$key]['product_id'] = $item['entity_id'];
            $inventoryData[$key]['stock_id'] = $stockId;
            $inventoryData[$key]['website_id'] = $websiteId;

            if (isset($item['stockInfo']['qty'])) {
                $sourceItemData[$key]['quantity'] = $item['stockInfo']['qty'];
            }
            if (isset($item['stockInfo']['is_in_stock'])) {
                $sourceItemData[$key]['status'] = $item['stockInfo']['is_in_stock'];
            }
            if (isset($sourceItemData[$key]['quantity']) || isset($sourceItemData[$key]['status'])) {
                $sourceItemData[$key]['source_code'] = $this->getSourceCodeForStock->execute((int) $stockId);
                $sourceItemData[$key]['sku'] = $item['sku'];
            }
        }

        $this->connection->insertOnDuplicate($inventoryTableName, $inventoryData);
        $this->connection->insertOnDuplicate($sourceItemTableName, $sourceItemData);
    }
}
