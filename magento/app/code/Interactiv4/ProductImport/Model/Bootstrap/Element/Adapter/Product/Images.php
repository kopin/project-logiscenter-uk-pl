<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImport\Model\Bootstrap\Element\Adapter\Product;

use Interactiv4\ImportExport\Api\Bootstrap\Element\Adapter\AdapterInterface;
use Interactiv4\ImportExport\Model\Bootstrap\Config;
use Interactiv4\ImportExport\Model\TraitModel\Config as ConfigTrait;
use Interactiv4\ProductImport\Model\Bootstrap\Element\Adapter\Product\Data\ProductMedia;
use Interactiv4\ProductImport\Model\Bootstrap\Element\Adapter\Product\ProductInterface as ProductAdapter;
use Interactiv4\ProductImport\Service\ImageExistChecker;
use Magento\Catalog\Api\Data\ProductInterface as CatalogProductInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product\Gallery\CreateHandler;
use Magento\Catalog\Model\Product\Gallery\Processor;
use Magento\Catalog\Model\Product\Gallery\UpdateHandler;
use Magento\Catalog\Model\ResourceModel\Product\Gallery;
use Magento\Store\Model\Store;

class Images implements AdapterInterface
{
    use ConfigTrait;

    private const IDENTIFIED = 'ImagesProduct';

    private $attributesToImport = [
        ProductAdapter::COLUMN_IMAGE,
        ProductAdapter::COLUMN_SMALL_IMAGE,
        ProductAdapter::COLUMN_THUMBNAIL,
    ];

    /**
     * @var Processor
     */
    private $imageProcessor;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var ImageExistChecker
     */
    private $checker;

    /**
     * @var UpdateHandler
     */
    private $updateHandler;

    /**
     * @var CreateHandler
     */
    private $createHandler;

    /**
     * @var Gallery
     */
    private $resourceModel;

    /**
     * @var Data\ProductMedia
     */
    private $media;

    /**
     * Images constructor.
     *
     * @param ProductRepositoryInterface $productRepository
     * @param Gallery                    $resourceModel
     * @param ProductMedia               $media
     */
    public function __construct(
        ProductRepositoryInterface $productRepository,
        Gallery $resourceModel,
        ProductMedia $media
    ) {
        $this->productRepository = $productRepository;
        $this->resourceModel = $resourceModel;
        $this->media = $media;
    }

    /**
     * {@inheritdoc}
     */
    // phpcs:disable
    public static function getIdentified()
    {
        // phpcs:enable
        return self::IDENTIFIED;
    }

    /**
     * {@inheritdoc}
     */
    public function process(array $data)
    {
        if (!isset($data[self::TO_ADAPTER][self::getIdentified()])) {
            return $data;
        }

        $config = $data['config'];
        $processedProductIds = $config->getOutData('processed_products') ?? [];
        foreach ($data[self::TO_ADAPTER][self::getIdentified()] as $item) {
            try {
                $product = $this->productRepository->get($item[ProductAdapter::COLUMN_SKU]);
                $id = $product->getId();
                if ($id) {
                    $this->removeExistingImages($product);
                    $this->importMainImages($item, $product);
                    $this->importAdditionalImages($item[ProductAdapter::COLUMN_ADDITIONAL_IMAGES], $product);
                    $processedProductIds[] = $id;
                }
            } catch (\Exception $e) {
                $this->log(
                    $config,
                    (string) __(
                        'Unexpected error saving images product item %1: %2',
                        $item[ProductAdapter::COLUMN_SKU],
                        $e->getMessage()
                    )
                );
            }
        }
        $config->setOutData(\array_unique($processedProductIds), 'processed_products');

        return $data;
    }

    /**
     * @param Config $config
     * @param string $msg
     */
    private function log(Config $config, string $msg): void
    {
        $logFile = $config->getInData('logFile') ?? '';
        $config->log($logFile, 'IMAGES SAVING: ' . $msg . PHP_EOL);
    }

    /**
     * Remove existing image attribute and gallery values.
     *
     * @param CatalogProductInterface $product
     *
     * @return void
     */
    private function removeExistingImages(CatalogProductInterface $product): void
    {
        $images = $product->getMediaGalleryImages();
        if (\count($images)) {
            foreach ($images as $image) {
                $valueIds[] = $image->getValueId();
                $this->resourceModel->deleteGalleryValueInStore(
                    $image->getValueId(),
                    $product->getData($product->getResource()->getLinkField()),
                    Store::DEFAULT_STORE_ID
                );
            }
            $this->resourceModel->deleteGallery($valueIds);
        }
    }

    /**
     * Import main images attributes.
     *
     * @param array                   $item
     * @param CatalogProductInterface $product
     *
     * @throws \Exception
     *
     * @return void
     */
    private function importMainImages(array $item, CatalogProductInterface $product): void
    {
        foreach ($this->attributesToImport as $attribute) {
            if (empty($item[$attribute]) || empty($item[$attribute][ProductAdapter::COLUMN_MEDIA_FILE_PATH])) {
                continue;
            }
            $this->media->processImages(
                [$item[$attribute]],
                (int) $product->getId(),
                $attribute
            );
        }
    }

    /**
     * Import additional gallery images.
     *
     * @param array                   $images
     * @param CatalogProductInterface $product
     *
     * @return void
     */
    private function importAdditionalImages(array $images, CatalogProductInterface $product): void
    {
        $additionalImages[] = ''; //to start with position 1 as additional images
        foreach ($images as $additionalImage) {
            if (!empty($additionalImage[ProductAdapter::COLUMN_MEDIA_FILE_PATH])) {
                $additionalImages[] = $additionalImage;
            }
        }
        unset($additionalImages[0]);

        if (\count($additionalImages)) {
            $this->media->processImages(
                $additionalImages,
                (int) $product->getId()
            );
        }
    }
}
