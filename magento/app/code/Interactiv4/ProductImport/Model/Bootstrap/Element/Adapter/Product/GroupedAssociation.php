<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImport\Model\Bootstrap\Element\Adapter\Product;

use Interactiv4\ImportExport\Api\Bootstrap\Element\Adapter\AdapterInterface;
use Interactiv4\ImportExport\Model\TraitModel\Config as ConfigTrait;
use Magento\GroupedProduct\Model\ResourceModel\Product\Link;

/**
 * Adapter responsible for saving grouped products children.
 */
class GroupedAssociation extends AbstractLink implements AdapterInterface
{
    use ConfigTrait;

    private const IDENTIFIED = 'AssociatedRelation';

    /**
     * {@inheritdoc}
     */
    // phpcs:disable
    public static function getIdentified()
    {
        // phpcs:enable
        return self::IDENTIFIED;
    }

    /**
     * {@inheritdoc}
     */
    public function process(array $data)
    {
        $associatedLinkData = $data[self::TO_ADAPTER][self::getIdentified()] ?? null;

        if (null === $associatedLinkData) {
            return $data;
        }

        $config = $data['config'];
        $processedProductIds = $config->getOutData('processed_products') ?? [];
        foreach ($associatedLinkData as $associatedLink) {
            $productSku = $associatedLink[ProductInterface::COLUMN_SKU];
            $id = $this->productResource->getIdBySku($productSku);
            if ($id) {
                $this->deleteExistingLinks($productSku, Link::LINK_TYPE_GROUPED);
                $newChildren = $this->getNewChildren($associatedLink[ProductInterface::COLUMN_ASSOCIATED_SKUS]);
                try {
                    $this->saveProductLinks($productSku, $newChildren, Link::LINK_TYPE_GROUPED);
                    $processedProductIds[] = $id;
                } catch (\Exception $e) {
                    $linkedProductSkus = \implode(', ', $newChildren);
                    $msg = (string) __(
                        'Unexpected error while saving grouped children for product item %1, linked products %2: %3',
                        $productSku,
                        $linkedProductSkus,
                        $e->getMessage()
                    );
                    $this->log($config, $msg);
                }
            }
        }
        $config->setOutData(\array_unique($processedProductIds), 'processed_products');

        return $data;
    }
}
