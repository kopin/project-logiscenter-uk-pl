<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImport\Model\Bootstrap\Element\Adapter\Product\Data;

use Interactiv4\ImportExport\Model\Bootstrap\Element\Adapter\Entity\CatalogEav;
use Interactiv4\ImportExport\Model\Bootstrap\Element\Adapter\Product\ProductInterface;
use Magento\Catalog\Model\ResourceModel\Product as ProductResource;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\DB\Adapter\AdapterInterface as DBConnection;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Filesystem\Io\File;
use Magento\MediaStorage\Model\File\Uploader;
use Magento\Store\Model\Store;

/**
 * Product media manager class.
 */
class ProductMedia implements ProductInterface
{
    /**
     * @var DirectoryList
     */
    private $directoryList;

    /**
     * @var File
     */
    private $file;

    /**
     * @var DBConnection
     */
    private $connection;

    /**
     * @var CatalogEav
     */
    private $catalogEav;

    /**
     * @var int
     */
    private $mediaGalleryAttId;

    /**
     * @var string
     */
    private $entityIdColumn;

    /**
     * @var array
     */
    private $productAttributes;

    /**
     * ImportImageService constructor.
     *
     * @param ProductResource $resource
     * @param DirectoryList   $directoryList
     * @param File            $file
     * @param CatalogEav      $catalogEav
     */
    public function __construct(
        ProductResource $resource,
        DirectoryList $directoryList,
        File $file,
        CatalogEav $catalogEav
    ) {
        $this->directoryList = $directoryList;
        $this->file = $file;
        $this->catalogEav = $catalogEav;
        $this->connection = $resource->getConnection();
        $this->entityIdColumn = $resource->getLinkField();
    }

    /**
     * Process product images import.
     *
     * @param array       $data
     * @param int         $productId
     * @param string|null $attribute
     *
     * @throws \Exception
     */
    public function processImages(array $data, int $productId, ?string $attribute = null)
    {
        foreach ($data as $position => $imgData) {
            $imageFile = $this->copyFile($imgData[self::COLUMN_MEDIA_FILE_PATH]);
            if ($imageFile) {
                $this->setImageToProduct(
                    $imgData,
                    $position,
                    $imageFile,
                    $productId,
                    $attribute
                );
            }
        }
    }

    /**
     * Media directory name for catalog product
     * pub/media/catalog/product.
     *
     * @throws FileSystemException
     *
     * @return string
     */
    protected function getMediaCatalogDir(): string
    {
        return $this->directoryList->getPath(DirectoryList::MEDIA) .
            DIRECTORY_SEPARATOR .
            'catalog' .
            DIRECTORY_SEPARATOR .
            'product' .
            DIRECTORY_SEPARATOR;
    }

    /**
     * Copy file to catalog destination folder.
     *
     * @param string $sourcePath
     *
     * @throws \Exception
     *
     * @return bool|string
     */
    private function copyFile(string $sourcePath)
    {
        $mediaDir = $this->getMediaCatalogDir();

        $dispersionPath = Uploader::getDispersionPath(\basename($sourcePath));
        $finalDir = $mediaDir . $dispersionPath;

        $this->file->checkAndCreateFolder($finalDir);

        $filePath = DIRECTORY_SEPARATOR . \basename($sourcePath);

        $destinationPath = $finalDir . $filePath;

        /* read file from URL and copy it to the new destination */
        $result = $this->file->fileExists($destinationPath);
        if (!$result) {
            $result = $this->file->read(
                $sourcePath,
                $destinationPath
            );
        }

        if ($result) {
            return $dispersionPath . $filePath;
        }

        return false;
    }

    /**
     * Assign image file to product.
     *
     * @param array       $data
     * @param int         $position
     * @param string      $imageFile
     * @param int         $productId
     * @param string|null $attribute
     */
    private function setImageToProduct(
        array $data,
        int $position,
        string $imageFile,
        int $productId,
        ?string $attribute
    ) {
        if (null !== $attribute) {
            $type = $this->catalogEav->findAttributeType($attribute, $this->getProductAttributes());
            $this->catalogEav->manageAttribute(
                $imageFile,
                $type,
                CatalogEav::ENTITY_NAME_PRODUCT,
                $productId
            );
        }
        if (!$this->findImageProduct($imageFile, $productId)) {
            $this->insertImageMediaGallery(
                $imageFile,
                $productId,
                $data[self::COLUMN_MEDIA_LABEL],
                $position
            );
        }
    }

    /**
     * Is same file exists in product.
     *
     * @param string $fileName
     * @param int    $productId
     *
     * @return string|bool
     */
    private function findImageProduct(
        string $fileName,
        int $productId
    ) {
        $tblMediaGallery = $this->connection->getTableName('catalog_product_entity_media_gallery');
        $tblMediaGalleryValue = $this->connection->getTableName('catalog_product_entity_media_gallery_value');

        return $this->connection->fetchOne(
            $this->connection->select()->from(
                ['mg' => $tblMediaGallery],
                'value_id'
            )->joinInner(
                ['mgv' => $tblMediaGalleryValue],
                'mg.value_id = mgv.value_id',
                []
            )->where(
                'mg.value = ?',
                $fileName
            )->where(
                'mgv.store_id = ?',
                Store::DEFAULT_STORE_ID
            )->where(
                'mgv.' . $this->entityIdColumn . ' = ?',
                $productId
            )
        );
    }

    /**
     * Insert image to media gallery table.
     *
     * @param string $fileName
     * @param int    $productId
     * @param string $label
     * @param int    $position
     */
    private function insertImageMediaGallery(
        string $fileName,
        int $productId,
        string $label,
        int $position
    ) {
        $this->connection->beginTransaction();
        try {
            $galleryAttrId = $this->getMediaGalleryAttributeId();
            $tableName = $this->connection->getTableName('catalog_product_entity_media_gallery');
            $this->connection->insert(
                $tableName,
                ['attribute_id' => $galleryAttrId, 'value' => $fileName, 'media_type' => 'image', 'disabled' => 0]
            );
            $valueId = $this->connection->lastInsertId();

            $this->connection->insert(
                $this->connection->getTableName('catalog_product_entity_media_gallery_value_to_entity'),
                ['value_id' => $valueId, $this->entityIdColumn => $productId]
            );

            $this->connection->insert(
                $this->connection->getTableName('catalog_product_entity_media_gallery_value'),
                [
                    'value_id' => $valueId,
                    'store_id' => Store::DEFAULT_STORE_ID,
                    $this->entityIdColumn => $productId,
                    'position' => $position,
                    'label' => $label,
                ]
            );

            $this->connection->commit();
        } catch (\Exception $e) {
            $this->connection->rollBack();
            throw new LocalizedException(__("Error saving media gallery data"), $e, $e->getCode());
        }
    }

    /**
     * Return media gallery attribute id.
     *
     * @return int
     */
    private function getMediaGalleryAttributeId(): int
    {
        if (!$this->mediaGalleryAttId) {
            foreach ($this->getProductAttributes() as $attr) {
                if ('media_gallery' === $attr['attribute_code']) {
                    $this->mediaGalleryAttId = (int) $attr['attribute_id'];
                    break;
                }
            }
        }

        return $this->mediaGalleryAttId;
    }

    /**
     * Return product entity attributes.
     *
     * @return array
     */
    private function getProductAttributes(): array
    {
        if (!$this->productAttributes) {
            $this->productAttributes = $this->catalogEav->getEntityAttributes(CatalogEav::ENTITY_NAME_PRODUCT);
        }

        return $this->productAttributes;
    }
}
