<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImport\Model\Bootstrap\Element\Adapter\Product;

use Interactiv4\ImportExport\Api\Bootstrap\Element\Adapter\AdapterInterface;
use Interactiv4\ImportExport\Model\TraitModel\Config as ConfigTrait;

/**
 * Adapter to save product links.
 */
class ProductLink extends AbstractLink implements AdapterInterface
{
    use ConfigTrait;

    private const IDENTIFIED = 'ProductLink';

    /**
     * {@inheritdoc}
     */
    // phpcs:disable
    public static function getIdentified()
    {
        // phpcs:enable
        return self::IDENTIFIED;
    }

    /**
     * {@inheritdoc}
     */
    public function process(array $data)
    {
        $linksData = $data[self::TO_ADAPTER][self::getIdentified()] ?? null;

        if (null === $linksData) {
            return $data;
        }

        $config = $data['config'];
        $processedProductIds = $config->getOutData('processed_products') ?? [];
        foreach ($linksData as $linkItems) {
            $productSku = $linkItems[ProductInterface::COLUMN_SKU];
            $id = $this->productResource->getIdBySku($productSku);
            if ($id) {
                foreach ($linkItems[ProductInterface::COLUMN_PRODUCT_LINK] as $item) {
                    $this->deleteExistingLinks($productSku, $item['link_type']);
                    $newChildren = $this->getNewChildren($item['products']);
                    try {
                        $this->saveProductLinks($productSku, $newChildren, $item['link_type']);
                        $processedProductIds[] = $id;
                    } catch (\Exception $e) {
                        $linkedProductSkus = \implode(', ', $newChildren);
                        $msg = (string) __(
                            'Unexpected error while saving product links for product item %1,' .
                            'linked products %2, link type %3: %4',
                            $productSku,
                            $linkedProductSkus,
                            $item['link_type'],
                            $e->getMessage()
                        );
                        $this->log($config, $msg);
                    }
                }
            }
        }
        $config->setOutData(\array_unique($processedProductIds), 'processed_products');

        return $data;
    }
}
