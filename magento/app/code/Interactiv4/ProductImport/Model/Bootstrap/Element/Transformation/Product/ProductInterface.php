<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImport\Model\Bootstrap\Element\Transformation\Product;

/**
 * Interface ProductInterface.
 */
interface ProductInterface
{
    public const COLUMN_CATEGORIES_ID = 'category_ids';

    public const COLUMN_STORE_CODE = 'store_code';

    public const COLUMN_SKU = 'sku';

    public const COLUMN_TYPE = 'type';

    public const COLUMN_STATUS = 'status';

    public const COLUMN_VISIBILITY = 'visibility';

    public const COLUMN_ATTRIBUTE_SET = 'attribute_set';

    public const COLUMN_TAX_CLASS = 'tax_class';

    public const COLUMN_URL_KEY = 'url_key';

    public const COLUMN_URL_KEY_CREATE_REDIRECT = 'url_key_create_redirect';

    public const COLUMN_IS_NEW = 'is_new';

    public const COLUMN_NAME = 'name';

    // Stock columns
    public const COLUMN_STOCK_QTY = 'qty';

    public const COLUMN_IN_STOCK = 'is_in_stock';

    public const COLUMN_STOCK_QTY_INCREMENTS = 'qty_increments';

    public const COLUMN_MIN_SALE_QTY = 'min_sale_qty';

    public const COLUMN_USE_CONFIG_MIN_SALE_QTY = 'use_config_min_sale_qty';

    public const COLUMN_ENABLE_QTY_INCREMENTS = 'enable_qty_increments';

    public const COLUMN_USE_CONFIG_QTY_INCREMENTS = 'use_config_qty_increments';

    public const COLUMN_USE_CONFIG_ENABLE_QTY_INC = 'use_config_enable_qty_inc';

    // Price columns
    public const COLUMN_PRICE = 'price';

    public const COLUMN_SPECIAL_PRICE = 'special_price';

    public const COLUMN_SPECIAL_FROM = 'special_from_date';

    public const COLUMN_SPECIAL_TO = 'special_to_date';

    public const COLUMN_COST = 'cost';

    public const COLUMN_MSRP = 'msrp';

    public const COLUMN_MSRP_ENABLED = 'msrp_enabled';

    public const COLUMN_MSRP_DISPLAY_ACTUAL_PRICE = 'msrp_display_actual_price_type';

    //Relation columns
    public const COLUMN_ASSOCIATED_SKUS = 'grouped_skus';

    public const COLUMN_RELATED_SKUS = 're_skus';

    public const COLUMN_UP_SELL_SKUS = 'up_skus';

    public const COLUMN_CROSS_SELL_SKUS = 'xs_skus';

    //Media const
    public const COLUMN_IMAGE = 'image';

    public const COLUMN_IMAGE_LABEL = 'image_label';

    public const COLUMN_SMALL_IMAGE = 'small_image';

    public const COLUMN_SMALL_IMAGE_LABEL = 'small_image_label';

    public const COLUMN_THUMBNAIL = 'thumbnail';

    public const COLUMN_THUMBNAIL_LABEL = 'thumbnail_label';

    public const COLUMN_ADDITIONAL_IMAGES = 'additional_images';
}
