<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImport\Model\Bootstrap\Element\Transformation\Product;

use Interactiv4\ImportExport\Api\Bootstrap\Element\Transformation\TransformationInterface;
use Interactiv4\ImportExport\Model\Bootstrap\Config;
use Interactiv4\ProductImport\Model\Bootstrap\Element\Adapter\Product\ProductInterface;
use Interactiv4\ProductImport\Model\Bootstrap\Element\Adapter\Product\ProductLink as ProductLinkAdapter;
use Interactiv4\ProductImport\Model\Bootstrap\Element\Transformation\Data\Product as TransformationResource;
use Magento\ImportExport\Model\Import;

class ProductLink implements TransformationInterface
{
    private const ALLOWED_MODES = [
        Import::BEHAVIOR_APPEND,
    ];

    /**
     * @var TransformationResource
     */
    private $transformationResource;

    /**
     * Simple constructor.
     *
     * @param TransformationResource $transformationResource
     */
    public function __construct(
        TransformationResource $transformationResource
    ) {
        $this->transformationResource = $transformationResource;
    }

    /**
     * @param array $data
     *
     * @return array
     */
    public function process(array $data): array
    {
        $config = $data['config'];
        $importMode = $config->getInData('importMode') ?? '';
        if (
            !isset($data['mainSource']['rows'])
            || !\in_array($importMode, self::ALLOWED_MODES)
        ) {
            return $data;
        }

        $headersInfo = $data['mainSource']['headersInfo'] ?? [];
        foreach ($data['mainSource']['rows'] as $index => $row) {
            try {
                $row = $this->transformationResource->prepareProductRelation($row, $headersInfo);
                if (null !== $row[ProductInterface::COLUMN_PRODUCT_LINK]) {
                    $data[ProductLinkAdapter::TO_ADAPTER][ProductLinkAdapter::getIdentified()][] = $row;
                }
            } catch (\Exception $e) {
                $this->log(
                    $config,
                    (string) __(
                        "Unexpected error processing row num %1. Relation product can't be processed: %2",
                        $index,
                        $e->getMessage()
                    )
                );
                continue;
            }
        }

        return $data;
    }

    /**
     * @param Config $config
     * @param string $msg
     */
    private function log(Config $config, string $msg): void
    {
        $logFile = $config->getInData('logFile') ?? '';
        $config->log($logFile, 'Product Relation Transformation Data Error: ' . $msg . PHP_EOL);
    }
}
