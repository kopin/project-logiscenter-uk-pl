<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImport\Model\Bootstrap\Element\Transformation\Data;

use Interactiv4\ProductImport\Model\Bootstrap\Element\Adapter\Product\ProductInterface;
use Interactiv4\ProductImport\Model\Bootstrap\Element\Adapter\Product\ProductInterface as ProductAdapter;
use Interactiv4\ProductImport\Model\Bootstrap\Element\Transformation\Product\ProductInterface as ProductTransformer;
use Interactiv4\ProductImport\Model\Source\SpecialHeader;
use Interactiv4\ProductImport\Model\Validation\HeaderInformation;
use Interactiv4\ProductImport\Service\ImageExistChecker;
use Magento\Catalog\Model\Product as ProductModel;
use Magento\Catalog\Model\Product\Link;
use Magento\Catalog\Model\Product\Type as ProductType;
use Magento\Downloadable\Model\Product\Type as DownloadableType;
use Magento\Eav\Api\AttributeOptionManagementInterface;
use Magento\Eav\Api\Data\AttributeOptionInterfaceFactory;
use Magento\GroupedProduct\Model\Product\Type\Grouped;

class Product
{
    private $specialHeaders;

    /**
     * @var AttributeOptionManagementInterface
     */
    private $attributeOptionManagement;

    /**
     * @var AttributeOptionInterfaceFactory
     */
    private $attributeOptionInterfaceFactory;

    /**
     * @var ImageExistChecker
     */
    private $checker;

    /**
     * Product constructor.
     *
     * @param AttributeOptionManagementInterface $attributeOptionManagement
     * @param AttributeOptionInterfaceFactory    $attributeOptionInterfaceFactory
     * @param SpecialHeader                      $specialHeaderSource
     * @param ImageExistChecker                  $checker
     */
    public function __construct(
        AttributeOptionManagementInterface $attributeOptionManagement,
        AttributeOptionInterfaceFactory $attributeOptionInterfaceFactory,
        SpecialHeader $specialHeaderSource,
        ImageExistChecker $checker
    ) {
        $this->attributeOptionManagement = $attributeOptionManagement;
        $this->attributeOptionInterfaceFactory = $attributeOptionInterfaceFactory;
        $this->specialHeaders = $specialHeaderSource->getHeaders();
        $this->checker = $checker;
    }

    /**
     * @param array $row
     *
     * @return array
     */
    public function prepareCategoryRelation(array $row): array
    {
        $preparedArray = [];
        $preparedArray[ProductInterface::COLUMN_SKU] = $this->getSku($row);
        $preparedArray[ProductInterface::COLUMN_CATEGORIES_ID] = $this->getCategories($row);

        return $preparedArray;
    }

    /**
     * @param array $row
     *
     * @return array
     */
    public function prepareImages(array $row): array
    {
        $preparedArray = [];
        if (!$this->hasToProcessImages($row)) {
            return $preparedArray;
        }

        $preparedArray[ProductInterface::COLUMN_SKU] = $this->getSku($row);
        $preparedArray[ProductInterface::COLUMN_IMAGE] = [];
        $preparedArray[ProductInterface::COLUMN_THUMBNAIL] = [];
        $preparedArray[ProductInterface::COLUMN_SMALL_IMAGE] = [];
        $preparedArray[ProductInterface::COLUMN_ADDITIONAL_IMAGES] = [];

        if (isset($row[ProductTransformer::COLUMN_IMAGE]) && !empty($row[ProductTransformer::COLUMN_IMAGE])) {
            $preparedArray[ProductInterface::COLUMN_IMAGE][ProductInterface::COLUMN_MEDIA_FILE_PATH] =
                $this->checker->getSourceImageAbsolutePath($row[ProductTransformer::COLUMN_IMAGE]);
            $preparedArray[ProductInterface::COLUMN_IMAGE][ProductInterface::COLUMN_MEDIA_LABEL] =
                $row[ProductTransformer::COLUMN_IMAGE_LABEL] ?? '';
        }

        if (isset($row[ProductTransformer::COLUMN_THUMBNAIL]) && !empty($row[ProductTransformer::COLUMN_THUMBNAIL])) {
            $preparedArray[ProductInterface::COLUMN_THUMBNAIL][ProductInterface::COLUMN_MEDIA_FILE_PATH] =
                $this->checker->getSourceImageAbsolutePath($row[ProductTransformer::COLUMN_THUMBNAIL]);
            $preparedArray[ProductInterface::COLUMN_THUMBNAIL][ProductInterface::COLUMN_MEDIA_LABEL] =
                $row[ProductTransformer::COLUMN_THUMBNAIL_LABEL] ?? '';
        }

        if (
            isset($row[ProductTransformer::COLUMN_SMALL_IMAGE])
            && !empty($row[ProductTransformer::COLUMN_SMALL_IMAGE])
        ) {
            $preparedArray[ProductInterface::COLUMN_SMALL_IMAGE][ProductInterface::COLUMN_MEDIA_FILE_PATH] =
                $this->checker->getSourceImageAbsolutePath($row[ProductTransformer::COLUMN_SMALL_IMAGE]);
            $preparedArray[ProductInterface::COLUMN_SMALL_IMAGE][ProductInterface::COLUMN_MEDIA_LABEL] =
                $row[ProductTransformer::COLUMN_SMALL_IMAGE_LABEL] ?? '';
        }

        // Process additional images column
        $additionalImagesResult = [];
        $additionalImages =
            isset($row[ProductTransformer::COLUMN_ADDITIONAL_IMAGES])
            && !empty($row[ProductTransformer::COLUMN_ADDITIONAL_IMAGES])
                ? \explode(
                    ',',
                    $row[ProductTransformer::COLUMN_ADDITIONAL_IMAGES]
                )
                : [];
        foreach ($additionalImages as $additionalImage) {
            $partialResult = [];
            $additionalImage = \explode('|', $additionalImage);
            if (isset($additionalImage[0]) && !empty($additionalImage[0])) {
                $partialResult[ProductInterface::COLUMN_MEDIA_FILE_PATH] =
                    $this->checker->getSourceImageAbsolutePath($additionalImage[0]);
                $partialResult[ProductInterface::COLUMN_MEDIA_LABEL] = $additionalImage[1] ?? '';
            }
            if (!empty($partialResult)) {
                $additionalImagesResult[] = $partialResult;
            }
        }

        $preparedArray[ProductInterface::COLUMN_ADDITIONAL_IMAGES] = $additionalImagesResult;

        return $preparedArray;
    }

    /**
     * @param array $row
     *
     * @return bool
     */
    private function hasToProcessImages(array $row): bool
    {
        return \array_key_exists(ProductTransformer::COLUMN_THUMBNAIL, $row)
            || \array_key_exists(ProductTransformer::COLUMN_SMALL_IMAGE, $row)
            || \array_key_exists(ProductTransformer::COLUMN_IMAGE, $row);
    }

    /**
     * @param array $row
     * @param array $headersInfo
     *
     * @return array
     */
    public function prepareAssociatedRelation(array $row, array $headersInfo): array
    {
        $preparedArray = [];
        $preparedArray[ProductInterface::STORE_INFO] = $this->getStoreInfo($row, $headersInfo);
        $preparedArray[ProductInterface::COLUMN_SKU] = $this->getSku($row);
        $preparedArray[ProductInterface::COLUMN_ASSOCIATED_SKUS] = $this->getAssociatedSkus($row);

        return $preparedArray;
    }

    /**
     * @param array $row
     * @param array $headersInfo
     *
     * @return array
     */
    public function prepareProductRelation(array $row, array $headersInfo): array
    {
        $preparedArray = [];
        $preparedArray[ProductInterface::STORE_INFO] = $this->getStoreInfo($row, $headersInfo);
        $preparedArray[ProductInterface::COLUMN_SKU] = $this->getSku($row);
        $preparedArray[ProductInterface::COLUMN_PRODUCT_LINK] = null;
        $crossSell = $this->getCrossSellSkus($row);
        if (null !== $crossSell) {
            $preparedArray[ProductInterface::COLUMN_PRODUCT_LINK][] =
                ['link_type' => Link::LINK_TYPE_CROSSSELL, 'products' => $crossSell];
        }
        $related = $this->getRelatedSkus($row);
        if (null !== $related) {
            $preparedArray[ProductInterface::COLUMN_PRODUCT_LINK][] =
                ['link_type' => Link::LINK_TYPE_RELATED, 'products' => $related];
        }
        $upSell = $this->getUpSellSkus($row);
        if (null !== $upSell) {
            $preparedArray[ProductInterface::COLUMN_PRODUCT_LINK][] =
                ['link_type' => Link::LINK_TYPE_UPSELL, 'products' => $upSell];
        }

        return $preparedArray;
    }

    /**
     * @param array $row
     * @param array $headersInfo
     * @param array $defaultValues
     *
     * @return array
     */
    public function prepare(array $row, array $headersInfo, array $defaultValues): array
    {
        $specialColumns = $this->prepareSpecialColumns($row, $headersInfo, $defaultValues);
        $dynamicColumns = $this->prepareDynamicColumns($row, $headersInfo);
        $preparedArray = \array_merge($specialColumns, $dynamicColumns);
        //Clean null values to ignore them
        //Keep empty ones (to be unset on saving)
        $preparedArray = \array_filter(
            $preparedArray,
            function ($value) {
                return null !== $value;
            }
        );
        if (!empty($preparedArray)) {
            $preparedArray[ProductInterface::STORE_INFO] = $this->getStoreInfo($row, $headersInfo);
            $preparedArray[ProductInterface::COLUMN_SKU] = $this->getSku($row);
        }

        return $preparedArray;
    }

    private function prepareSpecialColumns(array $row, array $headersInfo, array $defaultValues): array
    {
        $preparedArray = [];
        $preparedArray[ProductInterface::INTERNAL_PRODUCT_TYPE] = $this->getProductType($row);
        $preparedArray[ProductInterface::COLUMN_VISIBILITY] = $this->getVisibility($row, $headersInfo, $defaultValues);
        $preparedArray[ProductInterface::COLUMN_ATTRIBUTE_SET] = $this->getAttributeSetId($row, $headersInfo);
        $preparedArray[ProductInterface::COLUMN_TAX_CLASS_ID] = $this->getTaxClassId(
            $row,
            $headersInfo,
            $defaultValues
        );
        $preparedArray[ProductInterface::COLUMN_STATUS] = $this->getStatus($row, $headersInfo, $defaultValues);

        $urlKey = $row[ProductTransformer::COLUMN_URL_KEY] ?? null;
        $createRedirect = $row[ProductTransformer::COLUMN_URL_KEY_CREATE_REDIRECT] ?? null;
        if (null !== $urlKey) {
            $preparedArray[ProductInterface::COLUMN_URL_KEY] = $urlKey;
            $preparedArray[ProductInterface::COLUMN_URL_KEY_CREATE_REDIRECT] =
                isset($createRedirect) && '' !== $createRedirect
                    ? (int) $createRedirect
                    : 1;
        }

        //Set product zero on new products for creation. Price transformation will be execute after.
        $isNew = $row[ProductTransformer::COLUMN_IS_NEW] ?? false;
        if (true === $isNew) {
            $preparedArray[ProductAdapter::COLUMN_PRICE] = 0;
        }

        return $preparedArray;
    }

    private function getStoreInfo(array $row, array $headersInfo): array
    {
        $isNew = $row[ProductTransformer::COLUMN_IS_NEW] ?? false;
        switch ($isNew) {
            case true:
                return [0];
            case false:
                // if store_code column doesn't exist or it is empty
                if (!isset($row[ProductTransformer::COLUMN_STORE_CODE])) {
                    return [0];
                }

                return [$this->getStoreId($row, $headersInfo)];
        }
    }

    /**
     * @param array $row
     * @param array $headersInfo
     *
     * @return int
     */
    private function getStoreId(array $row, array $headersInfo): int
    {
        // Invalid store code will act as empty store code column or not existing column
        $options = $this->getColumnOptions($headersInfo, ProductTransformer::COLUMN_STORE_CODE);

        return \array_search($row[ProductTransformer::COLUMN_STORE_CODE], $options, true)
            ?: 0;
    }

    /**
     * @param array $row
     *
     * @return string|null
     */
    private function getProductType(array $row): ?string
    {
        return $row['current_product_type'] ?? $row[ProductTransformer::COLUMN_TYPE] ?? null;
    }

    /**
     * @param array $row
     *
     * @return string
     */
    private function getSku(array $row): ?string
    {
        return $row[ProductTransformer::COLUMN_SKU] ?? null;
    }

    /**
     * @param array $row
     * @param array $headersInfo
     *
     * @return int|null
     */
    private function getAttributeSetId(array $row, array $headersInfo): ?int
    {
        $options = $this->getColumnOptions($headersInfo, ProductTransformer::COLUMN_ATTRIBUTE_SET);

        $attributeSetName = $row[ProductTransformer::COLUMN_ATTRIBUTE_SET] ?? '';
        $value = \array_search($attributeSetName, $options, true);

        return (false !== $value) ? $value : null;
    }

    /**
     * @param array $row
     * @param array $headersInfo
     * @param array $defaultValues
     *
     * @return int|null
     */
    private function getVisibility(array $row, array $headersInfo, array $defaultValues): ?int
    {
        $isNew = $row[ProductTransformer::COLUMN_IS_NEW] ?? false;
        // for new products is there is not visibility column we take config default value
        if (
            true === $isNew
            && !\array_key_exists(ProductTransformer::COLUMN_VISIBILITY, $row)
        ) {
            switch ($this->getProductType($row)) {
                case ProductType::TYPE_SIMPLE:
                    return $defaultValues['defaultSimpleVisibility'] ?? null;
                case ProductType::TYPE_VIRTUAL:
                    return $defaultValues['defaultVirtualVisibility'] ?? null;
                case Grouped::TYPE_CODE:
                    return $defaultValues['defaultGroupedVisibility'] ?? null;
                case DownloadableType::TYPE_DOWNLOADABLE:
                    return $defaultValues['defaultDownloadableVisibility'] ?? null;
            }
        }

        $visibilityName = $row[ProductTransformer::COLUMN_VISIBILITY] ?? '';
        $options = $this->getColumnOptions($headersInfo, ProductTransformer::COLUMN_VISIBILITY);
        $visibilityValue = \array_search($visibilityName, $options, true);

        return false !== $visibilityValue ? $visibilityValue : null;
    }

    /**
     * @param array $row
     * @param array $headersInfo
     * @param array $defaultValues
     *
     * @return int|null
     */
    private function getTaxClassId(array $row, array $headersInfo, array $defaultValues): ?int
    {
        $isNew = $row[ProductTransformer::COLUMN_IS_NEW] ?? false;
        // for new products is there is not tax_class column we take config default value
        if (
            true === $isNew
            && !\array_key_exists(ProductTransformer::COLUMN_TAX_CLASS, $row)
        ) {
            return $defaultValues['defaultTaxClassValue'] ?? null;
        }

        $options = $this->getColumnOptions($headersInfo, ProductTransformer::COLUMN_TAX_CLASS);
        $taxClassName = $row[ProductTransformer::COLUMN_TAX_CLASS] ?? '';
        $taxClassId = \array_search($taxClassName, $options, true);

        return false !== $taxClassId ? $taxClassId : null;
    }

    /**
     * @param array $row
     * @param array $headersInfo
     * @param array $defaultValues
     *
     * @return int|null
     */
    private function getStatus(array $row, array $headersInfo, array $defaultValues): ?int
    {
        // for new products is there is not status column we take config default value
        $isNew = $row[ProductTransformer::COLUMN_IS_NEW] ?? false;
        if (
            true === $isNew
            && !\array_key_exists(ProductTransformer::COLUMN_STATUS, $row)
        ) {
            return $defaultValues['defaultStatus'] ?? null;
        }

        $options = $this->getColumnOptions($headersInfo, ProductTransformer::COLUMN_STATUS);
        $statusValue = $row[ProductTransformer::COLUMN_STATUS] ?? null;

        return \in_array((int) $statusValue, $options, true)
            ? (int) $statusValue
            : null;
    }

    /**
     * @param array $row
     *
     * @return array|null
     */
    private function getCategories(array $row): ?array
    {
        if (\array_key_exists(ProductTransformer::COLUMN_CATEGORIES_ID, $row)) {
            return '' !== $row[ProductTransformer::COLUMN_CATEGORIES_ID]
                ? \explode(',', $row[ProductTransformer::COLUMN_CATEGORIES_ID])
                : [];
        }

        return null;
    }

    /**
     * @param array $row
     *
     * @return array|null
     */
    private function getRelatedSkus(array $row): ?array
    {
        if (\array_key_exists(ProductTransformer::COLUMN_RELATED_SKUS, $row)) {
            return '' !== $row[ProductTransformer::COLUMN_RELATED_SKUS]
                ? \explode(',', $row[ProductTransformer::COLUMN_RELATED_SKUS])
                : [];
        }

        return null;
    }

    /**
     * @param array $row
     *
     * @return array|null
     */
    private function getUpsellSkus(array $row): ?array
    {
        if (\array_key_exists(ProductTransformer::COLUMN_UP_SELL_SKUS, $row)) {
            return '' !== $row[ProductTransformer::COLUMN_UP_SELL_SKUS]
                ? \explode(',', $row[ProductTransformer::COLUMN_UP_SELL_SKUS])
                : [];
        }

        return null;
    }

    /**
     * @param array $row
     *
     * @return array|null
     */
    private function getCrossSellSkus(array $row): ?array
    {
        if (\array_key_exists(ProductTransformer::COLUMN_CROSS_SELL_SKUS, $row)) {
            return '' !== $row[ProductTransformer::COLUMN_CROSS_SELL_SKUS]
                ? \explode(',', $row[ProductTransformer::COLUMN_CROSS_SELL_SKUS])
                : [];
        }

        return null;
    }

    /**
     * @param $row
     *
     * @return array
     */
    private function getAssociatedSkus($row): ?array
    {
        if (\array_key_exists(ProductTransformer::COLUMN_ASSOCIATED_SKUS, $row)) {
            return '' !== $row[ProductTransformer::COLUMN_ASSOCIATED_SKUS]
                ? \explode(',', $row[ProductTransformer::COLUMN_ASSOCIATED_SKUS])
                : [];
        }

        return null;
    }

    /**
     * @param array  $headersInfo
     * @param string $column
     *
     * @return array
     */
    private function getColumnOptions(array $headersInfo, string $column): array
    {
        return $headersInfo[$column][HeaderInformation::VALID_VALUES_KEY] ?? [];
    }

    /**
     * @param array $row
     * @param array $headersInfo
     *
     * @return array
     */
    private function prepareDynamicColumns(array $row, array $headersInfo): array
    {
        $result = [];

        foreach ($row as $key => $value) {
            if (\array_key_exists($key, $this->specialHeaders)) {
                continue;
            }

            $result[$key] = $this->getAttributeValue($key, $value, $headersInfo);
        }

        return $result;
    }

    private function getAttributeValue(string $attributeCode, $value, array $headersInfo)
    {
        if (null === $value || !isset($headersInfo[$attributeCode])) {
            return null;
        }

        if (0 === \strlen(\trim($value))) {
            return $value;
        }

        $validOptions = $headersInfo[$attributeCode][HeaderInformation::VALID_VALUES_KEY];

        $result = [];
        switch ($headersInfo[$attributeCode][HeaderInformation::TYPE_KEY]) {
            case 'select':
                $value = \trim($value);
                $optionId = \array_search($value, $validOptions, true);
                if (false === $optionId) {
                    $optionId = $this->addOption($attributeCode, $value);
                }

                return $optionId;
            case 'system select':
            case 'boolean':
                $optionId = \array_search(\trim($value), $validOptions, true);

                return false !== $optionId ? $optionId : null;
            case 'multi-select':
                $options = \explode(',', $value);
                foreach ($options as $optionValue) {
                    $optionValue = \trim($optionValue);
                    $optionId = \array_search($optionValue, $validOptions, true);
                    if (false === $optionId) {
                        $optionId = $this->addOption($attributeCode, $optionValue);
                    }
                    $result[] = $optionId;
                }

                return !empty($result) ? \implode(',', $result) : null;

            case 'system multi-select':
                $options = \explode(',', $value);
                foreach ($options as $optionValue) {
                    $optionId = \array_search(\trim($optionValue), $validOptions, true);
                    if (false === $optionId) {
                        continue;
                    }
                    $result[] = $optionId;
                }

                return !empty($result) ? \implode(',', $result) : null;
            default:
                return $value;
        }
    }

    /**
     * @param $attributeCode
     * @param $value
     *
     * @throws \Magento\Framework\Exception\StateException
     * @throws \Magento\Framework\Exception\InputException
     *
     * @return mixed
     */
    private function addOption(string $attributeCode, string $value): ?int
    {
        $option = $this->attributeOptionInterfaceFactory->create();
        $option->setLabel($value);
        $this->attributeOptionManagement->add(ProductModel::ENTITY, $attributeCode, $option);

        return (int) $option->getValue();
    }
}
