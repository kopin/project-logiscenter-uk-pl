<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImport\Model\Validation\FileValidator;

use Interactiv4\ProductImport\Model\Bootstrap\Element\Transformation\Product\ProductInterface;
use Magento\ImportExport\Model\Import;

class RequiredColumns extends FileValidatorAbstract
{
    /**
     * @var array
     */
    protected $modes = [
        Import::BEHAVIOR_APPEND,
        Import::BEHAVIOR_DELETE,
    ];

    private $requiredColumns = [
        ProductInterface::COLUMN_SKU,
    ];

    /**
     * {@inheritdoc}
     */
    public function isValid(array &$headers, array $rows): bool
    {
        foreach ($this->requiredColumns as $column) {
            if (!\in_array($column, $headers)) {
                $this->errors[] = \sprintf('The uploaded file does not have a %s column', $column);
            }
        }

        return empty($this->errors);
    }
}
