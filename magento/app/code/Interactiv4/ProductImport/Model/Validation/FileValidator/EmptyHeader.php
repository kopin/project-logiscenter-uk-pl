<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImport\Model\Validation\FileValidator;

use Magento\ImportExport\Model\Import;

class EmptyHeader extends FileValidatorAbstract
{
    /**
     * @var array
     */
    protected $modes = [
        Import::BEHAVIOR_APPEND,
    ];

    /**
     * {@inheritdoc}
     */
    public function isValid(array &$headers, array $rows): bool
    {
        foreach ($headers as $k => $header) {
            if (empty($header)) {
                $this->errors[] = \sprintf('The uploaded file includes empty header on the %sº column', $k + 1);
            }
        }

        return empty($this->errors);
    }
}
