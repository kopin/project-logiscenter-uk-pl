<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImport\Model\Validation\RowValidator;

use Interactiv4\ProductImport\Api\Validation\Validator\RowValidatorInterface;

abstract class RowValidatorAbstract implements RowValidatorInterface
{
    /**
     * @var array
     */
    protected $errors = [];

    /**
     * @var array
     */
    protected $modes = [];

    /**
     * {@inheritdoc}
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * @param $importMode
     *
     * @return bool
     */
    public function shouldValidate($importMode): bool
    {
        return \in_array($importMode, $this->modes);
    }
}
