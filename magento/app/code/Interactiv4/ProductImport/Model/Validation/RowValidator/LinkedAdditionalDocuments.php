<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImport\Model\Validation\RowValidator;

use Logiscenter\ProductDownloads\Model\Files\AdditionalLinks\GetNotExistingFiles;
use Magento\ImportExport\Model\Import;

class LinkedAdditionalDocuments extends RowValidatorAbstract
{
    /**
     * @var array
     */
    protected $modes = [
        Import::BEHAVIOR_APPEND,
    ];

    private $documentAttribute = 'additional_links';

    /**
     * @var GetNotExistingFiles
     */
    private $fileValidatorService;

    /**
     * LinkedAdditionalDocuments constructor.
     *
     * @param GetNotExistingFiles $fileValidatorService
     */
    public function __construct(
        GetNotExistingFiles $fileValidatorService
    ) {
        $this->fileValidatorService = $fileValidatorService;
    }

    /**
     * {@inheritdoc}
     */
    public function isValid(array $row): bool
    {
        $this->errors = [];
        if (\array_key_exists($this->documentAttribute, $row) && !empty($row[$this->documentAttribute])) {
            $invalidPathFiles = $this->fileValidatorService->execute($row[$this->documentAttribute]);
            if (!empty($invalidPathFiles)) {
                $this->errors[] = \sprintf('Additional Links files %s not found.', \implode(',', $invalidPathFiles));
            }
        }

        return empty($this->errors);
    }
}
