<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImport\Model\Validation\RowValidator;

use Interactiv4\ProductImport\Model\Validation\HeaderInformation;
use Magento\ImportExport\Model\Import;

class ValidValue extends RowValidatorAbstract
{
    /**
     * @var HeaderInformation
     */
    private $headerInfo;

    /**
     * ValidValue constructor.
     *
     * @param HeaderInformation $headerInfo
     */
    public function __construct(
        HeaderInformation $headerInfo
    ) {
        $this->headerInfo = $headerInfo;
    }

    /**
     * @var array
     */
    protected $modes = [
        Import::BEHAVIOR_APPEND,
    ];

    /**
     * {@inheritdoc}
     */
    public function isValid(array $row): bool
    {
        $this->errors = [];
        $invalidColumns = [];
        foreach ($row as $column => $value) {
            $columnInfo = $this->headerInfo->getColumnInformation($column);
            if (!empty($columnInfo) && '' !== $value && null !== $value && !$this->validateValue($value, $columnInfo)) {
                $invalidColumns[] = $column;
            }
        }
        if (!empty($invalidColumns)) {
            $this->errors[] = \sprintf('Invalid value for columns: %s', \implode(',', $invalidColumns));
        }

        return empty($this->errors);
    }

    /**
     * @param string $value
     * @param array  $columnInfo
     *
     * @return bool
     */
    private function validateValue(string $value, array $columnInfo): bool
    {
        $validValues = $columnInfo[HeaderInformation::VALID_VALUES_KEY] ?? [];
        switch ($columnInfo[HeaderInformation::TYPE_KEY]) {
            case 'select':
                //We accept new option values.
                return \strlen($value) <= 255;
            case 'multi-select':
                $options = \explode(',', $value);
                foreach ($options as $optionValue) {
                    if (\strlen(\trim($optionValue)) > 255) {
                        return false;
                    }
                }

                return true;
            case 'text':
                // We accept as text any string with arbitrary length
                return true;
            case 'varchar':
                return \strlen($value) <= 255;
            case 'integer':
            case 'decimal':
                return \is_numeric($value);
            case 'system select':
            case 'boolean':
                return \in_array(\trim($value), $validValues);
            case 'system multi-select':
                $options = \explode(',', $value);
                foreach ($options as $optionValue) {
                    if (!\in_array(\trim($optionValue), $validValues)) {
                        return false;
                    }
                }

                return true;
            case 'date':
                return $this->validateDate($value);
            default:
                return false;
        }
    }

    /**
     * @param string $date
     * @param string $format
     *
     * @return bool
     */
    protected function validateDate(string $date, string $format = 'Y-m-d'): bool
    {
        $d = \DateTime::createFromFormat($format, $date);

        return $d && $d->format($format) === $date;
    }
}
