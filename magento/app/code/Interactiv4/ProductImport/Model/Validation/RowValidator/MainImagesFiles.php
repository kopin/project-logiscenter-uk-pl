<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImport\Model\Validation\RowValidator;

use Interactiv4\ProductImport\Service\ImageExistChecker;
use Magento\ImportExport\Model\Import;

class MainImagesFiles extends RowValidatorAbstract
{
    /**
     * @var array
     */
    protected $modes = [
        Import::BEHAVIOR_APPEND,
    ];

    private $imageAttributes = [
        'image',
        'thumbnail',
        'small_image',
    ];

    /**
     * @var ImageExistChecker
     */
    private $imageExistService;

    /**
     * MainImagesFiles constructor.
     *
     * @param ImageExistChecker $imageExistService
     */
    public function __construct(
        ImageExistChecker $imageExistService
    ) {
        $this->imageExistService = $imageExistService;
    }

    /**
     * {@inheritdoc}
     */
    public function isValid(array $row): bool
    {
        $this->errors = [];
        foreach ($this->imageAttributes as $attribute) {
            if (
                \array_key_exists($attribute, $row)
                && !empty($row[$attribute])
                && !$this->imageExistService->isFileExistInSourceFolder($row[$attribute])
            ) {
                $this->errors[] = \sprintf('Image file %s in column %s not found.', $row[$attribute], $attribute);
            }
        }

        return empty($this->errors);
    }
}
