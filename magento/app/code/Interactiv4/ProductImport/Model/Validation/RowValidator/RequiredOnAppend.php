<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImport\Model\Validation\RowValidator;

use Interactiv4\ProductImport\Model\Bootstrap\Element\Transformation\Product\ProductInterface;
use Interactiv4\ProductImport\Model\Validation\HeaderInformation;
use Magento\ImportExport\Model\Import;

class RequiredOnAppend extends RowValidatorAbstract
{
    /**
     * @var array
     */
    protected $modes = [
        Import::BEHAVIOR_APPEND,
    ];

    private $requiredColumnsForExisting = [
        ProductInterface::COLUMN_SKU,
    ];

    /**
     * Columns attribute which must exist.
     *
     * @var string[]
     */
    private $requiredColumnsForNew = [
        ProductInterface::COLUMN_SKU,
        ProductInterface::COLUMN_TYPE,
        ProductInterface::COLUMN_ATTRIBUTE_SET,
        ProductInterface::COLUMN_URL_KEY,
        ProductInterface::COLUMN_NAME,
    ];

    /**
     * Columns attribute which must have value if exists.
     *
     * @var string[]
     */
    private $requiredValuesForNew = [
        ProductInterface::COLUMN_SKU,
        ProductInterface::COLUMN_TYPE,
        ProductInterface::COLUMN_ATTRIBUTE_SET,
        ProductInterface::COLUMN_URL_KEY,
        ProductInterface::COLUMN_TAX_CLASS,
        ProductInterface::COLUMN_STATUS,
        ProductInterface::COLUMN_VISIBILITY,
    ];

    /**
     * @var HeaderInformation
     */
    private $headerInformation;

    /**
     * @var array
     */
    private $dynamicRequiredAttributes;

    /**
     * RequiredOnAppend constructor.
     *
     * @param HeaderInformation $headerInformation
     */
    public function __construct(
        HeaderInformation $headerInformation
    ) {
        $this->headerInformation = $headerInformation;
    }

    /**
     * {@inheritdoc}
     */
    public function isValid(array $row): bool
    {
        $this->errors = [];

        switch (true) {
            case isset($row[ProductInterface::COLUMN_IS_NEW]) && true === $row[ProductInterface::COLUMN_IS_NEW]:
                $this->validateNew($row);
                break;
            default:
                $this->validateExisting($row);
        }
        $this->validateDynamicRequiredAttr($row);

        return empty($this->errors);
    }

    private function validateDynamicRequiredAttr($row): bool
    {
        $emptyValue = [];
        $requiredAttr = $this->getDynamicRequiredColumns($row);
        foreach ($requiredAttr as $attr) {
            if ('' === $row[$attr]) {
                $emptyValue[] = $attr;
            }
        }
        if (!empty($emptyValue)) {
            $this->errors[] = \sprintf(
                'No value specified in required column(s): %s ',
                \implode(',', $emptyValue)
            );
        }

        return empty($emptyValue);
    }

    /**
     * @param $row
     *
     * @return bool
     */
    private function validateExisting($row): bool
    {
        $missingValue = [];
        foreach ($this->requiredColumnsForExisting as $attribute) {
            if (\array_key_exists($attribute, $row) && empty($row[$attribute])) {
                $missingValue[] = $attribute;
            }
        }
        if (!empty($missingValue)) {
            $this->errors[] = \sprintf('No value specified in required column(s): %s', \implode(',', $missingValue));
        }

        return empty($missingValue);
    }

    /**
     * @param $row
     *
     * @return bool
     */
    private function validateNew($row): bool
    {
        $missingColumns = [];
        foreach ($this->requiredColumnsForNew as $columnName) {
            if (!\array_key_exists($columnName, $row)) {
                $missingColumns[] = $columnName;
            }
        }
        if (!empty($missingColumns)) {
            $this->errors[] = \sprintf(
                'Required column(s) not included for new product: %s',
                \implode(',', $missingColumns)
            );
        }

        $missingValues = [];
        foreach ($this->requiredValuesForNew as $columnName) {
            if (\array_key_exists($columnName, $row) && '' === $row[$columnName]) {
                $missingValues[] = $columnName;
            }
        }
        if (!empty($missingValues)) {
            $this->errors[] = \sprintf(
                'Required values are empty for new product: %s',
                \implode(',', $missingValues)
            );
        }

        return empty($missingColumns) && empty($missingValues);
    }

    /**
     * @param $row
     *
     * @return array
     */
    private function getDynamicRequiredColumns($row): array
    {
        if (!isset($this->dynamicRequiredAttributes)) {
            $this->dynamicRequiredAttributes = [];
            foreach ($row as $column => $value) {
                $headerInformation = $this->headerInformation->getColumnInformation($column);
                if (
                    !empty($headerInformation[HeaderInformation::TYPE_KEY])
                    && !empty($headerInformation[HeaderInformation::REQUIRED_KEY])
                ) {
                    $this->dynamicRequiredAttributes[] = $column;
                }
            }
        }

        return $this->dynamicRequiredAttributes;
    }
}
