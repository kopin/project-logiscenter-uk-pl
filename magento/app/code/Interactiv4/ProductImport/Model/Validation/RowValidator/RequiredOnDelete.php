<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImport\Model\Validation\RowValidator;

use Interactiv4\ProductImport\Model\Bootstrap\Element\Transformation\Product\ProductInterface;
use Magento\ImportExport\Model\Import;

class RequiredOnDelete extends RowValidatorAbstract
{
    /**
     * @var array
     */
    protected $modes = [
        Import::BEHAVIOR_DELETE,
    ];

    /**
     * Columns attribute which must exist and have value.
     *
     * @var string[]
     */
    private $requiredAttributes = [
        ProductInterface::COLUMN_SKU,
    ];

    /**
     * {@inheritdoc}
     */
    public function isValid(array $row): bool
    {
        $this->errors = [];
        $missingColumns = [];
        foreach ($this->requiredAttributes as $attribute) {
            if (!\array_key_exists($attribute, $row) || empty($row[$attribute])) {
                $missingColumns[] = $attribute;
            }
        }
        if (!empty($missingColumns)) {
            $this->errors[] = \sprintf('No value specified in required columns: %s', \implode(',', $missingColumns));
        }

        return empty($this->errors);
    }
}
