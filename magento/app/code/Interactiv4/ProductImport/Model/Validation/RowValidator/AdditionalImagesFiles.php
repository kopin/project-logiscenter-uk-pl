<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImport\Model\Validation\RowValidator;

use Interactiv4\ProductImport\Service\ImageExistChecker;
use Magento\ImportExport\Model\Import;

class AdditionalImagesFiles extends RowValidatorAbstract
{
    /**
     * @var array
     */
    protected $modes = [
        Import::BEHAVIOR_APPEND,
    ];

    private $imageAttribute = 'additional_images';

    /**
     * @var ImageExistChecker
     */
    private $imageExistService;

    /**
     * MainImagesFiles constructor.
     *
     * @param ImageExistChecker $imageExistService
     */
    public function __construct(
        ImageExistChecker $imageExistService
    ) {
        $this->imageExistService = $imageExistService;
    }

    /**
     * {@inheritdoc}
     */
    public function isValid(array $row): bool
    {
        $this->errors = [];
        $invalidPathFile = [];
        if (\array_key_exists($this->imageAttribute, $row) && !empty($row[$this->imageAttribute])) {
            $pathFiles = \explode(',', $row[$this->imageAttribute]);
            foreach ($pathFiles as $file) {
                $file = \strtok($file, '|');
                if (!$this->imageExistService->isFileExistInSourceFolder($file)) {
                    $invalidPathFile[] = $file;
                }
            }
        }

        if (!empty($invalidPathFile)) {
            $this->errors[] = \sprintf(
                ' Image files %s in additional_images not found',
                \implode(',', $invalidPathFile)
            );
        }

        return empty($this->errors);
    }
}
