<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImport\Model\Validation;

use Interactiv4\ProductImport\Model\Bootstrap\Element\Transformation\Product\ProductInterface;
use Interactiv4\ProductImport\Model\Source\SpecialHeader;
use Magento\Catalog\Model\Product as ProductModel;
use Magento\Catalog\Model\Product\Attribute\Backend\Price;
use Magento\Catalog\Model\Product\Attribute\Backend\Weight;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Catalog\Model\Product\AttributeSet\Options;
use Magento\Catalog\Model\Product\Type as ProductType;
use Magento\Catalog\Model\Product\Visibility;
use Magento\Downloadable\Model\Product\Type as DownloadableType;
use Magento\Eav\Model\Config;
use Magento\Eav\Model\Entity\Attribute\AbstractAttribute;
use Magento\Eav\Model\Entity\Attribute\Source\Boolean;
use Magento\Framework\Exception\LocalizedException;
use Magento\GroupedProduct\Model\Product\Type\Grouped as GroupedType;
use Magento\Msrp\Model\Product\Attribute\Source\Type\Price as PriceSource;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Tax\Model\TaxClass\Source\Product;

class HeaderInformation
{
    public const TYPE_KEY = 'type';
    public const VALID_VALUES_KEY = 'valid_values';
    public const REQUIRED_KEY = 'required';

    private $supportedTypes;

    private $dynamicHeader = [];

    private $specialHeaders;

    /**
     * @var Config
     */
    private $eavConfig;

    /**
     * @var Options
     */
    private $attributeSetOptions;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var Product
     */
    private $taxClasses;

    /**
     * @var PriceSource
     */
    private $typePriceSource;

    /**
     * @var Status
     */
    private $productStatusSource;

    /**
     * HeaderInformation constructor.
     *
     * @param Config                $eavConfig
     * @param Options               $attributeSetOptions
     * @param StoreManagerInterface $storeManager
     * @param Product               $taxClasses
     * @param SpecialHeader         $specialHeaderSource
     * @param PriceSource           $typePriceSource
     * @param array                 $supportedTypes
     */
    public function __construct(
        Config $eavConfig,
        Options $attributeSetOptions,
        StoreManagerInterface $storeManager,
        Product $taxClasses,
        SpecialHeader $specialHeaderSource,
        PriceSource $typePriceSource,
        Status $productStatus,
        array $supportedTypes = []
    ) {
        $this->eavConfig = $eavConfig;
        $this->attributeSetOptions = $attributeSetOptions;
        $this->storeManager = $storeManager;
        $this->taxClasses = $taxClasses;
        $this->specialHeaders = $specialHeaderSource->getHeaders();
        $this->typePriceSource = $typePriceSource;
        $this->productStatusSource = $productStatus;
        $this->supportedTypes = $supportedTypes;
    }

    /**
     * Get information for a column.
     *
     * Empty result means that the headers is not allowed
     *
     * @param $columnName
     *
     * @return array
     */
    public function getColumnInformation($columnName): array
    {
        if (empty($columnName)) {
            return [];
        }
        if (\array_key_exists($columnName, $this->specialHeaders)) {
            return $this->getSpecialHeaderInformation($columnName);
        }

        return $this->getDynamicHeaderInformation($columnName);
    }

    /**
     * @param $columnName
     *
     * @return array
     */
    private function getSpecialHeaderInformation($columnName): array
    {
        if (!\array_key_exists(self::VALID_VALUES_KEY, $this->specialHeaders[$columnName])) {
            $validValues = $this->getSpecialAllowedValues($columnName);
            $this->specialHeaders[$columnName][self::VALID_VALUES_KEY] = $validValues;
        }

        return $this->specialHeaders[$columnName];
    }

    /**
     * @param $columnName
     *
     * @return array
     */
    private function getDynamicHeaderInformation($columnName): array
    {
        if (!\array_key_exists($columnName, $this->dynamicHeader)) {
            try {
                $attribute = $this->eavConfig->getAttribute(ProductModel::ENTITY, $columnName);
                $info = $this->getAttributeInfo($attribute);
                $info[self::VALID_VALUES_KEY] = $this->getAllowedValues(
                    $info[self::TYPE_KEY],
                    $attribute
                );
            } catch (LocalizedException $e) {
                $info[self::TYPE_KEY] = '';
            }

            if (!\in_array($info[self::TYPE_KEY], $this->supportedTypes)) {
                $this->dynamicHeader[$columnName] = [];

                return $this->dynamicHeader[$columnName];
            }

            $this->dynamicHeader[$columnName] = $info;
        }

        return $this->dynamicHeader[$columnName];
    }

    /**
     * Get attribute info as array ['type' => 'attributeType', 'required' => 0/1].
     *
     * Returning array with empty TYPE_KEY means the column name is not a valid attribute
     *
     * @param AbstractAttribute|null $attribute
     *
     * @return array
     */
    private function getAttributeInfo(AbstractAttribute $attribute = null): array
    {
        $result[self::TYPE_KEY] = '';

        if (null === $attribute || null === $attribute->getId()) {
            return $result;
        }

        $frontEndInput = $attribute->getFrontendInput();
        $backendModel = $attribute->getBackendModel();
        $sourceModel = $attribute->getSourceModel();
        $backendType = $attribute->getBackendType();
        $isUserDefined = (int) $attribute->getIsUserDefined();
        $isRequired = (int) $attribute->getIsRequired();
        if (Boolean::class === $sourceModel && 'int' === $backendType && null === $backendModel) {
            $result[self::TYPE_KEY] = 'boolean';
        }
        if (\in_array($frontEndInput, ['text', 'textarea']) && 'varchar' === $backendType && null === $backendModel) {
            $result[self::TYPE_KEY] = 'varchar';
        }
        if (\in_array($frontEndInput, ['text', 'textarea']) && 'text' === $backendType && null === $backendModel) {
            $result[self::TYPE_KEY] = 'text';
        }

        if ('text' === $frontEndInput && 'int' === $backendType && null === $backendModel) {
            $result[self::TYPE_KEY] = 'integer';
        }

        if (
            'decimal' === $backendType
            && (('text' === $frontEndInput && null === $backendModel)
                || ('price' === $frontEndInput && Price::class === $backendModel)
                || ('weight' === $frontEndInput && Weight::class === $backendModel))
        ) {
            $result[self::TYPE_KEY] = 'decimal';
        }

        if (
            'select' === $frontEndInput
            && \in_array($backendType, ['int', 'varchar'])
            && 0 === $isUserDefined && $attribute->usesSource()
        ) {
            $result[self::TYPE_KEY] = 'system select';
        }

        if ('select' === $frontEndInput && 'int' === $backendType && 1 === $isUserDefined && $attribute->usesSource()) {
            $result[self::TYPE_KEY] = 'select';
        }

        if (
            'multiselect' === $frontEndInput
            && 'varchar' === $backendType
            && 0 === $isUserDefined
            && $attribute->usesSource()
        ) {
            $result[self::TYPE_KEY] = 'system multi-select';
        }

        if (
            'multiselect' === $frontEndInput
            && 'varchar' === $backendType
            && 1 === $isUserDefined
            && $attribute->usesSource()
        ) {
            $result[self::TYPE_KEY] = 'multi-select';
        }

        if ('date' === $frontEndInput && \in_array($backendType, ['static', 'datetime'])) {
            $result[self::TYPE_KEY] = 'date';
        }

        if (!empty($result[self::TYPE_KEY])) {
            $result[self::REQUIRED_KEY] = $isRequired;
        }

        return $result;
    }

    /**
     * @param AbstractAttribute|null $attribute
     * @param                        $type
     *
     * @return array|int[]
     */
    private function getAllowedValues($type, AbstractAttribute $attribute = null): array
    {
        if ('boolean' === $type) {
            return [0 => '0', 1 => '1'];
        }

        if (null === $attribute || null === $attribute->getId() || !$attribute->usesSource()) {
            return [];
        }

        $allowedValues = $attribute->getSource()->getAllOptions();
        $allowedValues = \array_filter(
            $allowedValues,
            function ($value) {
                return '' !== $value['value'];
            }
        );
        switch ($type) {
            case 'select':
            case 'multi-select':
            case 'system select':
            case 'system multi-select':
                $allowedValues = \array_map(
                    function ($option) {
                        return (string) $option;
                    },
                    \array_column($allowedValues, 'label', 'value')
                );
                break;
            default:
                $allowedValues = [];
        }

        return $allowedValues;
    }

    private function getSpecialAllowedValues($columnName): array
    {
        switch ($columnName) {
            case ProductInterface::COLUMN_TYPE:
                $allowedValues = [
                    ProductType::TYPE_SIMPLE,
                    ProductType::TYPE_VIRTUAL,
                    GroupedType::TYPE_CODE,
                    DownloadableType::TYPE_DOWNLOADABLE,
                ];
                break;
            case ProductInterface::COLUMN_TAX_CLASS:
                $allowedValues = $this->getTaxClassOptions();
                break;
            case ProductInterface::COLUMN_STORE_CODE:
                $allowedValues = $this->getStoreCodeOptions();
                break;
            case ProductInterface::COLUMN_ATTRIBUTE_SET:
                $allowedValues = $this->getAttributeSetOptions();
                break;
            case ProductInterface::COLUMN_VISIBILITY:
                $allowedValues = $this->getVisibilityOptions();
                break;
            case ProductInterface::COLUMN_STATUS:
                $allowedValues = $this->getProductStatusOptions();
                break;
            case ProductInterface::COLUMN_MSRP_DISPLAY_ACTUAL_PRICE:
                $allowedValues = $this->getDisplayActualPriceTypeOptions();
                break;
            default:
                $type = $this->specialHeaders[$columnName][self::TYPE_KEY];
                $attribute = $this->eavConfig->getAttribute(ProductModel::ENTITY, $columnName);
                $allowedValues = $this->getAllowedValues($type, $attribute);
        }

        return $allowedValues;
    }

    /**
     * @return array
     */
    private function getTaxClassOptions(): array
    {
        $options = \array_column(
            $this->taxClasses->toOptionArray(),
            'label',
            'value'
        );

        return \array_map(
            function ($option) {
                return (string) $option;
            },
            $options
        );
    }

    /**
     * @return array
     */
    private function getStoreCodeOptions(): array
    {
        $stores = $this->storeManager->getStores();

        return \array_map(
            function ($store) {
                return $store->getCode();
            },
            $stores
        );
    }

    /**
     * @return array
     */
    private function getAttributeSetOptions(): array
    {
        $options = $this->attributeSetOptions->toOptionArray();

        return \array_column($options, 'label', 'value');
    }

    /**
     * @return array
     */
    private function getVisibilityOptions(): array
    {
        $options = Visibility::getOptionArray();

        return \array_map(
            function ($val) {
                return (string) $val;
            },
            $options
        );
    }

    /**
     * @return array
     */
    private function getDisplayActualPriceTypeOptions(): array
    {
        return \array_column($this->typePriceSource->getAllOptions(), 'value', 'value');
    }

    /**
     * @return array
     */
    private function getProductStatusOptions(): array
    {
        return \array_column($this->productStatusSource->getAllOptions(), 'value', 'value');
    }
}
