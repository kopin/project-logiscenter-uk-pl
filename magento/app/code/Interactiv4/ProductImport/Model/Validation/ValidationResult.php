<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImport\Model\Validation;

use Interactiv4\ProductImport\Api\Validation\ValidationResultInterface;
use Interactiv4\ProductImport\Model\Bootstrap\Element\Transformation\Product\ProductInterface;

class ValidationResult implements ValidationResultInterface
{
    /**
     * @var bool
     */
    private $validFile = true;

    /**
     * @var array
     */
    private $validatedRows = [];

    /**
     * @var array
     */
    private $invalidatedRows = [];

    /**
     * @var array
     */
    private $rowErrorsMsg = [];

    /**
     * @var array
     */
    private $fileErrorsMsg = [];

    /**
     * @var int
     */
    private $totalRows = 0;

    /**
     * @var int
     */
    private $totalNewRows = 0;

    /**
     * @var int
     */
    private $totalUpdatedRows = 0;

    /**
     * {@inheritDoc}
     */
    public function isValid(): bool
    {
        $maxErrors = 10;

        return !(!$this->isValidFile() || $this->getTotalErrorRows() > $maxErrors || 0 === $this->getTotalValidRows());
    }

    /**
     * {@inheritDoc}
     */
    public function isValidFile(): bool
    {
        return $this->validFile;
    }

    /**
     * {@inheritDoc}
     */
    public function setValidFile(bool $validFile): void
    {
        $this->validFile = $validFile;
    }

    /**
     * {@inheritDoc}
     */
    public function getValidatedRows(): array
    {
        return $this->validatedRows;
    }

    /**
     * {@inheritDoc}
     */
    public function addValidatedRow(int $rowNum, array $row): void
    {
        if (isset($row[ProductInterface::COLUMN_IS_NEW]) && true === $row[ProductInterface::COLUMN_IS_NEW]) {
            ++$this->totalNewRows;
        }
        if (isset($row[ProductInterface::COLUMN_IS_NEW]) && false === $row[ProductInterface::COLUMN_IS_NEW]) {
            ++$this->totalUpdatedRows;
        }

        $this->validatedRows[$rowNum] = $row;
    }

    /**
     * {@inheritDoc}
     */
    public function getInvalidatedRows(): array
    {
        return $this->invalidatedRows;
    }

    /**
     * {@inheritDoc}
     */
    public function addInvalidatedRow(int $rowNum, array $row): void
    {
        $this->invalidatedRows[$rowNum] = $row;
    }

    /**
     * {@inheritDoc}
     */
    public function getRowErrorsMsg(): array
    {
        return $this->rowErrorsMsg;
    }

    /**
     * {@inheritDoc}
     */
    public function addRowErrorMsg(string $msg): void
    {
        $this->rowErrorsMsg[] = $msg;
    }

    /**
     * {@inheritDoc}
     */
    public function getFileErrorsMsg(): array
    {
        return $this->fileErrorsMsg;
    }

    /**
     * {@inheritDoc}
     */
    public function addFileErrorMsg(string $msg): void
    {
        $this->fileErrorsMsg[] = $msg;
    }

    /**
     * {@inheritDoc}
     */
    public function getTotalRows(): int
    {
        return $this->totalRows;
    }

    /**
     * {@inheritDoc}
     */
    public function setTotalRows($totalRows): int
    {
        return $this->totalRows = $totalRows;
    }

    /**
     * {@inheritDoc}
     */
    public function getTotalValidRows(): int
    {
        return \count($this->validatedRows);
    }

    /**
     * {@inheritDoc}
     */
    public function getTotalErrorRows(): int
    {
        return \count($this->invalidatedRows);
    }

    /**
     * {@inheritDoc}
     */
    public function getTotalNewRows(): int
    {
        return $this->totalNewRows;
    }

    /**
     * {@inheritDoc}
     */
    public function getTotalUpdatedRows(): int
    {
        return $this->totalUpdatedRows;
    }
}
