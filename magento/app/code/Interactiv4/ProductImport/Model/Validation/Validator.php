<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImport\Model\Validation;

use Interactiv4\ProductImport\Api\Validation\ValidationResultInterface;
use Interactiv4\ProductImport\Api\Validation\Validator\FileValidatorInterface;
use Interactiv4\ProductImport\Api\Validation\Validator\RowValidatorInterface;
use Interactiv4\ProductImport\Api\Validation\ValidatorInterface;
use Interactiv4\ProductImport\Model\Bootstrap\Element\Reader\CsvReader as Reader;
use Interactiv4\ProductImport\Model\Bootstrap\Element\Transformation\Product\ProductInterface;
use Magento\Framework\App\ResourceConnection;
use Magento\ImportExport\Model\Import;

class Validator implements ValidatorInterface
{
    private const BATCH_SIZE = 10000;

    private const COLUMNS_TO_BE_IGNORED_FOR_NEXT_OCCURRENCES = [
        'image',
        'small_image',
        'thumbnail',
        'image_label',
        'small_image_label',
        'thumbnail_label',
        'additional_images',
        'category_ids',
        'grouped_skus',
        're_skus',
        'up_skus',
        'xs_skus',
    ];

    private const COLUMNS_TO_BE_IGNORED_FOR_EXISTING_PRODUCTS = [
        'attribute_set',
        'type',
    ];

    /**
     * @var ValidationResultInterface
     */
    private $validatorResult;

    /**
     * @var array
     */
    private $rowValidators;

    /**
     * @var array
     */
    private $fileValidators;

    /**
     * @var string
     */
    private $importMode;

    /**
     * @var Reader
     */
    private $reader;

    /**
     * @var array
     */
    private $fileData = [];

    /**
     * @var HeaderInformation
     */
    private $headerInformation;

    /**
     * @var array
     */
    private $headersInformationValues = [];

    /**
     * @var array
     */
    private $productsTypeData;

    /**
     * @var ResourceConnection
     */
    private $resource;

    /**
     * Validator constructor.
     *
     * @param ValidationResultInterface $validatorResult
     * @param Reader                    $reader
     * @param HeaderInformation         $headerInformation
     * @param ResourceConnection        $resource
     */
    public function __construct(
        ValidationResultInterface $validatorResult,
        Reader $reader,
        HeaderInformation $headerInformation,
        ResourceConnection $resource,
        array $rowValidators = [],
        array $fileValidators = []
    ) {
        $this->validatorResult = $validatorResult;
        $this->reader = $reader;
        $this->headerInformation = $headerInformation;
        $this->resource = $resource;
        $this->rowValidators = $rowValidators;
        $this->fileValidators = $fileValidators;
    }

    /**
     * {@inheritdoc}
     */
    public function getImportMode(): string
    {
        return $this->importMode;
    }

    /**
     * {@inheritdoc}
     */
    public function setImportMode(string $importMode): ValidatorInterface
    {
        $this->importMode = $importMode;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getResult(): ValidationResultInterface
    {
        return $this->validatorResult;
    }

    /**
     * {@inheritdoc}
     */
    public function validateFile(string $filename): ValidatorInterface
    {
        $rows = $this->getRows($filename);
        $headers = $this->getHeaders($filename);

        return $this->validate($headers, $rows);
    }

    /**
     * @return array
     */
    public function getHeadersInformationValues(): array
    {
        return $this->headersInformationValues;
    }

    /**
     * {@inheritdoc}
     */
    public function validate(array $headers, array $rows): ValidatorInterface
    {
        try {
            $this->validateHeaders($headers);
            $this->validateGlobal($headers, $rows);
            if (!$this->validatorResult->isValidFile()) {
                return $this;
            }
            $this->validateRows($rows);
            if (0 === $this->validatorResult->getTotalValidRows()) {
                $this->validatorResult->addFileErrorMsg('There is no valid row in the file');
            }
        } catch (\Exception $e) {
            $this->validatorResult->setValidFile(false);
            $this->validatorResult->addRowErrorMsg('The uploaded file caused an unexpected error ' . $e->getMessage());
        }

        return $this;
    }

    /**
     * Validate headers and retain valid header information on headersInformationValues.
     *
     * @param $headers
     */
    public function validateHeaders($headers): void
    {
        $invalidHeaders = [];
        if (Import::BEHAVIOR_APPEND === $this->getImportMode()) {
            foreach ($headers as $k => $header) {
                if (!empty($header)) {
                    $headerInformation = $this->headerInformation->getColumnInformation($header);
                    if (!isset($headerInformation[HeaderInformation::TYPE_KEY])) {
                        $colPosition = $k + 1;
                        $invalidHeaders[$colPosition] = $header;
                        continue;
                    }
                    $this->headersInformationValues[$header] = $headerInformation;
                }
            }
            if (!empty($invalidHeaders)) {
                $this->validatorResult->setValidFile(false);
                $invalidHeaders = \implode(',', $invalidHeaders);
                $this->validatorResult->addFileErrorMsg(
                    \sprintf('The uploaded file contains invalid column(s): %s', $invalidHeaders)
                );
            }
        }
    }

    /**
     * Validate the file at global level.
     *
     * @param array $headers
     * @param array $rows
     */
    private function validateGlobal(array &$headers, array $rows): void
    {
        $this->validatorResult->setTotalRows(\count($rows));
        /** @var FileValidatorInterface $validator */
        foreach ($this->fileValidators as $validator) {
            if (!$validator->shouldValidate($this->getImportMode()) || $validator->isValid($headers, $rows)) {
                continue;
            }

            foreach ($validator->getErrors() as $error) {
                $this->validatorResult->addFileErrorMsg($error);
            }
            $this->validatorResult->setValidFile(false);
        }
    }

    /**
     * Validate each row.
     *
     * @param array $rows
     */
    private function validateRows(array $rows): void
    {
        $chunks = \array_chunk($rows, self::BATCH_SIZE, true);
        foreach ($chunks as $chunk) {
            $skus = \array_column($chunk, ProductInterface::COLUMN_SKU);
            $this->initProductTypeData($skus);
            foreach ($chunk as $rowNum => $row) {
                $this->adjustDataInRow($row);
                if (!$this->validateRow($rowNum, $row)) {
                    $this->validatorResult->addInvalidatedRow($rowNum, $row);
                    continue;
                }
                $this->validatorResult->addValidatedRow($rowNum, $row);
            }
        }
    }

    /**
     * @param int   $rowNum
     * @param array $row
     *
     * @return bool
     */
    private function validateRow(int $rowNum, array $row): bool
    {
        $isValid = true;
        /** @var RowValidatorInterface $validator */
        foreach ($this->rowValidators as $validator) {
            if (!$validator->shouldValidate($this->getImportMode()) || $validator->isValid($row)) {
                continue;
            }

            $sku = $row[ProductInterface::COLUMN_SKU] ?? '';
            foreach ($validator->getErrors() as $error) {
                $this->addRowErrorMsg($rowNum, $sku, $error);
            }
            $isValid = false;
        }

        return $isValid;
    }

    /**
     * @param $row
     */
    private function adjustDataInRow(&$row): void
    {
        \Magento\Framework\Profiler::start('adjustdatainrow');
        $existingValidSkus = \array_column($this->validatorResult->getValidatedRows(), ProductInterface::COLUMN_SKU);
        $this->addCurrentValues($row, $existingValidSkus);
        $this->removeIgnoredValues($row, $existingValidSkus);
        \Magento\Framework\Profiler::stop('adjustdatainrow');
    }

    /**
     * @param $row
     * @param $existingValidSkus
     */
    private function addCurrentValues(&$row, $existingValidSkus): void
    {
        $row[ProductInterface::COLUMN_IS_NEW] = true;
        $sku = $row[ProductInterface::COLUMN_SKU] ?? '';
        $exist = \array_key_exists($sku, $this->productsTypeData);

        if ($exist) {
            $row['current_product_type'] = $this->productsTypeData[$sku];
        }

        // Considering no new product when it exists on database or when a previous row has been validated
        if ($exist || \in_array($sku, $existingValidSkus)) {
            $row[ProductInterface::COLUMN_IS_NEW] = false;
        }
    }

    /**
     * Remove value which will be ignored.
     *
     * @param $row
     * @param $existingValidSkus
     */
    private function removeIgnoredValues(&$row, $existingValidSkus): void
    {
        $sku = $row[ProductInterface::COLUMN_SKU] ?? '';
        //Remove columns for next occurrences
        if (\in_array($sku, $existingValidSkus)) {
            foreach (self::COLUMNS_TO_BE_IGNORED_FOR_NEXT_OCCURRENCES as $column) {
                if (\array_key_exists($column, $row)) {
                    unset($row[$column]);
                }
            }
        }
        // Remove column for existing products
        if (false === $row[ProductInterface::COLUMN_IS_NEW]) {
            foreach (self::COLUMNS_TO_BE_IGNORED_FOR_EXISTING_PRODUCTS as $column) {
                if (\array_key_exists($column, $row)) {
                    unset($row[$column]);
                }
            }
        }
    }

    /**
     * @param array $skuList
     *
     * @return array
     */
    private function initProductTypeData(array $skuList): array
    {
        $connection = $this->resource->getConnection();
        $select = $connection->select()->from(
            'catalog_product_entity',
            ['sku', 'type_id']
        )->where(
            'sku IN(?)',
            $skuList
        );

        $this->productsTypeData = $connection->fetchPairs($select);

        return  $this->productsTypeData;
    }

    /**
     * @param $filename
     *
     * @return array
     */
    private function getFileData($filename): array
    {
        if (empty($this->fileData)) {
            $this->fileData = $this->reader->getFileData($filename) ?? [];
        }

        return $this->fileData;
    }

    /**
     * @param $filename
     *
     * @return array
     */
    private function getHeaders($filename): array
    {
        $data = $this->getFileData($filename);

        return $data['headers'] ?? [];
    }

    /**
     * @param $filename
     *
     * @return array
     */
    private function getRows($filename): array
    {
        $data = $this->getFileData($filename);

        return $data['rows'] ?? [];
    }

    /**
     * @param int    $rowNum
     * @param string $sku
     * @param string $message
     */
    private function addRowErrorMsg(int $rowNum, string $sku, string $message): void
    {
        $this->validatorResult->addRowErrorMsg(
            \sprintf('Row %s, sku %s skipped with error: %s ', $rowNum, $sku, $message)
        );
    }
}
