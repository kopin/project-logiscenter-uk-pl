<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImport\Model\Mail;

use Magento\Framework\Mail\EmailMessageInterface;
use Magento\Framework\Mail\EmailMessageInterfaceFactory;
use Magento\Framework\Mail\MimeMessageInterfaceFactory;
use Magento\Framework\Message\MessageInterface;

/**
 * Email with attachment message builder.
 */
class MessageBuilder
{
    /**
     * @var EmailMessageInterfaceFactory
     */
    private $emailMessageFactory;

    /**
     * @var MimeMessageInterfaceFactory
     */
    private $mimeMessageFactory;

    /**
     * @var EmailMessageInterface|MessageInterface
     */
    private $oldMessage;

    /**
     * @var array
     */
    private $messageParts = [];

    /**
     * @param EmailMessageInterfaceFactory $emailMessageFactory
     * @param MimeMessageInterfaceFactory  $mimeMessageFactory
     */
    public function __construct(
        EmailMessageInterfaceFactory $emailMessageFactory,
        MimeMessageInterfaceFactory $mimeMessageFactory
    ) {
        $this->emailMessageFactory = $emailMessageFactory;
        $this->mimeMessageFactory = $mimeMessageFactory;
    }

    /**
     * Build email message.
     *
     * @return EmailMessageInterface
     */
    public function build(): EmailMessageInterface
    {
        $parts = $this->oldMessage->getBody()->getParts();
        $parts = \array_merge($parts, $this->messageParts);
        $messageData = [
            'body' => $this->mimeMessageFactory->create(
                ['parts' => $parts]
            ),
            'from' => $this->oldMessage->getFrom(),
            'to' => $this->oldMessage->getTo(),
            'replyTo' => $this->getReplyTo(),
            'subject' => $this->oldMessage->getSubject(),
        ];

        return $this->emailMessageFactory->create($messageData);
    }

    /**
     * Old message setter.
     *
     * @param EmailMessageInterface|MessageInterface $oldMessage
     *
     * @return void
     */
    public function setOldMessage($oldMessage): void
    {
        $this->oldMessage = $oldMessage;
    }

    /**
     * Set new message parts.
     *
     * @param array $messageParts
     *
     * @return void
     */
    public function setMessageParts(array $messageParts): void
    {
        $this->messageParts = $messageParts;
    }

    /**
     * Return reply to addresses array.
     *
     * @return \Magento\Framework\Mail\Address[]
     */
    private function getReplyTo(): array
    {
        $replyToInOldMessage = $this->oldMessage->getReplyTo();

        if (\is_array($replyToInOldMessage) && \count($replyToInOldMessage)) {
            return $replyToInOldMessage;
        }

        return $this->oldMessage->getFrom();
    }
}
