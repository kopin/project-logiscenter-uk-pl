<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImport\Model\Mail;

use Laminas\Mime\Mime;
use Laminas\Mime\Part;
use Magento\Framework\Mail\MessageInterface;
use Magento\Framework\Mail\Template\FactoryInterface;
use Magento\Framework\Mail\Template\SenderResolverInterface;
use Magento\Framework\Mail\Template\TransportBuilder as Transport;
use Magento\Framework\Mail\TransportInterfaceFactory;
use Magento\Framework\ObjectManagerInterface;

/**
 * Send email with attachment transport builder.
 */
class TransportBuilder extends Transport
{
    /**
     * @var array
     */
    private $parts = [];

    /**
     * @var MessageBuilderFactory
     */
    protected $messageBuilderFactory;

    /**
     * @param FactoryInterface          $templateFactory
     * @param MessageInterface          $message
     * @param SenderResolverInterface   $senderResolver
     * @param ObjectManagerInterface    $objectManager
     * @param TransportInterfaceFactory $mailTransportFactory
     * @param MessageBuilderFactory     $messageBuilderFactory
     */
    public function __construct(
        FactoryInterface $templateFactory,
        MessageInterface $message,
        SenderResolverInterface $senderResolver,
        ObjectManagerInterface $objectManager,
        TransportInterfaceFactory $mailTransportFactory,
        MessageBuilderFactory $messageBuilderFactory
    ) {
        $this->messageBuilderFactory = $messageBuilderFactory;

        parent::__construct(
            $templateFactory,
            $message,
            $senderResolver,
            $objectManager,
            $mailTransportFactory
        );
    }

    /**
     * @param $content
     * @param null   $filename
     * @param string $mimeType
     * @param string $disposition
     * @param string $encoding
     *
     * @return $this
     */
    public function addAttachment(
        $content,
        $filename = null,
        string $mimeType = Mime::TYPE_OCTETSTREAM,
        string $disposition = Mime::DISPOSITION_ATTACHMENT,
        string $encoding = Mime::ENCODING_BASE64
    ) {
        $mp = new Part($content);
        $mp->encoding = $encoding;
        $mp->type = $mimeType;
        $mp->disposition = $disposition;
        $mp->filename = $filename;
        $this->parts[] = $mp;

        return $this;
    }

    /**
     * @throws \Magento\Framework\Exception\LocalizedException
     *
     * @return $this
     */
    protected function prepareMessage()
    {
        parent::prepareMessage();
        $messageBuilder = $this->messageBuilderFactory->create();
        $messageBuilder->setOldMessage($this->message);
        $messageBuilder->setMessageParts($this->parts);
        $this->message = $messageBuilder->build();

        return $this;
    }
}
