<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImport\Model\Consumer;

use Interactiv4\ProductImport\Model\Queue\ProcessData;
use Interactiv4\ProductImport\Service\Import\AppendProcess;
use Interactiv4\ProductImport\Service\Import\DeleteProcess;
use Interactiv4\ProductImport\Service\Import\ValidateProcess;
use Magento\ImportExport\Model\Import;

class ProductImportProcess
{
    /**
     * @var AppendProcess
     */
    private $appendService;

    /**
     * @var DeleteProcess
     */
    private $deleteService;

    /**
     * @var ValidateProcess
     */
    private $validateService;

    /**
     * ProductImportProcess constructor.
     *
     * @param AppendProcess   $appendService
     * @param DeleteProcess   $deleteService
     * @param ValidateProcess $validatorFactory
     */
    public function __construct(
        AppendProcess $appendService,
        DeleteProcess $deleteService,
        ValidateProcess $validatorFactory
    ) {
        $this->appendService = $appendService;
        $this->deleteService = $deleteService;
        $this->validateService = $validatorFactory;
    }

    /**
     * @param ProcessData $processData
     *
     * @return bool
     */
    public function execute(ProcessData $processData): bool
    {
        $mode = $processData->getMode();
        $file = $processData->getFile();
        $action = $processData->getAction();

        if ('validate' === $action) {
            $this->validateService->execute($file, $mode);
        }
        if ('import' === $action) {
            switch ($mode) {
                case Import::BEHAVIOR_APPEND:
                    $this->appendService->execute($file);
                    break;
                case Import::BEHAVIOR_DELETE:
                    $this->deleteService->execute($file);
                    break;
                default:
                    return false;
            }
        }

        return true;
    }
}
