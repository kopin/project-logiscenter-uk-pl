<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImport\Model\Source;

use Interactiv4\Framework\Data\AbstractOptionSource;
use Magento\ImportExport\Model\Import;

/**
 * Class ImportMode.
 */
class ImportMode extends AbstractOptionSource
{
    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            Import::BEHAVIOR_APPEND => __('Add/Update'),
            Import::BEHAVIOR_DELETE => __('Delete'),
        ];
    }
}
