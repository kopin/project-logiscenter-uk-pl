<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImport\Model\Source;

/**
 * Class SpecialHeader.
 */
class SpecialHeader
{
    /**
     * @var array
     */
    private $specialHeaders;

    /**
     * SpecialAttributes constructor.
     *
     * @param array $specialHeaders
     */
    public function __construct(
        array $specialHeaders = []
    ) {
        $this->specialHeaders = $specialHeaders;
    }

    /**
     * @return array
     */
    public function getHeaders(): array
    {
        return $this->specialHeaders;
    }
}
