<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImport\Model\Logger\Validation;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;
use Magento\Framework\Filesystem\DriverInterface;
use Magento\Framework\Logger\Handler\Base;
use Monolog\Logger;

class Handler extends Base
{
    /**
     * Logging level.
     *
     * @var int
     */
    protected $loggerType = Logger::INFO;

    /**
     * File name.
     *
     * @var string
     */
    protected $fileName = '';

    /**
     * @var Filesystem
     */
    protected $coreFilesystem;

    public function __construct(
        DriverInterface $filesystem,
        Filesystem $coreFilesystem
    ) {
        $this->coreFilesystem = $coreFilesystem;
        $this->fileName = $this->generateLogFileName();
        $filepath = $this->getFilePath();
        parent::__construct(
            $filesystem,
            $filepath
        );
    }

    /**
     * @return string
     */
    public function getLogFileName(): string
    {
        return $this->fileName;
    }

    /**
     * @return string
     */
    protected function generateLogFileName(): string
    {
        return 'product_import_validation' . '-' . \date('Y-m-d-H-i-s') . '.log';
    }

    /**
     * @return string
     */
    public function getLogFileAbsolutePath(): string
    {
        return $this->getFilePath() . $this->getLogFileName();
    }

    /**
     * @return string
     */
    public function getFilePath(): string
    {
        $dir = $this->coreFilesystem->getDirectoryRead(DirectoryList::VAR_DIR);

        return $dir->getAbsolutePath('log/debug/');
    }
}
