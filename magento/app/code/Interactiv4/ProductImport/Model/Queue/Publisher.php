<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImport\Model\Queue;

use Interactiv4\ProductImportConfig\Api\Config\ProductImportModuleStatusInterface;
use Magento\Framework\MessageQueue\PublisherInterface;

class Publisher
{
    private const TOPIC_NAME = 'interactiv4.product.import';

    /**
     * @var PublisherInterface
     */
    private $publisher;

    /**
     * @var ProcessDataFactory
     */
    private $processDataFactory;

    /**
     * @var ProductImportModuleStatusInterface
     */
    private $productImportStatus;

    /**
     * Publisher constructor.
     *
     * @param PublisherInterface                 $publisherInterface
     * @param ProcessDataFactory                 $processDataFactory
     * @param ProductImportModuleStatusInterface $productImportStatus
     */
    public function __construct(
        PublisherInterface $publisherInterface,
        ProcessDataFactory $processDataFactory,
        ProductImportModuleStatusInterface $productImportStatus
    ) {
        $this->publisher = $publisherInterface;
        $this->processDataFactory = $processDataFactory;
        $this->productImportStatus = $productImportStatus;
    }

    /**
     * Service en charge of publish a product import message in the queue.
     *
     * @param string $file
     * @param string $mode
     * @param string $action
     */
    public function execute(string $file, string $mode, string $action): void
    {
        if ($this->productImportStatus->isEnabled()) {
            $dataObject = $this->processDataFactory->create(
                [
                    'file' => $file,
                    'mode' => $mode,
                    'action' => $action,
                ]
            );
            $this->publisher->publish(self::TOPIC_NAME, $dataObject);
        }
    }
}
