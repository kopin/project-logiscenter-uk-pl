<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImport\Model\Queue;

/**
 * Data object for product import process queue request.
 */
class ProcessData
{
    /**
     * @var string
     */
    private $file;

    /**
     * @var string
     */
    private $mode;

    /**
     * @var string
     */
    private $action;

    /**
     * ProcessData constructor.
     *
     * @param string $file
     * @param string $mode
     * @param string $action
     */
    public function __construct(string $file, string $mode, string $action)
    {
        $this->file = $file;
        $this->mode = $mode;
        $this->action = $action;
    }

    /**
     * Retrieve file path.
     *
     * @return string
     */
    public function getFile(): string
    {
        return $this->file;
    }

    /**
     * Retrieve import mode.
     *
     * @return string
     */
    public function getMode(): string
    {
        return $this->mode;
    }

    /**
     * Retrieve action.
     *
     * @return string
     */
    public function getAction(): string
    {
        return $this->action;
    }
}
