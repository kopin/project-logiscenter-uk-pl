<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImport\Service\Import;

use Interactiv4\ProductImport\Api\Validation\ValidatorInterfaceFactory;
use Interactiv4\ProductImport\Model\Logger\Validation\Handler;
use Interactiv4\ProductImport\Model\Logger\Validation\HandlerFactory;
use Interactiv4\ProductImport\Service\SendFilesViaEmail;
use Interactiv4\ProductImportConfig\Api\Config\ProductImportConfigInterface;
use Interactiv4\ProductImportConfig\Api\Config\ProductImportModuleStatusInterface;
use Logiscenter\ProductDownloads\Service\ProcessUploads;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Validation\ValidationException;
use Psr\Log\LoggerInterface;

class ValidateProcess
{
    /**
     * @var ProcessUploads
     */
    private $validatorFactory;

    /**
     * @var ProductImportConfigInterface
     */
    private $productImportConfig;

    /**
     * @var SendFilesViaEmail
     */
    private $emailSender;

    /**
     * @var ProductImportModuleStatusInterface
     */
    private $productImportStatus;

    /**
     * @var Handler
     */
    private $handler;

    /**
     * @var HandlerFactory
     */
    private $logHandlerFactory;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * ValidateProcess constructor.
     *
     * @param LoggerInterface                    $logger
     * @param ProductImportConfigInterface       $productImportConfig
     * @param ProductImportModuleStatusInterface $productImportStatus
     * @param HandlerFactory                     $logHandlerFactory
     * @param SendFilesViaEmail                  $emailSender
     * @param ValidatorInterfaceFactory          $validatorFactory
     */
    public function __construct(
        LoggerInterface $logger,
        ProductImportConfigInterface $productImportConfig,
        ProductImportModuleStatusInterface $productImportStatus,
        HandlerFactory $logHandlerFactory,
        SendFilesViaEmail $emailSender,
        ValidatorInterfaceFactory $validatorFactory
    ) {
        $this->logger = $logger;
        $this->productImportConfig = $productImportConfig;
        $this->productImportStatus = $productImportStatus;
        $this->emailSender = $emailSender;
        $this->logHandlerFactory = $logHandlerFactory;
        $this->validatorFactory = $validatorFactory;
    }

    /**
     * @param $file
     * @param $mode
     *
     * @throws LocalizedException
     *
     * @return bool
     */
    public function execute($file, $mode): bool
    {
        $this->handler = $this->logHandlerFactory->create();
        $this->logger->setHandlers([$this->handler]);
        $result = false;
        try {
            if ($this->productImportStatus->isEnabled()) {
                $validator = $this->validatorFactory->create();
                $validationResult = $validator->setImportMode($mode)->validateFile($file)->getResult();
                $this->logResult($validationResult);
            }
        } catch (ValidationException $e) {
            $this->log($e->getMessage());
        } catch (\Exception $e) {
            $msg = __('Unexpected error during Product Import Validation Process:');
            $this->log($msg . $e->getMessage());
            $this->sendEmail();
            throw new LocalizedException($msg, $e, $e->getCode());
        }
        $this->sendEmail();

        return $result;
    }

    /**
     * @param $result
     */
    protected function sendEmail(): void
    {
        $recipients = $this->productImportConfig->getEmailRecipientList();
        $subject = __('Product Import Validation Result');
        $attachments = [$this->handler->getLogFileAbsolutePath()];
        $this->log('Sending email notification.');
        $this->log('Recipients: ' . \implode(',', $recipients));
        $this->log('Subject: ' . $subject);
        $this->log('Attachments: ' . $this->handler->getLogFileAbsolutePath());
        $this->emailSender->send($recipients, (string) $subject, $attachments);
    }

    /**
     * @param $result
     */
    private function logResult($result): void
    {
        if (!$result->isValid()) {
            $this->logInvalidResult($result);
        }
        $this->logValidResult($result);
    }

    private function logInvalidResult($result): void
    {
        $this->log(
            (string) __('ERROR: Import file is not valid. See details for more info:')
        );
        if ($result->getFileErrorsMsg()) {
            $this->log((string) __('%1 File error(s):', \count($result->getFileErrorsMsg())));
        }
        foreach ($result->getFileErrorsMsg() as $error) {
            $this->log((string) __($error));
        }
        if ($result->getTotalErrorRows()) {
            $this->log((string) __('%1 Row(s) with error:', $result->getTotalErrorRows()));
        }
        foreach ($result->getRowErrorsMsg() as $error) {
            $this->log((string) __($error));
        }
    }

    /**
     * @param $result
     */
    private function logValidResult($result): void
    {
        $this->log(
            (string) __('Import file is valid. Rows with error will be skipped:')
        );
        if ($result->getTotalRows()) {
            $this->log(
                (string) __('%1 row(s) found in import file.', $result->getTotalRows())
            );
        }
        $this->log((string) __('%1 Valid Row(s)', $result->getTotalValidRows()));
        $this->log((string) __('%1 rows for new products.', $result->getTotalNewRows()));
        $this->log((string) __('%1 rows for existing products.', $result->getTotalUpdatedRows()));
        if ($result->getTotalErrorRows()) {
            $this->log((string) __('%1 Invalid Row(s):', $result->getTotalErrorRows()));
        }
        foreach ($result->getRowErrorsMsg() as $error) {
            $this->log((string) __($error));
        }
    }

    /**
     * @param $msg
     */
    protected function log(string $msg): void
    {
        $this->logger->info($msg);
    }
}
