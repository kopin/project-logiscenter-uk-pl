<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImport\Service\Import;

use Exception;
use Interactiv4\ImportExport\Model\Bootstrap\Bootstrap;
use Interactiv4\ProductImport\Service\CacheCleanByTypes;
use Interactiv4\ProductImport\Service\InvalidateIndexersByIds;
use Interactiv4\ProductImport\Service\SendFilesViaEmail;
use Interactiv4\ProductImportConfig\Api\Config\ProductImportConfigInterface;
use Interactiv4\ProductImportConfig\Api\Config\ProductImportModuleStatusInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Filesystem;
use Magento\Framework\Validation\ValidationException;

/**
 * Class AbstractProcess.
 */
abstract class AbstractProcess
{
    protected const INTEGRATION_CODE = 'interactiv4_productimport_integration';

    protected const INDEX_TO_BE_INVALIDATED = [
        'catalog_category_product',
        'catalog_product_category',
        'catalogrule_rule',
        'catalog_product_attribute',
        'cataloginventory_stock',
        'inventory',
        'catalogrule_product',
        'catalog_product_price',
        'catalogsearch_fulltext',
        'aw_blog_product_post',
        'amasty_label',
        'amasty_label_main',
        'amasty_mostviewed_rule_product',
        'amasty_mostviewed_product_rule',
    ];

    protected const CACHE_TO_BE_CLEANED = [
        'full_page',
        'block_html',
        'collections',
        'eav',
    ];

    /**
     * @var Bootstrap
     */
    protected $bootstrapImport;

    /**
     * @var ProductImportModuleStatusInterface
     */
    protected $productImportConfig;

    /**
     * @var ProductImportModuleStatusInterface
     */
    protected $productImportStatus;

    /**
     * @var CacheCleanByTypes
     */
    protected $cacheService;

    /**
     * @var InvalidateIndexersByIds
     */
    protected $indexerService;

    /**
     * @var string
     */
    protected $logFile;

    protected $emailSender;

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * AbstractProcess constructor.
     *
     * @param Bootstrap                          $bootstrapImport
     * @param ProductImportConfigInterface       $productImportConfig
     * @param ProductImportModuleStatusInterface $productImportStatus
     * @param CacheCleanByTypes                  $cacheService
     * @param InvalidateIndexersByIds            $indexerService
     * @param SendFilesViaEmail                  $emailSender
     * @param Filesystem                         $filesystem
     */
    public function __construct(
        Bootstrap $bootstrapImport,
        ProductImportConfigInterface $productImportConfig,
        ProductImportModuleStatusInterface $productImportStatus,
        CacheCleanByTypes $cacheService,
        InvalidateIndexersByIds $indexerService,
        SendFilesViaEmail $emailSender,
        Filesystem $filesystem
    ) {
        $this->bootstrapImport = $bootstrapImport;
        $this->productImportConfig = $productImportConfig;
        $this->productImportStatus = $productImportStatus;
        $this->cacheService = $cacheService;
        $this->indexerService = $indexerService;
        $this->emailSender = $emailSender;
        $this->filesystem = $filesystem;
    }

    /**
     * @return array
     */
    abstract protected function getConfigData(): array;

    /**
     * @return string
     */
    abstract protected function getImportMode(): string;

    /**
     * @param $file
     *
     * @throws Exception
     *
     * @return bool
     */
    public function execute($file): bool
    {
        $result = false;
        try {
            if ($this->productImportStatus->isEnabled()) {
                $result = $this->runImportProcess($file);
                if ($result) {
                    $this->log('Import process has been finished with status OK');
                    $this->runPostProcess();
                }
            }
        } catch (ValidationException $e) {
            $this->log($e->getMessage());
        } catch (Exception $e) {
            $msg = __('Unexpected error during Product Import process:');
            $this->log($msg . $e->getMessage());
            $this->sendEmail(false);
            throw new LocalizedException($msg, $e, $e->getCode());
        }
        $this->sendEmail($result);

        return $result;
    }

    /**
     * @param $file
     *
     * @return bool
     */
    protected function runImportProcess($file): bool
    {
        $result = false;
        $init = $this->bootstrapImport->init(self::INTEGRATION_CODE);
        if ($init) {
            $this->generateLogFileName();
            $this->bootstrapImport->getConfig()->setInData($this->getLogFileName(), 'logFile');
            $this->bootstrapImport->getConfig()->setInData($file, 'filePath');
            $this->bootstrapImport->getConfig()->setInData($this->getImportMode(), 'importMode');
            foreach ($this->getConfigData() as $key => $value) {
                $this->bootstrapImport->getConfig()->setInData($value, $key);
            }
            $result = $this->bootstrapImport->getLauncher()->run();
        }

        return $result;
    }

    /**
     * @return void
     */
    protected function runPostProcess(): void
    {
        $this->invalidateIndex();
        $this->cleanCaches();
    }

    /**
     * @return void
     */
    protected function invalidateIndex(): void
    {
        $this->log('Reindexing...');
        $this->indexerService->execute(static::INDEX_TO_BE_INVALIDATED);
        $this->log('Reindex finished.');
    }

    /**
     * @return void
     */
    protected function cleanCaches(): void
    {
        $this->log('Cleaning cache...');
        $this->cacheService->clean(static::CACHE_TO_BE_CLEANED);
        $processedProducts = $this->bootstrapImport->getConfig()->getOutData('processed_products') ?? [];
        $this->cacheService->invalidateByProductId($processedProducts);
        $processedCategory = $this->bootstrapImport->getConfig()->getOutData('processed_category_relation') ?? [];
        $this->cacheService->invalidateByCategoryId($processedCategory);
        $this->log('Cache cleaned.');
    }

    /**
     * @return string
     */
    protected function getLogFileName(): string
    {
        return $this->logFile;
    }

    /**
     * @return string
     */
    protected function generateLogFileName(): string
    {
        $this->logFile = 'product_import_' . $this->getImportMode() . '-' . \date('Y-m-d-H-i-s');

        return $this->logFile;
    }

    /**
     * @return string
     */
    protected function getLogFileAbsolutePath()
    {
        $dir = $this->filesystem->getDirectoryRead(DirectoryList::VAR_DIR);

        return $dir->getAbsolutePath('log/debug/' . $this->getLogFileName() . '.log');
    }

    /**
     * @param $result
     */
    protected function sendEmail($result): void
    {
        $recipients = $this->productImportConfig->getEmailRecipientList();
        $subject = $result ? __('Product Import Process Success') : __('Product Import Process Error');
        $attachments = [$this->getLogFileAbsolutePath()];
        $this->log('Sending email notification.');
        $this->log('Recipients: ' . \implode(',', $recipients));
        $this->log('Subject: ' . $subject);
        $this->log('Attachments: ' . $this->getLogFileAbsolutePath());
        $this->emailSender->send($recipients, (string) $subject, $attachments);
    }

    /**
     * @param $msg
     */
    protected function log(string $msg): void
    {
        $this->bootstrapImport->getConfig()->log($this->getLogFileName(), $msg . PHP_EOL);
    }
}
