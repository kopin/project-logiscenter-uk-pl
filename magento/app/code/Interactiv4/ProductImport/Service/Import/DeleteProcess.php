<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImport\Service\Import;

use Magento\ImportExport\Model\Import;

class DeleteProcess extends AbstractProcess
{
    /**
     * {@inheritDoc}
     */
    public function getImportMode(): string
    {
        return Import::BEHAVIOR_DELETE;
    }

    /**
     * {@inheritDoc}
     */
    public function getConfigData(): array
    {
        return [];
    }
}
