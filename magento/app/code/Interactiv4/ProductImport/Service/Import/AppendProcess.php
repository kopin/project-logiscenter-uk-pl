<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImport\Service\Import;

use Interactiv4\ImportExport\Model\Bootstrap\Bootstrap;
use Interactiv4\ProductImport\Service\CacheCleanByTypes;
use Interactiv4\ProductImport\Service\InvalidateIndexersByIds;
use Interactiv4\ProductImport\Service\SendFilesViaEmail;
use Interactiv4\ProductImportConfig\Api\Config\ProductImportConfigInterface;
use Interactiv4\ProductImportConfig\Api\Config\ProductImportModuleStatusInterface;
use Logiscenter\ProductDownloads\Service\ProcessUploads;
use Magento\Framework\Filesystem;
use Magento\ImportExport\Model\Import;

class AppendProcess extends AbstractProcess
{
    /**
     * @var ProcessUploads
     */
    private $processUploadsService;

    /**
     * AppendProcess constructor.
     *
     * @param Bootstrap                          $bootstrapImport
     * @param ProductImportConfigInterface       $productImportConfig
     * @param ProductImportModuleStatusInterface $productImportStatus
     * @param CacheCleanByTypes                  $cacheService
     * @param InvalidateIndexersByIds            $indexerService
     * @param SendFilesViaEmail                  $emailSender
     * @param Filesystem                         $filesystem
     * @param ProcessUploads                     $processUploadsService
     */
    public function __construct(
        Bootstrap $bootstrapImport,
        ProductImportConfigInterface $productImportConfig,
        ProductImportModuleStatusInterface $productImportStatus,
        CacheCleanByTypes $cacheService,
        InvalidateIndexersByIds $indexerService,
        SendFilesViaEmail $emailSender,
        Filesystem $filesystem,
        ProcessUploads $processUploadsService
    ) {
        parent::__construct(
            $bootstrapImport,
            $productImportConfig,
            $productImportStatus,
            $cacheService,
            $indexerService,
            $emailSender,
            $filesystem
        );
        $this->processUploadsService = $processUploadsService;
    }

    public function getConfigData(): array
    {
        return [
            'imagesSourcePath' => $this->productImportConfig->getImagesPath(),
            'defaultProductValues' => $this->getDefaultValues(),
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function getImportMode(): string
    {
        return Import::BEHAVIOR_APPEND;
    }

    /**
     * {@inheritDoc}
     */
    protected function runPostProcess(): void
    {
        parent::runPostProcess();
        $processedProducts = $this->bootstrapImport->getConfig()->getOutData('processed_product_documents') ?? [];
        if (!empty($processedProducts)) {
            $this->log('Processing product documents');
            $this->processUploadsService->execute([], $processedProducts);
            $this->log('Product documents processed.');
        }
    }

    /**
     * @return array
     */
    private function getDefaultValues(): array
    {
        return [
            'defaultGroupedVisibility' => $this->productImportConfig->getGroupedVisibility(),
            'defaultSimpleVisibility' => $this->productImportConfig->getSimpleVisibility(),
            'defaultDownloadableVisibility' => $this->productImportConfig->getDownloadableVisibility(),
            'defaultVirtualVisibility' => $this->productImportConfig->getVirtualVisibility(),
            'defaultStatus' => $this->productImportConfig->getStatus(),
            'defaultTaxClassValue' => $this->productImportConfig->getTaxClass(),
        ];
    }
}
