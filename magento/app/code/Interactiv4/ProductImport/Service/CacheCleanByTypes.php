<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImport\Service;

use Magento\Catalog\Model\Category;
use Magento\Catalog\Model\Product;
use Magento\Framework\App\Cache\Manager;
use Magento\Framework\App\CacheInterface;

/**
 * Clean caches by caches types service class.
 */
class CacheCleanByTypes
{
    /**
     * @var Manager
     */
    private $cacheManager;

    /**
     * @var CacheInterface
     */
    private $cache;

    /**
     * CacheCleanByTypes constructor.
     *
     * @param Manager        $cacheManager
     * @param CacheInterface $cache
     */
    public function __construct(
        Manager $cacheManager,
        CacheInterface $cache
    ) {
        $this->cache = $cache;
        $this->cacheManager = $cacheManager;
    }

    /**
     * Cleans caches by cache types.
     *
     * @param string[] $cacheTypes
     */
    public function clean(array $cacheTypes): void
    {
        $this->cacheManager->clean($cacheTypes);
    }

    /**
     * @param array $productIds
     */
    public function invalidateByProductId(array $productIds): void
    {
        if (empty($productIds)) {
            return;
        }

        foreach ($productIds as $productId) {
            $arrayTags[] = Product::CACHE_TAG . '_' . $productId;
        }
        $this->cache->clean($arrayTags);
    }

    /**
     * @param array $categoryIds
     */
    public function invalidateByCategoryId(array $categoryIds): void
    {
        if (empty($categoryIds)) {
            return;
        }

        foreach ($categoryIds as $categoryId) {
            $arrayTags[] = Product::CACHE_PRODUCT_CATEGORY_TAG . '_' . $categoryId;
            $arrayTags[] = Category::CACHE_TAG . '_' . $categoryId;
        }

        $this->cache->clean($arrayTags);
    }
}
