<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImport\Service;

use Magento\Framework\App\ResourceConnection;
use Magento\Framework\DB\Adapter\AdapterInterface as DBConnection;

/**
 * Get source code for provided stock
 */
class GetSourceCodeForStock
{
    /**
     * @var array
     */
    private $cache;

    /**
     * @var DBConnection
     */
    private $connection;

    /**
     * @param ResourceConnection $resource
     */
    public function __construct(ResourceConnection $resource)
    {
        $this->connection = $resource->getConnection();
    }

    /**
     * Get source code by stock id
     *
     * @param int $stockId
     *
     * @return string
     */
    public function execute(int $stockId): string
    {
        if (isset($this->cache[$stockId])) {
            return $this->cache[$stockId];
        }

        $linkTable = $this->connection->getTableName('inventory_source_stock_link');
        $select = $this->connection->select()
            ->from($linkTable, 'source_code')
            ->where('stock_id', $stockId);

        $this->cache[$stockId] = $this->connection->fetchOne($select);

        return $this->cache[$stockId];
    }
}
