<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImport\Service;

use Interactiv4\ProductImport\Model\Mail\TransportBuilder;
use Magento\Framework\Filesystem\Io\File;
use Magento\Store\Model\Store;
use Magento\Store\Model\StoreManagerInterface;
use Psr\Log\LoggerInterface;

/**
 * Send files via email to recipients service class.
 */
class SendFilesViaEmail
{
    /**
     * @var TransportBuilder
     */
    protected $transportBuilder;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var File
     */
    private $file;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * Mail constructor.
     *
     * @param TransportBuilder      $transportBuilder
     * @param StoreManagerInterface $storeManager
     * @param File                  $file
     * @param LoggerInterface       $logger
     */
    public function __construct(
        TransportBuilder $transportBuilder,
        StoreManagerInterface $storeManager,
        File $file,
        LoggerInterface $logger
    ) {
        $this->transportBuilder = $transportBuilder;
        $this->storeManager = $storeManager;
        $this->file = $file;
        $this->logger = $logger;
    }

    /**
     * Send the email for a Help Center submission.
     *
     * @param array  $recipientsEmails
     * @param string $subject
     * @param array  $attachments
     *
     * @return void
     */
    public function send(array $recipientsEmails, string $subject, array $attachments)
    {
        try {
            // Build transport
            /* @var \Magento\Framework\Mail\TransportInterface $transport */
            $this->transportBuilder->setTemplateOptions(['area' => 'adminhtml', 'store' => Store::DEFAULT_STORE_ID])
                ->setTemplateIdentifier('interactiv4_productimportconfig_import_product_attachment')
                ->setTemplateVars(['subject' => $subject])
                ->setFromByScope('general', Store::DEFAULT_STORE_ID)
                ->addTo($recipientsEmails)
            ;
            // Attach files to transport
            foreach ($attachments as $file) {
                $this->transportBuilder->addAttachment($this->file->read($file), \basename($file));
            }
            $transport = $this->transportBuilder->getTransport();

            // Send transport
            $transport->sendMessage();
        } catch (\Exception $e) {
            $this->logger->critical($e->getMessage());
        }
    }
}
