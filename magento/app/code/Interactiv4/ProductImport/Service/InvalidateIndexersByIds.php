<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImport\Service;

use Magento\Framework\Indexer\IndexerInterfaceFactory;

/**
 * Invalidate indexers by ids service class.
 */
class InvalidateIndexersByIds
{
    /**
     * @var IndexerInterfaceFactory
     */
    private $indexerFactory;

    /**
     * @param IndexerInterfaceFactory $indexerFactory
     */
    public function __construct(IndexerInterfaceFactory $indexerFactory)
    {
        $this->indexerFactory = $indexerFactory;
    }

    /**
     * Invalidates indexers by ids.
     *
     * @param string[] $indexerIds
     */
    public function execute(array $indexerIds)
    {
        foreach ($indexerIds as $indexer) {
            $indexer = $this->indexerFactory->create()->load($indexer);
            $indexer->invalidate();
        }
    }
}
