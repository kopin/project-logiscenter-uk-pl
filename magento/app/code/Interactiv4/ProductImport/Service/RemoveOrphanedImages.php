<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImport\Service;

use FilesystemIterator;
use Magento\Catalog\Model\ResourceModel\Product\Gallery;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Filesystem;
use Magento\Framework\Filesystem\Directory\ReadInterface;
use Psr\Log\LoggerInterface;

/**
 * Remove orphaned image files service class.
 */
class RemoveOrphanedImages
{
    /**
     * @var ResourceConnection
     */
    private $resource;

    /**
     * @var ReadInterface
     */
    private $mediaDirectory;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * Cache images directories used that are omitted during cleaning.
     *
     * @var array
     */
    private $cacheDirectories;

    /**
     * @param ResourceConnection $resource
     * @param Filesystem         $filesystem
     * @param LoggerInterface    $logger
     * @param array              $cacheDirectories
     *
     * @throws FileSystemException
     */
    public function __construct(
        ResourceConnection $resource,
        Filesystem $filesystem,
        LoggerInterface $logger,
        array $cacheDirectories = []
    ) {
        $this->resource = $resource;
        $this->mediaDirectory = $filesystem->getDirectoryWrite(DirectoryList::MEDIA);
        $this->logger = $logger;
        $this->cacheDirectories = \array_merge(['/cache', '/swatch_image', '/swatch_thumb'], $cacheDirectories);
    }

    /**
     * Delete orphaned product and swatch images from media storage.
     *
     * @return void
     */
    public function execute(): void
    {
        $mediaGalleryPaths = $this->getUsedMediaGalleryPaths();
        $path = $this->mediaDirectory->getAbsolutePath('catalog/product');
        $this->cleanImages($mediaGalleryPaths, $path);

        $mediaGalleryPaths = $this->getImageProductSwatch();
        $path = $this->mediaDirectory->getAbsolutePath('attribute/swatch');
        $this->cleanImages($mediaGalleryPaths, $path);
    }

    /**
     * Delete images in specified directories that are not linked to rows in database.
     *
     * @param $mediaGalleryPaths
     * @param $directoryPath
     *
     * @return void
     */
    private function cleanImages($mediaGalleryPaths, $directoryPath): void
    {
        $this->logger->info(\sprintf('Clean Path: %s.', $directoryPath));
        if (!$this->mediaDirectory->isDirectory($directoryPath)) {
            $this->logger->info(\sprintf('Directory does not exist: %s. Skipping.', $directoryPath));

            return;
        }
        $iterator = new \RecursiveIteratorIterator(
            new \RecursiveDirectoryIterator(
                $directoryPath,
                FilesystemIterator::SKIP_DOTS | FilesystemIterator::FOLLOW_SYMLINKS
            )
        );

        $files = [];
        $unusedFiles = 0;
        $cachedFiles = 0;

        $this->logger->info('Unused files:');
        /** @var \SplFileInfo $info */
        foreach ($iterator as $info) {
            $filePath = \str_replace($directoryPath, '', $info->getPathname());

            if (0 === \strpos($filePath, '/placeholder')) {
                continue;
            }
            foreach ($this->cacheDirectories as $dirCache) {
                if (0 === \strpos($filePath, $dirCache)) {
                    ++$cachedFiles;
                    continue 2;
                }
            }
            $files[] = $filePath;
            if (!\in_array($filePath, $mediaGalleryPaths)) {
                ++$unusedFiles;
                $this->logger->info($filePath);
                try {
                    $this->mediaDirectory->delete($info->getPathname());
                } catch (FileSystemException $exception) {
                    $this->logger->critical(\sprintf('Unable to delete file: %s. Skipping', $filePath));
                }
            }
        }

        $this->logger->info('Unused files were removed!');

        $this->logger->info(\sprintf('Media Gallery entries: %s.', \count($mediaGalleryPaths)));
        $this->logger->info(\sprintf('Files in directory: %s.', \count($files)));
        $this->logger->info(\sprintf('Cached images: %s.', $cachedFiles));
        $this->logger->info(\sprintf('Unused files: %s.', $unusedFiles));
    }

    /**
     * Return media gallery images paths list from database.
     *
     * @return array
     */
    private function getUsedMediaGalleryPaths(): array
    {
        $mainTable = $this->resource->getTableName(Gallery::GALLERY_TABLE);
        $valueToEntityTable = $this->resource->getTableName(Gallery::GALLERY_VALUE_TO_ENTITY_TABLE);
        $connection = $this->resource->getConnection();
        $select = $connection
            ->select()
            ->from($mainTable, 'value')
            ->joinInner($valueToEntityTable, $mainTable . '.value_id = ' . $valueToEntityTable . '.value_id', [])
        ;

        return $connection->fetchCol($select);
    }

    /**
     * Return image products swatches paths list from database.
     *
     * @return array
     */
    private function getImageProductSwatch(): array
    {
        $connection = $this->resource->getConnection();

        $swatchImageTableName = $connection->getTableName('eav_attribute_option_swatch');
        $select = $connection->select()->from($swatchImageTableName, 'value')->where('type=2');

        return $connection->fetchCol($select);
    }
}
