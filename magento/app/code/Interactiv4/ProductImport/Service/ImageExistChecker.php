<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImport\Service;

use Interactiv4\ProductImportConfig\Api\Config\ProductImportConfigInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;
use Magento\Framework\Filesystem\Directory\ReadInterface;

/**
 * Image path and existence validator service class.
 */
class ImageExistChecker
{
    /**
     * @var Filesystem
     */
    private $varDirectory;

    /**
     * @var Filesystem
     */
    private $fileSystem;

    /**
     * @var ProductImportConfigInterface
     */
    private $productImportConfig;

    /**
     * @var string
     */
    private $sourceImageDirectory;

    /**
     * ImageExistChecker constructor.
     *
     * @param Filesystem                   $filesystem
     * @param ProductImportConfigInterface $productImportConfig
     */
    public function __construct(
        Filesystem $filesystem,
        ProductImportConfigInterface $productImportConfig
    ) {
        $this->fileSystem = $filesystem;
        $this->productImportConfig = $productImportConfig;
    }

    /**
     * Check whether the file exists in configured image source path directory.
     *
     * @param string $filePath
     *
     * @return bool
     */
    public function isFileExistInSourceFolder(string $filePath): bool
    {
        $varDirectory = $this->getVarDirectory();

        return $varDirectory->isFile($this->getSourceImagePath() . DIRECTORY_SEPARATOR . $filePath);
    }

    /**
     * Return file absolute path in var directory.
     *
     * @param string $relativeFilePath
     *
     * @return string
     */
    public function getSourceImageAbsolutePath(string $relativeFilePath): string
    {
        $varDirectory = $this->getVarDirectory();

        return $varDirectory->getAbsolutePath($this->getSourceImagePath() . DIRECTORY_SEPARATOR . $relativeFilePath);
    }

    /**
     * @return Filesystem|ReadInterface
     */
    private function getVarDirectory()
    {
        if (null === $this->varDirectory) {
            $this->varDirectory = $this->fileSystem->getDirectoryRead(DirectoryList::VAR_DIR);
        }

        return $this->varDirectory;
    }

    /**
     * @return string
     */
    private function getSourceImagePath(): string
    {
        if (null === $this->sourceImageDirectory) {
            $this->sourceImageDirectory = $this->productImportConfig->getImagesPath();
        }

        return $this->sourceImageDirectory;
    }
}
