<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImport\Test\Integration\Di;

use Interactiv4\Framework\Api\Di\Definition\DefinitionPoolInterface;
use Interactiv4\Framework\Api\Di\Definition\DefinitionPoolProviderInterface;
use Interactiv4\Framework\Api\Di\DiDefinitionTestInterface;
use Interactiv4\Framework\Api\Di\PreferencesDefinitionTestTrait;
use PHPUnit\Framework\TestCase;

/**
 * Class PreferencesDefinitionTest.
 *
 * Test preferences are defined in a specific way.
 *
 * @internal
 */
class PreferencesDefinitionTest extends TestCase implements DiDefinitionTestInterface, DefinitionPoolProviderInterface
{
    use PreferencesDefinitionTestTrait;

    /**
     * @var DefinitionPoolProviderInterface
     */
    private static $definitionProvider;

    /**
     * {@inheritdoc}
     */
    public static function tearDownAfterClass()
    {
        self::$definitionProvider = null;
        parent::tearDownAfterClass();
    }

    /**
     * {@inheritdoc}
     */
    public function getDefinitionPool(): DefinitionPoolInterface
    {
        self::$definitionProvider = self::$definitionProvider ?? new PreferencesDefinitionProvider();

        return self::$definitionProvider->getDefinitionPool();
    }
}
