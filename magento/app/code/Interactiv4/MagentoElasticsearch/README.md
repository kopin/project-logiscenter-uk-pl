# Interactiv4 Module MagentoSearch

Description
-----------
This module acts as a container for Magento Elasticsearch plugins and modifications, not tied specifically to any functionality.


Versioning
----------
This package follows semver for versioning.


Minimum Compatibility
---------------------
- PHP: ^7.2.0

- Magento: >=2.3.5 <2.3.6


Support
-------
Refer to [technical contacts](https://bitbucket.org/interactiv4/project-develop/wiki/Technical%20Contacts) for further information and support.


Credits
-------
Supported and maintained by Interactiv4 Team.


Contribution
------------
Any contribution is highly appreciated.
The best way to contribute code is to open a [pull request on Bitbucket](https://bitbucket.org/interactiv4/module-magento-search/pull-requests/new).


License
-------
Respect the [Magento OSL license](https://raw.githubusercontent.com/magento/magento2/2.3/LICENSE.txt).

Do not distribute or share this code unless you are authorized to do so.


Copyright
---------
Copyright (c) Interactiv4 S.L.
