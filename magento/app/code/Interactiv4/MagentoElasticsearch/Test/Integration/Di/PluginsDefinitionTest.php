<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\MagentoElasticsearch\Test\Integration\Di;

use Interactiv4\Framework\Api\Di\Definition\DefinitionPoolInterface;
use Interactiv4\Framework\Api\Di\Definition\DefinitionPoolProviderInterface;
use Interactiv4\Framework\Api\Di\DiDefinitionTestInterface;
use Interactiv4\Framework\Api\Di\PluginsDefinitionTestTrait;
use PHPUnit\Framework\TestCase;

/**
 * Class PluginsDefinitionTest.
 *
 * Test plugins are defined in a specific way.
 *
 * @internal
 */
class PluginsDefinitionTest extends TestCase implements DiDefinitionTestInterface, DefinitionPoolProviderInterface
{
    use PluginsDefinitionTestTrait;

    private static ?DefinitionPoolProviderInterface $definitionProvider = null;

    /**
     * {@inheritdoc}
     */
    public static function tearDownAfterClass(): void
    {
        self::$definitionProvider = null;
        parent::tearDownAfterClass();
    }

    /**
     * {@inheritdoc}
     */
    public function getDefinitionPool(): DefinitionPoolInterface
    {
        self::$definitionProvider ??= new PluginsDefinitionProvider();

        return self::$definitionProvider->getDefinitionPool();
    }
}
