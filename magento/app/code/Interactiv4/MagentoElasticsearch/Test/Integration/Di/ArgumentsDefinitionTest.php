<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\MagentoElasticsearch\Test\Integration\Di;

use Interactiv4\Framework\Api\Di\ArgumentsDefinitionTestTrait;
use Interactiv4\Framework\Api\Di\Definition\DefinitionPoolInterface;
use Interactiv4\Framework\Api\Di\Definition\DefinitionPoolProviderInterface;
use Interactiv4\Framework\Api\Di\DiDefinitionTestInterface;
use PHPUnit\Framework\TestCase;

/**
 * Class ArgumentsDefinitionTest.
 *
 * Test arguments are defined in a specific way.
 *
 * @internal
 */
class ArgumentsDefinitionTest extends TestCase implements DiDefinitionTestInterface, DefinitionPoolProviderInterface
{
    use ArgumentsDefinitionTestTrait;

    private static ?DefinitionPoolProviderInterface $definitionProvider = null;

    /**
     * {@inheritdoc}
     */
    public static function tearDownAfterClass(): void
    {
        self::$definitionProvider = null;
        parent::tearDownAfterClass();
    }

    /**
     * {@inheritdoc}
     */
    public function getDefinitionPool(): DefinitionPoolInterface
    {
        self::$definitionProvider ??= new ArgumentsDefinitionProvider();

        return self::$definitionProvider->getDefinitionPool();
    }
}
