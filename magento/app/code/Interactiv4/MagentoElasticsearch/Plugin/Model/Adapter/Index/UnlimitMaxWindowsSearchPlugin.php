<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\MagentoElasticsearch\Plugin\Model\Adapter\Index;

use Magento\Elasticsearch\Model\Adapter\Index\Builder;

/**
 * Class UnlimitMaxWindowsSearchPlugin.
 *
 * Unlimit max windows search parameter when creating ES indexer
 */
class UnlimitMaxWindowsSearchPlugin
{
    private const MAX_RESULT_WINDOW_SIZE = 1100000;

    /**
     * @param Builder $subject
     * @param array   $settings
     *
     * @return array
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterBuild(Builder $subject, array $settings): array
    {
        return \array_merge(
            $settings,
            ['max_result_window' => self::MAX_RESULT_WINDOW_SIZE]
        );
    }
}
