<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImportConfig\Plugin\Api\Status;

use Interactiv4\Environment\Plugin\Api\Status\CheckEnvironmentPlugin;

/**
 * Class CheckProductImportEnvironmentPlugin.
 *
 * Plugin to check environment. Usage:
 * Attach this plugin to any BooleanStatusReadInterface instance (e.g.: ModuleStatusInterface),
 * to chain this environment check to original isEnabled($additionalData = null) check.
 *
 * @see \Interactiv4\Contracts\SPL\BooleanStatus\Api\BooleanStatusReadInterface
 * @see \Interactiv4\ConfigHelper\Api\Config\Status\ModuleStatusInterface
 *
 * Forced to materialize file, since virtual plugins DO NOT WORK in production mode.
 *
 * @api
 */
class CheckProductImportEnvironmentPlugin extends CheckEnvironmentPlugin
{
}
