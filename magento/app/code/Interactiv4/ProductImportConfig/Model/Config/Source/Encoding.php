<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImportConfig\Model\Config\Source;

use Interactiv4\Framework\Data\AbstractOptionSource;

/**
 * Class Encoding source.
 */
class Encoding extends AbstractOptionSource
{
    public const UTF8_VALUE = 'UTF-8';

    public const ISO88591 = 'ISO-8859-1';

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            self::UTF8_VALUE => __('UTF-8'),
            self::ISO88591 => __('ISO-8859-1'),
        ];
    }
}
