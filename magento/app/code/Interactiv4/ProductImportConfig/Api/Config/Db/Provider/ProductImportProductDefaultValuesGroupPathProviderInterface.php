<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImportConfig\Api\Config\Db\Provider;

use Interactiv4\ConfigHelper\Api\Config\Db\Provider\DbConfigGroupPathProviderInterface;

/**
 * Interface ProductImportProductDefaultValuesGroupPathProviderInterface.
 *
 * Group path provider for productimport_productdefaultvalue group.
 *
 * @api
 */
interface ProductImportProductDefaultValuesGroupPathProviderInterface extends DbConfigGroupPathProviderInterface
{
    public const GROUP_PATH = 'productimport_productdefaultvalue';
}
