<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImportConfig\Api\Config\Db\Provider;

use Interactiv4\ConfigHelper\Api\Config\Db\Provider\DbConfigSectionPathProviderInterface;

/**
 * Interface ProductImportSectionPathProviderInterface.
 *
 * Section path provider for interactiv4_productimportconfig section.
 *
 * @api
 */
interface ProductImportSectionPathProviderInterface extends DbConfigSectionPathProviderInterface
{
    public const SECTION_PATH = 'interactiv4_productimportconfig';
}
