<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImportConfig\Api\Config\Db\Provider;

use Interactiv4\ConfigHelper\Api\Config\Db\Provider\DbConfigGroupPathProviderInterface;

/**
 * Interface ProductImportFilesUploadDirectoryGroupPathProviderInterface.
 *
 * Group path provider for productimport_uploaddirectory group.
 *
 * @api
 */
interface ProductImportUploadDirectoryGroupPathProviderInterface extends DbConfigGroupPathProviderInterface
{
    public const GROUP_PATH = 'productimport_uploaddirectory';
}
