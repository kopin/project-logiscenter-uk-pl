<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImportConfig\Api\Config;

/**
 * Interface ProductImportConfigInterface.
 *
 * Config provider interface
 *
 * @api
 */
interface ProductImportConfigInterface
{
    /**
     * @param int|string|null $storeId
     *
     * @return string
     */
    public function getEncoding($storeId = null): string;

    /**
     * @param int|string|null $storeId
     *
     * @return string
     */
    public function getSeparator($storeId = null): string;

    /**
     * @param int|string|null $storeId
     *
     * @return string
     */
    public function getEnclosure($storeId = null): string;

    /**
     * @param int|string|null $storeId
     *
     * @return string
     */
    public function getImagesPath($storeId = null): string;

    /**
     * @param int|string|null $storeId
     *
     * @return int
     */
    public function getTaxClass($storeId = null): int;

    /**
     * @param int|string|null $storeId
     *
     * @return int
     */
    public function getGroupedVisibility($storeId = null): int;

    /**
     * @param int|string|null $storeId
     *
     * @return int
     */
    public function getSimpleVisibility($storeId = null): int;

    /**
     * @param int|string|null $storeId
     *
     * @return int
     */
    public function getDownloadableVisibility($storeId = null): int;

    /**
     * @param int|string|null $storeId
     *
     * @return int
     */
    public function getVirtualVisibility($storeId = null): int;

    /**
     * @param int|string|null $storeId
     *
     * @return int
     */
    public function getStatus($storeId = null): int;

    /**
     * @param int|string|null $storeId
     *
     * @return array
     */
    public function getEmailRecipientList($storeId = null): array;
}
