<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImportConfig\Api\Config;

use Interactiv4\Environment\Api\AllowedEnvironmentsAwareInterface;

/**
 * Interface ProductImportEnvironmentConfigInterface.
 *
 * Environment config provider interface.
 *
 * @api
 */
interface ProductImportEnvironmentConfigInterface extends AllowedEnvironmentsAwareInterface
{
}
