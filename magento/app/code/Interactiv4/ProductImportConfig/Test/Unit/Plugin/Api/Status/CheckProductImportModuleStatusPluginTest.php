<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImportConfig\Test\Unit\Plugin\Api\Status;

use Interactiv4\Framework\Test\Unit\Plugin\Api\Status\CheckBooleanStatusPluginTestTrait;
use Interactiv4\ProductImportConfig\Plugin\Api\Status\CheckProductImportModuleStatusPlugin;
use PHPUnit\Framework\TestCase;

/**
 * Class CheckProductImportModuleStatusPlugin.
 *
 * Test plugin that allows to intercept module status.
 *
 * @group config
 *
 * @internal
 */
class CheckProductImportModuleStatusPluginTest extends TestCase
{
    use CheckBooleanStatusPluginTestTrait;

    /**
     * {@inheritdoc}
     */
    protected function getPluginClass(): string
    {
        return CheckProductImportModuleStatusPlugin::class;
    }
}
