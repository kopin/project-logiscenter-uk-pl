<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImportConfig\Test\Integration\Di;

use Interactiv4\Framework\Api\Di\Definition\DefinitionPoolInterface;
use Interactiv4\Framework\Api\Di\Definition\DefinitionPoolProviderInterface;
use Interactiv4\Framework\Di\Definition\Definition;
use Interactiv4\Framework\Di\Definition\DefinitionName;
use Interactiv4\Framework\Di\Definition\DefinitionPool;
use Interactiv4\Framework\Di\Definition\DefinitionValue;

/**
 * Class PreferencesDefinitionProvider.
 *
 * Preferences definition as they appear in di configuration files.
 */
class PreferencesDefinitionProvider implements DefinitionPoolProviderInterface
{
    /**
     * @var DefinitionPoolInterface
     */
    private $definitionPool;

    /**
     * {@inheritdoc}
     */
    public function getDefinitionPool(): DefinitionPoolInterface
    {
        if (!isset($this->definitionPool)) {
            $this->definitionPool = new DefinitionPool(
                new Definition(
                    new DefinitionName(
                        'Interactiv4\\ProductImportConfig\\Api\\Config\\ProductImportModuleStatusInterface'
                    ),
                    new DefinitionValue(
                        \Interactiv4\ProductImportConfig\Api\Config\ProductImportModuleStatusInterface::class,
                        \Interactiv4\ProductImportConfig\Config\ProductImportModuleStatus::class,
                        (string) \Magento\Framework\App\Area::AREA_GLOBAL,
                        false,
                        null,
                        null
                    )
                ),
                new Definition(
                    new DefinitionName(
                        'Interactiv4\\ProductImportConfig\\Api\\Config\\ProductImportStatusConfigInterface'
                    ),
                    new DefinitionValue(
                        \Interactiv4\ProductImportConfig\Api\Config\ProductImportStatusConfigInterface::class,
                        \Interactiv4\ProductImportConfig\Config\ProductImportStatusConfig::class,
                        (string) \Magento\Framework\App\Area::AREA_GLOBAL,
                        false,
                        null,
                        null
                    )
                ),
                new Definition(
                    new DefinitionName(
                        'Interactiv4\\ProductImportConfig\\Api\\Config\\ProductImportEnvironmentConfigInterface'
                    ),
                    new DefinitionValue(
                        \Interactiv4\ProductImportConfig\Api\Config\ProductImportEnvironmentConfigInterface::class,
                        \Interactiv4\ProductImportConfig\Config\ProductImportEnvironmentConfig::class,
                        (string) \Magento\Framework\App\Area::AREA_GLOBAL,
                        false,
                        null,
                        null
                    )
                ),
                new Definition(
                    new DefinitionName(
                        'Interactiv4\\ProductImportConfig\\Api\\Config\\ProductImportConfigInterface'
                    ),
                    new DefinitionValue(
                        \Interactiv4\ProductImportConfig\Api\Config\ProductImportConfigInterface::class,
                        \Interactiv4\ProductImportConfig\Config\ProductImportConfig::class,
                        (string) \Magento\Framework\App\Area::AREA_GLOBAL,
                        false,
                        null,
                        null
                    )
                )
            );
        }

        return $this->definitionPool;
    }
}
