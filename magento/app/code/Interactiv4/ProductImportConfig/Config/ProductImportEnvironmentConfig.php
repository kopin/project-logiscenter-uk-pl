<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImportConfig\Config;

use Interactiv4\ProductImportConfig\Api\Config\ProductImportEnvironmentConfigInterface;
use Interactiv4\ProductImportConfig\Config\Db\DbProductImportEnvironmentConfig;

/**
 * Class ProductImportEnvironmentConfig.
 *
 * This is not an api class, implementation may change at any time.
 *
 * @see ProductImportEnvironmentConfigInterface, use this to retrieve config.
 *
 * For example, it now relies on DbProductImportEnvironmentConfig to read information from database,
 * but this implementation can be changed when needed, without prior notice.
 */
class ProductImportEnvironmentConfig extends DbProductImportEnvironmentConfig implements
    ProductImportEnvironmentConfigInterface
{
}
