<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImportConfig\Config;

use Interactiv4\ProductImportConfig\Api\Config\ProductImportConfigInterface;
use Interactiv4\ProductImportConfig\Config\Db\DbProductImportConfig;
use Interactiv4\ProductImportConfig\Config\Db\DbProductImportFileFormatConfig;
use Interactiv4\ProductImportConfig\Config\Db\DbProductImportNotificationConfig;
use Interactiv4\ProductImportConfig\Config\Db\DbProductImportProductDefaultValuesConfig;
use Interactiv4\ProductImportConfig\Config\Db\DbProductImportUploadDirectoryConfig;
use Magento\Config\Model\ResourceModel\Config as ResourceConfig;
use Magento\Framework\App\Config\ScopeConfigInterface;

/**
 * Class ProductImportConfig.
 *
 * This is not an api class, implementation may change at any time.
 *
 * @see ProductImportConfigInterface, use this to retrieve config.
 *
 * For example, it now relies on DbProductImportConfig to read information from database,
 * but this implementation can be changed when needed, without prior notice.
 */
class ProductImportConfig extends DbProductImportConfig implements ProductImportConfigInterface
{
    /**
     * @var DbProductImportFileFormatConfig
     */
    private $dbProductImportFileFormatConfig;

    /**
     * @var DbProductImportUploadDirectoryConfig
     */
    private $dbProductImportUploadDirectoryConfig;

    /**
     * @var DbProductImportProductDefaultValuesConfig
     */
    private $dbProductImportProductDefaultConfig;

    /**
     * @var DbProductImportNotificationConfig
     */
    private $dbProductImportNotificationConfig;

    /**
     * ProductImportConfig constructor.
     *
     * @param ScopeConfigInterface                      $scopeConfig
     * @param ResourceConfig                            $resourceConfig
     * @param DbProductImportFileFormatConfig           $dbProductImportFileFormatConfig
     * @param DbProductImportUploadDirectoryConfig      $dbProductImportUploadDirectoryConfig
     * @param DbProductImportProductDefaultValuesConfig $dbProductImportProductDefaultConfig
     * @param DbProductImportNotificationConfig         $dbProductImportNotificationConfig
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        ResourceConfig $resourceConfig,
        DbProductImportFileFormatConfig $dbProductImportFileFormatConfig,
        DbProductImportUploadDirectoryConfig $dbProductImportUploadDirectoryConfig,
        DbProductImportProductDefaultValuesConfig $dbProductImportProductDefaultConfig,
        DbProductImportNotificationConfig $dbProductImportNotificationConfig
    ) {
        $this->dbProductImportFileFormatConfig = $dbProductImportFileFormatConfig;
        $this->dbProductImportUploadDirectoryConfig = $dbProductImportUploadDirectoryConfig;
        $this->dbProductImportProductDefaultConfig = $dbProductImportProductDefaultConfig;
        $this->dbProductImportNotificationConfig = $dbProductImportNotificationConfig;
        parent::__construct($scopeConfig, $resourceConfig);
    }

    /**
     * {@inheritdoc}
     */
    public function getEncoding($storeId = null): string
    {
        return $this->dbProductImportFileFormatConfig->getEncoding($storeId);
    }

    /**
     * {@inheritdoc}
     */
    public function getSeparator($storeId = null): string
    {
        return $this->dbProductImportFileFormatConfig->getSeparator($storeId);
    }

    /**
     * {@inheritdoc}
     */
    public function getEnclosure($storeId = null): string
    {
        return $this->dbProductImportFileFormatConfig->getEnclosure($storeId);
    }

    /**
     * {@inheritdoc}
     */
    public function getImagesPath($storeId = null): string
    {
        return $this->dbProductImportUploadDirectoryConfig->getImagesPath($storeId);
    }

    /**
     * {@inheritdoc}
     */
    public function getTaxClass($storeId = null): int
    {
        return (int) $this->dbProductImportProductDefaultConfig->getTaxClass($storeId);
    }

    /**
     * {@inheritdoc}
     */
    public function getGroupedVisibility($storeId = null): int
    {
        return (int) $this->dbProductImportProductDefaultConfig->getGroupedVisibility($storeId);
    }

    /**
     * {@inheritdoc}
     */
    public function getSimpleVisibility($storeId = null): int
    {
        return (int) $this->dbProductImportProductDefaultConfig->getSimpleVisibility($storeId);
    }

    /**
     * {@inheritdoc}
     */
    public function getDownloadableVisibility($storeId = null): int
    {
        return (int) $this->dbProductImportProductDefaultConfig->getDownloadableVisibility($storeId);
    }

    /**
     * {@inheritdoc}
     */
    public function getVirtualVisibility($storeId = null): int
    {
        return (int) $this->dbProductImportProductDefaultConfig->getVirtualVisibility($storeId);
    }

    /**
     * {@inheritdoc}
     */
    public function getStatus($storeId = null): int
    {
        return (int) $this->dbProductImportProductDefaultConfig->getStatus($storeId);
    }

    /**
     * {@inheritdoc}
     */
    public function getEmailRecipientList($storeId = null): array
    {
        $value = $this->dbProductImportNotificationConfig->getRecipientEmail($storeId);

        return $value ? \explode(',', $value) : [];
    }
}
