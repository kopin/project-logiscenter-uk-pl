<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImportConfig\Config;

use Interactiv4\ProductImportConfig\Api\Config\ProductImportModuleStatusInterface;
use Interactiv4\ProductImportConfig\Api\Config\ProductImportStatusConfigInterface;

/**
 * Class ProductImportModuleStatus.
 *
 * This is not an api class, implementation may change at any time.
 *
 * @see ProductImportModuleStatusInterface, use this to retrieve config.
 *
 * For example, it now relies on ProductImportStatusConfigInterface to read config,
 * but this implementation can be changed when needed, without prior notice.
 */
class ProductImportModuleStatus implements ProductImportModuleStatusInterface
{
    /**
     * @var ProductImportStatusConfigInterface
     */
    private $statusConfig;

    /**
     * ProductImportModuleStatus constructor.
     *
     * @param ProductImportStatusConfigInterface $statusConfig
     */
    public function __construct(
        ProductImportStatusConfigInterface $statusConfig
    ) {
        $this->statusConfig = $statusConfig;
    }

    /**
     * Compare boolean status, using optionally supplied data.
     *
     * @param bool            $expectedStatusValue
     * @param int|string|null $storeId
     *
     * @return bool
     */
    public function isBooleanStatus(
        bool $expectedStatusValue,
        $storeId = null
    ): bool {
        return $this->statusConfig->isBooleanStatus($expectedStatusValue, $storeId);
    }

    /**
     * Read boolean status, using optionally supplied data.
     *
     * @param int|string|null $storeId
     *
     * @return bool
     */
    public function isEnabled($storeId = null): bool
    {
        return $this->statusConfig->isEnabled($storeId);
    }
}
