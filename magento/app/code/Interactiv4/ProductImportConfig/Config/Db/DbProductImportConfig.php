<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImportConfig\Config\Db;

use Interactiv4\ConfigHelper\Api\Config\Db\DbConfigHelperInterface;
use Interactiv4\ConfigHelper\Api\Config\Db\Provider\DbConfigGroupPathProviderTrait;
use Interactiv4\ConfigHelper\Api\Config\Db\Provider\DbConfigSectionPathProviderTrait;
use Interactiv4\ConfigHelper\Model\Config\Db\DbConfigHelper;
use Interactiv4\ProductImportConfig\Api\Config\Db\Provider\ProductImportGroupPathProviderInterface;
use Interactiv4\ProductImportConfig\Api\Config\Db\Provider\ProductImportSectionPathProviderInterface;

/**
 * Class DbProductImportConfig.
 *
 * This is not an api class, implementation may change at any time.
 *
 * @see \Interactiv4\ProductImportConfig\Api\Config\ProductImportConfigInterface,
 * use this to retrieve config.
 */
class DbProductImportConfig extends DbConfigHelper implements
    ProductImportSectionPathProviderInterface,
    ProductImportGroupPathProviderInterface,
    DbConfigHelperInterface
{
    use DbConfigSectionPathProviderTrait;
    use DbConfigGroupPathProviderTrait;

    private const XML_FIELD_IS_IT_FRIDAY = 'is_it_friday';

    private const XML_FIELD_USELESS_TEXT = 'useless_text';

    /**
     * @param int|string|null $storeId
     *
     * @return bool
     */
    public function isItFriday($storeId = null): bool
    {
        return $this->getConfigFlag(self::XML_FIELD_IS_IT_FRIDAY, $storeId);
    }

    /**
     * @param int|string|null $storeId
     *
     * @return string
     */
    public function getUselessText($storeId = null): string
    {
        return (string) $this->getConfig(self::XML_FIELD_USELESS_TEXT, $storeId);
    }
}
