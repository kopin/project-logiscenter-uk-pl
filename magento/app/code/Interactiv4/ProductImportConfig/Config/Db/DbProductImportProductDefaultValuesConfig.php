<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImportConfig\Config\Db;

use Interactiv4\ConfigHelper\Api\Config\Db\DbConfigHelperInterface;
use Interactiv4\ConfigHelper\Api\Config\Db\Provider\DbConfigGroupPathProviderTrait;
use Interactiv4\ConfigHelper\Api\Config\Db\Provider\DbConfigSectionPathProviderTrait;
use Interactiv4\ConfigHelper\Model\Config\Db\DbConfigHelper;
use Interactiv4\ProductImportConfig\Api\Config\Db\Provider\ProductImportProductDefaultValuesGroupPathProviderInterface;
use Interactiv4\ProductImportConfig\Api\Config\Db\Provider\ProductImportSectionPathProviderInterface;

/**
 * Class DbProductImportConfig.
 *
 * This is not an api class, implementation may change at any time.
 *
 * @see \Interactiv4\ProductImportConfig\Api\Config\ProductImportConfigInterface,
 * use this to retrieve config.
 */
class DbProductImportProductDefaultValuesConfig extends DbConfigHelper implements
    ProductImportSectionPathProviderInterface,
    ProductImportProductDefaultValuesGroupPathProviderInterface,
    DbConfigHelperInterface
{
    use DbConfigSectionPathProviderTrait;
    use DbConfigGroupPathProviderTrait;

    private const XML_FIELD_TAX_CLASS = 'product_tax_class';

    private const XML_FIELD_GROUPED_VISIBILITY = 'product_grouped_visibility';

    private const XML_FIELD_SIMPLE_VISIBILITY = 'product_simple_visibility';

    private const XML_FIELD_DOWNLOADABLE_VISIBILITY = 'product_downloadable_visibility';

    private const XML_FIELD_VIRTUAL_VISIBILITY = 'product_virtual_visibility';

    private const XML_FIELD_STATUS = 'product_status';

    private const XML_FIELD_WEIGHT = 'product_weight';

    /**
     * @param int|string|null $storeId
     *
     * @return string
     */
    public function getTaxClass($storeId = null): string
    {
        return (string) $this->getConfig(self::XML_FIELD_TAX_CLASS, $storeId);
    }

    /**
     * @param int|string|null $storeId
     *
     * @return string
     */
    public function getGroupedVisibility($storeId = null): string
    {
        return (string) $this->getConfig(self::XML_FIELD_GROUPED_VISIBILITY, $storeId);
    }

    /**
     * @param int|string|null $storeId
     *
     * @return string
     */
    public function getSimpleVisibility($storeId = null): string
    {
        return (string) $this->getConfig(self::XML_FIELD_SIMPLE_VISIBILITY, $storeId);
    }

    /**
     * @param int|string|null $storeId
     *
     * @return string
     */
    public function getDownloadableVisibility($storeId = null): string
    {
        return (string) $this->getConfig(self::XML_FIELD_DOWNLOADABLE_VISIBILITY, $storeId);
    }

    /**
     * @param int|string|null $storeId
     *
     * @return string
     */
    public function getVirtualVisibility($storeId = null): string
    {
        return (string) $this->getConfig(self::XML_FIELD_VIRTUAL_VISIBILITY, $storeId);
    }

    /**
     * @param int|string|null $storeId
     *
     * @return string
     */
    public function getStatus($storeId = null): string
    {
        return (string) $this->getConfig(self::XML_FIELD_STATUS, $storeId);
    }

    /**
     * @param int|string|null $storeId
     *
     * @return string
     */
    public function getWeight($storeId = null): string
    {
        return (string) $this->getConfig(self::XML_FIELD_WEIGHT, $storeId);
    }
}
