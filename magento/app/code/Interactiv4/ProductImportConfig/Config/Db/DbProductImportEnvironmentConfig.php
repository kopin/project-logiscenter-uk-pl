<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImportConfig\Config\Db;

use Interactiv4\ConfigHelper\Api\Config\Db\DbConfigHelperInterface;
use Interactiv4\ConfigHelper\Api\Config\Db\Provider\DbConfigGroupPathProviderTrait;
use Interactiv4\ConfigHelper\Api\Config\Db\Provider\DbConfigSectionPathProviderTrait;
use Interactiv4\ConfigHelper\Model\Config\Db\DbConfigHelper;
use Interactiv4\Environment\Api\Config\Db\AllowedEnvironmentsInterface;
use Interactiv4\Environment\Api\Config\Db\AllowedEnvironmentsTrait;
use Interactiv4\EnvironmentConfig\Api\Config\Db\Provider\EnvironmentGroupPathProviderInterface;
use Interactiv4\ProductImportConfig\Api\Config\Db\Provider\ProductImportSectionPathProviderInterface;

/**
 * Class DbProductImportEnvironmentConfig.
 *
 * This is not an api class, implementation may change at any time.
 *
 * @see \Interactiv4\ProductImportConfig\Api\Config\ProductImportEnvironmentConfigInterface,
 * use this to retrieve config.
 */
class DbProductImportEnvironmentConfig extends DbConfigHelper implements
    ProductImportSectionPathProviderInterface,
    EnvironmentGroupPathProviderInterface,
    AllowedEnvironmentsInterface,
    DbConfigHelperInterface
{
    use DbConfigSectionPathProviderTrait;
    use DbConfigGroupPathProviderTrait;
    use AllowedEnvironmentsTrait;
}
