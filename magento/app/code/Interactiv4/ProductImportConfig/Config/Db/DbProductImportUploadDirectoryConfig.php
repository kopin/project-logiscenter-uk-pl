<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImportConfig\Config\Db;

use Interactiv4\ConfigHelper\Api\Config\Db\DbConfigHelperInterface;
use Interactiv4\ConfigHelper\Api\Config\Db\Provider\DbConfigGroupPathProviderTrait;
use Interactiv4\ConfigHelper\Api\Config\Db\Provider\DbConfigSectionPathProviderTrait;
use Interactiv4\ConfigHelper\Model\Config\Db\DbConfigHelper;
use Interactiv4\ProductImportConfig\Api\Config\Db\Provider\ProductImportSectionPathProviderInterface;
use Interactiv4\ProductImportConfig\Api\Config\Db\Provider\ProductImportUploadDirectoryGroupPathProviderInterface;

/**
 * Class DbProductImportConfig.
 *
 * This is not an api class, implementation may change at any time.
 *
 * @see \Interactiv4\ProductImportConfig\Api\Config\ProductImportConfigInterface,
 * use this to retrieve config.
 */
class DbProductImportUploadDirectoryConfig extends DbConfigHelper implements
    ProductImportSectionPathProviderInterface,
    ProductImportUploadDirectoryGroupPathProviderInterface,
    DbConfigHelperInterface
{
    use DbConfigSectionPathProviderTrait;
    use DbConfigGroupPathProviderTrait;

    private const XML_FIELD_IMAGES_PATH = 'images_path';

    /**
     * @param int|string|null $storeId
     *
     * @return string
     */
    public function getImagesPath($storeId = null): string
    {
        return (string) $this->getConfig(self::XML_FIELD_IMAGES_PATH, $storeId);
    }
}
