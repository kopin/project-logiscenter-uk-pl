<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductImportConfig\Config\Db;

use Interactiv4\ConfigHelper\Api\Config\Db\DbConfigHelperInterface;
use Interactiv4\ConfigHelper\Api\Config\Db\Provider\DbConfigGroupPathProviderTrait;
use Interactiv4\ConfigHelper\Api\Config\Db\Provider\DbConfigSectionPathProviderTrait;
use Interactiv4\ConfigHelper\Model\Config\Db\DbConfigHelper;
use Interactiv4\ProductImportConfig\Api\Config\Db\Provider\ProductImportFileFormatGroupPathProviderInterface;
use Interactiv4\ProductImportConfig\Api\Config\Db\Provider\ProductImportSectionPathProviderInterface;

/**
 * Class DbProductImportConfig.
 *
 * This is not an api class, implementation may change at any time.
 *
 * @see \Interactiv4\ProductImportConfig\Api\Config\ProductImportConfigInterface,
 * use this to retrieve config.
 */
class DbProductImportFileFormatConfig extends DbConfigHelper implements
    ProductImportSectionPathProviderInterface,
    ProductImportFileFormatGroupPathProviderInterface,
    DbConfigHelperInterface
{
    use DbConfigSectionPathProviderTrait;
    use DbConfigGroupPathProviderTrait;

    private const XML_FIELD_ENCODING = 'encoding';

    private const XML_FIELD_SEPARATOR = 'separator';

    private const XML_FIELD_ENCLOSURE = 'enclosure';

    /**
     * @param int|string|null $storeId
     *
     * @return string
     */
    public function getEncoding($storeId = null): string
    {
        return (string) $this->getConfig(self::XML_FIELD_ENCODING, $storeId);
    }

    /**
     * @param int|string|null $storeId
     *
     * @return string
     */
    public function getSeparator($storeId = null): string
    {
        return (string) $this->getConfig(self::XML_FIELD_SEPARATOR, $storeId);
    }

    /**
     * @param int|string|null $storeId
     *
     * @return string
     */
    public function getEnclosure($storeId = null): string
    {
        return (string) $this->getConfig(self::XML_FIELD_ENCLOSURE, $storeId);
    }
}
