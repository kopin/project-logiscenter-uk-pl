<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\RemoveEmptyGroupedFromIndex\Plugin\Shopby\Model\CatalogSearch\Indexer\Fulltext\DataProvider;

use Amasty\Shopby\Model\CatalogSearch\Indexer\Fulltext\DataProvider;
use Interactiv4\RemoveEmptyGroupedFromIndex\Model\Grouped;

/**
 * Remove grouped without children from index.
 */
class RemoveGroupedWithoutChildren
{
    /**
     * @var Grouped
     */
    private $groupedModel;

    /**
     * RemoveGroupedWithoutChildren constructor.
     *
     * @param Grouped $groupedModel
     */
    public function __construct(Grouped $groupedModel)
    {
        $this->groupedModel = $groupedModel;
    }

    /**
     * Remove empty grouped products from searchable products list.
     *
     * @param DataProvider $subject
     * @param              $result
     *
     * @return array
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterGetSearchableProducts(
        DataProvider $subject,
        $result
    ) {
        $updatedResult = [];

        foreach ($result as $product) {
            if ($this->shouldExclude($product)) {
                continue;
            }

            $updatedResult[] = $product;
        }

        return $updatedResult;
    }

    /**
     * Checks if grouped product has children.
     *
     * @param array $product
     *
     * @return bool
     */
    private function shouldExclude(array $product): bool
    {
        return 'grouped' === $product['type_id']
            && empty($this->groupedModel->getChildrenCount((int) $product['entity_id']));
    }
}
