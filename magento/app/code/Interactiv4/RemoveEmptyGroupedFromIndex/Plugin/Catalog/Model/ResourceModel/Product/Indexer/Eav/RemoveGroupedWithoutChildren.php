<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\RemoveEmptyGroupedFromIndex\Plugin\Catalog\Model\ResourceModel\Product\Indexer\Eav;

use Interactiv4\RemoveEmptyGroupedFromIndex\Model\Grouped;
use Magento\Catalog\Model\ResourceModel\Product\Indexer\Eav\AbstractEav;

/**
 * Remove grouped without children from index.
 */
class RemoveGroupedWithoutChildren
{
    /**
     * @var Grouped
     */
    private $groupedModel;

    /**
     * @param Grouped $groupedModel
     */
    public function __construct(Grouped $groupedModel)
    {
        $this->groupedModel = $groupedModel;
    }

    /**
     * Remove grouped without children.
     *
     * @param AbstractEav $subject
     * @param             $processIds
     *
     * @return array
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function beforeReindexEntities(
        AbstractEav $subject,
        $processIds
    ) {
        $ids = [];

        foreach ($processIds as $id) {
            if ($this->shouldExclude((int) $id)) {
                continue;
            }

            $ids[] = $id;
        }

        return !empty($ids) ? [$ids] : [null];
    }

    /**
     * Check if grouped product has children.
     *
     * @param int $id
     *
     * @return bool
     */
    private function shouldExclude(int $id): bool
    {
        return $this->groupedModel->isTypeIdGrouped($id) && empty($this->groupedModel->getChildrenCount($id));
    }
}
