<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\RemoveEmptyGroupedFromIndex\Model;

use Magento\Framework\App\ResourceConnection;
use Magento\GroupedProduct\Model\ResourceModel\Product\Link;

/**
 * Class to check grouped products.
 */
class Grouped
{
    /**
     * @var ResourceConnection
     */
    private $resourceConnection;

    /**
     * @param ResourceConnection $resourceConnection
     */
    public function __construct(ResourceConnection $resourceConnection)
    {
        $this->resourceConnection = $resourceConnection;
    }

    /**
     * Get count of associated products.
     *
     * @param int $productId
     *
     * @return int
     */
    public function getChildrenCount(int $productId): int
    {
        $connect = $this->resourceConnection->getConnection();
        $select = $connect->select();
        $select->from(['cpl' => $connect->getTableName('catalog_product_link')], 'COUNT(*)')
            ->where('cpl.product_id = ?', $productId)
            ->where('cpl.link_type_id = ?', Link::LINK_TYPE_GROUPED)
        ;

        return (int) $connect->fetchOne($select);
    }

    /**
     * Checks if product is grouped.
     *
     * @param int $productId
     *
     * @return bool
     */
    public function isTypeIdGrouped(int $productId): bool
    {
        $connect = $this->resourceConnection->getConnection();
        $select = $connect->select()
            ->from(['cpe' => $this->resourceConnection->getTableName('catalog_product_entity')], 'type_id')
            ->where('cpe.entity_id = ?', $productId)
        ;

        $result = $connect->fetchOne($select);

        return 'grouped' === $result;
    }
}
