<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductUpload\Block\Adminhtml\Import\Validate;

use Interactiv4\ProductImport\Api\Validation\ValidationResultInterface;
use Magento\Backend\Block\Template;

class Result extends Template
{
    /**
     * @var ValidationResultInterface
     */
    private $validationResult;

    /**
     * @param ValidationResultInterface $validator
     */
    public function setValidationResult(ValidationResultInterface $validator): void
    {
        $this->validationResult = $validator;
    }

    /**
     * @return ValidationResultInterface
     */
    public function getValidationResult(): ValidationResultInterface
    {
        return $this->validationResult;
    }
}
