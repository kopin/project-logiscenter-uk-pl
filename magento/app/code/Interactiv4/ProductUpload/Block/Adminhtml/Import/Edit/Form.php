<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductUpload\Block\Adminhtml\Import\Edit;

use Interactiv4\ProductImport\Model\Source\ImportMode;
use Magento\Backend\Block\Template\Context;
use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Framework\Data\FormFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Registry;

class Form extends Generic
{
    /**
     * @var ImportMode
     */
    private $importModeSource;

    /**
     * Form constructor.
     *
     * @param ImportMode  $importModeSource
     * @param Context     $context
     * @param Registry    $registry
     * @param FormFactory $formFactory
     * @param array       $data
     */
    public function __construct(
        ImportMode $importModeSource,
        Context $context,
        Registry $registry,
        FormFactory $formFactory,
        array $data = []
    ) {
        $this->importModeSource = $importModeSource;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * @throws LocalizedException
     *
     * @return Form
     * @codingStandardsIgnoreStart
     */
    protected function _prepareForm()
    {
        // @codingStandardsIgnoreEnd
        $form = $this->_formFactory->create(
            [
                'data' => [
                    'id' => 'edit_form',
                    'action' => $this->getUrl('productupload/*/schedule'),
                    'method' => 'post',
                    'enctype' => 'multipart/form-data',
                ],
            ]
        );

        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Import Settings')]);
        $fieldset->addField(
            'import_file',
            'file',
            [
                'name' => 'import_file',
                'label' => __('Select a CSV File to Import'),
                'title' => __('Select a CSV File to Import'),
                'required' => true,
                'class' => 'input-file',
            ]
        );
        $fieldset->addField(
            'import_mode',
            'select',
            [
                'name' => 'import_mode',
                'label' => __('Select Import mode'),
                'title' => __('Select Import mode'),
                'required' => true,
                'values' => $this->importModeSource->toOptionArray(),
            ]
        );

        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }
}
