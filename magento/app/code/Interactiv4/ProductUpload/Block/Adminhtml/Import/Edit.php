<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductUpload\Block\Adminhtml\Import;

use Magento\Backend\Block\Widget\Form\Container;

class Edit extends Container
{
    /**
     * @codingStandardsIgnoreStart
     */
    protected function _construct()
    {
        // @codingStandardsIgnoreEnd
        parent::_construct();

        $this->buttonList->remove('back');
        $this->buttonList->remove('reset');
        $this->buttonList->remove('save');

        $this->buttonList->add(
            'validate',
            ['label' => __('Validate'),
                'class' => 'primary',
                'onclick' => 'productUpload.validate();',
                'sort_order' => '10',
            ]
        );

        $this->buttonList->add(
            'import',
            ['label' => __('Import'),
                'class' => 'primary',
                'onclick' => 'productUpload.submit("import");',
                'sort_order' => '20',
                'data_attribute' => ['action' => 'import'],
            ]
        );

        $this->buttonList->add(
            'validate_background',
            ['label' => __('Validate in background'),
                'class' => 'primary',
                'onclick' => 'productUpload.submit("validate");',
                'data_attribute' => ['action' => 'validate'],
                'sort_order' => '30',
            ]
        );

        $this->_objectId = 'import_id';
        $this->_blockGroup = 'Interactiv4_ProductUpload';
        $this->_controller = 'adminhtml_import';
    }
}
