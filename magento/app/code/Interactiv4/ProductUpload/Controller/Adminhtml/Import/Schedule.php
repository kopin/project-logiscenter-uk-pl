<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductUpload\Controller\Adminhtml\Import;

use Interactiv4\Console\Exception\LocalizedException;
use Interactiv4\ProductImport\Model\Queue\Publisher;
use Interactiv4\ProductUpload\Controller\Adminhtml\Import;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\File\UploaderFactory;
use Magento\Framework\Filesystem\DirectoryList;
use Magento\Framework\View\Result\PageFactory;

class Schedule extends Import implements HttpPostActionInterface
{
    /**
     * @var UploaderFactory
     */
    private $uploaderFactory;

    /**
     * @var DirectoryList
     */
    private $directoryList;

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var Publisher
     */
    private $publisherService;

    /**
     * Schedule constructor.
     *
     * @param Context         $context
     * @param PageFactory     $resultPageFactory
     * @param Publisher       $publisherService
     * @param UploaderFactory $uploaderFactory
     * @param DirectoryList   $directoryList
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        Publisher $publisherService,
        UploaderFactory $uploaderFactory,
        DirectoryList $directoryList
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->publisherService = $publisherService;
        $this->uploaderFactory = $uploaderFactory;
        $this->directoryList = $directoryList;
    }

    public function execute()
    {
        $importMode = $this->getRequest()->getParam('import_mode');
        $action = $this->getRequest()->getParam('action');
        try {
            $fileName = $this->uploadFile();
            if (!$fileName || !$importMode || !$action) {
                throw new LocalizedException((string) __('Invalid file or import mode'));
            }

            $this->publisherService->execute($fileName, $importMode, $action);
            $this->messageManager->addSuccessMessage(
                'Thank you! The product file has been correctly queued.
                         You will receive an email when the process is completed.'
            );
            $resultRedirect = $this->resultRedirectFactory->create();
            $resultRedirect->setPath('*/*/queued');

            return $resultRedirect;
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            $resultRedirect = $this->resultRedirectFactory->create();
            $resultRedirect->setPath('*/*/index');

            return $resultRedirect;
        }
    }

    /**
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws FileSystemException
     *
     * @return string
     */
    private function uploadFile(): string
    {
        $fileUploader = $this->uploaderFactory->create(['fileId' => 'import_file']);
        // Set our parameters
        $fileUploader->setFilesDispersion(false);
        $fileUploader->setAllowRenameFiles(true);
        $fileUploader->setAllowedExtensions(['csv']);
        $fileUploader->setAllowCreateFolders(true);

        if (!$fileUploader->checkMimeType(['text/plain'])) {
            throw new LocalizedException((string) __('Invalid file type.'));
        }

        $destinationFolder = $this->directoryList->getPath('tmp') . DIRECTORY_SEPARATOR . 'product_import';
        $result = $fileUploader->save($destinationFolder);

        return $result['path'] . DIRECTORY_SEPARATOR . $result['file'];
    }
}
