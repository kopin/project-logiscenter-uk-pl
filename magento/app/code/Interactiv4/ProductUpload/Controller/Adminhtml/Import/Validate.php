<?php

/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

declare(strict_types=1);

namespace Interactiv4\ProductUpload\Controller\Adminhtml\Import;

use Interactiv4\ProductImport\Api\Validation\ValidatorInterfaceFactory;
use Interactiv4\ProductUpload\Block\Adminhtml\Import\Validate\Result;
use Interactiv4\ProductUpload\Controller\Adminhtml\Import;
use Magento\Backend\App\Action;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\File\UploaderFactory;
use Magento\Framework\Filesystem\DirectoryList;
use Magento\Framework\View\Result\Layout;

class Validate extends Import implements HttpPostActionInterface
{
    /**
     * @var UploaderFactory
     */
    private $uploaderFactory;

    /**
     * @var DirectoryList
     */
    private $directoryList;

    /**
     * @var ValidatorInterfaceFactory
     */
    private $validatorFactory;

    /**
     * Validate constructor.
     *
     * @param Action\Context            $context
     * @param UploaderFactory           $uploaderFactory
     * @param DirectoryList             $directoryList
     * @param ValidatorInterfaceFactory $validatorFactory
     */
    public function __construct(
        Action\Context $context,
        UploaderFactory $uploaderFactory,
        DirectoryList $directoryList,
        ValidatorInterfaceFactory $validatorFactory
    ) {
        parent::__construct($context);
        $this->uploaderFactory = $uploaderFactory;
        $this->directoryList = $directoryList;
        $this->validatorFactory = $validatorFactory;
    }

    public function execute()
    {
        /** @var Layout $resultLayout */
        $resultLayout = $this->resultFactory->create(ResultFactory::TYPE_LAYOUT);
        /** @var Result $resultBlock */
        $resultBlock = $resultLayout->getLayout()->getBlock('validate_result');

        try {
            $importMode = $this->getRequest()->getParam('import_mode');
            if (null === $importMode) {
                throw new LocalizedException(__('Invalid import mode'));
            }
            $fileName = $this->uploadFile();
            $validator = $this->validatorFactory->create();
            $result = $validator->setImportMode($importMode)
                ->validateFile($fileName)
                ->getResult()
            ;
            $resultBlock->setValidationResult($result);
            $this->_session->setImportProductFile($fileName);
            $this->_session->setImportMode($importMode);
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            $resultRedirect = $this->resultRedirectFactory->create();
            $resultRedirect->setPath('*/*/index');

            return $resultRedirect;
        }

        return $resultLayout;
    }

    /**
     * @throws LocalizedException
     * @throws FileSystemException
     *
     * @return string
     */
    private function uploadFile(): string
    {
        $fileUploader = $this->uploaderFactory->create(['fileId' => 'import_file']);
        // Set our parameters
        $fileUploader->setFilesDispersion(false);
        $fileUploader->setAllowRenameFiles(true);
        $fileUploader->setAllowedExtensions(['csv']);
        $fileUploader->setAllowCreateFolders(true);

        if (!$fileUploader->checkMimeType(['text/plain'])) {
            throw new LocalizedException(__('Invalid file type.'));
        }

        $destinationFolder = $this->directoryList->getPath('tmp') . DIRECTORY_SEPARATOR . 'product_import';
        $result = $fileUploader->save($destinationFolder);

        return $result['path'] . DIRECTORY_SEPARATOR . $result['file'];
    }
}
