define([
    'jquery',
    'slick'
], function ($) {
    'use strict';

    return function (widget) {

        $.widget('mage.awBlogRelatedSlider', widget, {
            /**
             * Initialize Slick plugin
             */
            init: function () {
                this.recalculateWidth();
                this.element.find(this.options.itemsSelector).slick({
                    adaptiveHeight: false,
                    autoplay: false,
                    autoplaySpeed: 3000,
                    arrows: true,
                    dots: false,
                    pauseOnHover: true,
                    pauseOnDotsHover: false,
                    respondTo: 'slider',
                    responsive: [
                        {
                            breakpoint: 800,
                            settings: {
                                slidesToShow: 3,
                                slidesToScroll: 3,
                                dots: true,
                                arrows: false,
                            }
                        },
                        {
                            breakpoint: 600,
                            settings: {
                                slidesToShow: 2,
                                slidesToScroll: 2,
                                dots: true,
                                arrows: false,
                            }
                        },
                        {
                            breakpoint: 400,
                            settings: {
                                slidesToShow: 2,
                                slidesToScroll: 2,
                                adaptiveHeight: true,
                                dots: true,
                                arrows: false,
                            }
                        }
                    ],
                    slidesToShow: 4,
                    slidesToScroll: 4,

                });
                this.element.css('visibility', 'visible');
            },
        });

        return $.mage.awBlogRelatedSlider;
    }
});