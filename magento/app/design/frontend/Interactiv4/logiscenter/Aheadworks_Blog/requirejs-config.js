/*
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

var config = {
    config: {
        mixins: {
            'Aheadworks_Blog/js/aw-blog-related-slider': {
                'Aheadworks_Blog/js/aw-blog-related-slider-mixin': true
            }
        }
    }
};

