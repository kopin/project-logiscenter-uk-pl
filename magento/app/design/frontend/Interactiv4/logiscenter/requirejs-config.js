/*
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

var config = {
    // config: {
    //     mixins: {
    //         'Magento_Ui/js/modal/modal': {
    //             'Magento_Ui/js/model/modal-mixin': true
    //         }
    //     }
    // },
    deps: [
        "js/main"
    ],
    paths: {
        'slick': 'js/vendor/slick/slick.min',
        'lazy': 'js/vendor/lazy/jquery.lazy.min',
        'mmenu': 'js/vendor/mmenu/jquery.mmenu.all',
        'jcf': 'js/vendor/jcf/jcf',
        'jcf.select': 'js/vendor/jcf/jcf.select',
        'jcf.scrollable': 'js/vendor/jcf/jcf.scrollable'
    },
    map: {
        '*': {
            sliderManager: 'js/modules/sliderManager',
            mwNavigation: 'js/modules/navigation',
            mwHeader: 'js/modules/header',
            mwFooter: 'js/modules/footer',
            mwSliderProducts: 'js/modules/sliderProducts',
            mwProduct: 'js/modules/product',
            mwQtySelector: 'js/modules/qtySelector',
            mwCart: 'js/modules/cart',
            mwForms: 'js/modules/forms',
            mwVideo: 'js/modules/video',
            mwMoreLess: 'js/modules/showMoreLess',
            amBlog: 'js/modules/blog',
            amBrands: 'js/modules/brands',
            mwClients: 'js/modules/clients'
        }
    },
    shim: {
        'slick': {
            'deps': ['jquery']
        },
        'lazy': {
            'deps': ['jquery']
        },
        'mmenu': {
            'deps': ['jquery']
        },
        'jcf': {
            'deps': ['jquery']
        },
        'jcf.select': {
            'deps': ['jquery', 'jcf']
        },
        'jcf.scrollable': {
            'deps': ['jquery', 'jcf']
        }
    },
    "config": {
        "mixins": {
            "mage/validation": {
                'js/mage/validation-mixin':true
            }
        }
    }
};
