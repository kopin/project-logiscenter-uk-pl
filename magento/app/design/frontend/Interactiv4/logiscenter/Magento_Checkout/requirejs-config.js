/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

/*Mixin when you use New Address in line, if you use pop-up, can you disable this*/
var config = {
    map: {
        '*': {
            'interactiv4_cart_initajax': 'Magento_Checkout/js/init-ajax'
        }
    },
    config: {
        mixins: {
            'Magento_Checkout/js/view/shipping': {
                'Magento_Checkout/js/view/shipping-mixin': false
            },
            'Magento_Checkout/js/model/step-navigator': {
                'Magento_Checkout/js/model/step-navigator-mixin': true
            },
            /* Start - Show summary in shipping step */
            'Magento_Checkout/js/view/summary/shipping': {
                'Magento_Checkout/js/view/summary/shipping-mixin': true
            },
            'Magento_Checkout/js/view/summary/abstract-total': {
                'Magento_Checkout/js/view/summary/abstract-total-mixin': true
            },
            /* End - Show summary in shipping step */
            /* Start - Mixins for Overview Step */
            'Magento_Checkout/js/view/payment/default': {
                'Magento_Checkout/js/view/payment/default-mixin': true
            },
            'Magento_Checkout/js/model/payment/additional-validators': {
                'Magento_Checkout/js/model/payment/additional-validators-mixin': true
            },
            'Magento_Checkout/js/view/summary/cart-items': {
                'Magento_Checkout/js/view/summary/cart-items-mixin': true
            },
            'Magento_Checkout/js/view/summary/item/details': {
                'Magento_Checkout/js/view/summary/item/details-mixin': true
            },
            /* End - Mixins for Overview Step */
            'Magento_Checkout/js/view/form/element/email': {
                'Magento_Checkout/js/view/form/element/email-mixin': true
            }
        }
    }
};
