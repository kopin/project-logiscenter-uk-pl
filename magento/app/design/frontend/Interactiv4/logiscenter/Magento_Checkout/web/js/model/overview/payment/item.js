/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

define(['ko'], (ko) => {
    'use strict';
    return ko.observable(null);
});
