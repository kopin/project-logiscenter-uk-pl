/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

define([
    'jquery',
    'ko',
    'Magento_Customer/js/model/customer',
    'Magento_Customer/js/action/check-email-availability',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/checkout-data'
], function ($, ko, customer, checkEmailAvailability, quote, checkoutData) {
    'use strict';

    var mixin = {
        defaults: {
            isRegistered: false,
            listens: {
                guestLinkClick: 'guestLinkClickAction',
                signInLinkClick: 'signInLinkClickAction'
            }
        },

        initConfig: function () {
            this._super();

            this.isPasswordVisible = this.resolveInitialPasswordVisibility();
            this.isRegistered = this.isPasswordVisible

            return this;
        },

        initObservable: function () {
            this._super()
                .observe(['guestLinkClick', 'isRegistered', 'signInLinkClick']);

            return this;
        },

        /**
         * Callback on changing email property
         */
        emailHasChanged: function () {
            var self = this;

            clearTimeout(this.emailCheckTimeout);

            if (self.validateEmail()) {
                quote.guestEmail = self.email();
                checkoutData.setValidatedEmailValue(self.email());
            }
            this.emailCheckTimeout = setTimeout(function () {
                if (self.validateEmail()) {
                    self.checkEmailAvailability();
                } else {
                    self.isPasswordVisible(false);
                    self.isRegistered(false);
                }
            }, self.checkDelay);

            checkoutData.setInputFieldEmailValue(self.email());
        },

        checkEmailAvailability: function () {
            this.validateRequest();
            this.isEmailCheckComplete = $.Deferred();
            this.isLoading(true);
            this.checkRequest = checkEmailAvailability(this.isEmailCheckComplete, this.email());

            $.when(this.isEmailCheckComplete).done(function () {
                this.isPasswordVisible(false);
                this.isRegistered(false);
            }.bind(this)).fail(function () {
                this.isPasswordVisible(true);
                this.isRegistered(true);
                checkoutData.setCheckedEmailValue(this.email());
            }.bind(this)).always(function () {
                this.isLoading(false);
            }.bind(this));
        },

        guestLinkClickAction: function () {
            this.isPasswordVisible(false);
        },

        signInLinkClickAction: function () {
            this.isPasswordVisible(true);
        },
    };

    return function (target) { // target == Result that Magento_Ui/.../columns returns.
        return target.extend(mixin); // new result that all other modules receive
    };
});
