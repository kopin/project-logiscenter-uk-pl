/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

define([
    'jquery',
    'underscore',
    'Magento_Catalog/js/price-utils',
    'Magento_Checkout/js/model/quote'
], function (
    $,
    _,
    priceUtils,
    quote
) {
    'use strict';
    var mixin = {
        quoteItemData: window.checkoutConfig.quoteItemData,

        getItem: function (item_id) {
            var itemElement = null;
            _.each(this.quoteItemData, function (element, index) {
                if (element.item_id == item_id) {
                    itemElement = element;
                }
            });
            return itemElement;
        },
        getUnitPrice: function (quoteItem) {
            var item = this.getItem(quoteItem.item_id);
            return priceUtils.formatPrice(item.base_price_incl_tax, quote.getPriceFormat());
        }
    };

    return function (target) {
        return target.extend(mixin);
    };
});
