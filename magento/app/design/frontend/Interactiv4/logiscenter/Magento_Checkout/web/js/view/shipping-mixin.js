/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

define([
    'jquery',
    'underscore',
    'Magento_Ui/js/form/form',
    'ko',
    'Magento_Checkout/js/action/create-shipping-address',
    'Magento_Checkout/js/action/select-shipping-address',
    'Magento_Checkout/js/checkout-data'
], function (
    $,
    _,
    Component,
    ko,
    createShippingAddress,
    selectShippingAddress,
    checkoutData
) {
    'use strict';

    var mixin = {
        isFormVisible: ko.observable(false),

        /**
         * Save new shipping address
         */
        saveNewAddress: function () {
            var addressData,
                newShippingAddress;

            this.source.set('params.invalid', false);
            this.triggerShippingDataValidateEvent();

            if (!this.source.get('params.invalid')) {
                addressData = this.source.get('shippingAddress');
                // if user clicked the checkbox, its value is true or false. Need to convert.
                addressData['save_in_address_book'] = this.saveInAddressBook ? 1 : 0;

                // New address must be selected as a shipping address
                newShippingAddress = createShippingAddress(addressData);
                selectShippingAddress(newShippingAddress);
                checkoutData.setSelectedShippingAddress(newShippingAddress.getKey());
                checkoutData.setNewCustomerShippingAddress($.extend(true, {}, addressData));
                this.isNewAddressAdded(true);
                this.hideForm();
            }
        },

        /**
         * Show address form
         */
        showForm: function () {
            this.isFormVisible(true);
            /*$('#opc-new-shipping-address').show();*/
        },

        /**
         * Hide address form
         */
        hideForm: function () {
            this.isFormVisible(false);
            /*$('#opc-new-shipping-address').hide();*/
        }
    };

    return function (target) {
        return target.extend(mixin);
    };
});
