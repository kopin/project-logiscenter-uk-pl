/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

define([
    'jquery',
    'mage/utils/wrapper'
], function (
    $,
    wrapper
) {
    'use strict';
    return function (stepNavigator) {
        stepNavigator.setHash = wrapper.wrapSuper(stepNavigator.setHash, function (hash) {
            this._super(hash);
            if (hash === "shipping") {
                $(window).scrollTop(0); //remove scroll to shipping when i arrive at checkout
            }
        });

        // Add class to the body in each step
        stepNavigator.getActiveItemIndex =  wrapper.wrapSuper(stepNavigator.getActiveItemIndex, function () {
            var activeIndex = this._super(),
                body = $('body'),
                shippingClass = 'checkout-step--shipping',
                paymentClass = 'checkout-step--payment',
                overviewClass = 'checkout-step--overview';

            if (activeIndex == '2'){
                body.removeClass(shippingClass);
                body.removeClass(paymentClass);
                body.addClass(overviewClass);
            }
            else if (activeIndex == '1') {
                body.removeClass(shippingClass);
                body.addClass(paymentClass);
                body.removeClass(overviewClass);
            }else {
                body.addClass(shippingClass);
                body.removeClass(paymentClass);
                body.removeClass(overviewClass);
            }
            return activeIndex;
        });

        return stepNavigator;
    };
});