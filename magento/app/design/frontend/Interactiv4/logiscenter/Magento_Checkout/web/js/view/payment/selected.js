/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

define([
    'underscore',
    'ko',
    'uiComponent',
    'Magento_Checkout/js/model/overview/payment/item'
], function (
    _,
    ko,
    Component,
    overviewPaymentItem
) {
    'use strict';

    return Component.extend({
        defaults: {
            template: 'Magento_Checkout/payment/selected'
        },

        paymentMethodSelect: ko.observable(''),

        /** @inheritdoc */
        initialize: function () {
            var self = this;

            this._super();

            overviewPaymentItem.subscribe(function (item) {
                if (item.title) {
                    self.paymentMethodSelect(item.title);
                }
            });

            return this;
        }
    });
});
