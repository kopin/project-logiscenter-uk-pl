/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

define([
    'jquery',
    'Magento_Checkout/js/model/step-navigator',
], function (
    $,
    stepNavigator
) {
    'use strict';

    var mixin = {
        //Show cart-items in sidebar in step shipping and payment or in summary overview
        canShow: function() {
            if (this.displayArea == 'summary' || stepNavigator.getActiveItemIndex() < 2) {
                return true;
            }
            return false;
        },
        showInOverview: function() {
            if (this.displayArea == 'summary') {
                return true;
            }
            return false;
        },
    };

    return function (target) {
        return target.extend(mixin);
    };
});
