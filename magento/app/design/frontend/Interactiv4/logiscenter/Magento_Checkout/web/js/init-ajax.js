/*
 * @author Interactiv4 Team
 * @copyright  Copyright © Interactiv4 (https://www.interactiv4.com)
 */

define([
    'mwCart',
    'mwQtySelector'
], function(ajaxCart, qtySelector) {
    qtySelector.initQtySelector();
    ajaxCart.initAjaxQty();
});
