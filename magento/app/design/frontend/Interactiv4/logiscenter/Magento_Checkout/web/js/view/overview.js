/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

define([
    'ko',
    'mage/translate',
    'uiComponent',
    'underscore',
    'Magento_Checkout/js/model/step-navigator',
    'Magento_Checkout/js/model/sidebar'
], function (
    ko,
    $t,
    Component,
    _,
    stepNavigator,
    sidebarModel
) {
    'use strict';

    /**
     * Overview Step - Step where client can review the order,
     */
    return Component.extend({
        defaults: {
            template: 'Magento_Checkout/overview',
            label: $t('Overview'),
            sortOrder: 25,
        },

        // add here your logic to display step,
        isVisible: ko.observable(false),

        /**
         * @returns {*}
         */
        initialize: function () {
            this._super();

            stepNavigator.registerStep(
                'overview',
                'overview',
                this.label,
                this.isVisible,
                _.bind(this.navigate, this),
                this.sortOrder
            );

            if (location.hash === '#overview') {
                stepNavigator.setHash('payment');
            }

            return this;
        },

        navigate: function () {
            this.isVisible(true);
        },

        backToShipping: function () {
            sidebarModel.hide();
            stepNavigator.navigateTo('shipping');
        },

        backToPayment: function () {
            stepNavigator.navigateTo('payment');
        }
    });
});
