/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

define([
    'mage/utils/wrapper',
    'Magento_Checkout/js/model/overview/validator'
], function (wrapper, overviewValidator) {
    'use strict';

    return function (additionalValidators) {
        additionalValidators.validate = wrapper.wrapSuper(additionalValidators.validate, function (hideError) {
            let result = this._super(hideError);
            if (result) {
                return overviewValidator.validate();
            }
            return result;
        });

        return additionalValidators;
    };
});
