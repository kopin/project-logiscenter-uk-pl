/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

define([
    'Magento_Checkout/js/model/step-navigator'
], (stepNavigator) => {
    'use strict';

    return {
        /**
         * Validate overview step
         *
         * @returns {Boolean}
         */
        validate: function () {
            if (stepNavigator.isProcessed('shipping')
                && !stepNavigator.isProcessed('payment')) {

                stepNavigator.next();
                return false;
            }
            return true;
        }
    };
});
