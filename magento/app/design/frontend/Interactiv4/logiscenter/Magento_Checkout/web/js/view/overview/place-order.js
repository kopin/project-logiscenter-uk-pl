/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

define([
    'underscore',
    'jquery',
    'uiComponent',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/model/step-navigator'
], function (
    _,
    $,
    Component,
    quote,
    stepNavigator
) {
    'use strict';

    return Component.extend({
        defaults: {
            template: 'Magento_Checkout/overview/place-order'
        },

        /**
         * @return {Boolean}
         */
        isVisible: function () {
            return (!quote.isVirtual()
                && stepNavigator.isProcessed('shipping')
                && stepNavigator.isProcessed('payment'))
                || (quote.isVirtual() && stepNavigator.isProcessed('payment'));
        },

        placeOrderTrigger: function () {
            $('#checkout-payment-method-load .payment-method._active button.checkout').trigger('click');
        }
    });
});
