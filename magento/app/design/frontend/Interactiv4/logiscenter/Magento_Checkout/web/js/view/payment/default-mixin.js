/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

define([
    'underscore',
    'mage/translate',
    'Magento_Checkout/js/model/overview/payment/item'
], function (
    _,
    $t,
    overviewPaymentItem
) {
    'use strict';
    var mixin = {

        /**
         * @return {Boolean}
         */
        selectPaymentMethod: function () {
            if (this.item && this.item.title) {
                overviewPaymentItem(this.item);
            } else {
                overviewPaymentItem({
                    item: {
                        title: $t('Payment title not found.')
                    }
                });
            }

            return this._super();
        },
    };

    return function (target) {
        return target.extend(mixin);
    };
});
