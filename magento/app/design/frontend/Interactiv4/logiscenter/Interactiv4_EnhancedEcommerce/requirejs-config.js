/*
 * @author Interactiv4 Team
 * @copyright  Copyright © Interactiv4 (https://www.interactiv4.com)
 */

var config = {
    map: {
        '*': {
            'interactiv4_cart_initajax': 'Magento_Checkout/js/init-ajax'
        }
    },
    config: {
        mixins: {
            'Interactiv4_EnhancedEcommerce/js/model/step-navigator': {
                'Interactiv4_EnhancedEcommerce/js/model/step-navigator-mixin': true
            }
        }
    }
};

