define([
    'Magento_CheckoutAgreements/js/model/agreement-validator',
    'Magento_Checkout/js/model/payment/additional-validators',
    'mage/translate'
    ],
    function (agreementValidator, additionalValidators, $t) {
        'use strict';

        var mixin = {
            validate: function (elm) {
                if (!this.isNewCard() && !this.stripePaymentsSelectedCard())
                    return this.showError($t('Please select a card!'));

                if (!agreementValidator.validate())
                    return this.showError($t('Please agree to the Terms and Conditions.'));

                return true;
            },

            placeOrder: function()
            {
                if (this.validate() &&
                    additionalValidators.validate() &&
                    this.isPlaceOrderActionAllowed() === true
                ) {
                    this._super();
                }
            }
        }

        return function (target) {
            return target.extend(mixin);
        };
    }
);
