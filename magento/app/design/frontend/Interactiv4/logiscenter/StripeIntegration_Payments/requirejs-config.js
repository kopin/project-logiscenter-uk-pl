/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

var config = {
    config: {
        mixins: {
            'StripeIntegration_Payments/js/view/payment/method-renderer/stripe_payments': {
                'StripeIntegration_Payments/js/view/payment/method-renderer/stripe_payments-mixin': true
            }
        }
    }
};
