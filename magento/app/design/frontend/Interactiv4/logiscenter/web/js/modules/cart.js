define([
    'jquery',
    'Magento_Checkout/js/action/get-totals',
    'Magento_Checkout/js/model/totals',
    'Magento_Customer/js/customer-data'
], function ($, getTotalsAction, totals, customerData) {
    var messagesResult = [];

    function processMessages() {
        messagesResult = [];
        var messages = $.cookieStorage.get('mage-messages');
        if (!_.isEmpty(messages)) {
            messages.forEach(addReturnMessage);
            customerData.set('messages', {messages: messagesResult});
            $.cookieStorage.set('mage-messages', '');
            $("html, body").animate({ scrollTop: 0 }, "slow");
        }
    }

    function addReturnMessage(item, index) {
        let messageArray = item.text.split("\n");
        messageArray.forEach(function (itemStr) {
            messagesResult.push({type: item.type, text: itemStr});
        });
    }

    return {
        initAjaxQty: function () {
            $(document).on('change', 'input[name$="[qty]"]', function(){
                var form = $('form#form-validate');
                var item = $('#item-total-count');
                $.ajax({
                    url: form.attr('action'),
                    data: form.serialize(),
                    showLoader: true,
                    success: function (res) {
                        var parsedResponse = $.parseHTML(res);
                        var result = $(parsedResponse).find("#form-validate");
                        var sections = ['cart'];

                        $("#form-validate").replaceWith(result);

                        /* Minicart reloading */
                        customerData.reload(sections, true);
                        processMessages();

                        /* Totals summary reloading */
                        var deferred = $.Deferred();
                        $.when(getTotalsAction([], deferred)).then(function() {
                            var items = totals.totals().items;
                            var qty = 0;
                            $(items).each(function() {
                                qty = qty + $(this)[0].qty;
                            });
                            item.text(qty);

                            if (!qty) {
                                window.location.reload();
                            }
                        });
                    },
                    error: function (xhr, status, error) {
                        var err = eval("(" + xhr.responseText + ")");
                        console.log(err.Message);
                    }
                });
            });
        }
    }
});