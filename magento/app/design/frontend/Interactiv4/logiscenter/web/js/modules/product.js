/*
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

define([
        "jquery"
    ],
    function($) {
        "strict";

        return {
            init: function() {
                $(document).on('click', '[data-event="read-more"]', function(e) {
                    e.preventDefault();
                    let container = $(this).closest('.grouped__content-product__col.description'),
                        original = container.find('.grouped__original-description'),
                        edited = container.find('.grouped__edited-description'),
                        actual = container.find('.grouped__actual-description'),
                        showLess = $(this).data('read-less'),
                        showMore = $(this).data('read-more');

                    if (container.hasClass('less')) {
                        actual.text($(edited).text());
                        container.removeClass('less');
                        $(this).text(showMore);
                    }
                    else {
                        actual.text($(original).text());
                        container.addClass('less');
                        $(this).text(showLess);
                    }
                });
            }
        }
    }
);