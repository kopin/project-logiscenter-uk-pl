/*
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

'use strict';
define([
        'jquery',
        'mmenu',
        'matchMedia',
        'mage/menu'
    ],
    function($) {
        return {
            stickyHeader: function() {
                const header = $('.page-header'),
                    content = $('.page-wrapper'),
                    pos = header.outerHeight(),
                    win = $(document),
                    messages = $('.page.messages');

                content.css('padding-top', header.outerHeight());
                messages.css('top', header.outerHeight() + 15);

                if (win.scrollTop() >= pos) {
                    if (!header.hasClass('sticky')) {
                        header.addClass('sticky');
                        content.css('padding-top', header.outerHeight());
                        messages.css('top', header.outerHeight() + 15);
                    }
                }

                win.on('scroll', function() {
                    if (win.scrollTop() >= pos)  {
                        if (!header.hasClass('sticky')) {
                            header.addClass('sticky');
                        }
                    }
                    else {
                        if (header.hasClass('sticky')) {
                            header.removeClass('sticky');
                        }
                    }
                    setTimeout(function() {
                        messages.css('top', header.outerHeight() + 15);
                        }, 500);
                });

                $(window).resize(function() {
                    setTimeout(function() {
                        content.css('padding-top', header.outerHeight());
                    }, 500);
                });
            },

            // Hide Header on on scroll down
            stickyHeaderMobile: function() {
                let didScroll,
                    lastScrollTop = 0,
                    delta = 5;
                const header = $('.page-header'),
                    headerHeight = header.outerHeight(),
                    win = $(document);

                $(window).scroll(function(event){
                    didScroll = true;
                });

                setInterval(function() {
                    if (didScroll) {
                        hasScrolled();
                        didScroll = false;
                    }
                }, 250);

                function hasScrolled() {
                    var st = win.scrollTop();

                    // Make sure they scroll more than delta
                    if(Math.abs(lastScrollTop - st) <= delta)
                        return;

                    // If they scrolled down and are past the headerHeight, hide header.
                    // This is necessary so you never see what is "behind" the header.
                    if (st > lastScrollTop && st > headerHeight){
                        // Scroll Down
                        header.css('top', - headerHeight);
                    } else {
                        // Scroll Up
                        if(st + $(window).height() < $(document).height()) {
                            header.css('top', "0px");
                        }
                    }

                    lastScrollTop = st;
                }
            },

            toggleSearch: function() {
                const search = $('.header__search'),
                    searchInput = $("#search"),
                    searchIcon = $('.minisearch__trigger');
                $(document).on('click', '.minisearch__trigger', function(e) {
                    e.preventDefault();
                    const headerRootElement = $(e.target).closest('.header__content');
                    if (!search.hasClass('active')) {
                        headerRootElement.addClass('search-form-is-active');
                        search.addClass('active');

                        searchIcon.appendTo(search);
                        searchIcon.addClass('open');

                        searchInput.focus();
                        searchInput.val('');
                    }
                    else {
                        headerRootElement.removeClass('search-form-is-active');

                        search.removeClass('active');

                        searchIcon.insertAfter(search);
                        searchIcon.removeClass('open');

                        //Add if use doofinder
                        $('.df-classic').removeAttr('visible');
                        $('.df-classic').attr('hidden',true);
                    }
                });

                $(window).resize(function() {
                    let headerRootElement = $('.header__content');
                    mediaCheck({
                        media: '(max-width: 991px)',
                        entry: function() {},
                        exit: function() {
                            headerRootElement.removeClass('search-form-is-active');

                            search.removeClass('active');

                            searchIcon.insertAfter(search);
                            searchIcon.removeClass('open');

                            //Add if use doofinder
                            $('.df-classic').removeAttr('visible');
                            $('.df-classic').attr('hidden',true);
                        }
                    });
                });
            }
        };
    }
);
