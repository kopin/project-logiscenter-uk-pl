/*
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

'use strict';
define([
    'jquery'
], function($) {
    return function(originalWidget){
        $.widget(
            'mage.validation',

            $['mage']['validation'],
            {
                listenFormValidateHandler: function(event, validation){
                    var firstActive = $(validation.errorList[0].element || []),
                        lastActive = $(validation.findLastActive() ||
                            validation.errorList.length && validation.errorList[0].element || []),
                        parent, windowHeight, successList;

                    if (lastActive.is(':hidden')) {
                        parent = lastActive.parent();
                        windowHeight = $(window).height();
                        $('html, body').animate({
                            scrollTop: parent.offset().top - windowHeight / 2
                        });
                    }

                    // ARIA (removing aria attributes if success)
                    successList = validation.successList;

                    if (successList.length) {
                        $.each(successList, function () {
                            $(this)
                                .removeAttr('aria-describedby')
                                .removeAttr('aria-invalid');
                        });
                    }

                    //update validate form, check sticky header
                    var header = $("header");
                    var sticky = $("header.sticky");
                    if(sticky.length) {
                        let stickyHeight = sticky.outerHeight();

                        if (firstActive.length) {
                            $('html, body').animate({
                                scrollTop: firstActive.offset().top - stickyHeight
                            });
                        }
                    } else {
                        if (firstActive.length) {
                            let headerHeight = header.outerHeight();
                            $('html, body').animate({
                                scrollTop: firstActive.offset().top - headerHeight
                            });
                            firstActive.focus();
                        }
                    }
                }
            });

        return $['mage']['validation'];
    };
});