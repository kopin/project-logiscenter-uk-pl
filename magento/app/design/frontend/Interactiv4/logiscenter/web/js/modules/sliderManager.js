/*
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

define([
        "jquery",
        "slick",
        "matchMedia",
    ],
    function ($) {
        "strict";

        return {
            setBackground: function () {
                $('.module-slider__items .module-slider__item .item__background').each(function() {
                    var item = this;
                    var imgDescktop = item.getAttribute("data-url-desktop");
                    var imgMobile = item.getAttribute("data-url-mobile");
                    mediaCheck({
                        media: '(max-width: 767px)',
                        entry: function() {
                            $(item).css('background-image', 'url(' + imgMobile + ')');
                        },
                        exit: function() {
                            $(item).css('background-image', 'url(' + imgDescktop + ')');
                        }
                    });
                });
            },
            createSlider: function (id, config) {
                if(config) {
                    $(id).not('.slick-initialized').slick(config);
                }
                else {
                    $(id).not('.slick-initialized').slick({
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        arrows: true,
                        dots: true,
                        fade: true,
                        autoplay: true,
                        autoplaySpeed: 1000,
                    });
                }
            }
        };
    }
);
