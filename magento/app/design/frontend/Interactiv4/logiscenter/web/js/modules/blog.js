/* eslint-disable no-undef */
/*
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

'use strict';
define([
        'jquery',
        'matchMedia',
        'slick'
    ],
    function($) {
        return {
            blogSlider: function() {
                var list = $('.recent-posts-widget .recent-posts .recent-posts__list');
                mediaCheck({
                    media: '(max-width: 992px)',
                    entry: function() {
                        if (!list.hasClass('slick-initialized')) {
                            list.slick({
                                infinite: true,
                                slidesToShow: 3,
                                slidesToScroll: 1,
                                arrows: false,
                                dots: false,
                                responsive: [
                                    {
                                        breakpoint: 992,
                                        settings: {
                                            slidesToShow: 2,
                                            dots: true
                                        }
                                    },
                                    {
                                        breakpoint: 768,
                                        settings: {
                                            slidesToShow: 1,
                                            dots: true
                                        }
                                    }
                                ]
                            });
                        }
                    },
                    exit: function() {}
                });
            }
        };
    }
);
