/*
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

define([
        "jquery",
        "mmenu",
        "matchMedia",
        "mage/menu"
    ],
    function ($) {
        "strict";

        return {
            // Use this function when the menu opens with click
            desktopNavigationByClick: function() {
                var menuItems = $('.navigation .navigation__list > .level0'),
                    navigation = $('.navigation');

                navigation.mouseleave(function() {
                    menuItems.each(function() {
                        $(this).removeClass('click-active');
                    });
                });
                menuItems.each(function() {
                    var item = $(this),
                        link = item.find('> a');
                    link.click(function(event) {
                        if(item.find('> .submenu').length) {
                            event.preventDefault();
                            if(item.hasClass('click-active')) {
                                item.removeClass('click-active');
                            }
                            else {
                                menuItems.each(function() {
                                    $(this).removeClass('click-active');
                                });
                                item.addClass('click-active');
                            }
                        }
                        else {
                            menuItems.each(function() {
                                $(this).removeClass('click-active');
                            });
                        }
                    })
                });
            },

            mobileNavigation: function() {
                const navigation = $('.navigation'),
                    toggle = $('.action.nav-toggle'),
                    customerLinks = $('.header__customer .dropdown-links .dropdown-links__content').clone(),
                    navSecondary = "<div class='navigation--secondary'><div class='dropdown-links__content'>" + customerLinks.html() + "</div></div>";

                if (navigation.length > 1) {
                    return;
                }

                var menu = navigation.mmenu({
                    slidingSubmenus: false,
                    extensions: ["fullscreen", "position-back", "position-bottom", "theme-white"],
                    navbars: false
                }, {
                    clone : true
                });

                var API = menu.data('mmenu');

                const navigationList = $('.mm-menu .mm-panels > .mm-panel:first-of-type');

                navigationList.append(navSecondary);

                var mmMenu = $('.navigation.mm-menu');

                mmMenu.prepend(toggle.clone().addClass('nav-toggle-close').click(function() {
                    API.close();
                }));

                toggle.click(function() {
                    API.open();
                });

            },

            relocateStoreSwitcher: function() {
                const language = $('.store-switcher');

                mediaCheck({
                    media: '(max-width: 991px)',
                    // Switch to Mobile Version
                    entry: function() {
                        if ($('.navigation--secondary .store-switcher').length == 0) {
                            language.insertAfter($('.navigation--secondary .dropdown-links__content'));
                        }
                    },
                    // Switch to Desktop Version
                    exit: function() {
                        if ($('.header__switcher .store-switcher').length == 0) {
                            language.appendTo($('.header__switcher'));
                        }
                    }
                });
            }
        };
    }
);