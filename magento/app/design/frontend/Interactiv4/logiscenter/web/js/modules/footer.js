/*
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

// Function from M2 Base. If it's not included, crash
define([
    'jquery',
    'matchMedia',
    'mage/tabs',
    'accordion'
], function($) {
    'use strict';
    return {
        mobileNavigation: function() {
            mediaCheck({
                // Switch to Mobile Version
                media: '(max-width: 767px)',
                entry: function()
                {
                    $(".footer__links-accordeon, .page-footer .footer__info-content").accordion({
                        "collapsible": true,
                        "active": false,
                        "multipleCollapsible": true,
                        "animate": 150
                    });
                },
                exit: function() {
                    $(".page-footer .footer__links [data-role='content']").css("display", "block");
                }
            });
        }
    };
});
