/*
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

define([
    'jquery'
],
function ($) {
    return {
        removeOverlay: function(overlay, videoWrapper) {
            $(overlay).click(function(){
                /*var intro = $(videoWrapper);
                var iframe = intro.find('iframe');

                setTimeout( function () {
                    var data = { method : 'play' },
                        url = iframe.attr('src').split('?')[0];

                    iframe[0].contentWindow.postMessage(JSON.stringify(data), url);
                }, 1000);*/

                $(this).fadeOut(200);
            });
        }
    };
});