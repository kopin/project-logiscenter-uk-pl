/*
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

'use strict';
define([
        'jquery',
        'mwNavigation',
        'mwHeader',
        'mwFooter',
        'mwSliderProducts',
        'mwForms',
        'mwMoreLess',
        'mwClients',
        'mwProduct'
    ],
    function($, mwNavigation, mwHeader, mwFooter, mwSliderProducts, mwForms, mwMoreLess, mwClients, mwProduct) {

        // Libraries

        // Here loads navigation functions and others
        $(function() {
            mediaCheck({
                media: '(max-width: 991px)',
                // Switch to Mobile Version
                entry: function() {
                    mwNavigation.mobileNavigation();
                    mwNavigation.relocateStoreSwitcher();
                    mwHeader.stickyHeaderMobile();
                },
                // Switch to Desktop Version
                exit: function() {
                    mwNavigation.relocateStoreSwitcher();
                    mwHeader.stickyHeader();
                    // Use this function when the menu opens with click
                    //mwNavigation.desktopNavigationByClick();
                }
            });
            // mwHeader.toggleSearch();
            mwFooter.mobileNavigation();
            mwSliderProducts.productSlider();
            mwForms.initSelectStyles();
            mwMoreLess.init();
            mwClients.createSlider();
            mwProduct.init();

            //Close message notification
            $(document).on('click', '.message .action-close', function () {
                $(this).parent().remove();
            });
        });
    });
