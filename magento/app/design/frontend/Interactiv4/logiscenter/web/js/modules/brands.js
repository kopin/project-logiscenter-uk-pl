/* eslint-disable no-undef */
/*
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

'use strict';
define([
        'jquery',
        'matchMedia',
        'slick'
    ],
    function($) {
        return {
            brandSlider: function(container, options) {
                var list = $(container).find('.brands-slider__items'),
                    items = $(container).find('li.item').length;

                if (items > options.slidesToShow ) {
                    list.slick(options);
                }
            }
        };
    }
);
