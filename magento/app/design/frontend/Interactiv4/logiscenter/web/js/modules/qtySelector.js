/*
 * @author Interactiv4 Team
 * @copyright  Copyright © Interactiv4 (https://www.interactiv4.com)
 */
'use strict';

define([
    'jquery'
    ],
    function($) {
        return {
        initQtySelector: function() {
            var maxQty = 99;
            var minQty = 1;
            var timeout = null;

            // Increment qty
            $(document).on('click', '.qtyplus', function(e) {

                // Stop acting like a button
                e.preventDefault();

                if (timeout) {
                    clearTimeout(timeout);
                }

                var fieldId = $(this).data('field');
                var currentInput = $(this).siblings('input[data-field="'+fieldId+'"]');
                var currentVal = parseInt(currentInput.val());

                if (!isNaN(currentVal) && currentVal < maxQty) {
                    currentInput.val(currentVal + minQty);
                } else {
                    currentInput.val(maxQty);
                }

                timeout = setTimeout(function() {
                        currentInput.trigger('change');
                    },
                    1000);

            });

            // Decrement qty
            $(document).on('click', '.qtyminus', function(e) {
                // Stop acting like a button
                e.preventDefault();

                if (timeout) {
                    clearTimeout(timeout);
                }

                var fieldId = $(this).data('field');
                var currentInput = $(this).siblings('input[data-field="'+fieldId+'"]');
                var currentVal = parseInt(currentInput.val());

                // If is not undefined
                if (!isNaN(currentVal) && currentVal > minQty) {
                    currentInput.val(currentVal - 1);
                } else {
                    currentInput.val(minQty);
                }

                timeout = setTimeout(function() {
                        currentInput.trigger('change');
                    },
                    1000);
            });

            // Show current qty and final price when the qty has been changed
            $(document).on('keyup', '.qty-selector qty-selector__qty', function() {
                this.value = (this.value + '').replace(/[^0-9]/g, '');
                var currentInput = $(this);
                var currentVal = parseInt(currentInput.val());

                if(isNaN(currentVal)) {
                    currentInput.val(minQty);
                }
                if(currentVal > maxQty) {
                    currentInput.val(maxQty);
                }
                if(currentVal < minQty) {
                    currentInput.val(minQty);
                }
            });
        }
    };
});
