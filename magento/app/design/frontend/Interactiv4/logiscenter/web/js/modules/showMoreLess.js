/*
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

define([
        "jquery"
    ],
    function($) {
        "strict";
        return {
            init: function() {
                $(document).ready(function() {
                    $('.btn--show-more-less').toggle(
                        function(e) {
                            $('.text-more').slideDown();
                            $(this).text($(this).data('text-less'));
                            $(this).addClass('less');
                            e.preventDefault();
                        },
                        function(e) {
                            $('.text-more').slideUp();
                            $(this).text($(this).data('text-more'));
                            $(this).removeClass('less');
                            e.preventDefault();
                        }
                    );
                });
            }
        }
    }
);
