/* eslint-disable no-undef */
/*
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

'use strict';
define([
        'jquery',
        'matchMedia',
        'slick'
    ],
    function($) {
        return {
            createSlider: function() {
                const list = $('.our-clients .items'),
                    btnLeft = window.btnLeft,
                    btnRight = window.btnRight;

                let slidesToShow = 6,
                    vertical = false;

                list.each(function() {
                    if (!$(this).hasClass('slick-initialized')) {
                        mediaCheck({
                            media: '(max-width: 991px)',
                            entry: function() {
                                slidesToShow = 2;
                            },
                            exit: function() {}
                        });

                        const items = $(this).find('li.item').length;
                        if (items > slidesToShow ) {
                            $(this).slick({
                                infinite: true,
                                slidesToShow: slidesToShow,
                                slidesToScroll: slidesToShow,
                                arrows: true,
                                dots: false,
                                vertical: vertical,
                                rows: 0,
                                prevArrow: '<button type="button" class="slick-prev">' +
                                    '<i class="module-icon">' +
                                    '   <svg class="icon icon-sm ico-chevron-left"><use xlink:href="' + btnLeft + '"></use></svg>' +
                                    '</i>' +
                                    '</button>',
                                nextArrow: '<button type="button" class="slick-next">' +
                                    '<i class="module-icon">' +
                                    '   <svg class="icon icon-sm ico-chevron-right"><use xlink:href="' + btnRight + '"></use></svg>' +
                                    '</i>' +
                                    '</button>',
                                responsive: [
                                    {
                                        breakpoint: 992,
                                        settings: {
                                            slidesToShow: 3,
                                            slidesToScroll: 1,
                                            vertical: false
                                        }
                                    },
                                    {
                                        breakpoint: 768,
                                        settings: {
                                            slidesToShow: 2,
                                            slidesToScroll: 1,
                                            vertical: false
                                        }
                                    }
                                ]
                            });
                        }
                    }
                });
            }
        };
    }
);
