/* eslint-disable no-undef */
/*
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

'use strict';
define([
        'jquery',
        'jcf',
        'jcf.select',
        'jcf.scrollable'
    ],
    function($, jcf) {
        return {
            initSelectStyles: function() {
                // set options for Checkbox module
                jcf.setOptions('Select', {
                    wrapNative: false,
                    wrapNativeOnMobile: true
                });

                jcf.replace($('.select-jcf'));
            }
        };
    }
);
