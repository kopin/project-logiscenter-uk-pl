/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

var config = {
    config: {
        mixins: {
            'Magento_Theme/js/view/breadcrumbs': {
                'Magento_Catalog/js/product/breadcrumbs-mixin': true
            },

            'Magento_Catalog/js/product/list/listing': {
                'Magento_Catalog/js/product/listing-mixin': true
            },

            'Magento_Catalog/js/product/list/toolbar': {
                'Magento_Catalog/js/product/list/toolbar-mixin': true
            }
        }
    }
};

