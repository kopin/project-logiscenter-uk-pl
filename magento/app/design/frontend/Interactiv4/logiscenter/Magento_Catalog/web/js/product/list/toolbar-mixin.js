define([
    'jquery',
    'jquery-ui-modules/widget',
    'mwForms'
], function ($, widget, mwForms) {

    'use strict';
    return function (widget) {
        $.widget('mage.productListToolbarForm', widget, {
            _bind: function (element, paramName, defaultValue) {
                if (element.is('select')) {
                    element.on('change', {
                        paramName: paramName,
                        'default': defaultValue
                    }, $.proxy(this._processSelect, this));
                } else {
                    element.on('click', {
                        paramName: paramName,
                        'default': defaultValue
                    }, $.proxy(this._processLink, this));
                }
                mwForms.initSelectStyles();
            },
        });

        return $.mage.productListToolbarForm;
    }
})