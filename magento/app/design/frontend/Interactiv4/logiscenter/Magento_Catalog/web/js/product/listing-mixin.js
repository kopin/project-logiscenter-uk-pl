define(['mwSliderProducts'], function (mwSliderProducts) {
    'use strict';

    var mixin = {

        /**
         *
         * @param {Column} elem
         */
        onAfterRender: function (elem) {
            window.setTimeout(function () {
                mwSliderProducts.productSlider();
            }, 100);
        }
    };

    return function (target) { // target == Result that Magento_Ui/.../columns returns.
        return target.extend(mixin); // new result that all other modules receive
    };
});