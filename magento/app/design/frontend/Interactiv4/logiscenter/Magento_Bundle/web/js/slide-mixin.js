/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

define([
    'jquery'
], ($) => {
    'use strict';

    var slideMixin = {
        _show: function () {
            $(this.options.slideSelector).addClass('disabled');

            return this._super();
        },

        _hide: function () {
            $(this.options.slideSelector).removeClass('disabled');

            return this._super();
        }
    };

    return function (targetWidget) {
        $.widget('mage.slide', targetWidget, slideMixin);

        return $.mage.slide;
    };
});
