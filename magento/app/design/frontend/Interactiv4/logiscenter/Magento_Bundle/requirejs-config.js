/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

var config = {
    config: {
        mixins: {
            'Magento_Bundle/js/slide': {
                'Magento_Bundle/js/slide-mixin': true
            }
        }
    }
};

