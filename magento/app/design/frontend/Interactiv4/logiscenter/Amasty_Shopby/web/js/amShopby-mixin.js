/*
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */
define([
    "jquery"
], function ($) {
    'use strict';

    return function () {
        $('.am-labels-folding form').find('a').each(function () {
            if ($(this).find('input').prop("checked")) {
                $(this).parents('.item').find('> .am-collapse-icon').addClass('_active');
            }
        });

        $("body").on('DOMSubtreeModified', "#am-shopby-container", function() {
            $('.am-labels-folding form').find('a').each(function () {
                if ($(this).find('input').prop("checked")) {
                    $(this).parents('.item').find('> .am-collapse-icon').addClass('_active');
                }
            });
        });
    };
});
