/*
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

var config = {
    config: {
        mixins: {
            'Amasty_Shopby/js/amShopby': {
                'Amasty_Shopby/js/amShopby-mixin': false
            }
        }
    },

    shim: {
        'js/modules/navigation': {
            deps: ['Amasty_Shopby/js/amShopbyResponsive']
        },

        'mage/menu': {
            deps: ['Amasty_Shopby/js/amShopbyResponsive']
        },
    }
};

