/*
 * @author Interactiv4 Team
 * @copyright  Copyright © Interactiv4 (https://www.interactiv4.com)
 */

define([
    'jquery'
], ($) => {
    'use strict';

    /**
     * @param options
     * @param currentValue
     * @return {string}
     */
    const getLabel = (options, currentValue) => {
        let label = '';
        options.forEach((option) => {
            if (option.value === currentValue) {
                label = option.label;
            }
        });
        return label;
    }

    /**
     * @param {Object} config
     * @param {Integer} config.currentWebsiteId
     * @param {Integer} config.currentStoreId
     * @param {string} config.htmlElementWebsite
     * @param {string} config.htmlElementStoreView
     * @param {string} config.htmlTrigger
     * @param {string} config.htmlMenuClose
     * @param {Array} config.websites
     * @param {Array} config.stores
     */
    return function (config)
    {
        let htmlElementWebsite = $(config.htmlElementWebsite),
            htmlElementStoreView = $(config.htmlElementStoreView);

        if (config.websites) {
            htmlElementWebsite.text(getLabel(config.websites, config.currentWebsiteId));
        }

        for (let [websiteId, websiteStores] of Object.entries(config.stores)) {
            if (websiteStores.length) {
                htmlElementStoreView.text(getLabel(websiteStores, config.currentStoreId));
            }
        }

        $(config.htmlTrigger).first().on('click', () => {
            $(config.htmlMenuClose).first().click();
        });
    }
});